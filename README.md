---------------------------
TEST Hash
---------------------------
Hash: 123
Hash 456
https://ranno.orumets.ee/?unique_hash=123

---------------------------
LIVE HASH
---------------------------
Hash: 6B52E02C43C832DEE0531AAAC458F8AF
https://ranno.orumets.ee/?unique_hash=6B52E02C43C832DEE0531AAAC458F8AF

https://forge.laravel.com/servers/177588/sites/532936/deploy/http?token=PGWgHdVub7PhBAi6KXoONPdRZK5EbePL1obbiZRB

---------------------------
CAL
---------------------------

Üldine näide:
https://www.experts-exchange.com/questions/26335404/How-do-I-set-vCalendar-CATEGORIES-color-and-Reminder-for-30min-before-appt.html

Alarmi näide:
https://www.kanzaki.com/docs/ical/valarm.html

Atendee:
https://www.kanzaki.com/docs/ical/attendee.html

Veel üks näide:
https://stackoverflow.com/questions/21955332/php-vcalendar-plain-text-description-visible-instead-of-html-text#45599202

Veel üks näide:
https://stevethomas.com.au/php/how-to-build-an-ical-calendar-with-php-and-mysql.html
