;(function($, window, document) {
    var pluginName = 'teliaAppendAround';
    var defaults = {};
    var initSelector = '.js-append-around';
    var attrName = 'data-set';

    /**
     * AppendAround.
     *
     * @class
     * @param {HTMLElement} element - The HTML element the AppendAround should be bound to.
     * @param {Object} options - An option map.
     */
    function AppendAround(element, options) {
        this.element = element;
        this.$element = $(this.element);
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.$parent = this.$element.parent();
        this.parent = this.$parent[0];
        this.setName = this.$parent.attr(attrName);
        this.set = $('[' + attrName + '="' + this.setName + '"]');

        this._init();
    }

    /**
     * Initialize the plugin.
     *
     * @function init
     * @private
     * @returns {void}
     */
    AppendAround.prototype._init = function() {
        this.appendToVisibleContainer();

        $(window).on('resize', this.appendToVisibleContainer.bind(this));
    };

    /**
     * Loop through possible containers and append element to the visible one.
     *
     * @function appendToVisibleContainer
     * @returns {void}
     */
    AppendAround.prototype.appendToVisibleContainer = function() {
        var that = this;

        if (that.isHidden(that.parent)) {
            var found = 0;

            that.set.each(function() {
                if (!that.isHidden(this) && !found) {
                    that.$element.appendTo(this);
                    found++;
                    that.parent = this;
                }
            });
        }
    };

    /**
     * Check if element is hidden.
     *
     * @function isHidden
     * @param {HTMLElement} elem - HTML Element.
     * @returns {boolean} If elem is hidden.
     */
    AppendAround.prototype.isHidden = function(elem) {
        return $(elem).css('display') === 'none';
    };

    /**
     * Destroy the plugin instance.
     *
     * @function destroy
     * @returns {void}
     */
    AppendAround.prototype.destroy = function() {
        $(window).off('resize', this.appendToVisibleContainer);
        this.$element.removeData('plugin_' + pluginName);
    };

    /**
     * Create jQuery plugin.
     *
     * @param {Object} options - Extended options.
     * @listens telia.appendAround
     * @returns {Array<AppendAround>} Array of AppendArounds.
     */
    $.fn[pluginName] = function(options) {
        var args = Array.prototype.slice.call(arguments, 1);

        return this.each(function() {
            var instance = $.data(this, 'plugin_' + pluginName);

            if (!instance) {
                if (options === 'destroy') {
                    return;
                }

                $.data(this, 'plugin_' + pluginName, new AppendAround(this, options));
            } else if (typeof options === 'string') {
                instance[options].apply(instance, args);
            }
        });
    };

    /**
     * @param {Event} event
     * @event enhance.telia.appendAround
     */
    $(document).on('enhance.telia.appendAround', function(event) {
        $(event.target).find(initSelector)[pluginName]();
    });

    /**
     * @param {Event} event
     * @event destroy.telia.appendAround
     */
    $(document).on('destroy.telia.appendAround', function(event) {
        $(event.target).find(initSelector)[pluginName]('destroy');
    });
})(jQuery, window, document);

/**
 * Auto-init the plugin on DOM ready.
 * @fires enhance.telia.appendAround
 */
$(function() {
    $(document).trigger('enhance.telia.appendAround');
});

/* globals svg4everybody */

svg4everybody();

;(function() {
    /**
     * Icon.
     *
     * @class
     */
    function Icon() {}

    /**
     * Get the HTML for an icon.
     *
     * @function render
     * @static
     * @param   {string} name -Icon name.
     * @param   {string} className - Additional class for icons.
     * @returns {string} HTML for an icon.
     */
    Icon.render = function(name, className) {
        className = typeof className !== 'undefined' ? ' ' + className : '';

        if (typeof name === 'undefined') {
            return;
        }

        return '<svg class="icon' + className + '"><use xlink:href="' + telia.svg_path + name + '"></use></svg>';
    };

    telia = typeof telia !== 'undefined' ? telia : {};
    telia.icon = Icon;
})();

;(function($, window, document) {
    var pluginName = 'teliaButton';
    var defaults = {
        disableIconWidows: true
    };
    var initSelector = '.btn';

    /**
     * Button.
     *
     * @class
     * @param {HTMLElement} element - The HTML element the Button should be bound to.
     * @param {Object} options - An option map.
     */
    function Button(element, options) {
        this.element = element;
        this.$element = $(this.element);
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;

        this._init();
    }

    /**
     * Initialize the plugin.
     *
     * @function _init
     * @private
     * @returns {void}
     */
    Button.prototype._init = function() {
        if (this.options.disableIconWidows) {
            this._disableIconWidows();
        }
    };

    /**
     * Disable icon widows.
     *
     * @function _disableIconWidows
     * @private
     * @returns {void}
     */
    Button.prototype._disableIconWidows = function() {
        var self = this.$element;

        self.contents().each(function() {
            // trim text nodes
            if (this.nodeValue) {
                this.nodeValue = this.nodeValue.trim();

                // if node still has value, it is text node and should be wrapped in span
                if (this.nodeValue) {
                    $(this).wrap('<span class="btn__text" />');
                }
            }

            // remove whitespace nodes
            if (this.nodeType == Node.TEXT_NODE && (/\S/).test(this.nodeValue) === false) this.remove();

            // trim nested text nodes for safety purposes
            if (this.nodeType == Node.ELEMENT_NODE) {
                // if there is already a nested element inside the button
                // and that element is not svg, add btn__text class to that
                // element to fix weird implementations
                if ($(this).is(':not(svg)')) {
                    $(this).addClass('btn__text');
                }

                $(this).contents().each(function() {
                    if (this.nodeValue) {
                        this.nodeValue = this.nodeValue.trim();
                    }
                });
            }
        });

        // loop through contents and insert non-breaking spaces to disable widows
        var contentsLength = self.contents().length;

        self.contents().each(function(index) {
            if (index < contentsLength - 1) {
                $(this).after('&nbsp;');
            }
        });

        self.addClass('btn--enhanced');
    };

    /**
     * Destroy plugin.
     *
     * @function destroy
     * @returns {void}
     */
    Button.prototype.destroy = function() {
        var self = this.$element;

        self.contents().each(function(index, node) {
            if (node.nodeType == Node.TEXT_NODE && (/\S/).test(node.nodeValue) === false) node.remove();
        });

        self.removeClass('btn--enhanced');

        $(this.element).removeData('plugin_' + pluginName);
    };

    /**
     * Create jQuery plugin.
     *
     * @param {Object} options - Extended options.
     * @listens telia.suggestion
     * @returns {Array<Button>} Array of Buttons.
     */
    $.fn[pluginName] = function(options) {
        var args = Array.prototype.slice.call(arguments, 1);

        return this.each(function() {
            var instance = $.data(this, 'plugin_' + pluginName);

            if (!instance) {
                if (options === 'destroy') {
                    return;
                }

                $.data(this, 'plugin_' + pluginName, new Button(this, options));
            } else if (typeof options === 'string') {
                instance[options].apply(instance, args);
            }
        });
    };

    /**
     * @param {Event} event
     * @event enhance.telia.button
     */
    $(document).on('enhance.telia.button', function(event) {
        $(event.target).find(initSelector)[pluginName]();
    });

    /**
     * @param {Event} even
     * @event destroy.telia.button
     */
    $(document).on('destroy.telia.button', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]('destroy');
    });
})(jQuery, window, document);

/**
 * Auto-init the plugin on DOM ready.
 * @fires enhance.telia.button
 */
$(function() {
    $(document).trigger('enhance.telia.button');
});

/* ========================================================================
* Bootstrap: collapse.js v3.3.5
* http://getbootstrap.com/javascript/#collapse
* ========================================================================
* Copyright 2011-2015 Twitter, Inc.
* Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
* ======================================================================== */

+function($) {
    'use strict';

    // CSS TRANSITION SUPPORT (Shoutout: http://www.modernizr.com/)
    // ============================================================

    /**
     * CSS transition support.
     *
     * @returns {mixed} Transition type.
     */
    function transitionEnd() {
        var el = document.createElement('bootstrap');

        var transEndEventNames = {
            WebkitTransition: 'webkitTransitionEnd',
            MozTransition: 'transitionend',
            OTransition: 'oTransitionEnd otransitionend',
            transition: 'transitionend'
        };

        for (var name in transEndEventNames) {
            if (el.style[name] !== undefined) {
                return {
                    end: transEndEventNames[name]
                };
            }
        }

        // explicit for ie8 (  ._.)
        return false;
    }

    // http://blog.alexmaccaw.com/css-transitions
    $.fn.emulateTransitionEnd = function(duration) {
        var called = false;
        var $el = this;

        $(this).one('bsTransitionEnd', function() { called = true; });

        /**
         * @returns {Function} Triggers transition end.
         */
        function callback() { if (!called) $($el).trigger($.support.transition.end); };
        setTimeout(callback, duration);

        return this;
    };

    $(function() {
        $.support.transition = transitionEnd();

        if (!$.support.transition) return;

        $.event.special.bsTransitionEnd = {
            bindType: $.support.transition.end,
            delegateType: $.support.transition.end,

            /**
             * 'bsTransitionEnd' event handler.
             *
             * @param   {Event} e - Event object.
             * @returns {void}
             */
            handle: function(e) {
                if ($(e.target).is(this)) return e.handleObj.handler.apply(this, arguments);
            }
        };
    });
}(jQuery);

+function($) {
    'use strict';

    /**
     * Collapse.
     *
     * @class
     * @param {HTMLElement} element - The HTML element the Collapse should be bound to.
     * @param {Object} options - An option map.
     */
    function Collapse(element, options) {
        this.$element      = $(element);
        this.options       = $.extend({}, Collapse.DEFAULTS, options);
        this.id            = element.id;
        this.$selector      = $('[data-toggle="collapse"][href="#' + this.id + '"],' +
        '[data-toggle="collapse"][data-target="#' + this.id + '"]');
        this.$trigger = this.$selector.length ? this.$selector : $(this.options.trigger);
        this.transitioning = null;
        this.$scrollable   = this.$element;

        var collapseContainer = this.$element.find('.collapse__container');

        if (collapseContainer.length) {
            this.$arrow        = $('<div class="collapse__arrow"></div>');
            this.$close        = $('<a href="#" data-toggle="collapse" class="collapse__close">' + telia.icon.render('#close-round') + '</a>');

            if (collapseContainer.hasClass('collapse__container--no-close')) {
                this.$close = false;
            }
            if (collapseContainer.hasClass('collapse__container--no-arrow')) {
                this.$arrow = false;
            }
        }

        if (this.options.parent) {
            this.$parent = this.getParent();
        } else {
            this.addAriaAndCollapsedClass(this.$element, this.$trigger);
        }

        if (this.options.toggle) this.toggle();
    };

    Collapse.VERSION  = '3.3.5';

    Collapse.TRANSITION_DURATION = 350;

    Collapse.DEFAULTS = {
        toggle: false,
        scrollSpeed: 500,
        topNavHeight: 50,
        newLocationOffset: 60,
        threshold: 100,
        autoScroll: true,

        /**
         * Callback function for content loading.
         *
         * @returns {void}
         */
        loadContent: function() {}
    };

    /**
     * Get element dimensions.
     *
     * @function dimension
     * @returns {string} Dimensions.
     */
    Collapse.prototype.dimension = function() {
        var hasWidth = this.$element.hasClass('width');

        return hasWidth ? 'width' : 'height';
    };

    /**
     * Show collapse.
     *
     * @function show
     * @param  {boolean} noScroll - If scrolling is allowed.
     * @returns {void}
     */
    Collapse.prototype.show = function(noScroll) {
        var shouldAnimate = true;

        if (this.$element.data('collapse-animate') != undefined) {
            shouldAnimate = this.$element.data('collapse-animate');
        }

        var that = this;

        if (this.transitioning || (this.$element.hasClass('is-visible') && this.$element.hasClass('collapse--visible'))) {
            if (!noScroll) {
                this.scroll(false);
            }

            return;
        }

        if (this.$trigger.data('content') == 'ajax') {
            this.options.loadContent.call(this, this.$element, loadContentReady);
        } else {
            showcontent.call(this);
        }

        /**
         * Load content ready.
         *
         * @param  {HTMLElement} html - HTML to be appended.
         * @returns {void}
         */
        function loadContentReady(html) {
            that.$element.find('.collapse__container').empty().append(html);
            showcontent.call(that);
        }

        /**
         * Show collapse content.
         *
         * @returns {void}
         */
        function showcontent() {
            var activesData;
            var actives = that.$parent && this.$parent.find('.collapse--visible, .collapse--collapsing, .collapse.is-visible, .collapse.is-collapsing');

            if (actives && actives.length) {
                activesData = actives.data('bs.collapse');
                if (activesData && activesData.transitioning) return;
            }

            var event = $.Event('show.bs.collapse');

            that.$element.trigger(event);
            if (event.isDefaultPrevented()) return;

            if (actives && actives.length) {
                Plugin.call(actives, 'hide');
                activesData || actives.data('bs.collapse', null);
            }

            var dimension = this.dimension();

            if (!shouldAnimate) {
                that.$element
                    .addClass('collapse--visible is-visible')[dimension](0)
                    .attr('aria-expanded', true)
                    .prepend(this.$arrow);
            } else {
                that.$element
                    .removeClass('collapse')
                    .addClass('collapse--collapsing is-collapsing')[dimension](0)
                    .attr('aria-expanded', true)
                    .prepend(this.$arrow);
            }

            $(document).trigger('refresh.telia', that.$element[0]);

            this.$trigger
                .removeClass('collapsed').addClass('is-active')
                .attr('aria-expanded', true);

            this.transitioning = 1;

            if (this.$trigger.length) {
                // filters
                this.setArrowPosition();
            }

            /**
             * Complete transition.
             *
             * @returns {void}
             */
            function complete() {
                that.$element
                    .removeClass('collapse--collapsing is-collapsing')
                    .addClass('collapse collapse--visible is-visible')[dimension]('');
                this.transitioning = 0;
                that.$element
                    .trigger('shown.bs.collapse')
                    .prepend(this.$close);

                this.setCloseButtonAction();

                $(window).trigger('resize');
            };

            if (!noScroll) {
                this.scroll(true);
            }

            this.setCollapseText();
            this.setCollapseIcon();

            if (!$.support.transition) return complete.call(this);

            var scrollSize = $.camelCase(['scroll', dimension].join('-'));

            that.$element
                .one('bsTransitionEnd', $.proxy(complete, this))
                .emulateTransitionEnd(Collapse.TRANSITION_DURATION)[dimension](that.$element[0][scrollSize]);

            if (this.$close) {
                this.$close.on('click', function(event) {
                    event.preventDefault();
                });
            }

            $(window).on('resize.telia.collapse.' + this.id, this.resizeHandler.bind(this));
        }
    };

    /**
     * Handle window resize.
     *
     * @function resizeHandler
     * @returns {void}
     */
    Collapse.prototype.resizeHandler = function() {
        if (this.$trigger.length) {
            this.setArrowPosition();
        }
    };

    /**
     * Scroll to collapse.
     *
     * @function scroll
     * @param  {boolean} setEvent - If event has to be set.
     * @returns {void}
     */
    Collapse.prototype.scroll = function(setEvent) {
        var event;
        var that = this;

        if (!this.options.autoScroll) {
            return;
        }

        if (setEvent) {
            this.$element.one('shown.bs.collapse', function() {
                scroller();
            });
        } else {
            scroller();
        }

        /**
         * Scroll to collapse.
         *
         * @returns {void}
         */
        function scroller() {
            event = $.Event('scroll.bs.collapse');
            that.$element.trigger(event);
            if (!that.$scrollable.isOnScreen(that.options.threshold)) {
                event = $.Event('scrolling.bs.collapse');
                that.$element.trigger(event);
                window.autoScroll(that.$scrollable, that.options.scrollSpeed, that.options.newLocationOffset);
            }

            event = $.Event('scrolled.bs.collapse');
            that.$element.trigger(event);
        }
    };

    /**
     * Hide collapse.
     *
     * @function hide
     * @returns {void}
     */
    Collapse.prototype.hide = function() {
        var shouldAnimate = true;

        if (this.$element.data('collapse-animate') != undefined) {
            shouldAnimate = this.$element.data('collapse-animate');
        }
        if (this.transitioning || (!this.$element.hasClass('collapse--visible') && !this.$element.hasClass('is-visible'))) return;

        var event = $.Event('hide.bs.collapse');

        this.$element.trigger(event);
        if (event.isDefaultPrevented()) return;

        var dimension = this.dimension();

        this.$element[dimension](this.$element[dimension]())[0].offsetHeight;

        if (!shouldAnimate) {
            this.$element
                .removeClass('collapse--visible is-visible')
                .attr('aria-expanded', false);
        } else {
            this.$element
                .addClass('collapse--collapsing is-collapsing')
                .removeClass('collapse collapse--visible is-visible')
                .attr('aria-expanded', false);
        }

        this.$trigger
            .removeClass('is-active').addClass('collapsed')
            .attr('aria-expanded', false);

        this.transitioning = 1;

        /**
         * Complete transition.
         *
         * @returns {void}
         */
        function complete() {
            this.transitioning = 0;
            this.$element
                .removeClass('collapse--collapsing is-collapsing')
                .addClass('collapse')
                .trigger('hidden.bs.collapse');

            $(window).trigger('resize');
        };

        $(document).trigger('reset.telia', this.$element[0]);

        this.setCollapseText();
        this.setCollapseIcon();

        if (!$.support.transition) return complete.call(this);

        this.$element[dimension](0)
            .one('bsTransitionEnd', $.proxy(complete, this))
            .emulateTransitionEnd(Collapse.TRANSITION_DURATION);

        $(window).off('resize.telia.collapse.' + this.id);
    };

    /**
     * Toggle collapse.
     *
     * @function toggle
     * @returns {void}
     */
    Collapse.prototype.toggle = function() {
        this[(this.$element.hasClass('collapse--visible') || this.$element.hasClass('is-visible')) ? 'hide' : 'show']();
    };

    /**
     * Get parent element.
     *
     * @function getParent
     * @returns {JQuery} Parent element.
     */
    Collapse.prototype.getParent = function() {
        return $(this.options.parent)
            .find('[data-toggle="collapse"][data-parent="' + this.options.parent + '"]')
            .each($.proxy(function(i, element) {
                var $element = $(element);

                this.addAriaAndCollapsedClass(getTargetFromTrigger($element), $element);
            }, this))
            .end();
    };

    /**
     * Add aria and toggle class for trigger.
     *
     * @function addAriaAndCollapsedClass
     * @param  {JQuery} $element - Collapse element.
     * @param  {JQuery} $trigger - Collapse trigger element.
     * @returns {void}
     */
    Collapse.prototype.addAriaAndCollapsedClass = function($element, $trigger) {
        var isOpen = $element.hasClass('collapse--visible') || $element.hasClass('is-visible');

        $element.attr('aria-expanded', isOpen);
        $trigger
            .toggleClass('collapsed', !isOpen)
            .toggleClass('is-active', isOpen)
            .attr('aria-expanded', isOpen);
    };

    /**
     * Set arrow position.
     *
     * @function setArrowPosition
     * @returns {void}
     */
    Collapse.prototype.setArrowPosition = function() {
        if (this.$arrow) {
            var left = this.$trigger.offset().left - this.$element.offset().left + this.$trigger.outerWidth() / 2 - 10;

            this.$arrow.css({
                left: left
            });
        }
    };

    /**
     * Set close button link.
     *
     * @function setCloseButtonAction
     * @returns {void}
     */
    Collapse.prototype.setCloseButtonAction = function() {
        if (this.$close) {
            this.$close.attr('href', '#' + this.id);
        }
    };

    /**
     * Set collapse trigger text.
     *
     * @function setCollapseText
     * @returns {void}
     */
    Collapse.prototype.setCollapseText = function() {
        this.$trigger.each(function() {
            var $this = $(this);

            if (!$this.data('more-text') || !$this.data('less-text')) {
                return;
            }
            var newText = $this.hasClass('is-active') ? $this.data('less-text') : $this.data('more-text');

            setTimeout(function() {
                if ($this.hasClass('collapse-toggle')) {
                    $this.find('.btn__text').html(newText);
                } else {
                    $this.html(newText);
                }
            }, 100);
        });
    };

    /**
     * Set collapse trigger icon.
     *
     * @function setCollapseIcon
     * @returns {void}
     */
    Collapse.prototype.setCollapseIcon = function() {
        this.$trigger.each(function() {
            var $this = $(this);

            if (!$this.data('more-icon') || !$this.data('less-icon')) {
                return;
            }
            var newIcon = $this.hasClass('is-active') ? $this.data('less-icon') : $this.data('more-icon');

            setTimeout(function() {
                if ($this.hasClass('collapse-toggle')) {
                    var icon = $this.find('.collapse-toggle__icon');

                    if (icon.find('use').length) {
                        // if the browser supports external use
                        icon.find('use').attr('xlink:href', telia.svg_path + newIcon);
                    } else {
                        // if the browser does not support external use and is polyfilled
                        icon.children().replaceWith('<use xlink:href="' + telia.svg_path + newIcon + '"></use>');
                    }
                }
            }, 100);
        });
    };

    /**
     * Gets target element from trigger.
     *
     * @function getTargetFromTrigger
     * @param  {JQuery} $trigger - Trigger JQuery element.
     * @returns {JQuery} Target jQuery element.
     */
    function getTargetFromTrigger($trigger) {
        var href;
        // strip for ie7
        var target = $trigger.attr('data-target') || (href = $trigger.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '');

        return $(target);
    }

    /**
     * Create plugin.
     *
     * @param {Object} option - Extended options.
     * @param {Array[]} args - Additional arguments.
     * @returns {Array<Collapse>} Array of Collapses.
     */
    function Plugin(option, args) {
        return this.each(function() {
            var $this   = $(this);
            var data    = $this.data('bs.collapse');
            var options = $.extend({}, Collapse.DEFAULTS, $this.data(), typeof option == 'object' && option);

            if (!data && options.toggle && (/show|hide/).test(option)) options.toggle = false;
            if (!data) $this.data('bs.collapse', (data = new Collapse(this, options)));
            if (typeof option == 'string') data[option](args);
        });
    }

    var old = $.fn.collapse;

    $.fn.collapse             = Plugin;
    $.fn.collapse.Constructor = Collapse;

    // COLLAPSE NO CONFLICT
    // ====================

    $.fn.collapse.noConflict = function() {
        $.fn.collapse = old;

        return this;
    };

    // COLLAPSE DATA-API
    // =================

    /**
     * @param {Event} e
     * @event click.bs.collapseLayer.data-api
     */
    $(document).on('click.bs.collapse.data-api', '[data-toggle="collapse"]', function(e) {
        var $this   = $(this);

        if (!$this.attr('data-target')) e.preventDefault();

        var $target = getTargetFromTrigger($this);
        var data    = $target.data('bs.collapse');
        var option  = data ? 'toggle' : $this.data();

        Plugin.call($target, option);
    });

    /**
     * @param {Event} e
     * @event click.bs.collapseLayer.data-api
     */
    $(document).on('click.bs.collapse.data-api', '[data-show="collapse"]', function(e) {
        var $this   = $(this);

        if (!$this.attr('data-target')) e.preventDefault();

        var $target = getTargetFromTrigger($this);
        var data    = $target.data('bs.collapse');
        var option  = data ? 'show' : $this.data();

        Plugin.call($target, option);
    });

    /**
     * @param {Event} e
     * @event click.bs.collapseLayer.data-api
     */
    $(document).on('click.bs.collapse.data-api', '[data-hide="collapse"]', function(e) {
        var $this   = $(this);

        if (!$this.attr('data-target')) e.preventDefault();

        var $target = getTargetFromTrigger($this);
        var data    = $target.data('bs.collapse');
        var option  = data ? 'hide' : $this.data();

        Plugin.call($target, option);
    });
}(jQuery);

;(function($, window, document) {
    var pluginName = 'teliaForm';
    var defaults = {};
    var initSelector = [
        '.init-datepicker',
        '.init-weekdatepicker'
    ];

    /**
     * Form.
     *
     * @class
     * @param {HTMLElement} element - The HTML element the Form should be bound to.
     * @param {Object} options - An option map.
     */
    function Form(element, options) {
        this.element = element;
        this.$element = $(element);

        this.options = $.extend({}, defaults, options);

        this._defaults = defaults;
        this._name = pluginName;

        this._init();
    }

    /**
     * Initialize the plugin.
     *
     * @function init
     * @private
     * @returns {void}
     */
    Form.prototype._init = function() {
        if (this.$element.hasClass('init-datepicker')) {
            this._initDatepicker();
        }
        if (this.$element.hasClass('init-weekdatepicker')) {
            this._initWeekDatepicker();
        }
    };

    /**
     * Initialize datepicker.
     *
     * @function initDatepicker
     * @private
     * @returns {void}
     */
    Form.prototype._initDatepicker = function() {
        var self = this.$element;
        var attr;
        var date;
        var arr;
        var newArr;

        self.prop('readonly', true);

        var options = {

            position: 'bottom right',
            autoClose: true,
            clearButton: true,

            /**
             * Trigger change event on input after date is selected.
             *
             * @param   {string} formattedDate - Formatted date.
             * @param   {Date} date - Original date.
             * @param   {Datepicker} inst - Datepicker plugin instance.
             * @returns {void}
             */
            onSelect: function(formattedDate, date, inst) {
                $(inst.el).trigger('change');
            }
        };

        if (self.attr('data-datepicker-mindate')) {
            attr = self.attr('data-datepicker-mindate');
            if (attr.substr('.').length > 0) {
                attr = attr.split('.');
                attr = attr.reverse().join('-');
            }
            date = new Date(attr);
            options.minDate = date;
        }

        if (self.attr('data-datepicker-maxdate')) {
            attr = self.attr('data-datepicker-maxdate');
            if (attr.substr('.').length > 0) {
                attr = attr.split('.');
                attr = attr.reverse().join('-');
            }
            date = new Date(attr);
            options.maxDate = date;
        }

        if (self.attr('data-datepicker-disabledates')) {
            attr = self.attr('data-datepicker-disabledates');
            arr = attr.split(',');
            newArr = arr.map(function(i) {
                if (i.substr('.').length > 0) {
                    i = i.split('.');
                    i = i.reverse().join('-');
                }
                var date = new Date(i);

                date.setHours(0, 0, 0, 0);

                return date.getTime();
            });

            options.onRenderCell = function(date, cellType) {
                if (cellType == 'day') {
                    var day = new Date(date);

                    day.setHours(0, 0, 0, 0);
                    var time = day.getTime();
                    var isDisabled = newArr.indexOf(time) != -1;

                    return {
                        disabled: isDisabled
                    };
                }
            };
        }

        if (self.attr('data-datepicker-enabledates')) {
            attr = self.attr('data-datepicker-enabledates');
            arr = attr.split(',');
            newArr = arr.map(function(i) {
                if (i.substr('.').length > 0) {
                    i = i.split('.');
                    i = i.reverse().join('-');
                }
                var date = new Date(i);

                date.setHours(0, 0, 0, 0);

                return date.getTime();
            });

            options.onRenderCell = function(date, cellType) {
                if (cellType == 'day') {
                    var day = new Date(date);

                    day.setHours(0, 0, 0, 0);
                    var time = day.getTime();
                    var isDisabled = newArr.indexOf(time) != -1;

                    return {
                        disabled: !isDisabled
                    };
                }
            };
        }

        if (self.attr('data-datepicker-ranges')) {
            attr = self.attr('data-datepicker-ranges');
            arr = attr.split('|');
            var minDate = new Date();
            var maxDate = new Date();
            var ranges = arr.map(function(i) {
                var childArr = i.split(',');

                childArr = childArr.map(function(i) {
                    if (i.substr('.').length > 0) {
                        i = i.split('.');
                        i = i.reverse().join('-');
                    }
                    var date = new Date(i);

                    date.setHours(0, 0, 0, 0);

                    if (date < minDate) {
                        minDate = date;
                    }

                    if (date > maxDate) {
                        maxDate = date;
                    }

                    return date.getTime();
                });

                return childArr;
            });

            options.minDate = minDate;
            options.maxDate = maxDate;
            options.onRenderCell = function(date, cellType) {
                if (cellType == 'day') {
                    var day = new Date(date);

                    day.setHours(0, 0, 0, 0);
                    var time = day.getTime();
                    var isDisabled = true;

                    for (var i = ranges.length - 1; i >= 0; i--) {
                        if (time >= ranges[i][0] && time <= ranges[i][1]) {
                            isDisabled = false;
                        }
                    }

                    return {
                        disabled: isDisabled
                    };
                }
            };
        }

        if ($('html').attr('lang')) {
            options.language = $('html').attr('lang');
        }

        self.datepicker(options);

        if (self.val() != '') {
            var strDate = self.val();
            var dateParts = strDate.split('.');

            var formattedDate = new Date(dateParts[2], (dateParts[1] - 1), dateParts[0]);

            self.data('datepicker').selectDate(new Date(Date.parse(formattedDate)));
        }
    };

    /**
     * Initialize week datepicker.
     *
     * @function initWeekDatepicker
     * @private
     * @returns {void}
     */
    Form.prototype._initWeekDatepicker = function() {
        this.$element.weekDatepicker();
    };


    /**
     * Initialize select.
     *
     * @function initSelect
     * @private
     * @returns {void}
     */
    Form.prototype._initSelect = function() {
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) && !this.$element.data('live-search')) {
            this.$element.selectpicker('mobile');
        } else {
            this.$element.selectpicker();
        }
    };

    /**
     * Create jQuery plugin.
     *
     * @param {Object} options - Extended options.
     * @listens form
     * @returns {Array<Form>} Array of Forms.
     */
    $.fn[pluginName] = function(options) {
        return this.each(function() {
            if (!$.data(this, 'plugin_' + pluginName)) {
                $.data(this, 'plugin_' + pluginName, new Form(this, options));
            }
        });
    };

    /**
     * @param {Event} event
     * @event enhance.form
     */
    $(document).on('enhance.form', function(event) {
        $(event.target).find(initSelector.join(', '))[pluginName]();
    });

    /**
     * @param {Event} event
     * @event enhance.form.datepicker
     */
    $(document).on('enhance.form.datepicker', function(event) {
        $(event.target).find('.init-datepicker, .init-weekdatepicker')[pluginName]();
    });
})(jQuery, window, document);


/**
 * Auto-init the plugin on DOM ready.
 * @fires enhance.form
 */
$(document).ready(function() {
    $(document).trigger('enhance.form');
});

/**
 * Old init for compatibility.
 *
 * @fires enhance.form
 * @deprecated Use enhance.form event directly.
 * @returns {void}
 */
function initFormComponents() {
    $(document).trigger('enhance.form');
}

window.initFormComponents = initFormComponents;

;(function($, window, document) {
    var pluginName = 'teliaCarouselHero';
    var defaults = {
        defaultEaseFn: 'cubic-bezier(0.645, 0.045, 0.355, 1.000)',
        prevArrow: '<button class="carousel__arrow carousel__arrow--left">' + telia.icon.render('#arrow-left-light') + '</button>',
        nextArrow: '<button class="carousel__arrow carousel__arrow--right">' + telia.icon.render('#arrow-right-light') + '</button>'
    };
    var initSelector = '.carousel--hero';

    /**
     * CarouselHero.
     *
     * @class
     * @param {HTMLElement} element - The HTML element the CarouselHero should be bound to.
     * @param {Object} options - An option map.
     */
    function CarouselHero(element, options) {
        this.element = element;
        this.$element = $(this.element);
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;

        this.init();
    }

    /**
     * Initialize the plugin.
     *
     * @function init
     * @returns {void}
     */
    CarouselHero.prototype.init = function() {
        if (this.$element.find('.carousel__item').length > 1) {
            if (!this.$element.hasClass('slick-initialized')) {
                this.bindEvents();
                this.setPriority();
                this.initSlick();
            }
        }
    };

    /**
     * Bind events.
     *
     * @function bindEvents
     * @returns {void}
     */
    CarouselHero.prototype.bindEvents = function() {
        var that = this;

        $(document).on('refresh.telia.carousel', function(event, target) {
            if (that.$element.hasClass('slick-initialized') && target && $.contains(target, that.element)) {
                that.$element.slick('refresh');
            }
        });

        this.$element.on('init', function(event, slick) {
            $(this).find('img').removeClass('lazyloading').addClass('lazyload lazypreload');

            that.startAnimatedSlide(event, slick, 0);
        });

        this.$element.on('afterChange', this.startAnimatedSlide);
    };

    /**
     * Initialize slick plugin.
     *
     * @function initSlick
     * @returns {void}
     */
    CarouselHero.prototype.initSlick = function() {
        this.$element.slick({
            dots: true,
            arrows: true,
            autoplay: true,
            autoplaySpeed: 8000,
            draggable: false,
            cssEase: this.options.defaultEaseFn,
            useTransform: true,
            speed: 1000,
            pauseOnHover: false,
            prevArrow: this.options.prevArrow,
            nextArrow: this.options.nextArrow,

            /**
             * Create custom paging elements.
             *
             * @param   {Slick} slider - Slick slider instance.
             * @returns {string} Paging element.
             */
            customPaging: function(slider) {
                if (slider.options.autoplay) {
                    var animationDuration = slider.options.autoplaySpeed + slider.options.speed;


                    return '<button data-role="none" role="button" aria-required="false" tabindex="0" class="carousel__indicator"><span class="carousel__progress" style="animation-duration: ' + animationDuration + 'ms"></span></button>';
                } else {
                    return '<button type="button" data-role="none" role="button" aria-required="false" tabindex="0" class="carousel__indicator carousel__indicator--dot"></button>';
                }
            },
            responsive: [
                {
                    breakpoint: 1300,
                    settings: {
                        arrows: false
                    }
                },
                {
                    breakpoint: 1024,
                    settings: {
                        draggable: true,
                        speed: 500,
                        arrows: false
                    }
                }
            ]
        });
    };

    /**
     * Set priority to slides.
     *
     * @function setPriority
     * @returns {void}
     */
    CarouselHero.prototype.setPriority = function() {
        var $initialEl = this.$element.find('.carousel__item')[0];
        // if one or more items has priority set

        if (this.$element.find('.carousel__item[data-priority-order]').length > 0) {
            // carousel items
            if (this.$element.find('.carousel__item[data-priority-order="0"]').length > 0) {
                $initialEl = this.$element.find('.carousel__item[data-priority-order="0"]')[0];
            } else {
                var carouselItems = this.$element.find('.carousel__item');
                // array with each items priority order number
                var weightArray = [];
                // loop of items to get array of priority order numbers

                carouselItems.each(function() {
                    // declaration of
                    var orderNumber = 0;
                    // get elements' priority number or else it will be 3
                    var thisPriorityNumber = $(this).data('priorityOrder') || 3;
                    // switch to turn priority numbers around

                    switch (thisPriorityNumber) {
                        case 1:
                            orderNumber = 3;
                            break;
                        case 3:
                            orderNumber = 1;
                            break;
                        default:
                            orderNumber = thisPriorityNumber;
                    }
                    // push priority order number to array
                    weightArray.push(orderNumber);
                });
                // sum of all elements in array
                var totalWeight = eval(weightArray.join('+'));
                // array of final items
                var weightedArray = [];
                // counter
                var current = 0;

                // loop through carousel items
                while (current < carouselItems.length) {
                    // for every item put it into a new array the number of times its weight
                    for (var i = 0; i < weightArray[current]; i++) {
                        weightedArray[weightedArray.length] = carouselItems[current];
                    }
                    current++;
                }

                // take random number from total sum of all items in priority numbers
                var randomItemKey = Math.floor(Math.random() * totalWeight);
                // random number is the key to one of the elements in weighted array

                $initialEl = weightedArray[randomItemKey];
            }
        }

        this.$element.prepend($initialEl);
    };

    /**
     * Start animated slide.
     *
     * @function startAnimatedSlide
     * @param  {Object} event - JQuery event.
     * @param  {HTMLElement} slick - Slick plugin instance.
     * @param  {number} currentSlide - Current slide index.
     * @returns {void}
     */
    CarouselHero.prototype.startAnimatedSlide = function(event, slick, currentSlide) {
        var self = slick.$slider;
        var nextSlide = self.find('[data-slick-index="' + currentSlide + '"]');

        if (nextSlide.hasClass('carousel__item--animated')) {
            nextSlide.addClass('slick-animated');
        }
        self.find('.carousel__item--animated').not(nextSlide).removeClass('slick-animated');

        /*
        sometimes there's a video as a background in the hero.
        if there's a html5 video in the next slide, start playing it.
        browsers tend to pause/stop video if it is modified via JS, as slick does here.
        */
        nextSlide.find('video').each(function() {
            this.play();
        });
    };

    /**
     * Destroy the plugin instance.
     *
     * @function destroy
     * @returns {void}
     */
    CarouselHero.prototype.destroy = function() {
        this.$element.slick('unslick');

        this.$element.removeData('plugin_' + pluginName);
    };

    /**
     * Create jQuery plugin.
     *
     * @param {Object} options - Extended options.
     * @listens telia.carouselHero
     * @returns {Array<CarouselHero>} Array of CarouselHeros.
     */
    $.fn[pluginName] = function(options) {
        var args = Array.prototype.slice.call(arguments, 1);

        return this.each(function() {
            var instance = $.data(this, 'plugin_' + pluginName);

            if (!instance) {
                if (options === 'destroy') {
                    return;
                }

                $.data(this, 'plugin_' + pluginName, new CarouselHero(this, options));
            } else if (typeof options === 'string') {
                instance[options].apply(instance, args);
            }
        });
    };

    /**
     * @param {Event} event
     * @event enhance.telia.carouselHero
     */
    $(document).on('enhance.telia.carouselHero', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]();
    });

    /**
     * @param {Event} event
     * @event destroy.telia.carouselHero
     */
    $(document).on('destroy.telia.carouselHero', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]('destroy');
    });
})(jQuery, window, document);

/**
 * Auto-init the plugin on DOM ready.
 * @fires enhance.telia.carouselHero
 */
$(function() {
    $(document).trigger('enhance.telia.carouselHero');
});

/**
 * Old init for compatibility.
 *
 * @fires enhance.telia.carouselHero
 * @deprecated Use enhance.telia.carouselHero event directly.
 * @returns {void}
 */
function initCarouselHero() {
    $(document).trigger('enhance.telia.carouselHero');
}
window.initCarouselHero = initCarouselHero;

;(function($, window, document, undefined) {
    var pluginName = 'teliaCarouselMultiple';
    var defaults = {
        defaultEaseFn: 'cubic-bezier(0.645, 0.045, 0.355, 1.000)',
        prevArrow: '<button class="carousel__arrow carousel__arrow--left">' + telia.icon.render('#arrow-left') + '</button>',
        nextArrow: '<button class="carousel__arrow carousel__arrow--right">' + telia.icon.render('#arrow-right') + '</button>'
    };
    var initSelector = '.carousel--multiple';

    /**
     * Carousel Multiple.
     *
     * @class
     * @param {HTMLElement} element - The HTML element the CarouselMultiple should be bound to.
     * @param {Object} options - An option map.
     */
    function CarouselMultiple(element, options) {
        this.element = element;
        this.$element = $(this.element);
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;

        this.init();
    }

    /**
     * Initialize the plugin.
     *
     * @function init
     * @returns {void}
     */
    CarouselMultiple.prototype.init = function() {
        if (!this.$element.hasClass('slick-initialized')) {
            this.bindEvents();
            this.initSlick();
        }
    };

    /**
     * Initialize slick plugin.
     *
     * @function initSlick
     * @returns {void}
     */
    CarouselMultiple.prototype.initSlick = function() {
        var dataDefaultSlides = this.$element.data('slides-default');
        var slidesDefault = dataDefaultSlides == undefined ? 1 : parseInt(dataDefaultSlides);

        this.$element.slick({
            dots: false,
            infinite: false,
            autoplay: false,
            mobileFirst: true,
            slidesToShow: slidesDefault,
            slidesToScroll: slidesDefault,
            draggable: false,
            cssEase: this.options.defaultEaseFn,
            useTransform: true,
            speed: 500,
            zIndex: 1,
            prevArrow: this.options.prevArrow,
            nextArrow: this.options.nextArrow,
            responsive: this.setResponsive(this.$element)
        });
    };

    /**
     * Bind events.
     *
     * @function bindEvents
     * @returns {void}
     */
    CarouselMultiple.prototype.bindEvents = function() {
        var that = this;

        $(document).on('refresh.telia.carousel', function(event, target) {
            if (that.$element.hasClass('slick-initialized') && target && $.contains(target, that.element)) {
                that.$element.slick('refresh');
            }
        });
    };

    /**
     * Set responsive config for slick.
     *
     * @function setResponsive
     * @returns {Object[]} Array of objects with breakpoints config.
     */
    CarouselMultiple.prototype.setResponsive = function() {
        var breakpoints = {
            xs: {
                width: 320,
                slides: 1
            },
            sm: {
                width: 600,
                slides: 3
            },
            md: {
                width: 1024,
                slides: 4
            },
            lg: {
                width: 1200,
                slides: 4
            }
        };
        var conf = [];

        for (var key in breakpoints) {
            var slide = {
                settings: {
                    draggable: true,
                    speed: 500
                }
            };

            if (breakpoints.hasOwnProperty(key)) {
                var dataDefault =  breakpoints[key];
                var dataBp = this.$element.data('slides-' + key);
                var slides = dataBp == undefined ? dataDefault.slides : dataBp;

                slide.breakpoint = dataDefault.width;
                slide.settings.slidesToShow = slides;
                slide.settings.slidesToScroll = slides;

                conf.push(slide);
            }
        }

        return conf;
    };

    /**
     * Destroy the plugin instance.
     *
     * @function destroy
     * @returns {void}
     */
    CarouselMultiple.prototype.destroy = function() {
        this.$element.slick('unslick');

        this.$element.removeData('plugin_' + pluginName);
    };

    /**
     * Create jQuery plugin.
     *
     * @param {Object} options - Extended options.
     * @listens telia.carouselMultiple
     * @returns {Array<CarouselMultiple>} Array of CarouselMultiples.
     */
    $.fn[pluginName] = function(options) {
        var args = Array.prototype.slice.call(arguments, 1);

        return this.each(function() {
            var instance = $.data(this, 'plugin_' + pluginName);

            if (!instance) {
                if (options === 'destroy') {
                    return;
                }

                $.data(this, 'plugin_' + pluginName, new CarouselMultiple(this, options));
            } else if (typeof options === 'string') {
                instance[options].apply(instance, args);
            }
        });
    };

    /**
     * @param {Event} event
     * @event enhance.telia.carouselMultiple
     */
    $(document).on('enhance.telia.carouselMultiple', function(event) {
        $(event.target).find(initSelector)[pluginName]();
    });

    /**
     * @param {Event} event
     * @event destroy.telia.carouselMultiple
     */
    $(document).on('destroy.telia.carouselMultiple', function(event) {
        $(event.target).find(initSelector)[pluginName]('destroy');
    });
})(jQuery, window, document);

/**
 * Auto-init the plugin on DOM ready.
 * @fires enhance.telia.carouselMultiple
 */
$(function() {
    $(document).trigger('enhance.telia.carouselMultiple');
});

/**
 * Old init for compatibility.
 *
 * @fires enhance.telia.carouselMultiple
 * @deprecated Use enhance.telia.carouselMultiple event directly.
 * @returns {void}
 */
function initCarouselMultiple() {
    $(document).trigger('enhance.telia.carouselMultiple');
}
window.initCarouselMultiple = initCarouselMultiple;

;(function($, window, document) {
    var pluginName = 'teliaCarouselCentered';
    var defaults = {
        defaultEaseFn: 'cubic-bezier(0.645, 0.045, 0.355, 1.000)',
        prevArrow: '<button class="carousel__arrow carousel__arrow--left">' + telia.icon.render('#arrow-left') + '</button>',
        nextArrow: '<button class="carousel__arrow carousel__arrow--right">' + telia.icon.render('#arrow-right') + '</button>'
    };
    var initSelector = '.carousel--centered';

    /**
     * CarouselCentered.
     *
     * @class
     * @param {HTMLElement} element - The HTML element the CarouselCentered should be bound to.
     * @param {Object} options - An option map.
     */
    function CarouselCentered(element, options) {
        this.element = element;
        this.$element = $(this.element);
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;

        this.init();
    }

    /**
     * Initialize the plugin.
     *
     * @function init
     * @returns {void}
     */
    CarouselCentered.prototype.init = function() {
        if (!this.$element.hasClass('slick-initialized')) {
            this.bindEvents();
            this.initSlick();
        }
    };

    /**
     * Bind events.
     *
     * @function bindEvents
     * @returns {void}
     */
    CarouselCentered.prototype.bindEvents = function() {
        var that = this;

        $(document).on('refresh.telia.carousel', function(event, target) {
            if (that.$element.hasClass('slick-initialized') && target && $.contains(target, that.element)) {
                that.$element.slick('refresh');
            }
        });
    };

    /**
     * Initialize slick plugin.
     *
     * @function initSlick
     * @returns {void}
     */
    CarouselCentered.prototype.initSlick = function() {
        var that = this;

        this.$element.on('init', function(event, slick) {
            var nextSlide = slick.currentSlide;
            var self = $(this);
            var $nextSlide = self.find('.slick-slide').eq(nextSlide);

            that.setFirstSlides(self, nextSlide);
            that.setCarouselContent($nextSlide.data('carouselContentId'), $nextSlide.data('content'));
        }).on('beforeChange', function(event, slick, currentSlide, nextSlide) {
            var self = $(this);
            var $nextSlide = self.find('.slick-slide').eq(nextSlide);

            that.setFirstSlides(self, nextSlide);
            that.setCarouselContent($nextSlide.data('carouselContentId'), $nextSlide.data('content'));
        }).slick({
            dots: false,
            autoplay: false,
            slidesToScroll: 1,
            infinite: false,
            initialSlide: 3,
            centerMode: true,
            focusOnSelect: true,
            slidesToShow: 7,
            draggable: false,
            touchMove: false,
            cssEase: this.options.defaultEaseFn,
            useTransform: true,
            speed: 500,
            zIndex: 1,
            prevArrow: this.options.prevArrow,
            nextArrow: this.options.nextArrow
        });
    };

    /**
     * Set first slides.
     *
     * @function setFirstSlides
     * @param  {HTMLElement} self - Carousel object.
     * @param  {number} nextSlide - Next slide index.
     * @returns {void}
     */
    CarouselCentered.prototype.setFirstSlides = function(self, nextSlide) {
        self.find('.slick-slide').removeClass('carousel--centered__slide-left carousel--centered__slide-left-2 carousel--centered__slide-left-3');
        self.find('.slick-slide').eq(nextSlide).prev().prev().prev().addClass('carousel--centered__slide-left');
        self.find('.slick-slide').eq(nextSlide).prev().prev().addClass('carousel--centered__slide-left-2');
        self.find('.slick-slide').eq(nextSlide).prev().addClass('carousel--centered__slide-left-3');
    };

    /**
     * Set carousel content.
     *
     * @function setCarouselContent
     * @param  {string} target - Attribute data-carousel-content-id value.
     * @param  {string} content - Attribute data-content value.
     * @returns {void}
     */
    CarouselCentered.prototype.setCarouselContent = function(target, content) {
        var $target = $('[data-carousel-content=' + target + ']');

        if ($target.length) {
            $target.html(content);
        }
    };

    /**
     * Destroy the plugin instance.
     *
     * @function destroy
     * @returns {void}
     */
    CarouselCentered.prototype.destroy = function() {
        this.$element.slick('unslick');

        this.$element.removeData('plugin_' + pluginName);
    };

    /**
     * Create jQuery plugin.
     *
     * @param {Object} options - Extended options.
     * @listens telia.carouselCentered
     * @returns {Array<CarouselCentered>} Arrat of CarouselCentereds.
     */
    $.fn[pluginName] = function(options) {
        var args = Array.prototype.slice.call(arguments, 1);

        return this.each(function() {
            var instance = $.data(this, 'plugin_' + pluginName);

            if (!instance) {
                if (options === 'destroy') {
                    return;
                }

                $.data(this, 'plugin_' + pluginName, new CarouselCentered(this, options));
            } else if (typeof options === 'string') {
                instance[options].apply(instance, args);
            }
        });
    };

    /**
     * @param {Event} event
     * @event enhance.telia.carouselCentered
     */
    $(document).on('enhance.telia.carouselCentered', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]();
    });

    /**
     * @param {Event} event
     * @event destroy.telia.carouselCentered
     */
    $(document).on('destroy.telia.carouselCentered', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]('destroy');
    });
})(jQuery, window, document);

/**
 * Auto-init the plugin on DOM ready.
 * @fires enhance.telia.carouselCentered
 */
$(function() {
    $(document).trigger('enhance.telia.carouselCentered');
});

/**
 * Old init for compatibility.
 *
 * @fires enhance.telia.carouselCentered
 * @deprecated Use enhance.telia.carouselCentered event directly.
 * @returns {void}
 */
function initCarouselCentered() {
    $(document).trigger('enhance.telia.carouselCentered');
}
window.initCarouselCentered = initCarouselCentered;

;(function($, window, document) {
    var pluginName = 'teliaCarouselFade';
    var defaults = {
        defaultEaseFn: 'cubic-bezier(0.645, 0.045, 0.355, 1.000)',
        prevArrow: '<button class="carousel__arrow carousel__arrow--left">' + telia.icon.render('#arrow-left') + '</button>',
        nextArrow: '<button class="carousel__arrow carousel__arrow--right">' + telia.icon.render('#arrow-right') + '</button>'
    };
    var initSelector = '.carousel--fade';

    /**
     * CarouselFade.
     *
     * @class
     * @param {HTMLElement} element - The HTML element the CarouselFade should be bound to.
     * @param {Object} options - An option map.
     */
    function CarouselFade(element, options) {
        this.element = element;
        this.$element = $(this.element);
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.carouselResizeTimer;

        this.init();
    }

    /**
     * Initialize the plugin.
     *
     * @function init
     * @returns {void}
     */
    CarouselFade.prototype.init = function() {
        var that = this;
        var initSlider = true;
        var initial;
        var widths;

        if (this.options.initialSlide == null) {
            var rec = this.$element.find('.form-option-card--recommended');

            if (rec.length > 0) {
                initial = rec.parent().index();
            } else {
                initial = 0;
            }
        } else {
            initial = this.options.initialSlide;
        }

        if (!this.$element.hasClass('slick-initialized')) {
            this.bindEvents();

            widths = this.getparentAndChildrenWidth(this.$element);

            if (widths.parent > widths.children) {
                initSlider = false;
            }

            if (initSlider) {
                this.initSlick(initial);

                if (this.$element.find('.form-option-card__input').length > 0) {
                    this.$element.find('.form-option-card__input').on('change', function() {
                        var slide = $(this).parent().parent().index();

                        if (that.$element.hasClass('slick-initialized')) {
                            that.$element.slick('slickGoTo', slide);
                        }
                    });
                }
            }
        } else {
            widths = this.getparentAndChildrenWidth(this.$element, '.slick-slide');

            if (widths.parent > widths.children) {
                initSlider = false;

                if (!initSlider) {
                    this.$element.slick('unslick');
                }
            }
        }

        $(window).on('resize.telia.carouselFade', this.resize.bind(this));
    };

    /**
     * Bind events.
     *
     * @function bindEvents
     * @returns {void}
     */
    CarouselFade.prototype.bindEvents = function() {
        var that = this;

        $(document).on('refresh.telia.carousel', function(event, target) {
            if (that.$element.hasClass('slick-initialized') && target && $.contains(target, that.element)) {
                that.$element.slick('refresh');
            }
        });
    };

    /**
     * Initialize Slick plugin.
     *
     * @function initSlick
     * @param  {number} initial - Initial slide index.
     * @returns {void}
     */
    CarouselFade.prototype.initSlick = function(initial) {
        this.$element.slick({
            dots: false,
            infinite: false,
            autoplay: false,
            centerMode: true,
            slidesToShow: 1,
            variableWidth: true,
            initialSlide: initial,
            slidesToScroll: 1,
            draggable: true,
            cssEase: this.options.defaultEaseFn,
            useTransform: true,
            speed: 300,
            zIndex: 1,
            prevArrow: this.options.prevArrow,
            nextArrow: this.options.nextArrow
        });
    };

    /**
     * Resize window initializes plugin.
     *
     * @function resize
     * @returns {void}
     */
    CarouselFade.prototype.resize = function() {
        clearTimeout(this.carouselResizeTimer);
        this.carouselResizeTimer = setTimeout(this.init.bind(this), 250);
    };

    /**
     * Calculates width of parent and children.
     *
     * @function getparentAndChildrenWidth
     * @param  {HTMLElement} obj - Current carousel element.
     * @param  {HTMLElement} children - Current carousel children.
     * @returns {Object} Width of parent and children.
     */
    CarouselFade.prototype.getparentAndChildrenWidth = function(obj, children) {
        var selector;

        if (children) {
            selector = children;
        } else {
            selector = '> *';
        }
        var childrenWidth = 0;
        var width = 0;
        var clone = obj.clone();

        clone.css('visibility', 'hidden').find(selector).empty();
        $('body').append(clone);
        clone.wrap('<div class="container"></div>');
        width = clone.outerWidth();

        clone.find(selector).each(function() {
            childrenWidth += $(this).outerWidth(true);
        });

        clone.parent().remove();

        return {
            parent: width,
            children: childrenWidth
        };
    };

    /**
     * Initialize slick plugin.
     *
     * @function destroy
     * @returns {void}
     */
    CarouselFade.prototype.destroy = function() {
        this.$element.slick('unslick');
        $(window).off('resize.telia.carouselFade', this.resize);

        this.$element.removeData('plugin_' + pluginName);
    };

    /**
     * Create jQuery plugin.
     *
     * @param {Object} options - Extended options.
     * @listens telia.carouselFade
     * @returns {Array<CarouselFade>} Array of CarouselFades.
     */
    $.fn[pluginName] = function(options) {
        var args = Array.prototype.slice.call(arguments, 1);

        return this.each(function() {
            var instance = $.data(this, 'plugin_' + pluginName);

            if (!instance) {
                if (options === 'destroy') {
                    return;
                }

                $.data(this, 'plugin_' + pluginName, new CarouselFade(this, options));
            } else if (typeof options === 'string') {
                instance[options].apply(instance, args);
            }
        });
    };

    /**
     * @param {Event} event
     * @event enhance.telia.carouselFade
     */
    $(document).on('enhance.telia.carouselFade', function(event, holder, initialSlide) {
        initSelector = holder ? holder : initSelector;

        $(event.target).find(initSelector).addBack(initSelector)[pluginName]({
            initialSlide: initialSlide
        });
    });

    /**
     * @param {Event} event
     * @event destroy.telia.carouselFade
     */
    $(document).on('destroy.telia.carouselFade', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]('destroy');
    });
})(jQuery, window, document);

/**
 * Auto-init the plugin on DOM ready.
 * @fires enhance.telia.carouselFade
 */
$(function() {
    $(document).trigger('enhance.telia.carouselFade');
});

/**
 * Old init for compatibility.
 *
 * @fires enhance.telia.carouselFade
 * @deprecated Use enhance.telia.carouselFade event directly.
 * @returns {void}
 */
function initCarouselFade() {
    $(document).trigger('enhance.telia.carouselFade');
}
window.initCarouselFade = initCarouselFade;

;(function($, window, document) {
    var pluginName = 'teliaCarouselDefault';
    var defaults = {
        defaultEaseFn: 'cubic-bezier(0.645, 0.045, 0.355, 1.000)',
        prevArrow: '<button class="carousel__arrow carousel__arrow--left">' + telia.icon.render('#arrow-left') + '</button>',
        nextArrow: '<button class="carousel__arrow carousel__arrow--right">' + telia.icon.render('#arrow-right') + '</button>'
    };
    var initSelector = '.carousel--default';

    /**
     * Carousel Default.
     *
     * @class
     * @param {HTMLElement} element - The HTML element the CarouselDefault should be bound to.
     * @param {Object} options - An option map.
     */
    function CarouselDefault(element, options) {
        this.element = element;
        this.$element = $(this.element);
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;

        this.init();
    }

    /**
     * Initialize the plugin.
     *
     * @function init
     * @returns {void}
     */
    CarouselDefault.prototype.init = function() {
        if (!this.$element.hasClass('slick-initialized')) {
            this.bindEvents();
            this.initSlick();
        }
    };

    /**
     * Bind events.
     *
     * @function bindEvents
     * @returns {void}
     */
    CarouselDefault.prototype.bindEvents = function() {
        var that = this;

        $(document).on('refresh.telia.carousel', function(event, target) {
            if (that.$element.hasClass('slick-initialized') && target && $.contains(target, that.element)) {
                that.$element.slick('refresh');
            }
        });
    };

    /**
     * Initialize slick plugin.
     *
     * @function initSlick
     * @returns {void}
     */
    CarouselDefault.prototype.initSlick = function() {
        this.$element.slick({
            dots: true,
            arrows: true,
            autoplay: true,
            autoplaySpeed: 8000,
            draggable: false,
            cssEase: this.options.defaultEaseFn,
            useTransform: true,
            speed: 500,
            prevArrow: this.options.prevArrow,
            nextArrow: this.options.nextArrow,

            /**
             * Create custom paging elements.
             *
             * @returns {string} Paging element.
             */
            customPaging: function() {
                return '<button type="button" data-role="none" role="button" aria-required="false" tabindex="0" class="carousel__indicator carousel__indicator--dot"></button>';
            },
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        draggable: true,
                        speed: 500
                    }
                }
            ]
        });
    };

    /**
     * Initialize slick plugin.
     *
     * @function destroy
     * @returns {void}
     */
    CarouselDefault.prototype.destroy = function() {
        this.$element.slick('unslick');
        this.$element.removeData('plugin_' + pluginName);
    };

    /**
     * Create jQuery plugin.
     *
     * @param {Object} options - Extended options.
     * @listens telia.carouselDefault
     * @returns {Array<CarouselDefault>} Array of CarouselDefaults.
     */
    $.fn[pluginName] = function(options) {
        var args = Array.prototype.slice.call(arguments, 1);

        return this.each(function() {
            var instance = $.data(this, 'plugin_' + pluginName);

            if (!instance) {
                if (options === 'destroy') {
                    return;
                }

                $.data(this, 'plugin_' + pluginName, new CarouselDefault(this, options));
            } else if (typeof options === 'string') {
                instance[options].apply(instance, args);
            }
        });
    };

    /**
     * @param {Event} event
     * @event enhance.telia.carouselDefault
     */
    $(document).on('enhance.telia.carouselDefault', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]();
    });

    /**
     * @param {Event} event
     * @event destroy.telia.carouselDefault
     */
    $(document).on('destroy.telia.carouselDefault', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]('destroy');
    });
})(jQuery, window, document);

/**
 * Auto-init the plugin on DOM ready.
 * @fires enhance.telia.carouselDefault
 */
$(function() {
    $(document).trigger('enhance.telia.carouselDefault');
});

/**
 * Old init for compatibility.
 *
 * @fires enhance.telia.carouselDefault
 * @deprecated Use enhance.telia.carouselDefault event directly.
 * @returns {void}
 */
function initCarouselDefault() {
    $(document).trigger('enhance.telia.carouselDefault');
}
window.initCarouselDefault = initCarouselDefault;

;(function($, window, document) {
    var pluginName = 'teliaModal';
    var defaults = {};
    var initSelector = '.modal';

    /**
     * Modal.
     *
     * @class
     * @param {HTMLElement} element - The HTML element the Modal should be bound to.
     * @param {Object} options - An option map.
     */
    function Modal(element, options) {
        this.element = element;
        this.$element = $(this.element);
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.container = false;

        this.init();
    }

    /**
     * Initialize the plugin.
     *
     * @function init
     * @returns {void}
     */
    Modal.prototype.init = function() {};

    /**
     * Open modal.
     *
     * @function open
     * @param  {HTMLElement} trigger - Trigger element.
     * @returns {void}
     */
    Modal.prototype.open = function(trigger) {
        if ($('.modal-container--visible').length > 0) {
            $('.modal-container--visible').each(function() {
                $(this).find(initSelector)[pluginName]('close');
            });
        }

        var that = this;
        var e = $.Event('open.telia.modal');

        this.$element.trigger(e);

        if (!this.container.length) {
            this.container = $('<div class="modal-container"></div>');
            this.inner = $('<div class="modal-container__inner"></div>');

            this.container.on('click.telia.modal.close', function(event) {
                if (event.target === event.currentTarget) {
                    event.preventDefault();
                    that.close();
                }
            });

            // force re-rendering of icons to fix a very weird chrome bug
            this.$element.find('.icon').each(function() {
                $(this).html($(this).html());
            });

            this.inner.append(this.$element);
            this.container.append(this.inner);

            $('body').append(this.container);
        }

        if (this.$element.find('.modal__close').length == 0) {
            this.closeBtn = $('<button class="modal__close">' + telia.icon.render('#close-round') + '</button>');

            this.$element.prepend(this.closeBtn);

            this.closeBtn.on('click.telia.modal.close', function(event) {
                event.preventDefault();
                that.close();
            });
        }

        this.container.addClass('modal-container--visible');
        $(document).trigger('refresh.telia', [this.element, trigger]);

        window.disableScroll(true);
        $(document).on('keyup.telia.modal', this.keyboard.bind(this));
    };

    /**
     * Close modal on esc.
     *
     * @function keyboard
     * @param  {Event} event - Event.
     * @returns {void}
     */
    Modal.prototype.keyboard = function(event) {
        if (event.keyCode == 27) {
            this.close();
        }
    };

    /**
     * Close modal.
     *
     * @function close
     * @returns {void}
     */
    Modal.prototype.close = function() {
        $(document).trigger('reset.telia', this.$element[0]);
        this.container.removeClass('modal-container--visible');
        window.enableScroll();
        var e = $.Event('close.telia.modal');

        this.$element.trigger(e);
        $(document).off('keyup.telia.modal');
    };

    /**
     * Destroy the plugin instance.
     *
     * @function destroy
     * @returns {void}
     */
    Modal.prototype.destroy = function() {
        this.$element.removeData('plugin_' + pluginName);
    };

    /**
     * Create jQuery plugin.
     *
     * @param {Object} options - Extended options.
     * @listens telia.modal
     * @returns {Array<Modal>} Array of Modals.
     */
    $.fn[pluginName] = function(options) {
        var args = Array.prototype.slice.call(arguments, 1);

        return this.each(function() {
            var instance = $.data(this, 'plugin_' + pluginName);

            if (!instance) {
                if (options === 'destroy') {
                    return;
                }

                $.data(this, 'plugin_' + pluginName, new Modal(this, options));
            } else if (typeof options === 'string') {
                instance[options].apply(instance, args);
            }
        });
    };

    /**
     * @param {Event} event
     * @event enhance.telia.modal
     */
    $(document).on('enhance.telia.modal', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]();
    });

    /**
     * @param {Event} event
     * @event destroy.telia.modal
     */
    $(document).on('destroy.telia.modal', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]('destroy');
    });

    /**
     * @param  {Event} event
     * @event click.telia.modal.open
     */
    $(document).on('click.telia.modal.open', '.js-modal', function(event) {
        event.preventDefault();
        var $this = $(this);
        var trigger = $(event.currentTarget);
        var target = false;

        if ($this.attr('data-modal-target')) {
            target = $this.attr('data-modal-target');
        } else if ($this.attr('href')) {
            target = $this.attr('href');
        }

        if (target) {
            $(target)[pluginName]('open', trigger);
        }
    });
})(jQuery, window, document);

/**
 * Auto-init the plugin on DOM ready.
 * @fires enhance.telia.modal
 */
$(function() {
    $(document).trigger('enhance.telia.modal');
});

/**
 * Old init for compatibility.
 *
 * @fires enhance.telia.modal
 * @deprecated Use enhance.telia.modal event directly.
 * @returns {void}
 */
function initModal() {
    $(document).trigger('enhance.telia.modal');
}
window.initModal = initModal;

;(function($, window, document) {
    var pluginName = 'imagePanning';
    var defaults = {};
    var initSelector = '.js-image-panning';

    /**
     * ImagePanning.
     *
     * @class
     * @param {HTMLElement} element - The HTML element the ImagePanning should be bound to.
     * @param {Object} options - An option map.
     */
    function ImagePanning(element, options) {
        this.element = element;
        this.$element = $(this.element);
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.$wrapper = $('<div class="image-panning__image-wrapper" />');
        this.isPanned = false;

        this._init();
    }

    /**
     * Initialize plugin.
     *
     * @function _init
     * @private
     * @returns {void}
     */
    ImagePanning.prototype._init = function() {
        var that = this;

        this.$element.wrap(this.$wrapper);
        this.$wrapper = this.$element.parent();

        $(document).on('reset.telia.imagePanning', this._reset.bind(this));

        this.$wrapper.on('click.telia.imagePanning', function() {
            if (!that.isPanned) {
                that._pan();
                that.$element.removeAttr('style');
            } else {
                that._reset();
            }
        });
    };

    /**
     * Pan image.
     *
     * @function _pan
     * @private
     * @returns {void}
     */
    ImagePanning.prototype._pan = function() {
        var that = this;

        this.isPanned = true;

        that.$element.addClass('is-panning');

        this.$element.parent().on('mousemove touchmove MSPointerMove pointermove', function(e, p) {
            var $this = $(this);

            e.preventDefault();

            var contH = $this.height();
            var contW = $this.width();
            var isTouch = e.type.indexOf('touch') !== -1;
            var isPointer = e.type.indexOf('pointer') !== -1;
            var evt;

            if (isPointer) {
                evt = e.originalEvent;
            } else {
                evt = isTouch ? e.originalEvent.touches[0] || e.originalEvent.changedTouches[0] : e;
            }

            var coords = [
                !p ? evt.pageY - $this.offset().top : contH / 2,
                !p ? evt.pageX - $this.offset().left : contW / 2
            ];
            var dest = [
                Math.round((that.$element.outerHeight(true) - contH) * (coords[0] / contH)),
                Math.round((that.$element.outerWidth(true) - contW) * (coords[1] / contW))
            ];

            that.$element.css({
                transform: 'translate(0, 0)',
                top: -dest[0],
                left: -dest[1]
            });
        });
    };

    /**
    * Reset picture position, remove mouse events.
    *
    * @function _reset
    * @private
    * @returns {void}
    */
    ImagePanning.prototype._reset = function() {
        this.isPanned = false;
        this.$element.parent().off('mousemove touchmove MSPointerMove pointermove');
        this.$element.removeClass('is-panning');
        this.$element.removeAttr('style');
    };

    /**
     * Destroy plugin.
     *
     * @function destroy
     * @returns {void}
     */
    ImagePanning.prototype.destroy = function() {
        this.$element.removeAttr('style');
        this.$element.parent().off('mousemove touchmove MSPointerMove pointermove');
        this.$element.off('reset.telia.imagePanning');
        this.$element.off('click.telia.imagePanning');
        this.$element.removeClass('is-panning');

        this.$element.removeData('plugin_' + pluginName);
    };

    /**
     * Create jQuery plugin.
     *
     * @param {Object} options - Extended options.
     * @listens telia.imagePanning
     * @returns {Array<ImagePanning>} Array of ImagePannings.
     */
    $.fn[pluginName] = function(options) {
        var args = Array.prototype.slice.call(arguments, 1);

        return this.each(function() {
            var instance = $.data(this, 'plugin_' + pluginName);

            if (!instance) {
                if (options === 'destroy') {
                    return;
                }

                $.data(this, 'plugin_' + pluginName, new ImagePanning(this, options));
            } else if (typeof options === 'string') {
                instance[options].apply(instance, args);
            }
        });
    };

    /**
     * @param {Event} event
     * @event enhance.telia.imagePanning
     */
    $(document).on('enhance.telia.imagePanning', function(event) {
        $(event.target).find(initSelector)[pluginName]();
    });

    /**
     * @param {Event} event
     * @event destroy.telia.imagePanning
     */
    $(document).on('destroy.telia.imagePanning', function(event) {
        $(event.target).find(initSelector)[pluginName]('destroy');
    });
})(jQuery, window, document);

/**
 * Auto-init the plugin on DOM ready
 * @fires enhance.telia.imagePanning
 */
$(function() {
    $(document).trigger('enhance.telia.imagePanning');
});

;(function($, window, document) {
    var pluginName = 'teliaCarouselPreview';
    var defaults = {
        prevArrow: '<button class="carousel__arrow carousel__arrow--left">' + telia.icon.render('#arrow-left') + '</button>',
        nextArrow: '<button class="carousel__arrow carousel__arrow--right">' + telia.icon.render('#arrow-right') + '</button>'
    };
    var initSelector = '.carousel--preview';

    /**
     * Carousel Preview.
     *
     * @class
     * @param {HTMLElement} element - The HTML element the CarouselPreview should be bound to.
     * @param {Object} options - An option map.
     */
    function CarouselPreview(element, options) {
        this.element = element;
        this.$element = $(this.element);
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.$slides = this.$element.find('.carousel__items');
        this.$nav;
        this.isZoom = this.$element.hasClass('is-zoomable');

        this.init();
    }

    /**
     * Initialize the plugin.
     *
     * @function init
     * @returns {void}
     */
    CarouselPreview.prototype.init = function() {
        if (!this.$slides.hasClass('slick-initialized')) {
            this.$nav = this.makeNav();
            this.$element.prepend(this.$nav);
            this.initSlick(this.isZoom);
        }

        if (!this.$slides.hasClass('slick-initialized') || this.isZoom) {
            this.bindEvents();
        }
    };

    /**
     * Bind events.
     *
     * @function bindEvents
     * @returns {void}
     */
    CarouselPreview.prototype.bindEvents = function() {
        var that = this;

        $(document).on('refresh.telia.carousel', function(event, target, trigger) {
            if (that.$slides.hasClass('slick-initialized') && target && $.contains(target, that.$slides[0])) {
                that.$slides.slick('refresh');

                if (that.isZoom && trigger) {
                    that.$slides.slick('slickGoTo', trigger.index());
                }
            }
        });
    };

    /**
     * Initialize slick plugin.
     *
     * @function initSlick
     * @param {boolean} isZoom - Has zoom functionality.
     * @returns {void}
     */
    CarouselPreview.prototype.initSlick = function(isZoom) {
        var that = this;

        if (isZoom) {
            this.$slides.slick({
                fade: true,
                autoplay: false,
                dots: false,
                arrows: false,
                draggable: false,
                swipe: false,
                touchMove: false
            });
        } else {
            this.$slides.slick({
                fade: true,
                mobileFirst: true,
                autoplay: false,
                dots: true,
                arrows: true,

                /**
                 * Create custom paging elements.
                 *
                 * @returns {string} Paging element.
                 */
                customPaging: function() {
                    return '<button type="button" data-role="none" role="button" aria-required="false" tabindex="0" class="carousel__indicator carousel__indicator--dot"></button>';
                },
                prevArrow: this.options.prevArrow,
                nextArrow: this.options.nextArrow,
                responsive: [
                    {
                        breakpoint: 599,
                        settings: {
                            arrows: false,
                            dots: false
                        }
                    }
                ]
            });
        }

        $(this.$nav).find('[data-show-slide]').on('click', function() {
            var i = $(this).data('show-slide');
            // skipping animation if second parameter is set to true

            that.$slides.slick('slickGoTo', i, true);
            $(this).addClass('is-current').siblings().removeClass('is-current');
        });
    };

    /**
     * Make navigation with pictures.
     *
     * @function makeNav
     * @returns {HTMLElement} Container of navigation elements.
     */
    CarouselPreview.prototype.makeNav = function() {
        var $navContainer = $('<div class="carousel__nav">');
        var $images = this.$slides.children();

        if ($images.length > 6) {
            $navContainer.addClass('carousel__nav--double');
        }

        $.each($images, function(i, val) {
            var $item = $('<div>', {
                class: 'carousel__nav-item',
                'data-show-slide': i
            });

            $(val).find('img').clone().appendTo($item);

            if (i == 0) {
                $item.addClass('is-current');
            }

            var $imagePanningImage = $item.find('.js-image-panning');

            if ($imagePanningImage.length !== 0) {
                $imagePanningImage.removeClass('image-panning__image js-image-panning');
            }

            $navContainer.append($item);
        });

        return $navContainer;
    };

    /**
     * Initialize slick plugin.
     *
     * @function destroy
     * @returns {void}
     */
    CarouselPreview.prototype.destroy = function() {
        this.$element.removeData('plugin_' + pluginName);
    };

    /**
     * Create jQuery plugin.
     *
     * @param {Object} options - Extended options.
     * @listens telia.carouselPreview
     * @returns {Array<CarouselPreview>} Array of CarouselPreviews.
     */
    $.fn[pluginName] = function(options) {
        var args = Array.prototype.slice.call(arguments, 1);

        return this.each(function() {
            var instance = $.data(this, 'plugin_' + pluginName);

            if (!instance) {
                if (options === 'destroy') {
                    return;
                }

                $.data(this, 'plugin_' + pluginName, new CarouselPreview(this, options));
            } else if (typeof options === 'string') {
                instance[options].apply(instance, args);
            }
        });
    };

    /**
     * @param {Event} event
     * @event enhance.telia.carouselPreview
     */
    $(document).on('enhance.telia.carouselPreview', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]();
    });

    /**
     * @param {Event} event
     * @event destroy.telia.carouselPreview
     */
    $(document).on('destroy.telia.carouselPreview', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]('destroy');
    });
})(jQuery, window, document);

/**
 * Auto-init the plugin on DOM ready.
 * @fires enhance.telia.carouselPreview
 */
$(function() {
    $(document).trigger('enhance.telia.carouselPreview');
});

/**
 * Old init for compatibility.
 *
 * @fires enhance.telia.carouselPreview
 * @deprecated Use enhance.telia.carouselPreview event directly.
 * @returns {void}
 */
function initCarouselPreview() {
    $(document).trigger('enhance.telia.carouselPreview');
}
window.initCarouselPreview = initCarouselPreview;

;(function($, window, document) {
    var pluginName = 'teliaCarouselCircular';
    var defaults = {
        current: 1,
        circleClass: '.carousel--circular__circle',
        itemClass: '.carousel--circular__item',
        itemActiveClass: '.carousel--circular__item--current',
        itemFadeoutClass: '.carousel--circular__item--fadeout',
        anchorClass: '.carousel--circular__anchor',
        anchorActiveClass: '.carousel--circular__anchor--current',
        navClass: '.carousel--circular__nav',
        navItemClass: '.carousel--circular__nav-item',
        navActiveItemClass: '.carousel--circular__nav-item--current',
        navLinkClass: '.carousel--circular__nav-link',
        buttonClass: '.carousel__arrow',
        nextClass: '.carousel__arrow--right',
        prevClass: '.carousel__arrow--left'
    };
    var initSelector = '.carousel--circular';

    /**
     * CarouselCircular.
     *
     * @class
     * @param {HTMLElement} element - The HTML element the CarouselCircular should be bound to.
     * @param {Object} options - An option map.
     */
    function CarouselCircular(element, options) {
        this.element = element;
        this.$element = $(this.element);
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.$circle = $('<div class="' + this.options.circleClass.substr(1) + '"></div>');
        this.$list = $('<ul class="' + this.options.navClass.substr(1) + '"></ul>');
        this.$nextButton = $('<button class="' + this.options.buttonClass.substr(1) + ' ' + this.options.nextClass.substr(1) + '">' + telia.icon.render('#arrow-right') + '</button>');
        this.$prevButton = $('<button class="' + this.options.buttonClass.substr(1) + ' ' + this.options.prevClass.substr(1) + '">' + telia.icon.render('#arrow-left') + '</button>');
        this.rotateDay = 0;
        this.rotateNight = 0;
        this.switchFadeDelay = 150;
        this.current = this.options.current;

        this._init();
    }

    /**
     * Initialize the plugin.
     *
     * @function init
     * @private
     * @returns {void}
     */
    CarouselCircular.prototype._init = function() {
        this._setHTML();
        this._bindEvents();
        this.setCurrent(this.current);
    };

    /**
     * Set up HTML for carousel.
     *
     * @function _setHTML
     * @private
     * @returns {void}
     */
    CarouselCircular.prototype._setHTML = function() {
        this.$element.find(this.options.itemClass).each(this._generateNav.bind(this));
        this.$element.prepend(this.$nextButton);
        this.$element.prepend(this.$prevButton);
        this.$element.prepend(this.$circle);
        this.$element.prepend(this.$list);
    };

    /**
     * Remove HTML that has been set by plugin.
     *
     * @function _unsetHTML
     * @private
     * @returns {void}
     */
    CarouselCircular.prototype._unsetHTML = function() {
        this.$list.remove();
        this.$circle.remove();
        this.$nextButton.remove();
        this.$prevButton.remove();
    };

    /**
     * Generate nav and anchor for an item.
     *
     * @function _generateNav
     * @private
     * @param   {integer} index - Index.
     * @param   {HTMLElement} target - Target element.
     * @returns {void}
     */
    CarouselCircular.prototype._generateNav = function(index, target) {
        index += 1;
        $(target).attr('data-id', index);
        $(target).find(this.options.anchorClass).clone().attr('data-id', index).appendTo(this.$circle);

        $('<li class="' + this.options.navItemClass.substr(1) + '"><a href="#" class="' + this.options.navLinkClass.substr(1) + '" data-id="' + index + '">' + $(target).data('title') + '</a></li>').appendTo(this.$list);
    };

    /**
     * Bind events.
     *
     * @function bindEvents
     * @private
     * @returns {void}
     */
    CarouselCircular.prototype._bindEvents = function() {
        this.$circle.on('click.telia.carouselCircular', this.options.anchorClass, this._anchorClick.bind(this));
        this.$element.on('click.telia.carouselCircular', this.options.navLinkClass, this._navClick.bind(this));
        this.$element.on('click.telia.carouselCircular', this.options.nextClass, this.next.bind(this));
        this.$element.on('click.telia.carouselCircular', this.options.prevClass, this.prev.bind(this));
        $(window).on('resize.telia.carouselCircular', this._resizeHandler.bind(this));
    };

    /**
     * Set current element on anchor click.
     *
     * @function _anchorClick
     * @private
     * @param   {Event} event - Event.
     * @returns {void}
     */
    CarouselCircular.prototype._anchorClick = function(event) {
        var id = $(event.currentTarget).data('id');

        event.preventDefault();
        this.setCurrent(id);
    };

    /**
     * Set current element on nav click.
     *
     * @function _navClick
     * @private
     * @param   {Event} event - Event.
     * @returns {void}
     */
    CarouselCircular.prototype._navClick = function(event) {
        var trigger = event.currentTarget;
        var id = $(trigger).data('id');

        event.preventDefault();
        $(this.options.navClass).find(this.options.navActiveItemClass).removeClass(this.options.navActiveItemClass.substr(1));
        $(trigger).parent().addClass(this.options.navActiveItemClass.substr(1));
        this.setCurrent(id);
    };

    /**
     * Set current element again on resize.
     *
     * @function _resizeHandler
     * @private
     * @returns {void}
     */
    CarouselCircular.prototype._resizeHandler = function() {
        this.setCurrent(this.getCurrent());
    };

    /**
     * Get ID of current item.
     *
     * @function getCurrent
     * @returns {integer} Current item ID.
     */
    CarouselCircular.prototype.getCurrent = function() {
        return this.current ? Number(this.current) : this._getMin();
    };

    /**
     * Set current item to ID.
     *
     * @function setCurrent
     * @param   {integer} id - Current element id.
     * @returns {void}
     */
    CarouselCircular.prototype.setCurrent = function(id) {
        var prev = this.getCurrent();

        this.current = id;
        this.$element.find(this.options.navLinkClass + '[data-id="' + id + '"]').parent().addClass(this.options.navActiveItemClass.substr(1)).siblings().removeClass(this.options.navActiveItemClass.substr(1));
        this.$element.find(this.options.anchorClass + '[data-id="' + id + '"]').addClass(this.options.anchorActiveClass.substr(1)).siblings().removeClass(this.options.anchorActiveClass.substr(1));

        var prevItem = this.$element.find(this.options.itemActiveClass);
        var nextItem = this.$element.find(this.options.itemClass + '[data-id="' + id + '"]');

        if (id != prev) {
            prevItem.removeClass(this.options.itemActiveClass.substr(1));
            setTimeout(fadeOutComplete.bind(this), this.switchFadeDelay);
        } else {
            prevItem.removeClass(this.options.itemActiveClass.substr(1));
            nextItem.addClass(this.options.itemActiveClass.substr(1));
        }

        this._setCarouselTransforms(prev);
        this.$circle.find(this.options.anchorClass).each(this._setAnchorTransforms.bind(this, prev));

        /**
         * Add class if fade out is complete.
         *
         * @returns {void}
         */
        function fadeOutComplete() {
            nextItem.addClass(this.options.itemActiveClass.substr(1));
            nextItem.parent().siblings().children().removeClass(this.options.itemActiveClass.substr(1));
        }
    };

    /**
     * Go to next element.
     *
     * @function next
     * @param   {Event} event - Event.
     * @returns {void}
     */
    CarouselCircular.prototype.next = function(event) {
        event.preventDefault();
        this.setCurrent(this._getNext());
    };

    /**
     * Go to previous element.
     *
     * @function prev
     * @param   {Event} event - Event.
     * @returns {void}
     */
    CarouselCircular.prototype.prev = function(event) {
        event.preventDefault();
        this.setCurrent(this._getPrevious());
    };

    /**
     * Get maximum element count.
     *
     * @function _getMax
     * @private
     * @returns {void}
     */
    CarouselCircular.prototype._getMax = function() {
        return Number(this.$circle.find(this.options.anchorClass).length);
    };

    /**
     * Get minimum element count.
     *
     * @function _getMin
     * @private
     * @returns {void}
     */
    CarouselCircular.prototype._getMin = function() {
        return 1;
    };

    /**
     * Get previous element ID.
     *
     * @function _getPrevious
     * @private
     * @returns {integer} Previous element ID.
     */
    CarouselCircular.prototype._getPrevious = function() {
        var current = this.getCurrent();

        if (current == this._getMin()) {
            current = this._getMax();
        } else {
            current--;
        }

        return current;
    };

    /**
     * Get next element ID.
     *
     * @function _getNext
     * @private
     * @returns {integer} Next element ID.
     */
    CarouselCircular.prototype._getNext = function() {
        var current = this.getCurrent();

        if (current == this._getMax()) {
            current = this._getMin();
        } else {
            current++;
        }

        return current;
    };

    /**
     * Get the distance between two elements.
     *
     * @function _getDistance
     * @private
     * @param   {integer} previous - Previous element width.
     * @param   {integer} current - Current element width.
     * @returns {integer} Distance between previous and current.
     */
    CarouselCircular.prototype._getDistance = function(previous, current) {
        var distance = previous - current;

        if (Math.abs(distance) > this._getMax() / 2) {
            distance = this._getMax() - Number(current) + Number(previous);
        }
        if (Math.abs(distance) > this._getMax() / 2) {
            distance = Number(previous) - Number(current) - this._getMax();
        }

        return distance;
    };

    /**
     * Get circle width.
     *
     * @function _getCircleWidth
     * @private
     * @returns {integer} Circle width.
     */
    CarouselCircular.prototype._getCircleWidth = function() {
        return parseInt(getComputedStyle(this.$circle[0], null).getPropertyValue('width'));
    };

    /**
     * Set carousel rotation.
     *
     * @function _setCarouselTransforms
     * @private
     * @param   {integer} previous - Previous element width.
     * @returns {void}
     */
    CarouselCircular.prototype._setCarouselTransforms = function(previous) {
        var angle = 360 / this._getMax();
        var distance = this._getDistance(previous, this.getCurrent());
        var beforeRotate = this.rotateNight;

        this.rotateDay = beforeRotate + (distance * (angle / 2));
        this.rotateNight = this.rotateDay;

        this.$circle.css({
            '-webkit-transform': 'rotate(' + this.rotateDay + 'deg)',
            '-moz-transform': 'rotate(' + this.rotateDay + 'deg)',
            '-ms-transform': 'rotate(' + this.rotateDay + 'deg)',
            '-o-transform': 'rotate(' + this.rotateDay + 'deg)',
            transform: 'rotate(' + this.rotateDay + 'deg)'
        });
    };

    /**
     * Set transforms for one anchor.
     *
     * @function _setAnchorTransforms
     * @private
     * @param   {integer} previous - Previous element width.
     * @param   {integer} index - Index.
     * @param   {HTMLElement} target - Target element.
     * @returns {void}
     */
    CarouselCircular.prototype._setAnchorTransforms = function(previous, index, target) {
        var self = $(target);
        var current = this.getCurrent();
        var translate = this._getCircleWidth() / 2;
        var angle = 360 / this._getMax();
        var rotateCont = this.rotateDay;
        var distance = this._getDistance(previous, current);
        var d = Math.abs(distance);
        var sign = Math.sign(distance);
        var id = Number(self.data('id'));
        var rotate = (id - 1) * angle;
        var match = current - (Math.floor(this._getMax() / 2) * sign);
        var m;
        var array = [];
        var j;

        if (!self.data('rotate')) {
            if (id > 1 + this._getMax() / 2) {
                rotate = (rotate / 2) + 180;
            } else {
                rotate = (id - 1) * (angle / 2);
            }
            self.data('rotate', rotate);
        }
        rotate = Number(self.data('rotate'));
        if (distance < 0) {
            if (match > this._getMax()) {
                match -= this._getMax();
            }

            for (j = 1; j <= d; j++) {
                m = match;
                array.push(m);
                match--;
                if (match < 1) {
                    match = this._getMax();
                }
            }
        } else {
            if (match < this._getMin()) {
                match += this._getMax();
            }

            for (j = 1; j <= d; j++) {
                m = match;
                array.push(m);
                match++;
                if (match > this._getMax()) {
                    match = this._getMin();
                }
            }
        }

        if ($.inArray(id, array) >= 0) {
            rotate += 180 * sign;
        }

        self.data('rotate', rotate);
        var rotateBack = 0 - rotate - rotateCont;

        self.css({
            '-webkit-transform': 'rotate(' + rotate + 'deg) translateY(-' + translate + 'px) rotate(' + rotateBack + 'deg)',
            '-moz-transform': 'rotate(' + rotate + 'deg) translateY(-' + translate + 'px) rotate(' + rotateBack + 'deg)',
            '-ms-transform': 'rotate(' + rotate + 'deg) translateY(-' + translate + 'px) rotate(' + rotateBack + 'deg)',
            '-o-transform': 'rotate(' + rotate + 'deg) translateY(-' + translate + 'px) rotate(' + rotateBack + 'deg)',
            transform: 'rotate(' + rotate + 'deg) translateY(-' + translate + 'px) rotate(' + rotateBack + 'deg)'
        });
    };

    /**
     * Initialize slick plugin.
     *
     * @function destroy
     * @returns {void}
     */
    CarouselCircular.prototype.destroy = function() {
        this._unsetHTML();
        this.$circle.off('click.telia.carouselCircular');
        this.$element.off('click.telia.carouselCircular');
        this.$element.off('click.telia.carouselCircular');
        this.$element.off('click.telia.carouselCircular');
        $(window).off('resize.telia.carouselCircular');
        this.$element.removeData('plugin_' + pluginName);
    };

    /**
     * Create jQuery plugin.
     *
     * @param {Object} options - Extended options.
     * @listens telia.carouselCircular
     * @returns {Array<CarouselCircular>} Array of CarouselCirculars.
     */
    $.fn[pluginName] = function(options) {
        var args = Array.prototype.slice.call(arguments, 1);

        return this.each(function() {
            var instance = $.data(this, 'plugin_' + pluginName);

            if (!instance) {
                if (options === 'destroy') {
                    return;
                }

                $.data(this, 'plugin_' + pluginName, new CarouselCircular(this, options));
            } else if (typeof options === 'string') {
                instance[options].apply(instance, args);
            }
        });
    };

    /**
     * @param {Event} event
     * @event enhance.telia.carouselCircular
     */
    $(document).on('enhance.telia.carouselCircular', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]();
    });

    /**
     * @param {Event} event
     * @event destroy.telia.carouselCircular
     */
    $(document).on('destroy.telia.carouselCircular', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]('destroy');
    });
})(jQuery, window, document);

/**
 * Auto-init the plugin on DOM ready.
 * @fires enhance.telia.carouselCircular
 */
$(function() {
    $(document).trigger('enhance.telia.carouselCircular');
});

/**
 * Old init for compatibility.
 *
 * @fires enhance.telia.carouselCircular
 * @deprecated Use enhance.telia.carouselCircular event directly.
 * @returns {void}
 */
function initCarouselCircular() {
    $(document).trigger('enhance.telia.carouselCircular');
}
window.initCarouselCircular = initCarouselCircular;

;(function($, window, document) {
    var pluginName = 'teliaCarouselProduct';
    var defaults = {
        defaultEaseFn: 'cubic-bezier(0.645, 0.045, 0.355, 1.000)',
        prevArrow: '<button class="carousel__arrow carousel__arrow--left">' + telia.icon.render('#arrow-left-light') + '</button>',
        nextArrow: '<button class="carousel__arrow carousel__arrow--right">' + telia.icon.render('#arrow-right-light') + '</button>'
    };
    var initSelector = '.carousel--product';

    /**
     * Carousel Product.
     *
     * @class
     * @param {HTMLElement} element - The HTML element the CarouselProduct should be bound to.
     * @param {Object} options - An option map.
     */
    function CarouselProduct(element, options) {
        this.element = element;
        this.$element = $(this.element);
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.$slider = this.$element.find('.carousel__slider');

        this.init();
    }

    /**
     * Initialize the plugin.
     *
     * @function init
     * @returns {void}
     */
    CarouselProduct.prototype.init = function() {
        if (this.$slider.find('.carousel__item').length > 1) {
            if (!this.$slider.hasClass('slick-initialized')) {
                this.bindEvents();
                this.setPriority();
                this.initSlick();
            }
        }
    };

    /**
     * Bind events.
     *
     * @function bindEvents
     * @returns {void}
     */
    CarouselProduct.prototype.bindEvents = function() {
        var that = this;

        $(document).on('refresh.telia.carousel', function(event, target) {
            if (that.$slider.hasClass('slick-initialized') && target && $.contains(target, that.$slider[0])) {
                that.$slider.slick('refresh');
            }
        });

        that.$slider.on('init', function(event, slick) {
            $(this).find('img').addClass('lazypreload');

            that.startAnimatedSlide(event, slick, 0);
        });

        this.$slider.on('afterChange', this.startAnimatedSlide);
    };

    /**
     * Initialize slick plugin.
     *
     * @function initSlick
     * @returns {void}
     */
    CarouselProduct.prototype.initSlick = function() {
        this.$slider.slick({
            dots: true,
            waitForAnimate: false,
            appendDots: $('.carousel__static'),
            dotsClass: 'slick-dots slick-dots--product-carousel',
            arrows: true,
            autoplay: false,
            autoplaySpeed: 8000,
            draggable: false,
            cssEase: this.options.defaultEaseFn,
            useTransform: true,
            speed: 1000,
            pauseOnHover: false,
            prevArrow: this.options.prevArrow,
            nextArrow: this.options.nextArrow,

            /**
             * Create custom paging elements.
             *
             * @param   {Slick} slider - Slick slider instance.
             * @param   {number} i - Slide index.
             * @returns {string} Paging element.
             */
            customPaging: function(slider, i) {
                var animationDuration = slider.options.autoplaySpeed + slider.options.speed;

                return '<button data-role="none" role="button" aria-required="false" tabindex="0" class="carousel__indicator carousel__indicator--product"><figure class="slider__paging-image">' + slider.$slides.eq(i).find('img').prop('outerHTML') + '</figure><span class="carousel__progress" style="animation-duration: ' + animationDuration + 'ms"></span></button>';
            },
            responsive: [
                {
                    breakpoint: 1023,
                    settings: {
                        dots: false
                    }
                }
            ]
        });
    };

    /**
     * Set priority to slides.
     *
     * @function setPriority
     * @returns {void}
     */
    CarouselProduct.prototype.setPriority = function() {
        var $initialEl = this.$slider.find('.carousel__item')[0];
        // if one or more items has priority set

        if (this.$slider.find('.carousel__item[data-priority-order]').length > 0) {
            // carousel items
            if (this.$slider.find('.carousel__item[data-priority-order="0"]').length > 0) {
                $initialEl = this.$slider.find('.carousel__item[data-priority-order="0"]')[0];
            } else {
                var carouselItems = this.$slider.find('.carousel__item');
                // array with each items priority order number
                var weightArray = [];
                // loop of items to get array of priority order numbers

                carouselItems.each(function() {
                    // declaration of
                    var orderNumber = 0;
                    // get elements' priority number or else it will be 3
                    var thisPriorityNumber = $(this).data('priorityOrder') || 3;
                    // switch to turn priority numbers around

                    switch (thisPriorityNumber) {
                        case 1:
                            orderNumber = 3;
                            break;
                        case 3:
                            orderNumber = 1;
                            break;
                        default:
                            orderNumber = thisPriorityNumber;
                    }
                    // push priority order number to array
                    weightArray.push(orderNumber);
                });
                // sum of all elements in array
                var totalWeight = eval(weightArray.join('+'));
                // array of final items
                var weightedArray = [];
                // counter
                var current = 0;

                // loop through carousel items
                while (current < carouselItems.length) {
                    // for every item put it into a new array the number of times its weight
                    for (var i = 0; i < weightArray[current]; i++) {
                        weightedArray[weightedArray.length] = carouselItems[current];
                    }
                    current++;
                }

                // take random number from total sum of all items in priority numbers
                var randomItemKey = Math.floor(Math.random() * totalWeight);
                // random number is the key to one of the elements in weighted array

                $initialEl = weightedArray[randomItemKey];
            }
        }

        this.$slider.prepend($initialEl);
    };

    /**
     * Start animated slide.
     *
     * @function startAnimatedSlide
     * @param  {Object} event - Event trigger on init or after change.
     * @param  {HTMLElement} slick - Slick instance.
     * @param  {number} currentSlide - Current slide index.
     * @returns {void}
     */
    CarouselProduct.prototype.startAnimatedSlide = function(event, slick, currentSlide) {
        var self = slick.$slider;
        var nextSlide = self.find('[data-slick-index="' + currentSlide + '"]');

        if (nextSlide.hasClass('carousel__item--animated')) {
            nextSlide.addClass('slick-animated');
        }
        self.find('.carousel__item--animated').not(nextSlide).removeClass('slick-animated');

        /*
        sometimes there's a video as a background in the hero.
        if there's a html5 video in the next slide, start playing it.
        browsers tend to pause/stop video if it is modified via JS, as slick does here.
        */
        nextSlide.find('video').each(function() {
            this.play();
        });
    };

    /**
     * Initialize slick plugin.
     *
     * @function destroy
     * @returns {void}
     */
    CarouselProduct.prototype.destroy = function() {
        this.$element.removeData('plugin_' + pluginName);
    };

    /**
     * Create jQuery plugin.
     *
     * @param {Object} options - Extended options.
     * @listens telia.carouselProduct
     * @returns {Array<CarouselProduct>} Array of CarouselProducts.
     */
    $.fn[pluginName] = function(options) {
        var args = Array.prototype.slice.call(arguments, 1);

        return this.each(function() {
            var instance = $.data(this, 'plugin_' + pluginName);

            if (!instance) {
                if (options === 'destroy') {
                    return;
                }

                $.data(this, 'plugin_' + pluginName, new CarouselProduct(this, options));
            } else if (typeof options === 'string') {
                instance[options].apply(instance, args);
            }
        });
    };

    /**
     * @param {Event} event
     * @event enhance.telia.carouselProduct
     */
    $(document).on('enhance.telia.carouselProduct', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]();
    });

    /**
     * @param {Event} event
     * @event destroy.telia.carouselProduct
     */
    $(document).on('destroy.telia.carouselProduct', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]('destroy');
    });
})(jQuery, window, document);

/**
 * Auto-init the plugin on DOM ready.
 * @fires enhance.telia.carouselProduct
 */
$(function() {
    $(document).trigger('enhance.telia.carouselProduct');
});

/**
 * Old init for compatibility.
 *
 * @fires enhance.telia.carouselProduct
 * @deprecated Use enhance.telia.carouselProduct event directly.
 * @returns {void}
 */
function initCarouselProduct() {
    $(document).trigger('enhance.telia.carouselProduct');
}
window.initCarouselProduct = initCarouselProduct;

;(function($, window, document) {
    var pluginName = 'teliaTooltip';
    var initSelector = '[data-toggle="tooltip"]';

    /**
     * Tooltip.
     *
     * @class
     * @param {HTMLElement} element - The HTML element the Tooltip should be bound to.
     * @param {Object} options - An option map.
     */
    function Tooltip(element, options) {
        this.type       = null;
        this.options    = null;
        this.enabled    = null;
        this.timeout    = null;
        this.hoverState = null;
        this.$element   = null;
        this.inState    = null;

        this.init('tooltip', element, options);
    };

    Tooltip.TRANSITION_DURATION = 150;

    Tooltip.DEFAULTS = {
        animation: true,
        placement: 'top',
        selector: false,
        template: '<div class="tooltip" role="tooltip"><div class="tooltip__arrow"></div><h4 class="tooltip__title"></h4><div class="tooltip__content"></div></div>',
        trigger: 'click',
        title: '',
        titleSize: 'h4',
        delay: 0,
        html: false,
        content: '',
        container: 'body',
        viewport: {
            selector: 'body',
            padding: 0
        },
        variant: '',
        close: false,
        onload: false,
        class: ''
    };

    /**
     * Initialize the plugin.
     *
     * @function init
     * @param {string} type - Type of tooltip.
     * @param {HTMLelement} element - Tooltip element.
     * @param {Object} options - Options.
     * @returns {void}
     */
    Tooltip.prototype.init = function(type, element, options) {
        var that = this;

        this.enabled   = true;
        this.type      = type;
        this.$element  = $(element);
        this._options  = this.$element.data();
        this.options   = this._getOptions(options);
        this.$viewport = this.options.viewport && $($.isFunction(this.options.viewport) ? this.options.viewport.call(this, this.$element) : (this.options.viewport.selector || this.options.viewport));
        this.inState   = {
            click: false,
            hover: false,
            focus: false
        };
        this.$close  = $('<a href="#" class="tooltip__close">' + telia.icon.render('#close-round') + '</a>');
        this.intent = window.whatInput.ask('loose') && window.whatInput.ask('loose') == 'initial' ? 'touch' : window.whatInput.ask('loose');

        if (this.$element[0] instanceof document.constructor && !this.options.selector) {
            throw new Error('`selector` option must be specified when initializing ' + this.type + ' on the window.document object!');
        }

        if (this._useDynamicTrigger()) {
            window.whatInput.registerOnChange(that._bindDynamicEnterEvents.bind(this), 'intent');

            this._bindDynamicEnterEvents(this.intent);
        } else {
            var triggers = this.options.trigger.split(' ');

            for (var i = triggers.length; i--;) {
                var trigger = triggers[i];

                if (trigger == 'click') {
                    this.$element.on('click.' + this.type, this.options.selector, $.proxy(this.toggle, this));
                    this.options.close = true;
                } else if (trigger != 'manual') {
                    var eventIn  = trigger == 'hover' ? 'mouseenter' : 'focusin';
                    var eventOut = trigger == 'hover' ? 'mouseleave' : 'focusout';

                    this.$element.on(eventIn  + '.' + this.type, this.options.selector, $.proxy(this.enter, this));
                    this.$element.on(eventOut + '.' + this.type, this.options.selector, $.proxy(this.leave, this));
                }
            }

            if (this.options.selector) {
                this._options = $.extend({}, this.options, {
                    trigger: 'manual',
                    selector: ''
                });
            } else {
                this._fixTitle();
            }
        }
    };

    /**
     * Use dynamic trigger if the instance does not have a custom trigger.
     * Also use dynamic trigger if custom trigger is hover, as hover is not available on all devices.
     *
     * @function _useDynamicTrigger
     * @private
     * @returns {boolean} If should use dynamic trigger.
     */
    Tooltip.prototype._useDynamicTrigger = function() {
        return this._options.trigger != 'click' && (this.options.trigger == Tooltip.DEFAULTS.trigger || this.options.trigger == 'hover');
    };

    /**
     * Bind enter events based on dynamic input type.
     *
     * @function _bindDynamicEnterEvents
     * @private
     * @param {string} type - New intent type.
     * @returns {void}
     */
    Tooltip.prototype._bindDynamicEnterEvents = function(type) {
        this.intent = type;
        this.options.close = this._options.close || false;
        this.$element.off('.' + this.type);

        if (this.intent == 'mouse') {
            //hover
            this.$element.on('mouseenter.' + this.type, this.options.selector, $.proxy(this.enter, this));
            this.$element.on('mouseleave.' + this.type, this.options.selector, $.proxy(this.leave, this));
        } else if (this.intent == 'keyboard') {
            // focus
            this.$element.on('focusin.' + this.type, this.options.selector, $.proxy(this.enter, this));
            this.$element.on('focusout.' + this.type, this.options.selector, $.proxy(this.leave, this));
        } else {
            // click
            this.options.close = true;
            this.$element.on('click.' + this.type, this.options.selector, $.proxy(this.toggle, this));
        }
    };

    /**
     * Get default options.
     *
     * @function getDefaults
     * @private
     * @returns {void}
     */
    Tooltip.prototype._getDefaults = function() {
        return Tooltip.DEFAULTS;
    };

    /**
     * Get tooltip options.
     *
     * @function getOptions
     * @param {Object} options - Options.
     * @private
     * @returns {void}
     */
    Tooltip.prototype._getOptions = function(options) {
        options = $.extend({}, this._getDefaults(), this.$element.data(), options);

        if (options.delay && typeof options.delay == 'number') {
            options.delay = {
                show: options.delay,
                hide: options.delay
            };
        }

        return options;
    };

    /**
     * Get delegated options.
     *
     * @function getDelegateOptions
     * @private
     * @returns {void}
     */
    Tooltip.prototype._getDelegateOptions = function() {
        var options  = {};
        var defaults = this._getDefaults();

        this._options && $.each(this._options, function(key, value) {
            if (defaults[key] != value) options[key] = value;
        });

        return options;
    };

    /**
     * Enter tooltip trigger element.
     *
     * @function enter
     * @param {Object} obj - Event.
     * @returns {void}
     */
    Tooltip.prototype.enter = function(obj) {
        var self = obj instanceof this.constructor ? obj : $(obj.currentTarget).data('plugin_' + pluginName);

        if (!self) {
            self = new this.constructor(obj.currentTarget, this._getDelegateOptions());
            $(obj.currentTarget).data('plugin_' + pluginName, self);
        }

        if (obj instanceof $.Event) {
            self.inState[obj.type == 'focusin' ? 'focus' : 'hover'] = true;
        }

        if (self._tip().hasClass('is-visible') || self.hoverState == 'is-visible') {
            self.hoverState = 'is-visible';

            return;
        }

        clearTimeout(self.timeout);

        self.hoverState = 'is-visible';

        if (!self.options.delay || !self.options.delay.show) return self.show();

        self.timeout = setTimeout(function() {
            if (self.hoverState == 'is-visible') self.show();
        }, self.options.delay.show);
    };

    /**
     * Check if tooltip state is true.
     *
     * @function isInStateTrue
     * @private
     * @returns {void}
     */
    Tooltip.prototype._isInStateTrue = function() {
        for (var key in this.inState) {
            if (this.inState[key]) return true;
        }

        return false;
    };

    /**
     * Leave tooltip trigger element.
     *
     * @function leave
     * @param {Object} obj - Event.
     * @returns {void}
     */
    Tooltip.prototype.leave = function(obj) {
        var self = obj instanceof this.constructor ? obj : $(obj.currentTarget).data('plugin_' + pluginName);

        if (!self) {
            self = new this.constructor(obj.currentTarget, this._getDelegateOptions());
            $(obj.currentTarget).data('plugin_' + pluginName, self);
        }

        if (obj instanceof $.Event) {
            self.inState[obj.type == 'focusout' ? 'focus' : 'hover'] = false;
        }

        if (self._isInStateTrue()) return;

        clearTimeout(self.timeout);

        self.hoverState = 'out';

        if (!self.options.delay || !self.options.delay.hide) return self.hide();

        self.timeout = setTimeout(function() {
            if (self.hoverState == 'out') self.hide();
        }, self.options.delay.hide);
    };

    /**
     * Show tooltip.
     *
     * @function show
     * @returns {void}
     */
    Tooltip.prototype.show = function() {
        /**
         * Complete tooltip showing.
         *
         * @returns {void}
         */
        function complete() {
            var prevHoverState = that.hoverState;

            that.$element.trigger('shown.telia.' + that.type);
            that.hoverState = null;

            if (prevHoverState == 'out') that.leave(that);

            $(document).on('keyup.telia.tooltip.' + that.$tip[0].id, that._keyboard.bind(that));
        };

        var e = $.Event('show.telia.' + this.type);

        if (this._hasContent() && this.enabled) {
            this.$element.trigger(e);

            var inDom = $.contains(this.$element[0].ownerDocument.documentElement, this.$element[0]);

            if (e.isDefaultPrevented() || !inDom) return;
            var that = this;

            var $tip = this._tip();

            var tipId = this._getUID(this.type);

            this._setContent();
            $tip.attr('id', tipId);
            this.$element.attr('aria-describedby', tipId);

            if (this.options.close) {
                if (this.options.title == '') {
                    $tip.addClass('tooltip--with-close');
                }
                $tip.addClass('tooltip--with-close-and-title');
                $tip.append(this.$close);

                this.$close.on('click', function(event) {
                    event.preventDefault();
                    that.hide();
                    that.inState.click = false;
                });
            }

            if (this.options.animation) $tip.addClass('tooltip--fade');

            var placement = typeof this.options.placement == 'function' ? this.options.placement.call(this, $tip[0], this.$element[0]) : this.options.placement;

            $tip
                .detach()
                .css({
                    top: 0,
                    left: 0,
                    display: 'block'
                })
                .data('plugin_' + pluginName, this);

            if (this.options.variant) {
                var variants = this.options.variant.split(' ');

                for (var i = 0; i < variants.length; i++) {
                    var variant = variants[i];

                    if (variant && variant != 'default') {
                        $tip.addClass('tooltip--' + variant);
                        if (variant == 'blue') {
                            $tip.find('.tooltip__arrow').removeClass('tooltip__arrow').addClass('tooltip__arrow-triangle');
                        }
                    }
                }
            }

            if (this.options.class) {
                var classes = this.options.class.split(' ');

                for (var j = 0; j < classes.length; j++) {
                    var oneclass = classes[j];

                    if (oneclass) {
                        $tip.addClass(oneclass);
                    }
                }
            }

            if (this.$element.closest('.modal').length) {
                if (this.options.container == Tooltip.DEFAULTS.container) {
                    this.options.container = this.$element.closest('.modal');
                }
            }

            this.options.container ? $tip.appendTo(this.options.container) : $tip.insertAfter(this.$element);
            this.$element.trigger('inserted.telia.' + this.type);

            var pos          = this._getPosition();
            var actualWidth  = $tip[0].offsetWidth;
            var actualHeight = $tip[0].offsetHeight;
            var tp;
            var fallbackPosition = 'top';

            // Get positions
            var tpt = {
                top: pos.top - actualHeight,
                left: pos.left + pos.width / 2 - actualWidth / 2
            };
            var tpb = {
                top: pos.top + pos.height,
                left: pos.left + pos.width / 2 - actualWidth / 2
            };
            var tpl = {
                top: pos.top + pos.height / 2 - actualHeight / 2,
                left: pos.left - actualWidth
            };
            var tpr = {
                top: pos.top + pos.height / 2 - actualHeight / 2,
                left: pos.left + pos.width
            };

            var headerHeight = 0;

            if (!this.$tip.hasClass('tooltip--top') && $('.header').length) {
                headerHeight = $('.header').height();
            }
            // Get position room
            var hast = (tpt.top > $(window).scrollTop() + headerHeight);
            var hasb = ((tpb.top + actualHeight) < ($(window).scrollTop() + $(window).height()));
            var hasl = (tpl.left > $(window).scrollLeft());
            var hasr = ((tpr.left + actualWidth) < ($(window).scrollLeft() + $(window).width()));

            switch (placement) {
                case 'top':
                    if (!hast) {
                        if (hasb) {
                            placement = 'bottom';
                        } else if (hasr) {
                            placement = 'right';
                        } else if (hasl) {
                            placement = 'left';
                        } else {
                            placement = fallbackPosition;
                        }
                    }
                    break;
                case 'bottom':
                    if (!hasb) {
                        if (hast) {
                            placement = 'top';
                        } else if (hasr) {
                            placement = 'right';
                        } else if (hasl) {
                            placement = 'left';
                        } else {
                            placement = fallbackPosition;
                        }
                    }
                    break;
                case 'left':
                    if (!hasl) {
                        if (hasr) {
                            placement = 'right';
                        } else if (hast) {
                            placement = 'top';
                        } else if (hasb) {
                            placement = 'bottom';
                        } else {
                            placement = fallbackPosition;
                        }
                    }
                    break;
                case 'right':
                    if (!hasr) {
                        if (hasl) {
                            placement = 'left';
                        } else if (hast) {
                            placement = 'top';
                        } else if (hasb) {
                            placement = 'bottom';
                        } else {
                            placement = fallbackPosition;
                        }
                    }
                    break;
            }

            switch (placement) {
                case 'top':
                    tp = tpt;
                    break;
                case 'bottom':
                    tp = tpb;
                    break;
                case 'left':
                    tp = tpl;
                    break;
                case 'right':
                    tp = tpr;
                    break;
            }

            this._applyPlacement(tp, placement);

            $tip.addClass(placement);

            if ($.support.transition && this.$tip.hasClass('tooltip--fade')) {
                $tip.one('bsTransitionEnd', complete).emulateTransitionEnd(Tooltip.TRANSITION_DURATION);
            } else {
                complete();
            }
        }
    };

    /**
     * Apply tooltip placement.
     *
     * @function applyPlacement
     * @param {Object} offset - Element offset.
     * @param {string} placement - Element placement.
     * @private
     * @returns {void}
     */
    Tooltip.prototype._applyPlacement = function(offset, placement) {
        var $tip   = this._tip();
        var width  = $tip[0].offsetWidth;
        var height = $tip[0].offsetHeight;

        // manually read margins because getBoundingClientRect includes difference
        var marginTop = parseInt($tip.css('margin-top'), 10);
        var marginLeft = parseInt($tip.css('margin-left'), 10);

        // we must check for NaN for ie 8/9
        if (isNaN(marginTop))  marginTop  = 0;
        if (isNaN(marginLeft)) marginLeft = 0;

        offset.top  += marginTop;
        offset.left += marginLeft;

        // $.fn.offset doesn't round pixel values
        // so we use setOffset directly with our own function B-0
        $.offset.setOffset($tip[0], $.extend({
            /**
             * Round pixel values to int.
             *
             * @param   {Object} props - Top and left positions.
             * @returns {void}
             */
            using: function(props) {
                $tip.css({
                    top: Math.round(props.top),
                    left: Math.round(props.left)
                });
            }
        }, offset), 0);

        $tip.addClass('is-visible');

        // check to see if placing tip in new offset caused the tip to resize itself
        var actualWidth  = $tip[0].offsetWidth;
        var actualHeight = $tip[0].offsetHeight;

        if (placement == 'top' && actualHeight != height) {
            offset.top = offset.top + height - actualHeight;
        }

        var delta = this._getViewportAdjustedDelta(placement, offset, actualWidth, actualHeight);

        if (delta.left) offset.left += delta.left;
        else offset.top += delta.top;

        var isVertical          = (/top|bottom/).test(placement);
        var arrowDelta          = isVertical ? delta.left * 2 - width + actualWidth : delta.top * 2 - height + actualHeight;
        var arrowOffsetPosition = isVertical ? 'offsetWidth' : 'offsetHeight';

        if (!this.options.onload) {
            $tip.offset(offset);
        }
        this._replaceArrow(arrowDelta, $tip[0][arrowOffsetPosition], isVertical);
    };

    /**
     * Arrow position.
     *
     * @function replaceArrow
     * @param {Int} delta - - Delta.
     * @param {Int} dimension - Dimension.
     * @param {boolean} isVertical - If is vertical.
     * @private
     * @returns {void}
     */
    Tooltip.prototype._replaceArrow = function(delta, dimension, isVertical) {
        this._arrow()
            .css(isVertical ? 'left' : 'top', 50 * (1 - delta / dimension) + '%')
            .css(isVertical ? 'top' : 'left', '');
    };

    /**
     * Set content.
     *
     * @function setContent
     * @private
     * @returns {void}
     */
    Tooltip.prototype._setContent = function() {
        var $tip  = this._tip();
        var title = this._getTitle();
        var content = this._getContent();
        var $title  = $tip.find('.tooltip__title');

        $title[this.options.html ? 'html' : 'text'](title);
        if (this.options.titleSize) {
            $title.addClass(this.options.titleSize);
        }
        if (this.options.html == 'object') {
            content = $(this.$element.data('content')).html();
            $tip.find('.tooltip__content').children().detach().end()['html'](content);
        } else {
            var method = 'text';

            // we use append for html objects to maintain js events
            if (this.options.html) {
                method = typeof content == 'string' ? 'html' : 'append';
            }
            $tip.find('.tooltip__content').children().detach().end()[method](content);
        }

        $tip.removeClass('tooltip--fade is-visible top bottom left right');

        // IE8 doesn't accept hiding via the `:empty` pseudo selector, we have to do
        // this manually by checking the contents.
        if (!$title.html()) $title.hide();
    };

    /**
     * If has title or content.
     *
     * @function hasContent
     * @private
     * @returns {void}
     */
    Tooltip.prototype._hasContent = function() {
        return this._getTitle() || this._getContent();
    };

    /**
     * Get content.
     *
     * @function getContent
     * @private
     * @returns {void}
     */
    Tooltip.prototype._getContent = function() {
        var $e = this.$element;
        var o  = this.options;

        return $e.attr('data-content') || (typeof o.content == 'function' ? o.content.call($e[0]) : o.content);
    };

    /**
     * Hide tooltip.
     *
     * @function hide
     * @param {Function} callback - Callback.
     * @returns {void}
     */
    Tooltip.prototype.hide = function(callback) {
        var that = this;
        var $tip = $(this.$tip);
        var e    = $.Event('hide.telia.' + this.type);

        /**
         * Complete hiding.
         *
         * @returns {void}
         */
        function complete() {
            if (that.hoverState != 'is-visible') $tip.detach();
            that.$element
                .removeAttr('aria-describedby')
                .trigger('hidden.telia.' + that.type);
            callback && callback();
        }

        if (this.$tip) {
            $(document).off('keyup.telia.tooltip.' + this.$tip[0].id);
        }
        this.$element.trigger(e);

        if (e.isDefaultPrevented()) return;

        $tip.removeClass('is-visible');

        if ($.support.transition && $tip.hasClass('tooltip--fade')) {
            $tip.one('bsTransitionEnd', complete).emulateTransitionEnd(Tooltip.TRANSITION_DURATION);
        } else {
            complete();
        }

        this.hoverState = null;

        return this;
    };

    /**
     * Fix title.
     *
     * @function fixTitle
     * @private
     * @returns {void}
     */
    Tooltip.prototype._fixTitle = function() {
        var $e = this.$element;

        if ($e.attr('title') || typeof $e.attr('data-original-title') != 'string') {
            $e.attr('data-original-title', $e.attr('title') || '').attr('title', '');
        }
    };

    /**
     * Get position.
     *
     * @function getPosition
     * @param {HTMLelement} $element - Tooltip element.
     * @private
     * @returns {void}
     */
    Tooltip.prototype._getPosition = function($element) {
        $element   = $element || this.$element;

        var el     = $element[0];
        var isBody = el.tagName == 'BODY';

        var elRect    = el.getBoundingClientRect();

        if (elRect.width == null) {
            // width and height are missing in IE8, so compute them manually; see https://github.com/twbs/bootstrap/issues/14093
            elRect = $.extend({}, elRect, {
                width: elRect.right - elRect.left,
                height: elRect.bottom - elRect.top
            });
        }
        var elOffset  = isBody ? {
            top: 0,
            left: 0
        } : $element.offset();
        var scroll    = {
            scroll: isBody ? document.documentElement.scrollTop || document.body.scrollTop : $element.scrollTop()
        };
        var outerDims = isBody ? {
            width: $(window).width(),
            height: $(window).height()
        } : null;

        return $.extend({}, elRect, scroll, outerDims, elOffset);
    };

    /**
     * Get viewport adjusted delta.
     *
     * @function getViewportAdjustedDelta
     * @param {string} placement - Placement.
     * @param {Object} pos - Position.
     * @param {Int} actualWidth - Actual width.
     * @param {Int} actualHeight - Actual height.
     * @private
     * @returns {void}
     */
    Tooltip.prototype._getViewportAdjustedDelta = function(placement, pos, actualWidth, actualHeight) {
        var delta = {
            top: 0,
            left: 0
        };

        if (!this.$viewport) return delta;

        var viewportPadding = this.options.viewport && this.options.viewport.padding || 0;
        var viewportDimensions = this._getPosition(this.$viewport);

        if (/right|left/.test(placement)) {
            var topEdgeOffset    = pos.top - viewportPadding - viewportDimensions.scroll;
            var bottomEdgeOffset = pos.top + viewportPadding - viewportDimensions.scroll + actualHeight;

            if (topEdgeOffset < viewportDimensions.top) {
                // top overflow
                delta.top = viewportDimensions.top - topEdgeOffset;
            } else if (bottomEdgeOffset > viewportDimensions.top + viewportDimensions.height) {
                // bottom overflow
                delta.top = viewportDimensions.top + viewportDimensions.height - bottomEdgeOffset;
            }
        } else {
            var leftEdgeOffset  = pos.left - viewportPadding;
            var rightEdgeOffset = pos.left + viewportPadding + actualWidth;

            if (leftEdgeOffset < viewportDimensions.left) {
                // left overflow
                delta.left = viewportDimensions.left - leftEdgeOffset;
            } else if (rightEdgeOffset > viewportDimensions.right) {
                // right overflow
                delta.left = viewportDimensions.left + viewportDimensions.width - rightEdgeOffset;
            }
        }

        return delta;
    };

    /**
     * Get title.
     *
     * @function getTitle
     * @private
     * @returns {void}
     */
    Tooltip.prototype._getTitle = function() {
        var title;
        var $e = this.$element;
        var o  = this.options;

        title = $e.attr('data-original-title') || (typeof o.title == 'function' ? o.title.call($e[0]) :  o.title);

        return title;
    };

    /**
     * Get unique id for tooltip.
     *
     * @function getUID
     * @param {string} prefix - Prefix.
     * @private
     * @returns {void}
     */
    Tooltip.prototype._getUID = function(prefix) {
        do prefix += ~~(Math.random() * 1000000);
        while (document.getElementById(prefix));

        return prefix;
    };

    /**
     * Get tooltip.
     *
     * @function tip
     * @private
     * @returns {void}
     */
    Tooltip.prototype._tip = function() {
        if (!this.$tip) {
            this.$tip = $(this.options.template);
            if (this.$tip.length != 1) {
                throw new Error(this.type + ' `template` option must consist of exactly 1 top-level element!');
            }
        }

        return this.$tip;
    };

    /**
     * Get tooltip box arrow.
     *
     * @function arrow
     * @private
     * @returns {void}
     */
    Tooltip.prototype._arrow = function() {
        return (this.$arrow = this.$arrow || this._tip().find('.tooltip__arrow, .tooltip__arrow-triangle'));
    };

    /**
     * Enable tooltip.
     *
     * @function enable
     * @returns {void}
     */
    Tooltip.prototype.enable = function() {
        this.enabled = true;
    };

    /**
     * Disable tooltip.
     *
     * @function disable
     * @returns {void}
     */
    Tooltip.prototype.disable = function() {
        this.enabled = false;
    };

    /**
     * Toggle tooltip.
     *
     * @function toggle
     * @param {Event} event - Event.
     * @returns {void}
     */
    Tooltip.prototype.toggle = function(event) {
        var self = this;

        if (event) {
            self = $(event.currentTarget).data('plugin_' + pluginName);
            if (!self) {
                self = new this.constructor(event.currentTarget, this._getDelegateOptions());
                $(event.currentTarget).data('plugin_' + pluginName, self);
            }
        }

        if (event) {
            self.inState.click = !self.inState.click;
            if (self._isInStateTrue()) self.enter(self);
            else self.leave(self);
        } else {
            self.tip().hasClass('is-visible') ? self.leave(self) : self.enter(self);
        }
    };

    /**
     * Hide tooltip on esc.
     *
     * @function keyboard
     * @param {Event} event - Event.
     * @private
     * @returns {void}
     */
    Tooltip.prototype._keyboard = function(event) {
        if (event.keyCode == 27) {
            this.hide();
        }
    },

    /**
     * Destroy plugin.
     *
     * @function destroy
     * @returns {void}
     */
    Tooltip.prototype.destroy = function() {
        var that = this;

        clearTimeout(this.timeout);
        this.hide(function() {
            $(document).off('click.telia.tooltip touchstart.telia.tooltip');
            window.whatInput.unRegisterOnChange(that._bindDynamicEnterEvents.bind(this));

            that.$element.off('.' + that.type).removeData('plugin_' + pluginName);
            if (that.$tip) {
                that.$tip.detach();
            }
            that.$tip = null;
            that.$arrow = null;
            that.$viewport = null;
        });
    };

    /**
     * Create jQuery plugin.
     *
     * @param {Object} options - Extended options.
     * @listens telia.tooltip
     * @returns {Array<Tooltip>} Array of tooltips.
     */
    $.fn[pluginName] = function(options) {
        var args = Array.prototype.slice.call(arguments, 1);

        return this.each(function() {
            var instance = $.data(this, 'plugin_' + pluginName);

            if (!instance) {
                if (options === 'destroy') {
                    return;
                }

                $.data(this, 'plugin_' + pluginName, new Tooltip(this, options));
            } else if (typeof options === 'string') {
                instance[options].apply(instance, args);
            }
        });
    };

    /**
     * @param {Event} event
     * @event enhance.telia.tooltip
     */
    $(document).on('enhance.telia.tooltip', function(event) {
        $(event.target).find(initSelector).each(function() {
            var self = $(this);

            self[pluginName]();

            if (self.data('onload')) {
                self[pluginName]('show');
            }
        });
    });

    /**
     * @param {Event} event
     * @event destroy.telia.tooltip
     */
    $(document).on('destroy.telia.tooltip', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]('destroy');
    });


    /**
     * @event resize.telia.tooltip
     */
    var tooltipResizeTimer;
    var cachedWindowWidth = $(window).width();

    $(window).on('resize.telia.tooltip', function() {
        var newWindowWidth = $(window).width();

        if (newWindowWidth != cachedWindowWidth) {
            clearTimeout(tooltipResizeTimer);
            tooltipResizeTimer = setTimeout(function() {
                $('.tooltip').each(function() {
                    var $this = $(this);

                    if ($this.is(':visible')) {
                        $this.addClass('tooltip--reset-transition');
                        $('#' + $this.attr('id'))[pluginName]('show');
                        $this.removeClass('tooltip--reset-transition');
                    }
                });
            }, 300);

            cachedWindowWidth = newWindowWidth;
        }
    });

    /**
     * @param {Event} event
     * @event click.telia.tooltip
     * @event touchstart.telia.tooltip
     */
    $(document).on('click.telia.tooltip touchstart.telia.tooltip', function(event) {
        $(initSelector).each(function() {
            if (!$(this).is(event.target) && $(this).has(event.target).length === 0 && $('.tooltip').has(event.target).length === 0) {
                $(this)[pluginName]('hide');
            }
        });
    });

    /**
     * @param {Event} event
     * @event hidden.telia.tooltip
     */
    $(document).on('hidden.telia.tooltip', function(event) {
        $(event.target).data('plugin_' + pluginName).inState.click = false;
    });
})(jQuery, window, document);

/**
 * Auto-init the plugin on DOM ready.
 * @fires enhance.telia.tooltip
 */
$(function() {
    $(document).trigger('enhance.telia.tooltip');
});

;(function($, window, document) {
    var pluginName = 'popover';
    var initSelector = '[data-toggle="popover"]';

    /**
     * Popover.
     *
     * @class
     * @param {HTMLElement} element - The HTML element the Popover should be bound to.
     * @param {Object} options - An option map.
     */
    function Popover(element, options) {
        this.type       = null;
        this.options    = null;
        this.enabled    = null;
        this.timeout    = null;
        this.hoverState = null;
        this.$element   = null;
        this.inState    = null;

        this.init('popover', element, options);
    };

    Popover.TRANSITION_DURATION = 150;

    Popover.DEFAULTS = {
        animation: true,
        placement: 'top',
        selector: false,
        template: '<div class="popover" role="popover"><div class="popover__arrow"></div><h4 class="popover__title"></h4><div class="popover__content"></div></div>',
        trigger: 'click',
        title: '',
        titleSize: 'h4',
        delay: 0,
        html: false,
        content: '',
        container: 'body',
        viewport: {
            selector: 'body',
            padding: 0
        },
        variant: '',
        close: false,
        onload: false,
        class: ''
    };

    /**
     * Initialize the plugin.
     *
     * @function init
     * @param {string} type - Type of popover.
     * @param {HTMLelement} element - Popover element.
     * @param {Object} options - Options.
     * @returns {void}
     */
    Popover.prototype.init = function(type, element, options) {
        var that = this;

        this.enabled   = true;
        this.type      = type;
        this.$element  = $(element);
        this._options  = this.$element.data();
        this.options   = this._getOptions(options);
        this.$viewport = this.options.viewport && $($.isFunction(this.options.viewport) ? this.options.viewport.call(this, this.$element) : (this.options.viewport.selector || this.options.viewport));
        this.inState   = {
            click: false,
            hover: false,
            focus: false
        };
        this.$close  = $('<a href="#" class="popover__close">' + telia.icon.render('#close-round') + '</a>');
        this.intent = window.whatInput.ask('loose') && window.whatInput.ask('loose') == 'initial' ? 'touch' : window.whatInput.ask('loose');

        if (this.$element[0] instanceof document.constructor && !this.options.selector) {
            throw new Error('`selector` option must be specified when initializing ' + this.type + ' on the window.document object!');
        }

        if (this._useDynamicTrigger()) {
            window.whatInput.registerOnChange(that._bindDynamicEnterEvents.bind(this), 'intent');

            this._bindDynamicEnterEvents(this.intent);
        } else {
            var triggers = this.options.trigger.split(' ');

            for (var i = triggers.length; i--;) {
                var trigger = triggers[i];

                if (trigger == 'click') {
                    this.$element.on('click.' + this.type, this.options.selector, $.proxy(this.toggle, this));
                    this.options.close = true;
                } else if (trigger != 'manual') {
                    var eventIn  = trigger == 'hover' ? 'mouseenter' : 'focusin';
                    var eventOut = trigger == 'hover' ? 'mouseleave' : 'focusout';

                    this.$element.on(eventIn  + '.' + this.type, this.options.selector, $.proxy(this.enter, this));
                    this.$element.on(eventOut + '.' + this.type, this.options.selector, $.proxy(this.leave, this));
                }
            }

            if (this.options.selector) {
                this._options = $.extend({}, this.options, {
                    trigger: 'manual',
                    selector: ''
                });
            } else {
                this._fixTitle();
            }
        }
    };

    /**
     * Use dynamic trigger if the instance does not have a custom trigger.
     * Also use dynamic trigger if custom trigger is hover, as hover is not available on all devices.
     *
     * @function _useDynamicTrigger
     * @private
     * @returns {boolean} If should use dynamic trigger.
     */
    Popover.prototype._useDynamicTrigger = function() {
        return this._options.trigger != 'click' && (this.options.trigger == Popover.DEFAULTS.trigger || this.options.trigger == 'hover');
    };

    /**
     * Bind enter events based on dynamic input type.
     *
     * @function _bindDynamicEnterEvents
     * @private
     * @param {string} type - New intent type.
     * @returns {void}
     */
    Popover.prototype._bindDynamicEnterEvents = function(type) {
        this.intent = type;
        this.options.close = this._options.close || false;
        this.$element.off('.' + this.type);

        if (this.intent == 'mouse') {
            //hover
            this.$element.on('mouseenter.' + this.type, this.options.selector, $.proxy(this.enter, this));
            this.$element.on('mouseleave.' + this.type, this.options.selector, $.proxy(this.leave, this));
        } else if (this.intent == 'keyboard') {
            // focus
            this.$element.on('focusin.' + this.type, this.options.selector, $.proxy(this.enter, this));
            this.$element.on('focusout.' + this.type, this.options.selector, $.proxy(this.leave, this));
        } else {
            // click
            this.options.close = true;
            this.$element.on('click.' + this.type, this.options.selector, $.proxy(this.toggle, this));
        }
    };

    /**
     * Get default options.
     *
     * @function getDefaults
     * @private
     * @returns {void}
     */
    Popover.prototype._getDefaults = function() {
        return Popover.DEFAULTS;
    };

    /**
     * Get popover options.
     *
     * @function getOptions
     * @param {Object} options - Options.
     * @private
     * @returns {void}
     */
    Popover.prototype._getOptions = function(options) {
        options = $.extend({}, this._getDefaults(), this.$element.data(), options);

        if (options.delay && typeof options.delay == 'number') {
            options.delay = {
                show: options.delay,
                hide: options.delay
            };
        }

        return options;
    };

    /**
     * Get delegated options.
     *
     * @function getDelegateOptions
     * @private
     * @returns {void}
     */
    Popover.prototype._getDelegateOptions = function() {
        var options  = {};
        var defaults = this._getDefaults();

        this._options && $.each(this._options, function(key, value) {
            if (defaults[key] != value) options[key] = value;
        });

        return options;
    };

    /**
     * Enter popover trigger element.
     *
     * @function enter
     * @param {Object} obj - Event.
     * @returns {void}
     */
    Popover.prototype.enter = function(obj) {
        var self = obj instanceof this.constructor ? obj : $(obj.currentTarget).data('plugin_' + pluginName);

        if (!self) {
            self = new this.constructor(obj.currentTarget, this._getDelegateOptions());
            $(obj.currentTarget).data('plugin_' + pluginName, self);
        }

        if (obj instanceof $.Event) {
            self.inState[obj.type == 'focusin' ? 'focus' : 'hover'] = true;
        }

        if (self._tip().hasClass('popover--visible') || self.hoverState == 'popover--visible') {
            self.hoverState = 'popover--visible';

            return;
        }

        clearTimeout(self.timeout);

        self.hoverState = 'popover--visible';

        if (!self.options.delay || !self.options.delay.show) return self.show();

        self.timeout = setTimeout(function() {
            if (self.hoverState == 'popover--visible') self.show();
        }, self.options.delay.show);
    };

    /**
     * Check if popover state is true.
     *
     * @function isInStateTrue
     * @private
     * @returns {void}
     */
    Popover.prototype._isInStateTrue = function() {
        for (var key in this.inState) {
            if (this.inState[key]) return true;
        }

        return false;
    };

    /**
     * Leave popover trigger element.
     *
     * @function leave
     * @param {Object} obj - Event.
     * @returns {void}
     */
    Popover.prototype.leave = function(obj) {
        var self = obj instanceof this.constructor ? obj : $(obj.currentTarget).data('plugin_' + pluginName);

        if (!self) {
            self = new this.constructor(obj.currentTarget, this._getDelegateOptions());
            $(obj.currentTarget).data('plugin_' + pluginName, self);
        }

        if (obj instanceof $.Event) {
            self.inState[obj.type == 'focusout' ? 'focus' : 'hover'] = false;
        }

        if (self._isInStateTrue()) return;

        clearTimeout(self.timeout);

        self.hoverState = 'out';

        if (!self.options.delay || !self.options.delay.hide) return self.hide();

        self.timeout = setTimeout(function() {
            if (self.hoverState == 'out') self.hide();
        }, self.options.delay.hide);
    };

    /**
     * Show popover.
     *
     * @function show
     * @returns {void}
     */
    Popover.prototype.show = function() {
        /**
         * Complete popover showing.
         *
         * @returns {void}
         */
        function complete() {
            var prevHoverState = that.hoverState;

            that.$element.trigger('shown.telia.' + that.type);
            that.hoverState = null;

            if (prevHoverState == 'out') that.leave(that);

            $(document).on('keyup.telia.popover.' + that.$tip[0].id, that._keyboard.bind(that));
        };

        var e = $.Event('show.telia.' + this.type);

        if (this._hasContent() && this.enabled) {
            this.$element.trigger(e);

            var inDom = $.contains(this.$element[0].ownerDocument.documentElement, this.$element[0]);

            if (e.isDefaultPrevented() || !inDom) return;
            var that = this;

            var $tip = this._tip();

            var tipId = this._getUID(this.type);

            this._setContent();
            $tip.attr('id', tipId);
            this.$element.attr('aria-describedby', tipId);

            if (this.options.close) {
                if (this.options.title == '') {
                    $tip.addClass('popover--with-close');
                }
                $tip.addClass('popover--with-close-and-title');
                $tip.append(this.$close);

                this.$close.on('click', function(event) {
                    event.preventDefault();
                    that.hide();
                    that.inState.click = false;
                });
            }

            if (this.options.animation) $tip.addClass('popover--fade');

            var placement = typeof this.options.placement == 'function' ? this.options.placement.call(this, $tip[0], this.$element[0]) : this.options.placement;

            $tip
                .detach()
                .css({
                    top: 0,
                    left: 0,
                    display: 'block'
                })
                .data('plugin_' + pluginName, this);

            if (this.options.variant) {
                var variants = this.options.variant.split(' ');

                for (var i = 0; i < variants.length; i++) {
                    var variant = variants[i];

                    if (variant && variant != 'default') {
                        $tip.addClass('popover--' + variant);
                        if (variant == 'blue') {
                            $tip.find('.popover__arrow').removeClass('popover__arrow').addClass('popover__arrow-triangle');
                        }
                    }
                }
            }

            if (this.options.class) {
                var classes = this.options.class.split(' ');

                for (var j = 0; j < classes.length; j++) {
                    var oneclass = classes[j];

                    if (oneclass) {
                        $tip.addClass(oneclass);
                    }
                }
            }

            if (this.$element.closest('.modal').length) {
                if (this.options.container == Popover.DEFAULTS.container) {
                    this.options.container = this.$element.closest('.modal');
                }
            }

            this.options.container ? $tip.appendTo(this.options.container) : $tip.insertAfter(this.$element);
            this.$element.trigger('inserted.telia.' + this.type);

            var pos          = this._getPosition();
            var actualWidth  = $tip[0].offsetWidth;
            var actualHeight = $tip[0].offsetHeight;
            var tp;
            var fallbackPosition = 'top';

            // Get positions
            var tpt = {
                top: pos.top - actualHeight,
                left: pos.left + pos.width / 2 - actualWidth / 2
            };
            var tpb = {
                top: pos.top + pos.height,
                left: pos.left + pos.width / 2 - actualWidth / 2
            };
            var tpl = {
                top: pos.top + pos.height / 2 - actualHeight / 2,
                left: pos.left - actualWidth
            };
            var tpr = {
                top: pos.top + pos.height / 2 - actualHeight / 2,
                left: pos.left + pos.width
            };

            var headerHeight = 0;

            if (!this.$tip.hasClass('popover--top') && $('.header').length) {
                headerHeight = $('.header').height();
            }
            // Get position room
            var hast = (tpt.top > $(window).scrollTop() + headerHeight);
            var hasb = ((tpb.top + actualHeight) < ($(window).scrollTop() + $(window).height()));
            var hasl = (tpl.left > $(window).scrollLeft());
            var hasr = ((tpr.left + actualWidth) < ($(window).scrollLeft() + $(window).width()));

            switch (placement) {
                case 'top':
                    if (!hast) {
                        if (hasb) {
                            placement = 'bottom';
                        } else if (hasr) {
                            placement = 'right';
                        } else if (hasl) {
                            placement = 'left';
                        } else {
                            placement = fallbackPosition;
                        }
                    }
                    break;
                case 'bottom':
                    if (!hasb) {
                        if (hast) {
                            placement = 'top';
                        } else if (hasr) {
                            placement = 'right';
                        } else if (hasl) {
                            placement = 'left';
                        } else {
                            placement = fallbackPosition;
                        }
                    }
                    break;
                case 'left':
                    if (!hasl) {
                        if (hasr) {
                            placement = 'right';
                        } else if (hast) {
                            placement = 'top';
                        } else if (hasb) {
                            placement = 'bottom';
                        } else {
                            placement = fallbackPosition;
                        }
                    }
                    break;
                case 'right':
                    if (!hasr) {
                        if (hasl) {
                            placement = 'left';
                        } else if (hast) {
                            placement = 'top';
                        } else if (hasb) {
                            placement = 'bottom';
                        } else {
                            placement = fallbackPosition;
                        }
                    }
                    break;
            }

            switch (placement) {
                case 'top':
                    tp = tpt;
                    break;
                case 'bottom':
                    tp = tpb;
                    break;
                case 'left':
                    tp = tpl;
                    break;
                case 'right':
                    tp = tpr;
                    break;
            }

            this._applyPlacement(tp, placement);

            $tip.addClass(placement);

            if ($.support.transition && this.$tip.hasClass('popover--fade')) {
                $tip.one('bsTransitionEnd', complete).emulateTransitionEnd(Popover.TRANSITION_DURATION);
            } else {
                complete();
            }
        }
    };

    /**
     * Apply popover placement.
     *
     * @function applyPlacement
     * @param {Object} offset - Element offset.
     * @param {string} placement - Element placement.
     * @private
     * @returns {void}
     */
    Popover.prototype._applyPlacement = function(offset, placement) {
        var $tip   = this._tip();
        var width  = $tip[0].offsetWidth;
        var height = $tip[0].offsetHeight;

        // manually read margins because getBoundingClientRect includes difference
        var marginTop = parseInt($tip.css('margin-top'), 10);
        var marginLeft = parseInt($tip.css('margin-left'), 10);

        // we must check for NaN for ie 8/9
        if (isNaN(marginTop))  marginTop  = 0;
        if (isNaN(marginLeft)) marginLeft = 0;

        offset.top  += marginTop;
        offset.left += marginLeft;

        // $.fn.offset doesn't round pixel values
        // so we use setOffset directly with our own function B-0
        $.offset.setOffset($tip[0], $.extend({
            /**
             * Round pixel values to int.
             *
             * @param   {Object} props - Top and left positions.
             * @returns {void}
             */
            using: function(props) {
                $tip.css({
                    top: Math.round(props.top),
                    left: Math.round(props.left)
                });
            }
        }, offset), 0);

        $tip.addClass('popover--visible');

        // check to see if placing tip in new offset caused the tip to resize itself
        var actualWidth  = $tip[0].offsetWidth;
        var actualHeight = $tip[0].offsetHeight;

        if (placement == 'top' && actualHeight != height) {
            offset.top = offset.top + height - actualHeight;
        }

        var delta = this._getViewportAdjustedDelta(placement, offset, actualWidth, actualHeight);

        if (delta.left) offset.left += delta.left;
        else offset.top += delta.top;

        var isVertical          = (/top|bottom/).test(placement);
        var arrowDelta          = isVertical ? delta.left * 2 - width + actualWidth : delta.top * 2 - height + actualHeight;
        var arrowOffsetPosition = isVertical ? 'offsetWidth' : 'offsetHeight';

        if (!this.options.onload) {
            $tip.offset(offset);
        }
        this._replaceArrow(arrowDelta, $tip[0][arrowOffsetPosition], isVertical);
    };

    /**
     * Arrow position.
     *
     * @function replaceArrow
     * @param {Int} delta - - Delta.
     * @param {Int} dimension - Dimension.
     * @param {boolean} isVertical - If is vertical.
     * @private
     * @returns {void}
     */
    Popover.prototype._replaceArrow = function(delta, dimension, isVertical) {
        this._arrow()
            .css(isVertical ? 'left' : 'top', 50 * (1 - delta / dimension) + '%')
            .css(isVertical ? 'top' : 'left', '');
    };

    /**
     * Set content.
     *
     * @function setContent
     * @private
     * @returns {void}
     */
    Popover.prototype._setContent = function() {
        var $tip  = this._tip();
        var title = this._getTitle();
        var content = this._getContent();
        var $title  = $tip.find('.popover__title');

        $title[this.options.html ? 'html' : 'text'](title);
        if (this.options.titleSize) {
            $title.addClass(this.options.titleSize);
        }
        if (this.options.html == 'object') {
            content = $(this.$element.data('content')).html();
            $tip.find('.popover__content').children().detach().end()['html'](content);
        } else {
            var method = 'text';

            // we use append for html objects to maintain js events
            if (this.options.html) {
                method = typeof content == 'string' ? 'html' : 'append';
            }
            $tip.find('.popover__content').children().detach().end()[method](content);
        }

        $tip.removeClass('popover--fade popover--visible top bottom left right');

        // IE8 doesn't accept hiding via the `:empty` pseudo selector, we have to do
        // this manually by checking the contents.
        if (!$title.html()) $title.hide();
    };

    /**
     * If has title or content.
     *
     * @function hasContent
     * @private
     * @returns {void}
     */
    Popover.prototype._hasContent = function() {
        return this._getTitle() || this._getContent();
    };

    /**
     * Get content.
     *
     * @function getContent
     * @private
     * @returns {void}
     */
    Popover.prototype._getContent = function() {
        var $e = this.$element;
        var o  = this.options;

        return $e.attr('data-content') || (typeof o.content == 'function' ? o.content.call($e[0]) : o.content);
    };

    /**
     * Hide popover.
     *
     * @function hide
     * @param {Function} callback - Callback.
     * @returns {void}
     */
    Popover.prototype.hide = function(callback) {
        var that = this;
        var $tip = $(this.$tip);
        var e    = $.Event('hide.telia.' + this.type);

        /**
         * Complete hiding.
         *
         * @returns {void}
         */
        function complete() {
            if (that.hoverState != 'popover--visible') $tip.detach();
            that.$element
                .removeAttr('aria-describedby')
                .trigger('hidden.telia.' + that.type);
            callback && callback();
        }

        if (this.$tip) {
            $(document).off('keyup.telia.popover.' + this.$tip[0].id);
        }
        this.$element.trigger(e);

        if (e.isDefaultPrevented()) return;

        $tip.removeClass('popover--visible');

        if ($.support.transition && $tip.hasClass('popover--fade')) {
            $tip.one('bsTransitionEnd', complete).emulateTransitionEnd(Popover.TRANSITION_DURATION);
        } else {
            complete();
        }

        this.hoverState = null;

        return this;
    };

    /**
     * Fix title.
     *
     * @function fixTitle
     * @private
     * @returns {void}
     */
    Popover.prototype._fixTitle = function() {
        var $e = this.$element;

        if ($e.attr('title') || typeof $e.attr('data-original-title') != 'string') {
            $e.attr('data-original-title', $e.attr('title') || '').attr('title', '');
        }
    };

    /**
     * Get position.
     *
     * @function getPosition
     * @param {HTMLelement} $element - Popover element.
     * @private
     * @returns {void}
     */
    Popover.prototype._getPosition = function($element) {
        $element   = $element || this.$element;

        var el     = $element[0];
        var isBody = el.tagName == 'BODY';

        var elRect    = el.getBoundingClientRect();

        if (elRect.width == null) {
            // width and height are missing in IE8, so compute them manually; see https://github.com/twbs/bootstrap/issues/14093
            elRect = $.extend({}, elRect, {
                width: elRect.right - elRect.left,
                height: elRect.bottom - elRect.top
            });
        }
        var elOffset  = isBody ? {
            top: 0,
            left: 0
        } : $element.offset();
        var scroll    = {
            scroll: isBody ? document.documentElement.scrollTop || document.body.scrollTop : $element.scrollTop()
        };
        var outerDims = isBody ? {
            width: $(window).width(),
            height: $(window).height()
        } : null;

        return $.extend({}, elRect, scroll, outerDims, elOffset);
    };

    /**
     * Get viewport adjusted delta.
     *
     * @function getViewportAdjustedDelta
     * @param {string} placement - Placement.
     * @param {Object} pos - Position.
     * @param {Int} actualWidth - Actual width.
     * @param {Int} actualHeight - Actual height.
     * @private
     * @returns {void}
     */
    Popover.prototype._getViewportAdjustedDelta = function(placement, pos, actualWidth, actualHeight) {
        var delta = {
            top: 0,
            left: 0
        };

        if (!this.$viewport) return delta;

        var viewportPadding = this.options.viewport && this.options.viewport.padding || 0;
        var viewportDimensions = this._getPosition(this.$viewport);

        if (/right|left/.test(placement)) {
            var topEdgeOffset    = pos.top - viewportPadding - viewportDimensions.scroll;
            var bottomEdgeOffset = pos.top + viewportPadding - viewportDimensions.scroll + actualHeight;

            if (topEdgeOffset < viewportDimensions.top) {
                // top overflow
                delta.top = viewportDimensions.top - topEdgeOffset;
            } else if (bottomEdgeOffset > viewportDimensions.top + viewportDimensions.height) {
                // bottom overflow
                delta.top = viewportDimensions.top + viewportDimensions.height - bottomEdgeOffset;
            }
        } else {
            var leftEdgeOffset  = pos.left - viewportPadding;
            var rightEdgeOffset = pos.left + viewportPadding + actualWidth;

            if (leftEdgeOffset < viewportDimensions.left) {
                // left overflow
                delta.left = viewportDimensions.left - leftEdgeOffset;
            } else if (rightEdgeOffset > viewportDimensions.right) {
                // right overflow
                delta.left = viewportDimensions.left + viewportDimensions.width - rightEdgeOffset;
            }
        }

        return delta;
    };

    /**
     * Get title.
     *
     * @function getTitle
     * @private
     * @returns {void}
     */
    Popover.prototype._getTitle = function() {
        var title;
        var $e = this.$element;
        var o  = this.options;

        title = $e.attr('data-original-title') || (typeof o.title == 'function' ? o.title.call($e[0]) :  o.title);

        return title;
    };

    /**
     * Get unique id for popover.
     *
     * @function getUID
     * @param {string} prefix - Prefix.
     * @private
     * @returns {void}
     */
    Popover.prototype._getUID = function(prefix) {
        do prefix += ~~(Math.random() * 1000000);
        while (document.getElementById(prefix));

        return prefix;
    };

    /**
     * Get popover.
     *
     * @function tip
     * @private
     * @returns {void}
     */
    Popover.prototype._tip = function() {
        if (!this.$tip) {
            this.$tip = $(this.options.template);
            if (this.$tip.length != 1) {
                throw new Error(this.type + ' `template` option must consist of exactly 1 top-level element!');
            }
        }

        return this.$tip;
    };

    /**
     * Get popover box arrow.
     *
     * @function arrow
     * @private
     * @returns {void}
     */
    Popover.prototype._arrow = function() {
        return (this.$arrow = this.$arrow || this._tip().find('.popover__arrow, .popover__arrow-triangle'));
    };

    /**
     * Enable popover.
     *
     * @function enable
     * @returns {void}
     */
    Popover.prototype.enable = function() {
        this.enabled = true;
    };

    /**
     * Disable popover.
     *
     * @function disable
     * @returns {void}
     */
    Popover.prototype.disable = function() {
        this.enabled = false;
    };

    /**
     * Toggle popover.
     *
     * @function toggle
     * @param {Event} event - Event.
     * @returns {void}
     */
    Popover.prototype.toggle = function(event) {
        var self = this;

        if (event) {
            self = $(event.currentTarget).data('plugin_' + pluginName);
            if (!self) {
                self = new this.constructor(event.currentTarget, this._getDelegateOptions());
                $(event.currentTarget).data('plugin_' + pluginName, self);
            }
        }

        if (event) {
            self.inState.click = !self.inState.click;
            if (self._isInStateTrue()) self.enter(self);
            else self.leave(self);
        } else {
            self.tip().hasClass('popover--visible') ? self.leave(self) : self.enter(self);
        }
    };

    /**
     * Hide popover on esc.
     *
     * @function keyboard
     * @param {Event} event - Event.
     * @private
     * @returns {void}
     */
    Popover.prototype._keyboard = function(event) {
        if (event.keyCode == 27) {
            this.hide();
        }
    },

    /**
     * Destroy plugin.
     *
     * @function destroy
     * @returns {void}
     */
    Popover.prototype.destroy = function() {
        var that = this;

        clearTimeout(this.timeout);
        this.hide(function() {
            $(document).off('click.telia.popover touchstart.telia.popover');
            window.whatInput.unRegisterOnChange(that._bindDynamicEnterEvents.bind(this));

            that.$element.off('.' + that.type).removeData('plugin_' + pluginName);
            if (that.$tip) {
                that.$tip.detach();
            }
            that.$tip = null;
            that.$arrow = null;
            that.$viewport = null;
        });
    };

    /**
     * Create jQuery plugin.
     *
     * @param {Object} options - Extended options.
     * @listens telia.popover
     * @returns {Array<Popover>} Array of popovers.
     */
    $.fn[pluginName] = function(options) {
        var args = Array.prototype.slice.call(arguments, 1);

        return this.each(function() {
            var instance = $.data(this, 'plugin_' + pluginName);

            if (!instance) {
                $.data(this, 'plugin_' + pluginName, new Popover(this, options));
            } else if (typeof options === 'string') {
                instance[options].apply(instance, args);
            }
        });
    };

    /**
     * @param {Event} event
     * @event enhance.telia.popover
     */
    $(document).on('enhance.telia.popover', function(event) {
        $(event.target).find(initSelector).each(function() {
            var self = $(this);

            self[pluginName]();

            if (self.data('onload')) {
                self[pluginName]('show');
            }
        });
    });

    /**
     * @param {Event} event
     * @event destroy.telia.popover
     */
    $(document).on('destroy.telia.popover', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]('destroy');
    });

    /**
     * @event resize.telia.popover
     */
    var popoverResizeTimer;
    var cachedWindowWidth = $(window).width();

    $(window).on('resize.telia.popover', function() {
        var newWindowWidth = $(window).width();

        if (newWindowWidth != cachedWindowWidth) {
            clearTimeout(popoverResizeTimer);
            popoverResizeTimer = setTimeout(function() {
                $('.popover').each(function() {
                    var $this = $(this);

                    if ($this.is(':visible')) {
                        $this.addClass('popover--reset-transition');
                        $('#' + $this.attr('id'))[pluginName]('show');
                        $this.removeClass('popover--reset-transition');
                    }
                });
            }, 300);

            cachedWindowWidth = newWindowWidth;
        }
    });

    /**
     * @param {Event} event
     * @event click.telia.popover
     * @event touchstart.telia.popover
     */
    $(document).on('click.telia.popover touchstart.telia.popover', function(event) {
        $(initSelector).each(function() {
            if (!$(this).is(event.target) && $(this).has(event.target).length === 0 && $('.popover').has(event.target).length === 0) {
                $(this)[pluginName]('hide');
            }
        });
    });

    /**
     * @param {Event} event
     * @event hidden.telia.popover
     */
    $(document).on('hidden.telia.popover', function(event) {
        $(event.target).data('plugin_' + pluginName).inState.click = false;
    });
})(jQuery, window, document);

/**
 * Auto-init the plugin on DOM ready.
 * @fires enhance.telia.popover
 */
$(function() {
    $(document).trigger('enhance.telia.popover');
});

/**
 * Old init for compatibility.
 *
 * @fires enhance.telia.popover
 * @deprecated Use enhance.telia.popover event directly.
 * @returns {void}
 */
function initPopover() {
    $(document).trigger('enhance.telia.popover');
}
window.initPopover = initPopover;


(function(root, factory) {
    root['Chartist.plugins.legend'] = factory(root.Chartist);
}(this, function(Chartist) {
    'use strict';

    var defaultOptions = {
        className: '',
        legendNames: false,
        clickable: true
    };

    Chartist.plugins = Chartist.plugins || {};

    /**
     * This Chartist plugin creates a legend to show next to the chart.
     *
     * @param  {Object} options - An option map.
     * @returns {void}
     */
    Chartist.plugins.legend = function(options) {
        options = Chartist.extend({}, defaultOptions, options);

        return function(chart) {
            /**
             * Update chart.
             *
             * @param  {Event} e - Event.
             * @returns {void}
             */
            function legendChildClickEvent(e) {
                e.preventDefault();

                var seriesIndex = parseInt(this.getAttribute('data-legend'));
                var removedSeriesIndex = removedSeries.indexOf(seriesIndex);

                if (removedSeriesIndex > -1) {
                    // Add to series again.
                    removedSeries.splice(removedSeriesIndex, 1);
                    this.classList.remove('inactive');
                } else if (chart.data.series.length > 1) {
                    removedSeries.push(seriesIndex);
                    this.classList.add('inactive');
                }

                // Reset the series to original and remove each series that
                // is still removed again, to remain index order.
                var seriesCopy = originalSeries.slice(0);

                // Reverse sort the removedSeries to prevent removing the wrong index.
                removedSeries.sort().reverse();

                removedSeries.forEach(function(series) {
                    seriesCopy.splice(series, 1);
                });

                chart.data.series = seriesCopy;

                chart.update();
            }

            // Set a unique className for each series so that when a series is removed,
            // the other series still have the same color.
            if (options.clickable) {
                chart.data.series.forEach(function(series, seriesIndex) {
                    if (typeof series !== 'object') {
                        series = {
                            data: series
                        };
                    }

                    series.className = series.className || chart.options.classNames.series + '-' + Chartist.alphaNumerate(seriesIndex);
                });
            }

            var chartElement = chart.container;

            if (!chartElement) {
                return;
            }
            chartElement.innerHTML += '<ul class="ct-legend"></ul>';
            var legendElement = chartElement.querySelector('.ct-legend');

            if (chart instanceof Chartist.Pie) {
                legendElement.classList.add('ct-legend-inside');
            }
            if (typeof options.className === 'string' && options.className.length > 0) {
                legendElement.classList.add(options.className);
            }

            var removedSeries = [];
            var originalSeries = chart.data.series.slice(0);

            // Get the right array to use for generating the legend.
            var legendNames = chart.data.series;

            if (chart instanceof Chartist.Pie) {
                legendNames = chart.data.labels;
            }
            legendNames = options.legendNames || legendNames;

            // Loop through all legends to set each name in a list item.
            var legendHtml = legendNames.map(function(legend, i) {
                var legendName = legend.name || legend;

                return '<li class="ct-series-' + i.toString() + '" data-legend="' + i.toString() + '">' + legendName + '</li>';
            }).join('');

            legendElement.innerHTML = legendHtml;

            if (options.clickable) {
                var legendElementChildren = legendElement.querySelectorAll('li');

                Array.prototype.forEach.call(legendElementChildren, function(legendElementChild) {
                    legendElementChild.onclick = legendChildClickEvent;
                });
            }
        };
    };

    return Chartist.plugins.legend;
}));

/** ***************************************************************************
 *                                                                            *
 *  SVG Path Rounding Function                                                *
 *  Copyright (C) 2014 Yona Appletree                                         *
 *                                                                            *
 *  Licensed under the Apache License, Version 2.0 (the "License");           *
 *  you may not use this file except in compliance with the License.          *
 *  You may obtain a copy of the License at                                   *
 *                                                                            *
 *      http://www.apache.org/licenses/LICENSE-2.0                            *
 *                                                                            *
 *  Unless required by applicable law or agreed to in writing, software       *
 *  distributed under the License is distributed on an "AS IS" BASIS,         *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  *
 *  See the License for the specific language governing permissions and       *
 *  limitations under the License.                                            *
 *                                                                            *
 *****************************************************************************/

/**
 * SVG Path rounding function. Takes an input path string and outputs a path
 * string where all line-line corners have been rounded. Only supports absolute
 * commands at the moment.
 *
 * @param {Object} pathString - The SVG input path.
 * @param {number} radius - The amount to round the corners, either a value in the SVG
 *               coordinate space, or, if useFractionalRadius is true, a value
 *               from 0 to 1.
 * @param {boolean} useFractionalRadius - If true, the curve radius is expressed as a
 *               fraction of the distance between the point being curved and
 *               the previous and next points.
 * @param {Array<number>} corners - Array defining what corners to round starting from 1.
 * @returns {Object} A new SVG path string with the rounding.
 */
function roundPathCorners(pathString, radius, useFractionalRadius, corners) {
    /**
     * Move towards length.
     *
     * @param  {number} movingPoint - Moving point.
     * @param  {number} targetPoint - Target point.
     * @param  {number} amount - Amount.
     * @returns {Object} Object of points.
     */
    function moveTowardsLength(movingPoint, targetPoint, amount) {
        var width = (targetPoint.x - movingPoint.x);
        var height = (targetPoint.y - movingPoint.y);

        var distance = Math.sqrt(width * width + height * height);

        return moveTowardsFractional(movingPoint, targetPoint, Math.min(1, amount / distance));
    }

    /**
     * [moveTowardsFractional description].
     *
     * @param  {number} movingPoint - Moving point.
     * @param  {number} targetPoint - Target point.
     * @param  {number} fraction - Fraction.
     * @returns {Object} Object of points.
     */
    function moveTowardsFractional(movingPoint, targetPoint, fraction) {
        return {
            x: movingPoint.x + (targetPoint.x - movingPoint.x) * fraction,
            y: movingPoint.y + (targetPoint.y - movingPoint.y) * fraction
        };
    }

    /**
     * Adjusts the ending position of a command.
     *
     * @param  {number} cmd - Width of cmd.
     * @param  {Object} newPoint - Object of points.
     * @returns {void}
     */
    function adjustCommand(cmd, newPoint) {
        if (cmd.length > 2) {
            cmd[cmd.length - 2] = newPoint.x;
            cmd[cmd.length - 1] = newPoint.y;
        }
    }

    /**
     * Gives an {x, y} object for a command's ending position.
     *
     * @param  {number} cmd - Width of cmd.
     * @returns {Object} Object of points.
     */
    function pointForCommand(cmd) {
        return {
            x: parseFloat(cmd[cmd.length - 2]),
            y: parseFloat(cmd[cmd.length - 1])
        };
    }

    // Split apart the path, handing concatonated letters and numbers
    var pathParts = pathString
        .split(/[,\s]/)
        .reduce(function(parts, part) {
            var match = part.match('([a-zA-Z])(.+)');

            if (match) {
                parts.push(match[1]);
                parts.push(match[2]);
            } else {
                parts.push(part);
            }

            return parts;
        }, []);

    // Group the commands with their arguments for easier handling
    var commands = pathParts.reduce(function(commands, part) {
        if (parseFloat(part) == part && commands.length) {
            commands[commands.length - 1].push(part);
        } else {
            commands.push([part]);
        }

        return commands;
    }, []);

    // The resulting commands, also grouped
    var resultCommands = [];

    if (commands.length > 1) {
        var startPoint = pointForCommand(commands[0]);

        // Handle the close path case with a "virtual" closing line
        var virtualCloseLine = null;

        if (commands[commands.length - 1][0] == 'Z' && commands[0].length > 2) {
            virtualCloseLine = ['L', startPoint.x, startPoint.y];
            commands[commands.length - 1] = virtualCloseLine;
        }

        // We always use the first command (but it may be mutated)
        resultCommands.push(commands[0]);

        for (var cmdIndex = 1; cmdIndex < commands.length; cmdIndex++) {
            var prevCmd = resultCommands[resultCommands.length - 1];

            var curCmd = commands[cmdIndex];

            // Handle closing case
            var nextCmd = (curCmd == virtualCloseLine) ? commands[1] : commands[cmdIndex + 1];

            // Nasty logic to decide if this path is a candidite.
            // Also only round the corners definedin corners array

            if ((corners.indexOf(cmdIndex) !== -1) && nextCmd && prevCmd && (prevCmd.length > 2) && curCmd[0] == 'L' && nextCmd.length > 2 && nextCmd[0] == 'L') {
                // Calc the points we're dealing with
                var prevPoint = pointForCommand(prevCmd);
                var curPoint = pointForCommand(curCmd);
                var nextPoint = pointForCommand(nextCmd);

                // The start and end of the cuve are just our point moved towards the previous and next points, respectivly
                var curveStart;
                var curveEnd;

                curveStart = moveTowardsLength(curPoint, prevPoint, radius);
                curveEnd = moveTowardsLength(curPoint, nextPoint, radius);

                // Adjust the current command and add it
                adjustCommand(curCmd, curveStart);
                curCmd.origPoint = curPoint;
                resultCommands.push(curCmd);

                // The curve control points are halfway between the start/end of the curve and
                // the original point
                var startControl = moveTowardsFractional(curveStart, curPoint, .5);
                var endControl = moveTowardsFractional(curPoint, curveEnd, .5);

                // Create the curve
                var curveCmd = ['C', startControl.x, startControl.y, endControl.x, endControl.y, curveEnd.x, curveEnd.y];
                // Save the original point for fractional calculations

                curveCmd.origPoint = curPoint;
                resultCommands.push(curveCmd);
            } else {
                // Pass through commands that don't qualify
                resultCommands.push(curCmd);
            }
        }

        // Fix up the starting point and restore the close path if the path was orignally closed
        if (virtualCloseLine) {
            var newStartPoint = pointForCommand(resultCommands[resultCommands.length - 1]);

            resultCommands.push(['Z']);
            adjustCommand(resultCommands[0], newStartPoint);
        }
    } else {
        resultCommands = commands;
    }

    return resultCommands.reduce(function(str, c) { return str + c.join(' ') + ' '; }, '');
}

window.roundPathCorners = roundPathCorners;


/* globals Chartist, roundPathCorners */

(function(ElinaCharts, $) {
    var charts = [];

    /**
     * ElinaCharts.createDonutChart.
     *
     * @class
     * @param  {HTMLElement} element - The HTML element the ElinaCharts.createDonutChart should be bound to.
     * @param  {Object} data - Data to show.
     * @param  {Object} options - An option map.
     * @returns {void}
     */
    ElinaCharts.createDonutChart = function(element, data, options) {
        var $element = $(element);
        var $content = $('<div class="chart__content"></div>');
        var defaultOptions = {
            donut: true,
            unit: 'GB',
            startAngle: 0,
            donutWidth: 20,
            showLabel: false,
            donutLabelHeader: false,
            donutLabel: '',
            donutLabelStyle: false,
            plugins: []
        };

        /**
         * Create legend.
         *
         * @param  {Array[]} labels - Array of labels.
         * @param  {Array[]} series - Array of series.
         * @returns {Array[]} Array of legend.
         */
        function createMixedLegend(labels, series) {
            var legend = [];

            for (var i = 0; i < labels.length; i++) {
                legend.push(('<span class="chart__legend-item ' + series[i].className + '" >' + labels[i] + '</span> ' + '(' + series[i].value + ' ' + options.unit + ')'));
            }

            return legend;
        }

        options = Chartist.extend({}, defaultOptions, options);
        if (!$element.hasClass('chart--legend-tooltip')) {
            options.plugins.push(
                Chartist.plugins.legend({
                    legendNames: createMixedLegend(data.labels, data.series),
                    clickable: false,
                    className: 'chart__legend'
                })
            );
        }
        $element.addClass('chart--donut chart--visible');
        if (!$element.find('.chart__content').length) {
            $element.append($content);
        } else {
            $content = $element.find('.chart__content');
        }
        var pie = new Chartist.Pie($content[0], data, options);

        if ($element.hasClass('chart--legend-tooltip')) {
            pie.on('draw', function(data) {
                if (data.type === 'slice') {
                    // creating meta object for tooltip content
                    var value;
                    var meta = {};
                    var className = '';

                    if (typeof  data.series == 'object') {
                        value = data.series.value;
                        meta.value = value;
                    }
                    meta.seriesName = Chartist.alphaNumerate(data.index);
                    var template = '<p class="chart__tooltip-row chart__tooltip-row--no-indicator ' + (meta.className || '') + '">' + (meta.name || '') +
                    ' <span class="chart__tooltip-data">' + value + ' ' + options.unit + '</span></p>';

                    if (meta.customInfo && meta.customInfo.length) {
                        for (var i = 0; i < meta.customInfo.length; i++) {
                            template += '<p class="chart__tooltip-row ' + (meta.customInfo[i].className || '') + ' ' + meta.customInfo[i].className + '--' + meta.seriesName + '">' +
                            (meta.customInfo[i].name || '') + ' <span class="chart__tooltip-data">' +
                            (meta.customInfo[i].value  || '') + '</span></p>';
                        }
                    }

                    var pathString = data.element.attr('d');

                    var path = new Chartist.Svg('path', {
                        d: pathString,
                        style: 'fill-opacity: 1',
                        'ct:value': value,
                        'data-toggle': 'tooltip',
                        'data-variant': 'blue',
                        'data-trigger': 'hover',
                        'data-placement': 'right',
                        'data-html': true,
                        'data-content': template,
                        'data-original-title': pie.data.labels[data.index],
                        'data-title-size': 'h5'

                    }, 'ct-slice-donut ' + className);

                    // now replace it in svg
                    data.element.replace(path);
                }
            });
        }

        pie.on('created', function(data) {
            var g = data.svg.elem('g', {
                class: ''
            });

            g.elem('text', {
                class: 'chart__label ' + (!options.donutLabelStyle ? '' : 'chart__label--' + options.donutLabelStyle),
                x: '50%',
                y: '50%',
                'text-anchor': 'middle',
                dy: '.3em'
            }).text(options.donutLabel);

            if (options.donutLabelHeader) {
                g.elem('text', {
                    class: 'chart__label chart__label--small',
                    x: '50%',
                    y: '35%',
                    'text-anchor': 'middle',
                    dy: '.3em'
                }).text(options.donutLabelHeader);
            }
            // trigger resize to update containing plugins
            $(document).trigger('resize.telia');

            $('path[data-toggle="tooltip"]').teliaTooltip({
                container: 'body'
            });
        });
        charts.push(pie);

        return pie;
    };
    ElinaCharts.idCounter = 0;

    /**
     * Increment idCounter.
     *
     * @returns {void}
     */
    function getIncrementedId() {
        return ElinaCharts.idCounter++;
    }

    /**
     * Create masks.
     *
     * @param  {Object} data - Data for masks.
     * @param  {Object} options - Options for masks.
     * @returns {Object} Mask object.
     */
    function createMasks(data, options) {
        // Project the threshold value on the chart Y axis
        // Select the defs element within the chart or create a new one
        var defs = data.svg.querySelector('defs') || data.svg.elem('defs');
        var projectedThreshold = data.chartRect.height() - data.axisY.projectValue(options.threshold) + data.chartRect.y2;
        var width = data.svg.width();
        var height = data.svg.height();

        // Create mask for upper part above threshold
        defs.elem('mask', {
            x: 0,
            y: 0,
            width: width,
            height: height,
            id: options.maskNames.aboveThreshold + options.maskId
        })
            .elem('rect', {
                x: 0,
                y: 0,
                width: width,
                height: projectedThreshold,
                fill: 'url(#pattern-stripe)'
            });

        return defs;
    }

    /**
     * Find bottom values.
     *
     * @param  {Array[]} series - Array of series.
     * @returns {Array[]} Array of series.
     */
    function findTopBottomValues(series) {
        // identify parts of bars that should have rounded corners
        for (var i = 0; i < series[0].length; i++) {
            var firstFound = false;
            var lastIndex;
            var value;

            for (var j = 0; j < series.length; j++) {
                if (typeof series[j][i] === 'object') {
                    if (series[j][i].value && series[j][i].value !== 0) {
                        lastIndex = j;
                        if (!firstFound) {
                            firstFound = true;
                            series[j][i].first = true;
                        }
                    }
                } else if (series[j][i] !== 0) {
                    lastIndex = j;
                    if (!firstFound) {
                        value = series[j][i];
                        series[j][i] = {
                            value: value
                        };
                        firstFound = true;
                        series[j][i].first = true;
                    }
                }
            }
            if (firstFound) {
                if (typeof series[lastIndex][i] !== 'object') {
                    value = series[lastIndex][i];
                    series[lastIndex][i] = {
                        value: value
                    };
                }
                series[lastIndex][i].last = true;
            }
        }

        return series;
    }

    /**
     * ElinaCharts.createBarChart.
     *
     * @class
     * @param  {HTMLElement} element - The HTML element the ElinaCharts.createBarChart should be bound to.
     * @param  {Object} data - Data to show.
     * @param  {Object} options - An option map.
     * @returns {void}
     */
    ElinaCharts.createBarChart = function(element, data, options) {
        var $element = $(element);
        var $content = $('<div class=\'chart__content\'></div>');
        var defaultOptions = {
            unit: 'GB',
            stackBars: true,
            maskId: getIncrementedId(),
            axisY: {
                /**
                 * Format label with unit.
                 *
                 * @param   {number} value - Chart value.
                 * @returns {string} Formatted label.
                 */
                labelInterpolationFnc: function(value) {
                    return value !== 0 ? value + ' ' + options.unit : value;
                },
                labelOffset: {
                    x: 0,
                    y: 6
                },
                offset: 45,
                onlyInteger: true,
                scaleMinSpace: 50
            },
            barWidth: 20,
            threshold: false,
            classNames: {
                aboveThreshold: 'ct-threshold-above',
                belowThreshold: 'ct-threshold-below'
            },
            maskNames: {
                aboveThreshold: 'ct-threshold-mask-above',
                belowThreshold: 'ct-threshold-mask-below'
            }
        };

        options = Chartist.extend({}, defaultOptions, options);
        data.series = findTopBottomValues(data.series);

        $element.addClass('chart--bar chart--visible');

        if (!$element.find('.chart__content').length) {
            $element.append($content);
        } else {
            $content = $element.find('.chart__content');
        }

        var bar = new Chartist.Bar($content[0], data, options).on('draw', function(data) {
            // replacing lines with paths for rounded corners
            if (data.type === 'bar') {
                var item = data.series[data.index];
                var className = '';

                if (typeof data == 'object' && item.className) {
                    className = item.className;
                }
                var pathString = [
                    'M',
                    data.x1 - options.barWidth / 2,
                    data.y1,
                    'L',
                    data.x1 - options.barWidth / 2,
                    data.y2,
                    'L',
                    data.x2 + options.barWidth / 2,
                    data.y2,
                    'L',
                    data.x2 + options.barWidth / 2,
                    data.y1 + ' Z'
                ].join(' ');

                if (data.series[data.index].first) {
                    pathString = roundPathCorners(pathString, options.barWidth / 2, false, [3, 4]);
                }
                if (data.series[data.index].last) {
                    pathString = roundPathCorners(pathString, options.barWidth / 2, false, [1, 2]);
                }
                // creating meta object for tooltip content
                var value;
                var meta = {};

                if (typeof  data.series[data.index] == 'object') {
                    value = data.series[data.index].value;
                    meta = data.series[data.index];
                } else {
                    value = data.series[data.index];
                    meta.value = value;
                }
                meta.tick = data.axisX.ticks[data.index];
                meta.seriesName = Chartist.alphaNumerate(data.seriesIndex);
                var template = '<p class="chart__tooltip-row chart__tooltip-row--' +
                meta.seriesName + ' ' + (meta.className || '') + '">' + (meta.name || '') +
                ' <span class="chart__tooltip-data">' + value + ' ' + options.unit + '</span></p>';

                if (meta.customInfo && meta.customInfo.length) {
                    for (var i = 0; i < meta.customInfo.length; i++) {
                        template += '<p class="chart__tooltip-row ' + (meta.customInfo[i].className || '') + ' ' + meta.customInfo[i].className + '--' + meta.seriesName + '">' +
                        (meta.customInfo[i].name || '') + ' <span class="chart__tooltip-data">' +
                        (meta.customInfo[i].value  || '') + '</span></p>';
                    }
                }

                var path = new Chartist.Svg('path', {
                    d: pathString,
                    style: 'fill-opacity: 1',
                    'ct:value': value,
                    'data-toggle': 'tooltip',
                    'data-variant': 'blue',
                    'data-trigger': 'hover',
                    'data-placement': 'right',
                    'data-html': true,
                    'data-content': template,
                    'data-original-title': meta.tick,
                    'data-title-size': 'h5'

                }, 'ct-bar ct-bar-path ' + className);

                // now replace it in svg
                var result = data.element.replace(path);

                if (options.threshold !== false) {
                    result.parent()
                        .elem(result._node.cloneNode(true))
                        .attr({
                            mask: 'url(#' + options.maskNames.aboveThreshold + options.maskId + ')'
                        })
                        .addClass(options.classNames.aboveThreshold);
                }
            }
        }).on('created', function(data) {
            if (options.threshold !== false) {
                createMasks(data, options);
            }
            var defs = data.svg.querySelector('defs') || data.svg.elem('defs');
            var pattern = defs.elem('pattern', {
                id: 'pattern-stripe',
                width: 10,
                height: 10,
                patternUnits: 'userSpaceOnUse',
                patternTransform: 'rotate(45)'
            });
            var mask = defs.elem('mask', {
                id: 'pattern-mask'
            });

            pattern.elem('rect', {
                width: 5,
                height: 10,
                transform: 'translate(0,0)',
                fill: 'white'
            });
            mask.elem('rect', {
                x: 0,
                y: 0,
                width: '100%',
                height: '100%',
                fill: 'url(#pattern-stripe)'
            });
            $('path[data-toggle="tooltip"]').teliaTooltip({
                container: 'body'
            });
        });

        return bar;
    };

    /**
     * ElinaCharts.initCharts.
     *
     * @returns {void}
     */
    ElinaCharts.initCharts = function() {
        $('[data-chart=\'barchart\']').each(function() {
            var $this   = $(this);
            var data    = $this.data('chartData');
            var options = $this.data('chartOptions');

            if (!data) {
                return;
            }

            ElinaCharts.createBarChart(this, data, options);
        });

        $('[data-chart=\'donutchart\']').each(function() {
            var $this   = $(this);
            var data    = $this.data('chartData');
            var options = $this.data('chartOptions');

            if (!data) {
                return;
            }

            ElinaCharts.createDonutChart(this, data, options);
        });
    };

    /**
     * ElinaCharts.update.
     *
     * @returns {void}
     */
    ElinaCharts.update = function() {
        Object.keys(charts).forEach(function(key) {
            charts[key].update();
        });
    };
})(window.ElinaCharts = window.ElinaCharts || {}, $);

/**
 * Auto-init the plugin on DOM ready.
 */
$(function() {
    window.ElinaCharts.initCharts();
});

/**
 * Refresh plugin on refresh event.
 */
$(document).on('refresh.telia.chart', function() {
    window.ElinaCharts.update();
});

;(function($, window, document) {
    var pluginName = 'teliaCollapseTrigger';
    var defaults = {};
    var initSelector = '[data-collapse-trigger]';
    var breakpoints = {
        xs: '(min-width: 0px)',
        sm: '(min-width: 600px)',
        md: '(min-width: 1024px)',
        lg: '(min-width: 1200px)'
    };

    /**
     * CollapseTrigger.
     *
     * @class
     * @param {HTMLElement} element - The HTML element the CollapseTrigger should be bound to.
     * @param {Object} options - An option map.
     */
    function CollapseTrigger(element, options) {
        this.element = element;
        this.$element = $(this.element);
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.value = jQuery.parseJSON(this.$element.attr('data-collapse-trigger'));
        this.$target = this.getTargetFromTrigger();
        this.userHasManipulated = false;

        this.init();
    }

    /**
     * Initialize the plugin.
     *
     * @function init
     * @returns {void}
     */
    CollapseTrigger.prototype.init = function() {
        this.toggleCollapse();

        this.$element.on('click.telia.collapseTrigger', this.clickHandler.bind(this));
        $(window).on('resize.telia.collapseTrigger.' + this.$target[0].id, this.toggleCollapse.bind(this));
    };

    /**
     * Toggle the collapse.
     *
     * @function toggleCollapse
     * @returns {void}
     */
    CollapseTrigger.prototype.toggleCollapse = function() {
        if (!this.userHasManipulated) {
            if (this.checkMedia()) {
                this.$target.collapse('show', true);
            } else {
                this.$target.collapse('hide');
            }
        }
    };

    /**
     * Check if current media condition matches element rules.
     *
     * @function checkMedia
     * @returns {boolean} If is open.
     */
    CollapseTrigger.prototype.checkMedia = function() {
        var open = false;

        for (var key in this.value) {
            if (this.value.hasOwnProperty(key)) {
                if (window.matchMedia(breakpoints[key]).matches) {
                    if (this.value[key]) {
                        open = true;
                    } else {
                        open = false;
                    }
                }
            }
        }

        return open;
    };

    /**
     * On click, set userHasManipulated to true to disallow reponsive logic.
     *
     * @function clickHandler
     * @returns {void}
     */
    CollapseTrigger.prototype.clickHandler = function() {
        this.userHasManipulated = true;
        this.destroy();
    };

    /**
     * Get target collapse element from trigger.
     *
     * @function getTargetFromTrigger
     * @returns {*|HTMLElement} JQuery element.
     */
    CollapseTrigger.prototype.getTargetFromTrigger = function() {
        var href;
        var target = this.$element.attr('data-target') || (href = this.$element.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '');

        return $(target);
    };

    /**
     * Destroy plugin.
     *
     * @function destroy
     * @returns {void}
     */
    CollapseTrigger.prototype.destroy = function() {
        this.$element.off('click.telia.collapseTrigger');
        $(window).off('resize.telia.collapseTrigger.' + this.$target[0].id);
        this.$element.removeData('plugin_' + pluginName);
    };

    /**
     * Create jQuery plugin.
     *
     * @param {Object} options - Extended options.
     * @listens telia.collapseTrigger
     * @returns {Array<CollapseTrigger>} Array of CollapseTriggers.
     */
    $.fn[pluginName] = function(options) {
        var args = Array.prototype.slice.call(arguments, 1);

        return this.each(function() {
            var instance = $.data(this, 'plugin_' + pluginName);

            if (!instance) {
                if (options === 'destroy') {
                    return;
                }

                $.data(this, 'plugin_' + pluginName, new CollapseTrigger(this, options));
            } else if (typeof options === 'string') {
                instance[options].apply(instance, args);
            }
        });
    };

    /**
     * @param {Event} event
     * @event enhance.telia.collapseTrigger
     */
    $(document).on('enhance.telia.collapseTrigger', function(event) {
        $(event.target).find(initSelector)[pluginName]();
    });

    /**
     * @param {Event} event
     * @event destroy.telia.collapseTrigger
     */
    $(document).on('destroy.telia.collapseTrigger', function(event) {
        $(event.target).find(initSelector)[pluginName]('destroy');
    });
})(jQuery, window, document);

/**
 * Auto-init the plugin on DOM ready.
 * @fires enhance.telia.collapseTrigger
 */
$(function() {
    $(document).trigger('enhance.telia.collapseTrigger');
});

;(function($) {
    var pluginName = 'teliaCollapseModal';
    var defaults = {
        closeButton: true,
        offset: 25
    };

    /**
     * CollapseModal.
     *
     * @class
     * @param {HTMLElement} element - The HTML element the CollapseModal should be bound to.
     * @param {Object} options - An option map.
     */
    function CollapseModal(element, options) {
        this.$element       = $(element);
        this.options        = $.extend({}, $.fn.collapse.Constructor.DEFAULTS, defaults, options);
        this.id             = element.id;
        this.$selector       = $('[data-toggle="collapse-modal"][href="#' + element.id + '"], [data-toggle="collapse-modal"][data-target="#' + element.id + '"]');
        this.$trigger = this.$selector.length ? this.$selector : $(this.options.trigger);
        this.transitioning = null;
        this.$isOpen = false;
        this.$arrow = $('<div class="collapse__arrow"/>');
        this.$close = $('<a href="#" data-hide="collapse-modal" class="collapse__close">' + telia.icon.render('#close-round') + '</a>');
        this.$wrapper = $('<div class="collapse-modal"></div>');
        this.$container = $('<div class="container"></div>');
        this.$overlay = $('<div class="collapse-modal__overlay"></div>');
        this.$scrollable   = this.$element;
        this.$parent = $('body');

        if (this.options.toggle) this.toggle();
    };

    if (!$.fn.collapse) throw new Error('CollapseModal requires collapse.js');

    CollapseModal.prototype = $.extend({}, $.fn.collapse.Constructor.prototype);

    // For easier access to parent class methods
    CollapseModal.prototype.super = $.extend({}, $.fn.collapse.Constructor.prototype);

    CollapseModal.constructor = CollapseModal;

    /**
     * Toggle collapse-modal.
     *
     * @function toggle
     * @returns {void}
     */
    CollapseModal.prototype.toggle = function() {
        this[(this.$element.hasClass('is-visible') || this.$element.hasClass('collapse--visible')) ? 'hide' : 'show']();
    };

    /**
     * Show collapse-modal.
     *
     * @function show
     * @returns {void}
     */
    CollapseModal.prototype.show = function() {
        if (this.transitioning || (this.$element.hasClass('is-visible') && this.$element.hasClass('collapse--visible'))) {
            this.super.show.call(this);

            return;
        }

        // only one collapse-modal can be visible at a time
        this.hideOtherCollapseModals();

        this.$container.append(this.$element);
        this.$wrapper.append(this.$container).append(this.$overlay);
        this.$wrapper.removeClass('hidden').addClass('is-collapsing');

        this.$overlay.on('click.telia.collapseModal', this.hide.bind(this));
        $(document).on('keyup.telia.modal', this.keyboard.bind(this));

        var that = this;

        setTimeout(function() {
            that.setWrapperPosition();
            that.$parent.append(that.$wrapper);
            that.$isOpen = true;
            that.super.show.call(that);
            that.$wrapper.removeClass('is-collapsing');
        }, 10);
    };

    /**
     * Hide collapse-modal.
     *
     * @function hide
     * @returns {void}
     */
    CollapseModal.prototype.hide = function() {
        /**
         * Add and remove classes if collapse-modal is hidden.
         *
         * @returns {void}
         */
        function completed() {
            this.$wrapper.addClass('hidden').removeClass('is-collapsing');
        }

        if (this.$isOpen) {
            this.$wrapper.addClass('is-collapsing');
            this.super.hide.call(this);
            this.$isOpen = false;

            if (!$.support.transition) return completed.call(this);

            this.$element.one('hidden.bs.collapse', $.proxy(completed, this));
        }
    };

    /**
     * Close collapseModal on esc.
     *
     * @function keyboard
     * @param  {Event} event - Event.
     * @returns {void}
     */
    CollapseModal.prototype.keyboard = function(event) {
        if (event.keyCode == 27) {
            this.hide();
        }
    };

    /**
     * Set top position of wrapper.
     *
     * @function setWrapperPosition
     * @returns {void}
     */
    CollapseModal.prototype.setWrapperPosition = function() {
        var top = this.$trigger.offset().top + this.$trigger.height() + this.options.offset;

        this.$wrapper.css('top', top);
    };

    /**
     * Find visible collapse-modals and hide them.
     *
     * @function hideOtherCollapseModals
     * @returns {void}
     */
    CollapseModal.prototype.hideOtherCollapseModals = function() {
        var others = this.$parent.find('.collapse').not(this.$element).filter(function() {
            return typeof $(this).data('telia.collapseModal') == 'object';
        });

        if (others.length) {
            others[pluginName]('hide');
        }
    };

    /**
     * Handle window resize.
     *
     * @function resizeHandler
     * @returns {void}
     */
    CollapseModal.prototype.resizeHandler = function() {
        this.super.resizeHandler.call(this);
        this.setWrapperPosition();
    };

    /**
     * Get target from trigger.
     *
     * @param  {JQuery} $trigger - Trigger element.
     * @returns {JQuery} Target element.
     */
    function getTargetFromTrigger($trigger) {
        var href;
        // strip for ie7
        var target = $trigger.attr('data-target') || (href = $trigger.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '');

        return $(target);
    }

    /**
     * Create plugin.
     *
     * @param {Object} option - Extended options.
     * @param {Array[]} args - Extended arguments.
     * @returns {Array<CollapseModal>} Array of CollapseModals.
     */
    $.fn[pluginName] = function(option, args) {
        return this.each(function() {
            var $this = $(this);
            var data = $this.data('telia.collapseModal');
            var options = $.extend({}, defaults, $this.data(), typeof option == 'object' && option);

            if (!data && options.toggle && (/show|hide/).test(option)) options.toggle = false;
            if (!data) $this.data('telia.collapseModal', (data = new CollapseModal(this, options)));
            if (typeof option == 'string') data[option](args);
        });
    };

    /**
     * @param {Event} e
     * @event click.telia.CollapseModal.data-api
     */
    $(document).on('click.telia.collapseModal', '[data-toggle="collapse-modal"]', function(event) {
        var $this   = $(this);

        if (!$this.attr('data-target')) event.preventDefault();
        var $target = getTargetFromTrigger($this);
        var data = $target.data('telia.collapseModal');
        var extraOptions = {
            trigger: this
        };

        if (!$this.attr('data-target')) {
            // strip for ie7
            extraOptions.target = $this.attr('href').replace(/.*(?=#[^\s]+$)/, '');
            event.preventDefault();
        }

        var option = data ? 'toggle' : $.extend({}, $this.data(), extraOptions);

        $target[pluginName](option);
    });

    /**
     * @param {Event} e
     * @event click.telia.collapseModal
     */
    $(document).on('click.telia.collapseModal', '[data-show="collapse-modal"]', function(event) {
        var $this   = $(this);

        if (!$this.attr('data-target')) event.preventDefault();
        var $target = getTargetFromTrigger($this);
        var extraOptions = {
            trigger: this
        };

        if (!$this.attr('data-target')) {
            // strip for ie7
            extraOptions.target = $this.attr('href').replace(/.*(?=#[^\s]+$)/, '');
            event.preventDefault();
        }

        var option = 'show';

        $target[pluginName](option);
    });

    /**
     * @param {Event} e
     * @event click.telia.collapseModal
     */
    $(document).on('click.telia.collapseModal', '[data-hide="collapse-modal"]', function(event) {
        var $this   = $(this);

        if (!$this.attr('data-target')) event.preventDefault();
        var $target = getTargetFromTrigger($this);
        var extraOptions = {
            trigger: this
        };

        if (!$this.attr('data-target')) {
            // strip for ie7
            extraOptions.target = $this.attr('href').replace(/.*(?=#[^\s]+$)/, '');
            event.preventDefault();
        }

        var option = 'hide';

        $target[pluginName](option);
    });
})(jQuery, window, document);

/* ========================================================================
* Bootstrap: dropdown.js v3.3.6
* http://getbootstrap.com/javascript/#dropdowns
* ========================================================================
* Copyright 2011-2015 Twitter, Inc.
* Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
* ======================================================================== */

+function($) {
    'use strict';

    var backdrop = '.dropdown__backdrop';
    var toggle   = '[data-toggle="dropdown"]';

    /**
     * Dropdown.
     *
     * @class
     * @param {HTMLElement} element - Dropdown HTML element.
     */
    function Dropdown(element) {
        $(element).on('click.bs.dropdown', this.toggle);
    };

    Dropdown.VERSION = '3.3.6';

    /**
     * Get parent element.
     *
     * @param  {JQuery} $this - Dropdown JQuery element.
     * @returns {JQuery} JQuery parent element.
     */
    function getParent($this) {
        var selector = $this.attr('data-target');

        if (!selector) {
            selector = $this.attr('href');
            // strip for ie7
            selector = selector && (/#[A-Za-z]/).test(selector) && selector.replace(/.*(?=#[^\s]*$)/, '');
        }

        var $parent = selector && $(selector);

        return $parent && $parent.length ? $parent : $this.parent();
    }

    /**
     * Clear menus.
     *
     * @param  {Event} e - Event.
     * @returns {void}
     */
    function clearMenus(e) {
        if (e && e.which === 3) return;
        $(backdrop).remove();
        $(toggle).each(function() {
            var $this         = $(this);
            var $parent       = getParent($this);
            var relatedTarget = {
                relatedTarget: this
            };

            if (!$parent.hasClass('open')) return;

            if (e && e.type == 'click' && (/input|textarea/i).test(e.target.tagName) && $.contains($parent[0], e.target)) return;

            $parent.trigger(e = $.Event('hide.bs.dropdown', relatedTarget));

            if (e.isDefaultPrevented()) return;

            $this.attr('aria-expanded', 'false');
            $parent.removeClass('open').trigger($.Event('hidden.bs.dropdown', relatedTarget));
        });
    }

    /**
     * Toggle dropdown.
     *
     * @param  {Event} e - Toggle event.
     * @returns {void}
     */
    Dropdown.prototype.toggle = function(e) {
        var $this = $(this);

        if ($this.is('.disabled, :disabled')) return;

        var $parent  = getParent($this);
        var isActive = $parent.hasClass('open');

        clearMenus();

        if (!isActive) {
            if ('ontouchstart' in document.documentElement) {
                // if mobile we use a backdrop because click events don't delegate
                $(document.createElement('div'))
                    .addClass('dropdown__backdrop')
                    .insertAfter($(this))
                    .on('click', clearMenus);
            }

            var relatedTarget = {
                relatedTarget: this
            };

            $parent.trigger(e = $.Event('show.bs.dropdown', relatedTarget));

            if (e.isDefaultPrevented()) return;

            $this
                .trigger('focus')
                .attr('aria-expanded', 'true');

            $parent
                .toggleClass('open')
                .one('hide.bs.dropdown', function(e) {
                    e.preventDefault();
                })
                .trigger($.Event('shown.bs.dropdown', relatedTarget));
        }

        e.preventDefault();
    };

    /**
     * Bind usage with keyboard.
     *
     * @param  {Event} e - Click event.
     * @returns {void}
     */
    Dropdown.prototype.keydown = function(e) {
        if (!(/(38|40|27|32)/).test(e.which) || (/input|textarea/i).test(e.target.tagName)) return;

        var $this = $(this);

        e.preventDefault();
        e.stopPropagation();

        if ($this.is('.disabled, :disabled')) return;

        var $parent  = getParent($this);
        var isActive = $parent.hasClass('open');

        if (!isActive && e.which != 27 || isActive && e.which == 27) {
            if (e.which == 27) $parent.find(toggle).trigger('focus');

            return $this.trigger('click');
        }

        var desc = ' li:not(.disabled):visible a';
        var $items = $parent.find('.dropdown__menu' + desc);

        if (!$items.length) return;

        var index = $items.index(e.target);

        // up
        if (e.which == 38 && index > 0)                 index--;
        // down
        if (e.which == 40 && index < $items.length - 1) index++;
        if (!~index)                                    index = 0;

        $items.eq(index).trigger('focus');
    };

    /**
     * Create plugin.
     *
     * @param {Object} option - Extended options.
     * @returns {Array<Dropdown>} Array of Dropdowns.
     */
    function Plugin(option) {
        return this.each(function() {
            var $this = $(this);
            var data  = $this.data('bs.dropdown');

            if (!data) $this.data('bs.dropdown', (data = new Dropdown(this)));
            if (typeof option == 'string') data[option].call($this);
        });
    }

    var old = $.fn.dropdown;

    $.fn.dropdown             = Plugin;
    $.fn.dropdown.Constructor = Dropdown;

    // DROPDOWN NO CONFLICT
    // ====================

    $.fn.dropdown.noConflict = function() {
        $.fn.dropdown = old;

        return this;
    };

    // APPLY TO STANDARD DROPDOWN ELEMENTS
    // ===================================

    $(document)
        .on('click.bs.dropdown.data-api', clearMenus)
        .on('click.bs.dropdown.data-api', '.dropdown form', function(e) { e.stopPropagation(); })
        .on('click.bs.dropdown.data-api', toggle, Dropdown.prototype.toggle)
        .on('keydown.bs.dropdown.data-api', toggle, Dropdown.prototype.keydown)
        .on('keydown.bs.dropdown.data-api', '.dropdown__menu', Dropdown.prototype.keydown);
}(jQuery);

;(function($) {
    var pluginName = 'teliaEqualHeight';
    var defaults = {};
    var initSelector = '[data-equalheight]';

    /**
     * EqualHeight.
     *
     * @class
     * @param {JQuery} $elements - Elements in the group.
     * @param {Object} options - An option map.
     */
    function EqualHeight($elements, options) {
        this.$elements = $elements;
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.resizeTimer = 0;

        this.init();
    }

    /**
     * Initialize the plugin.
     *
     * @function init
     * @returns {void}
     */
    EqualHeight.prototype.init = function() {
        this.setHeights();

        $(window).on('resize.telia.equalHeight', this.resizeHandler.bind(this));
        $(document).on('refresh.telia.equalHeight', this.refreshHandler.bind(this));
    };

    /**
     * Delay the resize method to make sure resizing has finished.
     *
     * @function resizeHandler
     * @returns {void}
     */
    EqualHeight.prototype.resizeHandler = function() {
        var that = this;

        clearTimeout(this.resizeTimer);
        this.resizeTimer = setTimeout(function() {
            that.setHeights();
        }, 250);
    };

    /**
     * Check if current instance is in target and set new heights if true.
     *
     * @function refreshHandler
     * @param   {Event} event - An event object.
     * @param   {HTMLElement} target - Containing HTML element.
     * @returns {void}
     */
    EqualHeight.prototype.refreshHandler = function(event, target) {
        var contains = false;

        // loop through all elements to see if any of them is inside target
        this.$elements.each(function() {
            if ($.contains(target, this)) {
                contains = true;
            }
        });

        if (target && contains) {
            this.setHeights();
        }
    };

    /**
     * Set equal heights to all elements in the group.
     *
     * @function setHeights
     * @returns {void}
     */
    EqualHeight.prototype.setHeights = function() {
        var currentTallest = 0;
        var currentRowStart = 0;
        var rowDivs = [];
        var $el;
        var topPosition = 0;

        this.$elements.each(function() {
            $el = $(this);
            $el.css({
                'min-height': '0'
            });
            topPosition = $el.offset().top;
            if (currentRowStart != topPosition) {
                for (var currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
                    rowDivs[currentDiv].css({
                        'min-height': currentTallest
                    });
                }
                rowDivs.length = 0;
                currentRowStart = topPosition;
                currentTallest = $el.height() + 1;
                rowDivs.push($el);
            } else {
                rowDivs.push($el);
                currentTallest = (currentTallest < $el.height()) ? ($el.height() + 1) : (currentTallest);
            }
            if (rowDivs.length > 1) {
                for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
                    rowDivs[currentDiv].css({
                        'min-height': currentTallest
                    });
                }
            }
        });
    };

    /**
     * Destroy the plugin instance.
     *
     * @function destroy
     * @returns {void}
     */
    EqualHeight.prototype.destroy = function() {
        $(window).off('resize.telia.equalHeight');

        this.$elements.css('min-height', '');
        $(this.$elements[0]).removeData('plugin_' + pluginName);
    };

    /**
     * Create jQuery plugin.
     *
     * @param {Object} options - Extended options.
     * @listens telia.equalHeight
     * @returns {Array<EqualHeight>} Array of EqualHeights.
     */
    $.fn[pluginName] = function(options) {
        var args = Array.prototype.slice.call(arguments, 1);
        var groups = {};

        this.each(function() {
            var $this = $(this);
            var groupId = $this.attr('data-equalheight');

            if (groupId in groups) {
                groups[groupId] = groups[groupId].add($this);
            } else {
                groups[groupId] = $this;
            }
        });

        $.each(groups, function() {
            var first = this[0];
            var instance = $.data(first, 'plugin_' + pluginName);

            if (!instance) {
                if (options === 'destroy') {
                    return;
                }

                $.data(first, 'plugin_' + pluginName, new EqualHeight(this, options));
            } else if (typeof options === 'string') {
                instance[options].apply(instance, args);
            }
        });

        return this;
    };

    /**
     * @event enhance.telia.equalHeight
     * @param   {Event} event
     */
    $(document).on('enhance.telia.equalHeight', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]();
    });

    /**
     * @event destroy.telia.equalHeight
     * @param   {Event} event
     */
    $(document).on('destroy.telia.equalHeight', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]('destroy');
    });
}(jQuery));

/**
 * Auto-init the plugin on DOM ready.
 * @fires enhance.telia.equalHeight
 */
$(function() {
    $(document).trigger('enhance.telia.equalHeight');
});

/**
 * Old init for compatibility.
 * @fires enhance.telia.equalHeight
 * @deprecated Use enhance.telia.equalHeight event directly.
 */
$(document).on('equalheight', function() {
    $(document).trigger('enhance.telia.equalHeight');
});

;(function($, window, document) {
    var pluginName = 'teliaCompare';
    var defaults = {};
    var initSelector = '.compare';

    /**
     * Compare.
     *
     * @class
     * @param {HTMLElement} element - The HTML element the Compare should be bound to.
     * @param {Object} options - An option map.
     */
    function Compare(element, options) {
        this.element = element;
        this.$element = $(this.element);
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;

        this.table = this.$element.children('.compare__table');
        this.tableHeight = this.table.height();
        this.productsWrapper = this.table.find('.compare__products-wrapper');
        this.tableColumns = this.productsWrapper.children('.compare__products-columns');
        this.productsRow = this.tableColumns.find('.grid');
        this.products = this.productsRow.eq(0).find('.compare__product:not(.compare__product--placeholder)');
        this.productsPlaceholders = this.productsRow.eq(0).find('.compare__product--placeholder');
        this.placeholder = $('.compare__product--placeholder');
        this.productsNumber = this.products.length;
        this.productsPlaceholdersNumber = this.productsPlaceholders.length;
        this.productWidth = this.products.eq(0).offsetWidth;
        this.leftScrolling = false;
        this.navigation = this.table.children('.compare__navigation');

        this.init();
    }

    /**
     * Initialize the plugin.
     *
     * @function init
     * @returns {void}
     */
    Compare.prototype.init = function() {
        this.bindEvents();
        this.checkResize();
        this.print();

        // First column doesn't take right height without this hack
        $('[data-fake-equalheight]').each(function() {
            var $this = $(this);

            $this.attr('data-equalheight', $this.attr('data-fake-equalheight'));
        });

        setTimeout(function() {
            $(document).trigger('enhance.telia.equalHeight');
        }, 100);
    };

    /**
     * Bind events.
     *
     * @function bindEvents
     * @returns {void}
     */
    Compare.prototype.bindEvents = function() {
        var that = this;

        // detect scroll left inside producst table
        that.productsWrapper.on('scroll', this.scroll.bind(this));

        // scroll inside products table
        this.navigation.on('click', '.compare__arrow', this.updateSlider.bind(this));

        $(window).on('resize.telia.compare', this.checkResize.bind(this));

        // detect window scroll - fix product top-info on scrolling
        $(window).on('scroll.telia.compare', this.checkScrolling.bind(this));
    };

    /**
     * Window scroll method.
     *
     * @function scroll
     * @returns {void}
     */
    Compare.prototype.scroll = function() {
        var that = this;

        if (!this.leftScrolling) {
            this.leftScrolling = true;
            (!window.requestAnimationFrame) ? setTimeout(function() { that.updateLeftScrolling(); }, 250) : window.requestAnimationFrame(function() { that.updateLeftScrolling(); });
        }
    };

    /**
     * Update scrolling left position.
     *
     * @function updateLeftScrolling
     * @returns {void}
     */
    Compare.prototype.updateLeftScrolling = function() {
        var scrollLeft = this.productsWrapper.scrollLeft();

        (scrollLeft > 0) ? this.table.addClass('compare__table--scrolling') : this.table.removeClass('compare__table--scrolling');

        this.leftScrolling =  false;

        this.updateNavigationVisibility(scrollLeft);
    };

    /**
     * Update navigation visibility.
     *
     * @function updateNavigationVisibility
     * @param {number} scrollLeft - Position of scrolled from left.
     * @returns {void}
     */
    Compare.prototype.updateNavigationVisibility = function(scrollLeft) {
        if (scrollLeft > 0) {
            this.navigation.find('.compare__arrow--left').removeClass('compare__arrow--inactive');
        } else {
            this.navigation.find('.compare__arrow--left').addClass('compare__arrow--inactive');
        }

        if (scrollLeft < this.tableColumns.outerWidth(true) - this.productsWrapper.width() && this.tableColumns.outerWidth(true) > this.productsWrapper.width()) {
            this.navigation.find('.compare__arrow--right').removeClass('compare__arrow--inactive');
        } else {
            this.navigation.find('.compare__arrow--right').addClass('compare__arrow--inactive');
        }
    };

    /**
     * Update properties.
     *
     * @function updateProperties
     * @returns {void}
     */
    Compare.prototype.updateProperties = function() {
        this.productWidth = this.products.eq(0).offsetWidth;
        var productsWidth = this.productWidth * this.productsNumber;
        var productPlaceholdersWidth = this.productWidth * this.productsPlaceholdersNumber;
        var windowWidth = $(window).width() - 20;

        if ((productsWidth + productPlaceholdersWidth) >= windowWidth) {
            this.tableColumns.css('width', productsWidth + 'px');
            this.placeholder.addClass('hidden');
        } else {
            this.tableColumns.css('width', productsWidth + productPlaceholdersWidth + 'px');
            this.placeholder.removeClass('hidden');
        }

        if ($(window).width() < this.tableColumns.outerWidth(true)) {
            this.navigation.find('.compare__arrow--right').removeClass('compare__arrow--inactive');
        } else {
            this.navigation.find('.compare__arrow--right').addClass('compare__arrow--inactive');
        }
    };

    /**
     * Update slider.
     *
     * @function updateSlider
     * @param {Event} event - Event.
     * @returns {void}
     */
    Compare.prototype.updateSlider = function(event) {
        var scrollLeft = this.productsWrapper.scrollLeft();

        scrollLeft = $(event.target).hasClass('compare__arrow--right') ? scrollLeft + this.productWidth : scrollLeft - this.productWidth;

        if (scrollLeft < 0) {
            scrollLeft = 0;
        } else if (scrollLeft > this.tableColumns.outerWidth(true) - this.productsWrapper.width()) {
            scrollLeft = this.tableColumns.outerWidth(true) - this.productsWrapper.width();
        }

        this.productsWrapper.animate({
            scrollLeft: scrollLeft
        }, 200);
    };

    /**
     * Check scrolling.
     *
     * @function checkScrolling
     * @returns {void}
     */
    Compare.prototype.checkScrolling = function() {
        var windowWidth = $(window).width();

        if (windowWidth < 1024) {
            var scrollTop = $(window).scrollTop();

            if (this.$element.length) {
                var compareTop = this.$element.offset().top;
                var compareHeight = this.$element.height();

                if (scrollTop > compareTop) {
                    this.$element.find('.compare__arrow').addClass('compare__arrow--fixed');
                } else {
                    this.$element.find('.compare__arrow').removeClass('compare__arrow--fixed');
                }

                if (scrollTop > compareHeight) {
                    this.$element.find('.compare__arrow').removeClass('compare__arrow--fixed');
                }
            }
        } else {
            this.$element.find('.compare__arrow').removeClass('compare__arrow--fixed');
        }
    };

    /**
     * Check resize.
     *
     * @function checkResize
     * @returns {void}
     */
    Compare.prototype.checkResize = function() {
        this.updateProperties();
    };

    /**
     * Make page ready for print.
     *
     * @function print
     * @returns {void}
     */
    Compare.prototype.print = function() {
        /**
         * Initialize equalHeight.
         *
         * @returns {void}
         */
        function setEqualHeight() {
            $(document).trigger('enhance.telia.equalHeight');
        };

        if (window.matchMedia) {
            var mediaQueryList = window.matchMedia('print');

            mediaQueryList.addListener(function(mql) {
                if (mql.matches) {
                    setEqualHeight();
                }
            });
        }

        window.onbeforeprint = setEqualHeight;
        window.onafterprint = setEqualHeight;
    };

    /**
     * Destroy the plugin instance.
     *
     * @function destroy
     * @returns {void}
     */
    Compare.prototype.destroy = function() {
        $(window).off('resize.telia.compare');
        $(window).off('scroll.telia.compare');

        this.$element.removeData('plugin_' + pluginName);
    };

    /**
     * Create jQuery plugin.
     *
     * @param {Object} options - Extended options.
     * @listens telia.compare
     * @returns {Array<Compare>} Array of Compares.
     */
    $.fn[pluginName] = function(options) {
        var args = Array.prototype.slice.call(arguments, 1);

        return this.each(function() {
            var instance = $.data(this, 'plugin_' + pluginName);

            if (!instance) {
                if (options === 'destroy') {
                    return;
                }

                $.data(this, 'plugin_' + pluginName, new Compare(this, options));
            } else if (typeof options === 'string') {
                instance[options].apply(instance, args);
            }
        });
    };

    /**
     * @param {Event} event
     * @event enhance.telia.compare
     */
    $(document).on('enhance.telia.compare', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]();
    });

    /**
     * @param {Event} event
     * @event destroy.telia.compare
     */
    $(document).on('destroy.telia.compare', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]('destroy');
    });
})(jQuery, window, document);

/**
 * Auto-init the plugin on DOM ready.
 * @fires enhance.telia.compare
 */
$(function() {
    $(document).trigger('enhance.telia.compare');
});

/**
 * Old init for compatibility.
 *
 * @fires enhance.telia.compare
 * @deprecated Use enhance.telia.compare event directly.
 * @returns {void}
 */
function initCompare() {
    $(document).trigger('enhance.telia.compare');
}
window.initCompare = initCompare;

;(function($, window, document) {
    var pluginName = 'teliaFilter';
    var defaults = {};
    var initSelector = '.js-filter';

    /**
     * Filter.
     *
     * @class
     * @param {HTMLElement} element - The HTML element the Filter should be bound to.
     * @param {Object} options - An option map.
     */
    function Filter(element, options) {
        this.element = element;
        this.$element = $(this.element);
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;

        this.$filter = this.$element.find('.filter__main');
        this.$desktopFilter = this.$element.find('.filter__main-wrapper');
        this.$sidemenu = this.$element.find('.filter__sidemenu');
        this.$mobileFilter = this.$sidemenu.find('.filter__main');
        this.$filterTrigger = this.$element.find('.filter__trigger');
        this.$showFilter = this.$element.find('.js-showFilter');
        this.$closeFilter = this.$element.find('.js-closeFilter');
        this.$mobileContent = this.$element.find('.filter__mobile-content');
        this.$mobileBtnGroup = this.$element.find('.filter__mobile-btn-group');
        this.$backdrop = $('filter-backdrop');
        this.filterResizeTimer;
        this.BREAK = 1024;

        this.init();
    }

    /**
     * Initialize the plugin.
     *
     * @function init
     * @returns {void}
     */
    Filter.prototype.init = function() {
        this.bindEvents();
        this.setFiltersOpen();
    };

    /**
     * Bind events.
     *
     * @function bindEvents
     * @returns {void}
     */
    Filter.prototype.bindEvents = function() {
        this.$filterTrigger.on('click', this.toggleFilterCollapse.bind(this));
        this.$showFilter.on('click.telia.filter', this.openFilterSidemenu.bind(this));
        this.$closeFilter.on('click.telia.filter', this.closeFilterSidemenu.bind(this));
        $(document).on('click', '.filter-backdrop', this.closeFilterSidemenu.bind(this));
        $(window).on('resize.telia.filter', this.resize.bind(this));
    };

    /**
     * Toggle filter collapse and not tooltip.
     *
     * @param {Event} event - Event.
     * @returns {void}
     */
    Filter.prototype.toggleFilterCollapse = function(event) {
        var $target = $(event.target);

        if (!$target.hasClass('filter__tooltip')) {
            $target.toggleClass('is-active');
            $target.next('.filter__collapse').collapse('toggle');
        }
    };

    /**
     * Resize window initializes plugin.
     *
     * @function resize
     * @returns {void}
     */
    Filter.prototype.resize = function() {
        var that = this;

        clearTimeout(this.filterResizeTimer);
        this.filterResizeTimer = setTimeout(function() {
            if ($(window).width() >= that.BREAK) {
                if (that.$sidemenu.hasClass('is-visible')) {
                    that.closeFilterSidemenu();
                }
                if (!$.trim(that.$desktopFilter.html()).length) {
                    that.cloneFilter();
                }
            }
        }, 250);
    };

    /**
     * Clone filter from mobile to desktop.
     *
     * @function cloneFilter
     * @returns {void}
     */
    Filter.prototype.cloneFilter = function() {
        this.$mobileContent.find('.filter__mobile-btn-group').remove();
        this.$mobileFilter.appendTo(this.$desktopFilter);
    };

    /**
     * Open mobile filter inside sidemenu and show backdrop.
     *
     * @function openFilterSidemenu
     * @returns {void}
     */
    Filter.prototype.openFilterSidemenu = function() {
        if (this.$filter.find('.form-check:first').data('xhref') && this.$filter.find('.form-check:first').data('xhref').toString().toLowerCase().indexOf('&setmobilefilteropen=true') < 0) {
            this.$filter.find('.form-check').each(function() {
                var $this = $(this);

                $this.data('xhref', $this.data('xhref') + '&setMobileFilterOpen=true');
            });
        }

        if (!$.trim(this.$mobileContent.html()).length) {
            this.$mobileFilter = this.$filter.appendTo(this.$mobileContent);
            this.$mobileBtnGroup.clone(true).appendTo(this.$mobileFilter);
        }

        this.$mobileFilter.addClass('is-visible');
        this.$sidemenu.addClass('is-visible');

        window.disableScroll();
        this.showFilterBackDrop();
    };

    /**
     * Close mobile filter inside sidemenu and remove backdrop.
     *
     * @function closeFilterSidemenu
     * @param {Event} event - Event.
     * @returns {void}
     */
    Filter.prototype.closeFilterSidemenu = function(event) {
        if (event) {
            event.preventDefault();
        }

        this.$sidemenu.removeClass('is-visible');
        this.$mobileFilter.removeClass('is-visible');

        if (this.$filter.find('.form-check:first').data('xhref') && this.$filter.find('.form-check:first').data('xhref').toString().toLowerCase().indexOf('&setmobilefilteropen=true') > 0) {
            this.$filter.find('.form-check').each(function() {
                var $this = $(this);

                $this.data('xhref', $this.data('xhref').replace('&setmobilefilteropen=true', ''));
            });
        }

        window.enableScroll();
        this.hideFilterBackDrop();
    };

    /**
     * Show backdrop.
     *
     * @function showFilterBackDrop
     * @returns {void}
     */
    Filter.prototype.showFilterBackDrop = function() {
        var that = this;

        if (this.$backdrop.length > 0) {
            return;
        }

        this.$backdrop = $('<div class="filter-backdrop" />').appendTo($('body'));

        setTimeout(function() {
            that.$backdrop.addClass('is-visible');
        }, 10);
    };

    /**
     * Hide backdrop.
     *
     * @function hideFilterBackDrop
     * @returns {void}
     */
    Filter.prototype.hideFilterBackDrop = function() {
        var that = this;

        this.$backdrop.removeClass('is-visible');

        setTimeout(function() {
            that.$backdrop.remove();
            that.$backdrop = $('filter-backdrop');
        }, 200);
    };

    /**
     * Set filters open.
     *
     * @function setFiltersOpen
     * @returns {void}
     */
    Filter.prototype.setFiltersOpen = function() {
        // Backend specific code
        if ($('#SetMobileFiltersOpen').val() == 'True' && this.$showFilter.is(':visible')) {
            this.openFilterSidemenu();
        }
    };

    /**
     * Initialize slick plugin.
     *
     * @function destroy
     * @returns {void}
     */
    Filter.prototype.destroy = function() {
        $(window).off('resize.telia.filter');
        this.$showFilter.off('click.telia.filter');
        this.$closeFilter.off('click.telia.filter');

        this.closeFilterSidemenu();
        this.cloneFilter();

        this.$element.removeData('plugin_' + pluginName);
    };

    /**
     * Create jQuery plugin.
     *
     * @param {Object} options - Extended options.
     * @listens telia.filter
     * @returns {Array<Filter>} Array of Filters.
     */
    $.fn[pluginName] = function(options) {
        var args = Array.prototype.slice.call(arguments, 1);

        return this.each(function() {
            var instance = $.data(this, 'plugin_' + pluginName);

            if (!instance) {
                if (options === 'destroy') {
                    return;
                }

                $.data(this, 'plugin_' + pluginName, new Filter(this, options));
            } else if (typeof options === 'string') {
                instance[options].apply(instance, args);
            }
        });
    };

    /**
     * @param {Event} event
     * @event enhance.telia.filter
     */
    $(document).on('enhance.telia.filter', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]();
    });

    /**
     * @param {Event} event
     * @event destroy.telia.filter
     */
    $(document).on('destroy.telia.filter', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]('destroy');
    });
})(jQuery, window, document);

/**
 * Auto-init the plugin on DOM ready.
 * @fires enhance.telia.filter
 */
$(function() {
    $(document).trigger('enhance.telia.filter');
});

/**
 * Old init for compatibility.
 *
 * @fires enhance.telia.filter
 * @deprecated Use enhance.telia.filter event directly.
 * @returns {void}
 */
function initFilters() {
    $(document).trigger('enhance.telia.filter');
}
window.initFilters = initFilters;

;(function($, window, document) {
    var pluginName = 'teliaFittext';
    var defaults = {};
    var initSelector = '.js-fittext';

    /**
     * Fittext.
     *
     * @class
     * @param {HTMLElement} element - The HTML element the Fittext should be bound to.
     * @param {Object} options - An option map.
     */
    function Fittext(element, options) {
        this.element = element;
        this.$element = $(this.element);
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.originalFontSize = parseInt(this.$element.css('font-size'));

        this.init();
    }

    /**
     * Initialize the plugin.
     *
     * @function init
     * @returns {void}
     */
    Fittext.prototype.init = function() {
        this.$element.wrapInner('<span class="js-fittext__inner" />');

        this.bindEvents();
        this.resizer();
    };

    /**
     * Bind events.
     *
     * @function bindEvents
     * @returns {void}
     */
    Fittext.prototype.bindEvents = function() {
        var that = this;

        $(window).on('resize.telia.fittext orientationchange.telia.fittext', this.resizer.bind(this));

        $(document).on('refresh.telia.fittext', function(event, target) {
            if (target && $.contains(target, that.element)) {
                that.resizer();
            }
        });
    };

    /**
     * Resize text.
     *
     * @function resizer
     * @returns {void}
     */
    Fittext.prototype.resizer = function() {
        var newFontSize = this.originalFontSize;
        var $inner = this.$element.find('.js-fittext__inner');

        this.$element.css('font-size', newFontSize);

        while ($inner.width() > this.$element.width()) {
            newFontSize -= 1;
            this.$element.css('font-size', newFontSize);
        }
    };

    /**
     * Destroy the plugin instance.
     *
     * @function destroy
     * @returns {void}
     */
    Fittext.prototype.destroy = function() {
        this.$element.css('font-size', '');
        $(window).off('resize.telia.fittext orientationchange.telia.fittext');

        this.$element.removeData('plugin_' + pluginName);
    };

    /**
     * Create jQuery plugin.
     *
     * @param {Object} options - Extended options.
     * @listens telia.fittext
     * @returns {Array<Fittext>} Array of Fittexts.
     */
    $.fn[pluginName] = function(options) {
        var args = Array.prototype.slice.call(arguments, 1);

        return this.each(function() {
            var instance = $.data(this, 'plugin_' + pluginName);

            if (!instance) {
                if (options === 'destroy') {
                    return;
                }

                $.data(this, 'plugin_' + pluginName, new Fittext(this, options));
            } else if (typeof options === 'string') {
                instance[options].apply(instance, args);
            }
        });
    };

    /**
     * @param {Event} event
     * @event enhance.telia.fittext
     */
    $(document).on('enhance.telia.fittext', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]();
    });

    /**
     * @param {Event} event
     * @event destroy.telia.fittext
     */
    $(document).on('destroy.telia.fittext', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]('destroy');
    });
})(jQuery, window, document);

/**
 * Auto-init the plugin on DOM ready.
 * @fires enhance.telia.fittext
 */
$(function() {
    $(document).trigger('enhance.telia.fittext');
});

/**
 * Old init for compatibility.
 *
 * @fires enhance.telia.fittext
 * @deprecated Use enhance.telia.fittext event directly.
 * @returns {void}
 */
function initFittext() {
    $(document).trigger('enhance.telia.fittext');
}
window.initFittext = initFittext;

;(function($, window, document) {
    var pluginName = 'teliaConditionalDataValidator';
    var defaults = {};
    var initSelector = '.form-conditional-trigger';

    /**
     * ConditionalDataValidator.
     *
     * @class
     * @param {HTMLElement} element - The HTML element the ConditionalDataValidator should be bound to.
     * @param {Object} options - An option map.
     */
    function ConditionalDataValidator(element, options) {
        this.element = element;
        this.$element = $(this.element);
        this.options = $.extend({}, defaults, options, this.$element.data());
        this._defaults = defaults;
        this._name = pluginName;

        this._init();
    }

    /**
     * Initialize the plugin.
     *
     * @function _init
     * @private
     * @returns {void}
     */
    ConditionalDataValidator.prototype._init = function() {
        this.$element.on('change.telia.conditionalDataValidator', this._change.bind(this));
    };

    /**
     * Input change handler.
     *
     * @function _change
     * @private
     * @param {Event} event - An event object.
     * @returns {void}
     */
    ConditionalDataValidator.prototype._change = function(event) {
        var self = event.currentTarget;

        if ($(self).is(':checked')) {
            this._run($(self), $(self).attr('name'), $(self).val());
        } else {
            this._run($(self), $(self).attr('name'), false);
        }
    };

    /**
     * Run the data validator.
     *
     * @function _run
     * @private
     * @param   {JQuery} obj - Input to validate.
     * @param   {string} name - Input's name attribute.
     * @param   {string|boolean} value - Input's value or boolean.
     * @param   {boolean} ignorePreback - If true, ignore preback function.
     * @returns {void}
     */
    ConditionalDataValidator.prototype._run = function(obj, name, value, ignorePreback) {
        var that = this;
        var groups = $('[data-conditional-group="' + name + '"]');

        groups.each(function() {
            var group = $(this);

            group.addClass('hidden');
            if (group.attr('data-conditional-preback') && !ignorePreback) {
                if (typeof window[group.attr('data-conditional-preback')] == 'function') {
                    if (window[group.attr('data-conditional-preback')]({
                        target: obj,
                        group: group
                    })) {
                        return setTimeout(function() { that._run(obj, name, value, true); }, 50);
                    }
                }
            }
            var condition = group.attr('data-conditional-' + name + '-value');

            if (condition == 'any' && value !== false) {
                group.removeClass('hidden');
            } else if (value !== false) {
                var conditions = condition.split(',');

                if ($.inArray(value, conditions) !== -1) {
                    group.removeClass('hidden');
                } else if (group.find('input:checked').length > 0) {
                    that._run(obj, group.find('input:checked').prop('checked', false).attr('name'), false);
                }
            }
            if (group.attr('data-conditional-callback')) {
                if (typeof window[group.attr('data-conditional-callback')] == 'function') {
                    window[group.attr('data-conditional-callback')]({
                        target: obj,
                        group: group
                    });
                }
            }
            // trigger refresh event to refresh equalheight etc
            $(document).trigger('refresh.telia', this);
        });
    };

    /**
     * Destroy the plugin instance.
     *
     * @function destroy
     * @returns {void}
     */
    ConditionalDataValidator.prototype.destroy = function() {
        this.$element.removeData('plugin_' + pluginName);
    };

    /**
     * Create jQuery plugin.
     *
     * @param {Object} options - Extended options.
     * @listens telia.conditionalDataValidator
     * @returns {Array<ConditionalDataValidator>|Number} Array of ConditionalDataValidators or Number if using getValue method.
     */
    $.fn[pluginName] = function(options) {
        var args = Array.prototype.slice.call(arguments, 1);
        var returnValue = [];

        this.each(function() {
            var instance = $.data(this, 'plugin_' + pluginName);

            if (!instance) {
                if (options === 'destroy') {
                    return;
                }

                var newInstance = new ConditionalDataValidator(this, options);

                returnValue.push(newInstance);
                $.data(this, 'plugin_' + pluginName, newInstance);
            } else if (typeof options === 'string') {
                var methodReturnValue = instance[options].apply(instance, args);

                if (methodReturnValue) {
                    returnValue = methodReturnValue;
                } else {
                    returnValue.push(instance);
                }
            }
        });

        return returnValue;
    };

    /**
     * @param {Event} event
     * @event enhance.telia.conditionalDataValidator
     */
    $(document).on('enhance.telia.conditionalDataValidator', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]();
    });

    /**
     * @param {Event} event
     * @event enhance.form.textfield
     * @deprecated use enhance.telia.textfield event instead
     */
    $(document).on('enhance.form.conditional', function(event) {
        $(document).trigger('enhance.telia.conditionalDataValidator', event);
    });

    /**
     * @param {Event} event
     * @event destroy.telia.conditionalDataValidator
     */
    $(document).on('destroy.telia.conditionalDataValidator', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]('destroy');
    });
})(jQuery, window, document);

/**
 * Auto-init the plugin on DOM ready.
 * @fires enhance.telia.conditionalDataValidator
 */
$(function() {
    $(document).trigger('enhance.telia.conditionalDataValidator');
});

var Datepicker;

(function(window, $, undefined) {
    var pluginName = 'datepicker';
    var autoInitSelector = '.datepicker-here';
    var $body;
    var $datepickersContainer;
    var containerBuilt = false;
    var baseTemplate = '' +
            '<div class="datepicker">' +
            '<i class="datepicker--pointer"></i>' +
            '<nav class="datepicker--nav"></nav>' +
            '<div class="datepicker--content"></div>' +
            '</div>';
    var defaults = {
        classes: '',
        inline: false,
        language: 'et',
        startDate: new Date(),
        firstDay: '',
        weekends: [6, 0],
        dateFormat: '',
        altField: '',
        altFieldDateFormat: '@',
        toggleSelected: true,
        keyboardNav: true,

        position: 'bottom left',
        offset: 12,

        view: 'days',
        minView: 'days',

        showOtherMonths: true,
        selectOtherMonths: true,
        moveToOtherMonthsOnSelect: true,

        showOtherYears: true,
        selectOtherYears: true,
        moveToOtherYearsOnSelect: true,

        minDate: '',
        maxDate: '',
        disableNavWhenOutOfRange: true,

        // Boolean or Number
        multipleDates: false,
        multipleDatesSeparator: ',',
        range: false,

        todayButton: false,
        clearButton: false,

        showEvent: 'focus',
        autoClose: false,

        // navigation
        monthsFiled: 'monthsShort',
        prevHtml: telia.icon.render('#arrow-left'),
        nextHtml: telia.icon.render('#arrow-right'),
        navTitles: {
            days: 'MM yyyy',
            months: 'yyyy',
            years: 'yyyy1 - yyyy2'
        },

        // events
        onSelect: '',
        onChangeMonth: '',
        onChangeYear: '',
        onChangeDecade: '',
        onChangeView: '',
        onRenderCell: ''
    };
    var hotKeys = {
        ctrlRight: [17, 39],
        ctrlUp: [17, 38],
        ctrlLeft: [17, 37],
        ctrlDown: [17, 40],
        shiftRight: [16, 39],
        shiftUp: [16, 38],
        shiftLeft: [16, 37],
        shiftDown: [16, 40],
        altUp: [18, 38],
        altRight: [18, 39],
        altLeft: [18, 37],
        altDown: [18, 40],
        ctrlShiftUp: [16, 17, 38]
    };
    var datepicker;

    /**
     * Datepicker.
     *
     * @class
     * @param   {HTMLElement} el - The HTML element the Datepicker should be bound to.
     * @param   {Object} options - An option map.
     * @returns {void}
     */
    Datepicker = function(el, options) {
        this.el = el;
        this.$el = $(el);

        this.opts = $.extend(true, {}, defaults, options, this.$el.data());

        if ($body == undefined) {
            $body = $('body');
        }

        if (!this.opts.startDate) {
            this.opts.startDate = new Date();
        }

        if (this.el.nodeName == 'INPUT') {
            this.elIsInput = true;
        }

        if (this.opts.altField) {
            this.$altField = typeof this.opts.altField == 'string' ? $(this.opts.altField) : this.opts.altField;
        }

        this.inited = false;
        this.visible = false;
        // Need to prevent unnecessary rendering
        this.silent = false;

        this.currentDate = this.opts.startDate;
        this.currentView = this.opts.view;
        this._createShortCuts();
        this.selectedDates = [];
        this.views = {};
        this.keys = [];
        this.minRange = '';
        this.maxRange = '';

        this.init();
    };

    datepicker = Datepicker;

    datepicker.prototype = {
        viewIndexes: ['days', 'months', 'years'],

        /**
         * Initialize plugin.
         *
         * @returns {void}
         */
        init: function() {
            if (!containerBuilt && !this.opts.inline && this.elIsInput) {
                this._buildDatepickersContainer();
            }
            this._buildBaseHtml();
            this._defineLocale(this.opts.language);
            this._syncWithMinMaxDates();

            if (this.elIsInput) {
                if (!this.opts.inline) {
                    // Set extra classes for proper transitions
                    this._setPositionClasses(this.opts.position);
                    this._bindEvents();
                }
                if (this.opts.keyboardNav) {
                    this._bindKeyboardEvents();
                }
                this.$datepicker.on('mousedown', this._onMouseDownDatepicker.bind(this));
                this.$datepicker.on('mouseup', this._onMouseUpDatepicker.bind(this));
            }

            if (this.opts.classes) {
                this.$datepicker.addClass(this.opts.classes);
            }

            this.views[this.currentView] = new Datepicker.Body(this, this.currentView, this.opts);
            this.views[this.currentView].show();
            this.nav = new Datepicker.Navigation(this, this.opts);
            this.view = this.currentView;

            this.$datepicker.on('mouseenter', '.datepicker--cell', this._onMouseEnterCell.bind(this));
            this.$datepicker.on('mouseleave', '.datepicker--cell', this._onMouseLeaveCell.bind(this));

            this.inited = true;
        },

        /**
         * Create shortcuts.
         *
         * @private
         * @returns {void}
         */
        _createShortCuts: function() {
            this.minDate = this.opts.minDate ? this.opts.minDate : new Date(-8639999913600000);
            this.maxDate = this.opts.maxDate ? this.opts.maxDate : new Date(8639999913600000);
        },

        /**
         * Bind events.
         *
         * @private
         * @returns {void}
         */
        _bindEvents: function() {
            this.$el.on(this.opts.showEvent, this._onShowEvent.bind(this));
            this.$el.on('blur', this._onBlur.bind(this));
            this.$el.on('input', this._onInput.bind(this));
            $(window).on('resize', this._onResize.bind(this));
        },

        /**
         * Bind keyboard events.
         *
         * @private
         * @returns {void}
         */
        _bindKeyboardEvents: function() {
            this.$el.on('keydown', this._onKeyDown.bind(this));
            this.$el.on('keyup', this._onKeyUp.bind(this));
            this.$el.on('hotKey', this._onHotKey.bind(this));
        },

        /**
         * Check if day is on weekend.
         *
         * @param   {string} day - Day name.
         * @returns {boolean} True if day is on weekend.
         */
        isWeekend: function(day) {
            return this.opts.weekends.indexOf(day) !== -1;
        },

        /**
         * Define locale.
         *
         * @private
         * @param   {string} lang - Language string.
         * @returns {void}
         */
        _defineLocale: function(lang) {
            if (typeof lang == 'string') {
                this.loc = Datepicker.language[lang];
                if (!this.loc) {
                    this.loc = $.extend(true, {}, Datepicker.language.et);
                }

                this.loc = $.extend(true, {}, Datepicker.language.et, Datepicker.language[lang]);
            } else {
                this.loc = $.extend(true, {}, Datepicker.language.et, lang);
            }

            if (this.opts.dateFormat) {
                this.loc.dateFormat = this.opts.dateFormat;
            }

            if (this.opts.firstDay) {
                this.loc.firstDay = this.opts.firstDay;
            }
        },

        /**
         * Build datepicker container.
         *
         * @private
         * @returns {void}
         */
        _buildDatepickersContainer: function() {
            containerBuilt = true;
            $body.append('<div class="datepickers-container" id="datepickers-container"></div>');
            $datepickersContainer = $('#datepickers-container');
        },


        /**
         * Build base HTML.
         *
         * @private
         * @returns {void}
         */
        _buildBaseHtml: function() {
            var $appendTarget;
            var $inline = $('<div class="datepicker-inline">');

            if (this.el.nodeName == 'INPUT') {
                if (!this.opts.inline) {
                    $appendTarget = $datepickersContainer;
                } else {
                    $appendTarget = $inline.insertAfter(this.$el);
                }
            } else {
                $appendTarget = $inline.appendTo(this.$el);
            }

            this.$datepicker = $(baseTemplate).appendTo($appendTarget);
            this.$content = $('.datepicker--content', this.$datepicker);
            this.$nav = $('.datepicker--nav', this.$datepicker);
        },

        /**
         * Trigger onChange callback.
         *
         * @private
         * @returns {void}
         */
        _triggerOnChange: function() {
            if (!this.selectedDates.length) {
                return this.opts.onSelect('', '', this);
            }

            var selectedDates = this.selectedDates;
            var parsedSelected = datepicker.getParsedDate(selectedDates[0]);
            var formattedDates;
            var _this = this;
            var dates = new Date(parsedSelected.year, parsedSelected.month, parsedSelected.date);

            formattedDates = selectedDates.map(function(date) {
                return _this.formatDate(_this.loc.dateFormat, date);
            }).join(this.opts.multipleDatesSeparator);

            // Create new dates array, to separate it from original selectedDates
            if (this.opts.multipleDates) {
                dates = selectedDates.map(function(date) {
                    var parsedDate = datepicker.getParsedDate(date);

                    return new Date(parsedDate.year, parsedDate.month, parsedDate.date);
                });
            }

            this.opts.onSelect(formattedDates, dates, this);
        },

        /**
         * Set date to next day/month/year.
         *
         * @returns {void}
         */
        next: function() {
            var d = this.parsedDate;
            var o = this.opts;

            switch (this.view) {
                case 'days':
                    this.date = new Date(d.year, d.month + 1, 1);
                    if (o.onChangeMonth) o.onChangeMonth(this.parsedDate.month, this.parsedDate.year);
                    break;
                case 'months':
                    this.date = new Date(d.year + 1, d.month, 1);
                    if (o.onChangeYear) o.onChangeYear(this.parsedDate.year);
                    break;
                case 'years':
                    this.date = new Date(d.year + 10, 0, 1);
                    if (o.onChangeDecade) o.onChangeDecade(this.curDecade);
                    break;
            }
        },

        /**
         * Set date to previous day/month/year.
         *
         * @returns {void}
         */
        prev: function() {
            var d = this.parsedDate;
            var o = this.opts;

            switch (this.view) {
                case 'days':
                    this.date = new Date(d.year, d.month - 1, 1);
                    if (o.onChangeMonth) o.onChangeMonth(this.parsedDate.month, this.parsedDate.year);
                    break;
                case 'months':
                    this.date = new Date(d.year - 1, d.month, 1);
                    if (o.onChangeYear) o.onChangeYear(this.parsedDate.year);
                    break;
                case 'years':
                    this.date = new Date(d.year - 10, 0, 1);
                    if (o.onChangeDecade) o.onChangeDecade(this.curDecade);
                    break;
            }
        },

        /**
         * Format date to locale.
         *
         * @param   {string} string - Date format string.
         * @param   {Date} date - Date object.
         * @returns {string} Formatted date.
         */
        formatDate: function(string, date) {
            date = date || this.date;
            var result = string;
            var boundary = this._getWordBoundaryRegExp;
            var locale = this.loc;
            var decade = datepicker.getDecade(date);
            var d = datepicker.getParsedDate(date);

            switch (true) {
                case (/@/).test(result):
                    result = result.replace(/@/, date.getTime());
                    // falls through
                case (/dd/).test(result):
                    result = result.replace(boundary('dd'), d.fullDate);
                    // falls through
                case (/d/).test(result):
                    result = result.replace(boundary('d'), d.date);
                    // falls through
                case (/DD/).test(result):
                    result = result.replace(boundary('DD'), locale.days[d.day]);
                    // falls through
                case (/D/).test(result):
                    result = result.replace(boundary('D'), locale.daysShort[d.day]);
                    // falls through
                case (/mm/).test(result):
                    result = result.replace(boundary('mm'), d.fullMonth);
                    // falls through
                case (/m/).test(result):
                    result = result.replace(boundary('m'), d.month + 1);
                    // falls through
                case (/MM/).test(result):
                    result = result.replace(boundary('MM'), this.loc.months[d.month]);
                    // falls through
                case (/M/).test(result):
                    result = result.replace(boundary('M'), locale.monthsShort[d.month]);
                    // falls through
                case (/yyyy/).test(result):
                    result = result.replace(boundary('yyyy'), d.year);
                    // falls through
                case (/yyyy1/).test(result):
                    result = result.replace(boundary('yyyy1'), decade[0]);
                    // falls through
                case (/yyyy2/).test(result):
                    result = result.replace(boundary('yyyy2'), decade[1]);
                    // falls through
                case (/yy/).test(result):
                    result = result.replace(boundary('yy'), d.year.toString().slice(-2));
            }

            return result;
        },

        /**
         * Get word boundary regExp.
         *
         * @private
         * @param   {string} sign - Date format partial string.
         * @returns {RegExp} RegExp object.
         */
        _getWordBoundaryRegExp: function(sign) {
            return new RegExp('\\b(?=[a-zA-Z0-9äöüßÄÖÜ<])' + sign + '(?![>a-zA-Z0-9äöüßÄÖÜ])');
        },

        /**
         * Select date.
         *
         * @param   {Date} date - New date instance.
         * @returns {void}
         */
        selectDate: function(date) {
            var _this = this;
            var opts = _this.opts;
            var d = _this.parsedDate;
            var selectedDates = _this.selectedDates;
            var len = selectedDates.length;
            var newDate = '';

            if (!(date instanceof Date)) return;

            if (_this.view == 'days') {
                if ((date.getMonth() != d.month || date.getFullYear() != d.year) && opts.moveToOtherMonthsOnSelect) {
                    newDate = new Date(date.getFullYear(), date.getMonth(), 1);
                }
            }

            if (_this.view == 'years') {
                if (date.getFullYear() != d.year && opts.moveToOtherYearsOnSelect) {
                    newDate = new Date(date.getFullYear(), 0, 1);
                }
            }

            if (newDate) {
                _this.silent = true;
                _this.date = newDate;
                _this.silent = false;
                _this.nav._render();
            }

            if (opts.multipleDates) {
                if (len === opts.multipleDates) return;
                if (!_this._isSelected(date)) {
                    _this.selectedDates.push(date);
                }
            } else if (opts.range) {
                if (len == 2) {
                    _this.selectedDates = [date];
                    _this.minRange = date;
                    _this.maxRange = '';
                } else if (len == 1) {
                    _this.selectedDates.push(date);
                    if (!_this.maxRange) {
                        _this.maxRange = date;
                    } else {
                        _this.minRange = date;
                    }
                    _this.selectedDates = [_this.minRange, _this.maxRange];
                } else {
                    _this.selectedDates = [date];
                    _this.minRange = date;
                }
            } else {
                _this.selectedDates = [date];
            }

            _this._setInputValue();

            if (opts.onSelect && opts.minView == this.currentView) {
                _this._triggerOnChange();
            }

            if (opts.autoClose) {
                if (!opts.multipleDates && !opts.range) {
                    _this.hide();
                } else if (opts.range && _this.selectedDates.length == 2) {
                    _this.hide();
                }
            }

            _this.views[this.currentView]._render();
        },

        /**
         * Remove date from selected dates.
         *
         * @param   {Date} date - Date instance.
         * @returns {boolean} True if date is removed.
         */
        removeDate: function(date) {
            var selected = this.selectedDates;
            var _this = this;

            if (!(date instanceof Date)) return;

            return selected.some(function(curDate, i) {
                if (datepicker.isSame(curDate, date)) {
                    selected.splice(i, 1);

                    if (!_this.selectedDates.length) {
                        _this.minRange = '';
                        _this.maxRange = '';
                    }

                    _this.views[_this.currentView]._render();
                    _this._setInputValue();

                    if (_this.opts.onSelect) {
                        _this._triggerOnChange();
                    }

                    return true;
                }
            });
        },

        /**
         * Set date to today's date.
         *
         * @returns {void}
         */
        today: function() {
            this.silent = true;
            this.view = this.opts.minView;
            this.silent = false;
            this.date = new Date();
        },

        /**
         * Clear input value.
         *
         * @returns {void}
         */
        clear: function() {
            this.selectedDates = [];
            this.minRange = '';
            this.maxRange = '';
            this.views[this.currentView]._render();
            this._setInputValue();
            if (this.opts.onSelect) {
                this._triggerOnChange();
            }
        },

        /**
         * Updates datepicker options.
         *
         * @param {string|Object} param - Parameter's name to update. If object then it will extend current options.
         * @param {string|number|Object} value - New param value.
         * @returns {HTMLElement} Datepicker element.
         */
        update: function(param, value) {
            var len = arguments.length;

            if (len == 2) {
                this.opts[param] = value;
            } else if (len == 1 && typeof param == 'object') {
                this.opts = $.extend(true, this.opts, param);
            }

            this._createShortCuts();
            this._syncWithMinMaxDates();
            this._defineLocale(this.opts.language);
            this.nav._addButtonsIfNeed();
            this.nav._render();
            this.views[this.currentView]._render();

            if (this.elIsInput && !this.opts.inline) {
                this._setPositionClasses(this.opts.position);
                if (this.visible) {
                    this.setPosition(this.opts.position);
                }
            }

            if (this.opts.classes) {
                this.$datepicker.addClass(this.opts.classes);
            }

            return this;
        },

        /**
         * Sync with min and max dates.
         *
         * @private
         * @returns {void}
         */
        _syncWithMinMaxDates: function() {
            var curTime = this.date.getTime();

            this.silent = true;
            if (this.minTime > curTime) {
                this.date = this.minDate;
            }

            if (this.maxTime < curTime) {
                this.date = this.maxDate;
            }
            this.silent = false;
        },

        /**
         * Check if checkDate is selected.
         *
         * @private
         * @param   {Date} checkDate - Date instace that should be checked.
         * @param   {string} cellType - Cell type.
         * @returns {boolean} If checkDate is selected.
         */
        _isSelected: function(checkDate, cellType) {
            return this.selectedDates.some(function(date) {
                return datepicker.isSame(date, checkDate, cellType);
            });
        },

        /**
         * Set input value.
         *
         * @private
         * @returns {void}
         */
        _setInputValue: function() {
            var _this = this;
            var format = this.loc.dateFormat;
            var altFormat = this.opts.altFieldDateFormat;
            var value = this.selectedDates.map(function(date) {
                return _this.formatDate(format, date);
            });
            var altValues;

            if (this.$altField) {
                altValues = this.selectedDates.map(function(date) {
                    return _this.formatDate(altFormat, date);
                });
                altValues = altValues.join(this.opts.multipleDatesSeparator);
                this.$altField.val(altValues);
            }

            value = value.join(this.opts.multipleDatesSeparator);

            this.$el.val(value);
        },

        /**
         * Check if date is between minDate and maxDate.
         *
         * @private
         * @param {Object} date - Date object.
         * @param {string} type - Cell type.
         * @returns {boolean} If date is between minDate and maxDate.
         */
        _isInRange: function(date, type) {
            var time = date.getTime();
            var d = datepicker.getParsedDate(date);
            var min = datepicker.getParsedDate(this.minDate);
            var max = datepicker.getParsedDate(this.maxDate);
            var dMinTime = new Date(d.year, d.month, min.date).getTime();
            var dMaxTime = new Date(d.year, d.month, max.date).getTime();
            var types = {
                day: time >= this.minTime && time <= this.maxTime,
                month: dMinTime >= this.minTime && dMaxTime <= this.maxTime,
                year: d.year >= min.year && d.year <= max.year
            };

            return type ? types[type] : types.day;
        },

        /**
         * Get dimensions of an element.
         *
         * @private
         * @param   {JQuery} $el - Element whose dimensions to get.
         * @returns {Object} Element dimensions.
         */
        _getDimensions: function($el) {
            var offset = $el.offset();

            return {
                width: $el.outerWidth(),
                height: $el.outerHeight(),
                left: offset.left,
                top: offset.top
            };
        },

        /**
         * Get date from cell.
         *
         * @private
         * @param   {JQuery} cell - Cell element.
         * @returns {Date} Date from cell.
         */
        _getDateFromCell: function(cell) {
            var curDate = this.parsedDate;
            var year = cell.data('year') || curDate.year;
            var month = cell.data('month') == undefined ? curDate.month : cell.data('month');
            var date = cell.data('date') || 1;

            return new Date(year, month, date);
        },

        /**
         * Set position classes.
         *
         * @private
         * @param   {string} pos - Position option.
         * @returns {void}
         */
        _setPositionClasses: function(pos) {
            pos = pos.split(' ');
            var main = pos[0];
            var sec = pos[1];
            var classes = 'datepicker -' + main + '-' + sec + '- -from-' + main + '-';

            if (this.visible) classes += ' active';

            this.$datepicker
                .removeAttr('class')
                .addClass(classes);
        },

        /**
         * Set position.
         *
         * @param   {string} position - Position option.
         * @returns {void}
         */
        setPosition: function(position) {
            position = position || this.opts.position;

            var dims = this._getDimensions(this.$el);
            var selfDims = this._getDimensions(this.$datepicker);
            var pos = position.split(' ');
            var top;
            var left;
            var offset = this.opts.offset;
            var main = pos[0];
            var secondary = pos[1];
            var pointerRight = 3;
            var bodyRect;
            var elRect;

            switch (main) {
                case 'top':
                    top = dims.top - selfDims.height - offset;
                    break;
                case 'right':
                    left = dims.left + dims.width + offset;
                    break;
                case 'bottom':
                    top = dims.top + dims.height + offset;
                    break;
                case 'left':
                    left = dims.left - selfDims.width - offset;
                    break;
            }

            switch (secondary) {
                case 'top':
                    top = dims.top;
                    break;
                case 'right':
                    left = dims.left + dims.width - selfDims.width;
                    break;
                case 'bottom':
                    top = dims.top + dims.height - selfDims.height;
                    break;
                case 'left':
                    left = dims.left;
                    break;
                case 'center':
                    if (/left|right/.test(main)) {
                        top = dims.top + dims.height / 2 - selfDims.height / 2;
                    } else {
                        left = dims.left + dims.width / 2 - selfDims.width / 2;
                    }
            }

            bodyRect = $('body')[0].getBoundingClientRect();
            elRect = this.$el[0].getBoundingClientRect();

            if ((elRect.right + 15) < bodyRect.right) {
                left += 15;
                pointerRight += 15;
            }

            $(this.$datepicker[0].firstChild).css({
                right: pointerRight
            });
            this.$datepicker.css({
                left: left,
                top: top
            });
        },

        /**
         * Show datepicker.
         *
         * @returns {void}
         */
        show: function() {
            this.setPosition(this.opts.position);
            this.$datepicker.addClass('active');
            this.visible = true;

            if ($('body').hasClass('scroll-disabled')) {
                this.$el.parents().on('scroll', this._onResize.bind(this));
            }
        },

        /**
         * Hide datepicker.
         *
         * @returns {void}
         */
        hide: function() {
            this.$datepicker
                .removeClass('active')
                .css({
                    left: '-10000px'
                });

            this.focused = '';
            this.keys = [];

            this.inFocus = false;
            this.visible = false;
            this.$el.blur();

            if ($('body').hasClass('scroll-disabled')) {
                this.$el.parents().off('scroll', this._onResize.bind(this));
            }
        },

        /**
         * Move view down.
         *
         * @param   {Date} date - Date instance.
         * @returns {void}
         */
        down: function(date) {
            this._changeView(date, 'down');
        },

        /**
         * Move view up.
         *
         * @param   {Date} date - Date instance.
         * @returns {void}
         */
        up: function(date) {
            this._changeView(date, 'up');
        },

        /**
         * Change view in some direction.
         *
         * @private
         * @param   {Date} date - Date instance.
         * @param   {string} dir - Direction.
         * @returns {void}
         */
        _changeView: function(date, dir) {
            date = date || this.focused || this.date;

            var nextView = dir == 'up' ? this.viewIndex + 1 : this.viewIndex - 1;

            if (nextView > 2) nextView = 2;
            if (nextView < 0) nextView = 0;

            this.silent = true;
            this.date = new Date(date.getFullYear(), date.getMonth(), 1);
            this.silent = false;
            this.view = this.viewIndexes[nextView];
        },

        /**
         * Handle hotkey presses.
         *
         * @private
         * @param   {string} key - Hotkey type.
         * @returns {void}
         */
        _handleHotKey: function(key) {
            var date = datepicker.getParsedDate(this._getFocusedDate());
            var focusedParsed;
            var o = this.opts;
            var newDate;
            var totalDaysInNextMonth;
            var monthChanged = false;
            var yearChanged = false;
            var decadeChanged = false;
            var y = date.year;
            var m = date.month;
            var d = date.date;

            switch (key) {
                case 'ctrlRight':
                case 'ctrlUp':
                    m += 1;
                    monthChanged = true;
                    break;
                case 'ctrlLeft':
                case 'ctrlDown':
                    m -= 1;
                    monthChanged = true;
                    break;
                case 'shiftRight':
                case 'shiftUp':
                    yearChanged = true;
                    y += 1;
                    break;
                case 'shiftLeft':
                case 'shiftDown':
                    yearChanged = true;
                    y -= 1;
                    break;
                case 'altRight':
                case 'altUp':
                    decadeChanged = true;
                    y += 10;
                    break;
                case 'altLeft':
                case 'altDown':
                    decadeChanged = true;
                    y -= 10;
                    break;
                case 'ctrlShiftUp':
                    this.up();
                    break;
            }

            totalDaysInNextMonth = datepicker.getDaysCount(new Date(y, m));
            newDate = new Date(y, m, d);

            // If next month has less days than current, set date to total days in that month
            if (totalDaysInNextMonth < d) d = totalDaysInNextMonth;

            // Check if newDate is in valid range
            if (newDate.getTime() < this.minTime) {
                newDate = this.minDate;
            } else if (newDate.getTime() > this.maxTime) {
                newDate = this.maxDate;
            }

            this.focused = newDate;

            focusedParsed = datepicker.getParsedDate(newDate);
            if (monthChanged && o.onChangeMonth) {
                o.onChangeMonth(focusedParsed.month, focusedParsed.year);
            }
            if (yearChanged && o.onChangeYear) {
                o.onChangeYear(focusedParsed.year);
            }
            if (decadeChanged && o.onChangeDecade) {
                o.onChangeDecade(this.curDecade);
            }
        },

        /**
         * Register key.
         *
         * @private
         * @param   {string} key - Key to be registered.
         * @returns {void}
         */
        _registerKey: function(key) {
            var exists = this.keys.some(function(curKey) {
                return curKey == key;
            });

            if (!exists) {
                this.keys.push(key);
            }
        },

        /**
         * Unregister key.
         *
         * @private
         * @param   {string} key - Key to be unregistered.
         * @returns {void}
         */
        _unRegisterKey: function(key) {
            var index = this.keys.indexOf(key);

            this.keys.splice(index, 1);
        },

        /**
         * Check if a hotkey is pressed.
         *
         * @private
         * @returns {boolean} True if a hotkey is pressed.
         */
        _isHotKeyPressed: function() {
            var currentHotKey;
            var found = false;
            var _this = this;
            var pressedKeys = this.keys.sort();

            for (var hotKey in hotKeys) {
                currentHotKey = hotKeys[hotKey];
                if (pressedKeys.length != currentHotKey.length) continue;

                if (currentHotKey.every(function(key, i) { return key == pressedKeys[i]; })) {
                    _this._trigger('hotKey', hotKey);
                    found = true;
                }
            }

            return found;
        },

        /**
         * Trigger event on element.
         *
         * @private
         * @param   {string} event - Event name to trigger.
         * @param   {Object} args - Arguments passed to event.
         * @returns {void}
         */
        _trigger: function(event, args) {
            this.$el.trigger(event, args);
        },

        /**
         * Focus next cell.
         *
         * @private
         * @param   {number} keyCode - Pressed key code.
         * @param   {string} type - Cell type.
         * @returns {void}
         */
        _focusNextCell: function(keyCode, type) {
            type = type || this.cellType;

            var date = datepicker.getParsedDate(this._getFocusedDate());
            var y = date.year;
            var m = date.month;
            var d = date.date;

            if (this._isHotKeyPressed()) {
                return;
            }

            switch (keyCode) {
                case 37:
                    // left
                    type == 'day' ? (d -= 1) : '';
                    type == 'month' ? (m -= 1) : '';
                    type == 'year' ? (y -= 1) : '';
                    break;
                case 38:
                    // up
                    type == 'day' ? (d -= 7) : '';
                    type == 'month' ? (m -= 3) : '';
                    type == 'year' ? (y -= 4) : '';
                    break;
                case 39:
                    // right
                    type == 'day' ? (d += 1) : '';
                    type == 'month' ? (m += 1) : '';
                    type == 'year' ? (y += 1) : '';
                    break;
                case 40:
                    // down
                    type == 'day' ? (d += 7) : '';
                    type == 'month' ? (m += 3) : '';
                    type == 'year' ? (y += 4) : '';
                    break;
            }

            var nd = new Date(y, m, d);

            if (nd.getTime() < this.minTime) {
                nd = this.minDate;
            } else if (nd.getTime() > this.maxTime) {
                nd = this.maxDate;
            }

            this.focused = nd;
        },

        /**
         * Get focused date.
         *
         * @private
         * @returns {Date} Focused date instance.
         */
        _getFocusedDate: function() {
            var focused  = this.focused || this.selectedDates[this.selectedDates.length - 1];
            var d = this.parsedDate;

            if (!focused) {
                switch (this.view) {
                    case 'days':
                        focused = new Date(d.year, d.month, new Date().getDate());
                        break;
                    case 'months':
                        focused = new Date(d.year, d.month, 1);
                        break;
                    case 'years':
                        focused = new Date(d.year, 0, 1);
                        break;
                }
            }

            return focused;
        },

        /**
         * Get cell.
         *
         * @private
         * @param   {Date} date - Date instance.
         * @param   {string} type - Cell type.
         * @returns {JQuery} Found cell.
         */
        _getCell: function(date, type) {
            type = type || this.cellType;

            var d = datepicker.getParsedDate(date);
            var selector = '.datepicker--cell[data-year="' + d.year + '"]';
            var $cell;

            switch (type) {
                case 'month':
                    selector = '[data-month="' + d.month + '"]';
                    break;
                case 'day':
                    selector += '[data-month="' + d.month + '"][data-date="' + d.date + '"]';
                    break;
            }
            $cell = this.views[this.currentView].$el.find(selector);

            return $cell.length ? $cell : '';
        },

        /**
         * Show event callback.
         *
         * @private
         * @returns {void}
         */
        _onShowEvent: function() {
            if (!this.visible) {
                this.show();
            }
        },

        /**
         * Blur event callback.
         *
         * @private
         * @returns {void}
         */
        _onBlur: function() {
            if (!this.inFocus && this.visible) {
                this.hide();
            }
        },

        /**
         * Mousedown event callback.
         *
         * @private
         * @returns {void}
         */
        _onMouseDownDatepicker: function() {
            this.inFocus = true;
        },

        /**
         * Mouseup event callback.
         *
         * @private
         * @returns {void}
         */
        _onMouseUpDatepicker: function() {
            this.inFocus = false;
            this.$el.focus();
        },

        /**
         * Input event callback.
         *
         * @private
         * @returns {void}
         */
        _onInput: function() {
            var val = this.$el.val();

            if (!val) {
                this.clear();
            }
        },

        /**
         * Resize event callback.
         *
         * @private
         * @returns {void}
         */
        _onResize: function() {
            if (this.visible) {
                this.setPosition();
            }
        },

        /**
         * Keydown event handler.
         *
         * @private
         * @param   {Event} e - Event object.
         * @returns {void}
         */
        _onKeyDown: function(e) {
            var code = e.which;

            this._registerKey(code);

            // Arrows
            if (code >= 37 && code <= 40) {
                e.preventDefault();
                this._focusNextCell(code);
            }

            // Enter
            if (code == 13) {
                if (this.focused) {
                    if (this._getCell(this.focused).hasClass('-disabled-')) return;
                    if (this.view != this.opts.minView) {
                        this.down();
                    } else {
                        var alreadySelected = this._isSelected(this.focused, this.cellType);

                        if (!alreadySelected) {
                            this.selectDate(this.focused);
                        } else if (alreadySelected && this.opts.toggleSelected) {
                            this.removeDate(this.focused);
                        }
                    }
                }
            }

            // Esc
            if (code == 27) {
                this.hide();
            }
        },

        /**
         * Keyup event handler.
         *
         * @private
         * @param   {Event} e - Event object.
         * @returns {void}
         */
        _onKeyUp: function(e) {
            var code = e.which;

            this._unRegisterKey(code);
        },

        /**
         * Hotkey event handler.
         *
         * @private
         * @param   {Event} e - Event object.
         * @param   {string} hotKey - Hotkey type.
         * @returns {void}
         */
        _onHotKey: function(e, hotKey) {
            this._handleHotKey(hotKey);
        },

        /**
         * Mouseenter event handler.
         *
         * @private
         * @param   {Event} e - Event object.
         * @returns {void}
         */
        _onMouseEnterCell: function(e) {
            var $cell = $(e.target).closest('.datepicker--cell');
            var date = this._getDateFromCell($cell);

            // Prevent from unnecessary rendering and setting new currentDate
            this.silent = true;

            if (this.focused) {
                this.focused = '';
            }

            $cell.addClass('-focus-');

            this.focused = date;
            this.silent = false;

            if (this.opts.range && this.selectedDates.length == 1) {
                this.minRange = this.selectedDates[0];
                this.maxRange = '';
                if (datepicker.less(this.minRange, this.focused)) {
                    this.maxRange = this.minRange;
                    this.minRange = '';
                }
                this.views[this.currentView]._update();
            }
        },

        /**
         * Mouseleave event handler.
         *
         * @private
         * @param   {Event} e - Event object.
         * @returns {void}
         */
        _onMouseLeaveCell: function(e) {
            var $cell = $(e.target).closest('.datepicker--cell');

            $cell.removeClass('-focus-');

            this.silent = true;
            this.focused = '';
            this.silent = false;
        },

        /**
         * Set focused cell value.
         *
         * @param   {string} val - Cell value.
         * @returns {void}
         */
        set focused(val) {
            if (!val && this.focused) {
                var $cell = this._getCell(this.focused);

                if ($cell.length) {
                    $cell.removeClass('-focus-');
                }
            }
            this._focused = val;
            if (this.opts.range && this.selectedDates.length == 1) {
                this.minRange = this.selectedDates[0];
                this.maxRange = '';
                if (datepicker.less(this.minRange, this._focused)) {
                    this.maxRange = this.minRange;
                    this.minRange = '';
                }
            }
            if (this.silent) return;
            this.date = val;
        },

        /**
         * Get focused cell value.
         *
         * @returns {string} Focused cell value.
         */
        get focused() {
            return this._focused;
        },

        /**
         * Get parsed date.
         *
         * @returns {Object} Parsed date.
         */
        get parsedDate() {
            return datepicker.getParsedDate(this.date);
        },

        /**
         * Set date to val.
         *
         * @param   {Date} val - New date.
         * @returns {Date} New date.
         */
        set date(val) {
            if (!(val instanceof Date)) return;

            this.currentDate = val;

            if (this.inited && !this.silent) {
                this.views[this.view]._render();
                this.nav._render();
                if (this.visible && this.elIsInput) {
                    this.setPosition();
                }
            }

            return val;
        },

        /**
         * Get current date.
         *
         * @returns {Date} Current date.
         */
        get date() {
            return this.currentDate;
        },

        /**
         * Set view to val.
         *
         * @param   {string} val - New view index.
         * @returns {string} New view index.
         */
        set view(val) {
            this.viewIndex = this.viewIndexes.indexOf(val);

            if (this.viewIndex < 0) {
                return;
            }

            this.prevView = this.currentView;
            this.currentView = val;

            if (this.inited) {
                if (!this.views[val]) {
                    this.views[val] = new Datepicker.Body(this, val, this.opts);
                } else {
                    this.views[val]._render();
                }

                this.views[this.prevView].hide();
                this.views[val].show();
                this.nav._render();

                if (this.opts.onChangeView) {
                    this.opts.onChangeView(val);
                }
                if (this.elIsInput && this.visible) this.setPosition();
            }

            return val;
        },

        /**
         * Get current view.
         *
         * @returns {string} Current view.
         */
        get view() {
            return this.currentView;
        },

        /**
         * Get cell type.
         *
         * @returns {string} Cell type.
         */
        get cellType() {
            return this.view.substring(0, this.view.length - 1);
        },

        /**
         * Get minimum time.
         *
         * @returns {Date} Minimum time.
         */
        get minTime() {
            var min = datepicker.getParsedDate(this.minDate);

            return new Date(min.year, min.month, min.date).getTime();
        },

        /**
         * Get maximum time.
         *
         * @returns {Date} Maximum time.
         */
        get maxTime() {
            var max = datepicker.getParsedDate(this.maxDate);

            return new Date(max.year, max.month, max.date).getTime();
        },

        /**
         * Get current decade.
         *
         * @returns {Array} Current decade.
         */
        get curDecade() {
            return datepicker.getDecade(this.date);
        }
    };

    //  Utils
    // -------------------------------------------------

    /**
     * Get days count.
     *
     * @param   {Date} date - Date whsose days count to get.
     * @returns {number} - Days count.
     */
    datepicker.getDaysCount = function(date) {
        return new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate();
    };

    /**
     * Get parsed date from date object.
     *
     * @param   {Date} date - Date object.
     * @returns {Object} - Parsed date object.
     */
    datepicker.getParsedDate = function(date) {
        return {
            year: date.getFullYear(),
            month: date.getMonth(),
            // One based
            fullMonth: (date.getMonth() + 1) < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1,
            date: date.getDate(),
            fullDate: date.getDate() < 10 ? '0' + date.getDate() : date.getDate(),
            day: date.getDay()
        };
    };

    /**
     * Get decade from date.
     *
     * @param   {Date} date - Date object.
     * @returns {Array} - Decade start and end year.
     */
    datepicker.getDecade = function(date) {
        var firstYear = Math.floor(date.getFullYear() / 10) * 10;

        return [firstYear, firstYear + 9];
    };

    /**
     * Find template.
     *
     * @param   {string} str - Template name.
     * @param   {Object} data - Data object.
     * @returns {string} - Template.
     */
    datepicker.template = function(str, data) {
        return str.replace(/#\{([\w]+)\}/g, function(source, match) {
            if (data[match] || data[match] === 0) {
                return data[match];
            }
        });
    };

    /**
     * Determine if two dates are the same.
     *
     * @param   {Date} date1 - First date.
     * @param   {Date} date2 - Second date.
     * @param   {string} type - Comparison type.
     * @returns {boolean} - True if the two dates are the same.
     */
    datepicker.isSame = function(date1, date2, type) {
        if (!date1 || !date2) return false;
        var d1 = datepicker.getParsedDate(date1);
        var d2 = datepicker.getParsedDate(date2);
        var _type = type ? type : 'day';
        var conditions = {
            day: d1.date == d2.date && d1.month == d2.month && d1.year == d2.year,
            month: d1.month == d2.month && d1.year == d2.year,
            year: d1.year == d2.year
        };

        return conditions[_type];
    };

    /**
     * Determine if date is earlier than dateCompareTo.
     *
     * @param   {Date} dateCompareTo - Date to compare to.
     * @param   {Date} date - Date to compare.
     * @returns {boolean} - True if date is earlier than dateCompareTo.
     */
    datepicker.less = function(dateCompareTo, date) {
        if (!dateCompareTo || !date) return false;

        return date.getTime() < dateCompareTo.getTime();
    };

    /**
     * Determine if date is later than dateCompareTo.
     *
     * @param   {Date} dateCompareTo - Date to compare to.
     * @param   {Date} date - Date to compare.
     * @returns {boolean} - True if date is later than dateCompareTo.
     */
    datepicker.bigger = function(dateCompareTo, date) {
        if (!dateCompareTo || !date) return false;

        return date.getTime() > dateCompareTo.getTime();
    };

    Datepicker.language = {
        et: {
            days: ['Pühapäev', 'Esmaspäev', 'Teisipäev', 'Kolmapäev', 'Neljapäev', 'Reede', 'Laupäev'],
            daysShort: ['P', 'E', 'T', 'K', 'N', 'R', 'L'],
            daysMin: ['P', 'E', 'T', 'K', 'N', 'R', 'L'],
            months: ['Jaanuar', 'Veebruar', 'Märts', 'Aprill', 'Mai', 'Juuni', 'Juuli', 'August', 'September', 'Oktoober', 'November', 'Detsember'],
            monthsShort: ['Jaan', 'Veebr', 'Märts', 'Apr', 'Mai', 'Juuni', 'Juuli', 'Aug', 'Sept', 'Okt', 'Nov', 'Dets'],
            today: 'Täna',
            clear: 'Tühjenda',
            dateFormat: 'dd.mm.yyyy',
            firstDay: 1
        }
    };

    /**
     * Create jQuery plugin.
     *
     * @param {Object} options - Extended options.
     * @listens telia.datepicker
     * @returns {Array<Datepicker>} Array of Datepickers.
     */
    $.fn[pluginName] = function(options) {
        return this.each(function() {
            if (!$.data(this, pluginName)) {
                $.data(this,  pluginName,
                    new Datepicker(this, options));
            } else {
                var _this = $.data(this, pluginName);

                _this.opts = $.extend(true, _this.opts, options);
                _this.update();
            }
        });
    };

    /**
     * Auto-init the plugin on DOM ready.
     */
    $(function() {
        $(autoInitSelector).datepicker();
    });
})(window, jQuery);
;(function() {
    var templates = {
        days: '' +
        '<div class="datepicker--days datepicker--body">' +
        '<div class="datepicker--days-names"></div>' +
        '<div class="datepicker--cells datepicker--cells-days"></div>' +
        '</div>',
        months: '' +
        '<div class="datepicker--months datepicker--body">' +
        '<div class="datepicker--cells datepicker--cells-months"></div>' +
        '</div>',
        years: '' +
        '<div class="datepicker--years datepicker--body">' +
        '<div class="datepicker--cells datepicker--cells-years"></div>' +
        '</div>'
    };
    var D = Datepicker;

    /**
     * Datepicker Body.
     *
     * @class
     * @param   {Datepicker} d - Datepicker instance.
     * @param   {string} type - Type.
     * @param   {Object} opts - An option map.
     * @returns {void}
     */
    D.Body = function(d, type, opts) {
        this.d = d;
        this.type = type;
        this.opts = opts;

        this.init();
    };

    D.Body.prototype = {
        /**
         * Initialize datepicker body.
         *
         * @returns {void}
         */
        init: function() {
            this._buildBaseHtml();
            this._render();

            this._bindEvents();
        },

        /**
         * Bind events.
         *
         * @private
         * @returns {void}
         */
        _bindEvents: function() {
            this.$el.on('click', '.datepicker--cell', $.proxy(this._onClickCell, this));
        },

        /**
         * Build base HTML.
         *
         * @private
         * @returns {void}
         */
        _buildBaseHtml: function() {
            this.$el = $(templates[this.type]).appendTo(this.d.$content);
            this.$names = $('.datepicker--days-names', this.$el);
            this.$cells = $('.datepicker--cells', this.$el);
        },

        /**
         * Get day names HTML.
         *
         * @private
         * @param   {string} firstDay - First day name.
         * @param   {string} curDay - Current day name.
         * @param   {string} html - Day names HTML.
         * @param   {index} i - Day index.
         * @returns {string} Day names HTML.
         */
        _getDayNamesHtml: function(firstDay, curDay, html, i) {
            curDay = curDay != undefined ? curDay : firstDay;
            html = html ? html : '';
            i = i != undefined ? i : 0;

            if (i > 7) return html;
            if (curDay == 7) return this._getDayNamesHtml(firstDay, 0, html, ++i);

            html += '<div class="datepicker--day-name' + (this.d.isWeekend(curDay) ? ' -weekend-' : '') + '">' + this.d.loc.daysMin[curDay] + '</div>';

            return this._getDayNamesHtml(firstDay, ++curDay, html, ++i);
        },

        /**
         * Get cell contents.
         *
         * @private
         * @param   {Date} date - Cell date instance.
         * @param   {string} type - Cell type.
         * @returns {Object} Object with HTML and classes.
         */
        _getCellContents: function(date, type) {
            var classes = 'datepicker--cell datepicker--cell-' + type;
            var currentDate = new Date();
            var parent = this.d;
            var opts = parent.opts;
            var d = D.getParsedDate(date);
            var render = {};
            var html = d.date;

            if (opts.onRenderCell) {
                render = opts.onRenderCell(date, type) || {};
                html = render.html ? render.html : html;
                classes += render.classes ? ' ' + render.classes : '';
            }

            switch (type) {
                case 'day':
                    if (parent.isWeekend(d.day)) classes += ' -weekend-';
                    if (d.month != this.d.parsedDate.month) {
                        classes += ' -other-month-';
                        if (!opts.selectOtherMonths) {
                            classes += ' -disabled-';
                        }
                        if (!opts.showOtherMonths) html = '';
                    }
                    break;
                case 'month':
                    html = parent.loc[parent.opts.monthsFiled][d.month];
                    break;
                case 'year':
                    var decade = parent.curDecade;

                    if (d.year < decade[0] || d.year > decade[1]) {
                        classes += ' -other-decade-';
                        if (!opts.selectOtherYears) {
                            classes += ' -disabled-';
                        }
                        if (!opts.showOtherYears) html = '';
                    }
                    html = d.year;
                    break;
            }

            if (opts.onRenderCell) {
                render = opts.onRenderCell(date, type) || {};
                html = render.html ? render.html : html;
                classes += render.classes ? ' ' + render.classes : '';
            }

            if (opts.range) {
                if (D.isSame(parent.minRange, date, type)) classes += ' -range-from-';
                if (D.isSame(parent.maxRange, date, type)) classes += ' -range-to-';

                if (parent.selectedDates.length == 1 && parent.focused) {
                    if (
                        (D.bigger(parent.minRange, date) && D.less(parent.focused, date)) ||
                        (D.less(parent.maxRange, date) && D.bigger(parent.focused, date))) {
                        classes += ' -in-range-';
                    }

                    if (D.less(parent.maxRange, date) && D.isSame(parent.focused, date)) {
                        classes += ' -range-from-';
                    }
                    if (D.bigger(parent.minRange, date) && D.isSame(parent.focused, date)) {
                        classes += ' -range-to-';
                    }
                } else if (parent.selectedDates.length == 2) {
                    if (D.bigger(parent.minRange, date) && D.less(parent.maxRange, date)) {
                        classes += ' -in-range-';
                    }
                }
            }

            if (D.isSame(currentDate, date, type)) classes += ' -current-';
            if (parent.focused && D.isSame(date, parent.focused, type)) classes += ' -focus-';
            if (parent._isSelected(date, type)) classes += ' -selected-';
            if (!parent._isInRange(date, type) || render.disabled) classes += ' -disabled-';

            return {
                html: html,
                classes: classes
            };
        },

        /**
         * Calculates days number to render. Generates days html and returns it.
         *
         * @private
         * @param {Object} date - Date object.
         * @returns {string} Days html.
         */
        _getDaysHtml: function(date) {
            var totalMonthDays = D.getDaysCount(date);
            var firstMonthDay = new Date(date.getFullYear(), date.getMonth(), 1).getDay();
            var lastMonthDay = new Date(date.getFullYear(), date.getMonth(), totalMonthDays).getDay();
            var daysFromPevMonth = firstMonthDay - this.d.loc.firstDay;
            var daysFromNextMonth = 6 - lastMonthDay + this.d.loc.firstDay;

            daysFromPevMonth = daysFromPevMonth < 0 ? daysFromPevMonth + 7 : daysFromPevMonth;
            daysFromNextMonth = daysFromNextMonth > 6 ? daysFromNextMonth - 7 : daysFromNextMonth;

            var startDayIndex = -daysFromPevMonth + 1;
            var m;
            var y;
            var html = '';

            for (var i = startDayIndex, max = totalMonthDays + daysFromNextMonth; i <= max; i++) {
                y = date.getFullYear();
                m = date.getMonth();

                html += this._getDayHtml(new Date(y, m, i));
            }

            return html;
        },

        /**
         * Get day HTML.
         *
         * @private
         * @param   {Date} date - Day date instance.
         * @returns {string} Day HTML.
         */
        _getDayHtml: function(date) {
            var content = this._getCellContents(date, 'day');

            return '<div class="' + content.classes + '" ' +
                'data-date="' + date.getDate() + '" ' +
                'data-month="' + date.getMonth() + '" ' +
                'data-year="' + date.getFullYear() + '">' + content.html + '</div>';
        },

        /**
         * Generates months html.
         *
         * @private
         * @param {Object} date - Date instance.
         * @returns {string} Months html.
         */
        _getMonthsHtml: function(date) {
            var html = '';
            var d = D.getParsedDate(date);
            var i = 0;

            while (i < 12) {
                html += this._getMonthHtml(new Date(d.year, i));
                i++;
            }

            return html;
        },

        /**
         * Generates month html.
         *
         * @private
         * @param {Object} date - Date instance.
         * @returns {string} Month html.
         */
        _getMonthHtml: function(date) {
            var content = this._getCellContents(date, 'month');

            return '<div class="' + content.classes + '" data-month="' + date.getMonth() + '">' + content.html + '</div>';
        },

        /**
         * Generates years html.
         *
         * @private
         * @param {Object} date - Date instance.
         * @returns {string} Years html.
         */
        _getYearsHtml: function(date) {
            var decade = D.getDecade(date);
            var firstYear = decade[0] - 1;
            var html = '';
            var i = firstYear;

            for (i; i <= decade[1] + 1; i++) {
                html += this._getYearHtml(new Date(i, 0));
            }

            return html;
        },

        /**
         * Generates year html.
         *
         * @private
         * @param {Object} date - Date instance.
         * @returns {string} Year html.
         */
        _getYearHtml: function(date) {
            var content = this._getCellContents(date, 'year');

            return '<div class="' + content.classes + '" data-year="' + date.getFullYear() + '">' + content.html + '</div>';
        },

        _renderTypes: {
            /**
             * Render days.
             *
             * @returns {void}
             */
            days: function() {
                var dayNames = this._getDayNamesHtml(this.d.loc.firstDay);
                var days = this._getDaysHtml(this.d.currentDate);

                this.$cells.html(days);
                this.$names.html(dayNames);
            },

            /**
             * Render months.
             *
             * @returns {void}
             */
            months: function() {
                var html = this._getMonthsHtml(this.d.currentDate);

                this.$cells.html(html);
            },

            /**
             * Render years.
             *
             * @returns {void}
             */
            years: function() {
                var html = this._getYearsHtml(this.d.currentDate);

                this.$cells.html(html);
            }
        },

        /**
         * Render current type.
         *
         * @private
         * @returns {void}
         */
        _render: function() {
            this._renderTypes[this.type].bind(this)();
        },

        /**
         * Update datepicker.
         *
         * @private
         * @returns {void}
         */
        _update: function() {
            var $cells = $('.datepicker--cell', this.$cells);
            var _this = this;
            var classes;
            var $cell;
            var date;

            $cells.each(function() {
                $cell = $(this);
                date = _this.d._getDateFromCell($(this));
                classes = _this._getCellContents(date, _this.d.cellType);
                $cell.attr('class', classes.classes);
            });
        },

        /**
         * Show datepicker.
         *
         * @returns {void}
         */
        show: function() {
            this.$el.addClass('active');
            this.acitve = true;
        },

        /**
         * Hide datepicker.
         *
         * @returns {void}
         */
        hide: function() {
            this.$el.removeClass('active');
            this.active = false;
        },

        //  Events
        // -------------------------------------------------

        /**
         * Click handler.
         *
         * @private
         * @param   {JQuery} el - Element.
         * @returns {void}
         */
        _handleClick: function(el) {
            var date = el.data('date') || 1;
            var month = el.data('month') || 0;
            var year = el.data('year') || this.d.parsedDate.year;
            // Change view if min view does not reach yet

            if (this.d.view != this.opts.minView) {
                this.d.down(new Date(year, month, date));

                return;
            }
            // Select date if min view is reached
            var selectedDate = new Date(year, month, date);
            var alreadySelected = this.d._isSelected(selectedDate, this.d.cellType);

            if (!alreadySelected) {
                this.d.selectDate(selectedDate);
            } else if (alreadySelected && this.opts.toggleSelected) {
                this.d.removeDate(selectedDate);
            }
        },

        /**
         * Cell click handler.
         *
         * @private
         * @param   {Event} e - Event object.
         * @returns {void}
         */
        _onClickCell: function(e) {
            var $el = $(e.target).closest('.datepicker--cell');

            if ($el.hasClass('-disabled-')) return;

            this._handleClick.bind(this)($el);
        }
    };
})();

;(function() {
    var template = '' +
        '<div class="datepicker--nav-action" data-action="prev">#{prevHtml}</div>' +
        '<div class="datepicker--nav-title">#{title}</div>' +
        '<div class="datepicker--nav-action" data-action="next">#{nextHtml}</div>';
    var buttonsContainerTemplate = '<div class="datepicker--buttons"></div>';
    var button = '<span class="datepicker--button" data-action="#{action}">#{label}</span>';

    /**
     * Datepicker Navigation.
     *
     * @param   {Datepicker} d - Datepicker instance.
     * @param   {Object} opts - An option map.
     * @returns {void}
     */
    Datepicker.Navigation = function(d, opts) {
        this.d = d;
        this.opts = opts;

        this.$buttonsContainer = '';

        this.init();
    };

    Datepicker.Navigation.prototype = {
        /**
         * Intiialize datepicker navigation.
         *
         * @returns {void}
         */
        init: function() {
            this._buildBaseHtml();
            this._bindEvents();
        },

        /**
         * Bind events.
         *
         * @private
         * @returns {void}
         */
        _bindEvents: function() {
            this.d.$nav.on('click', '.datepicker--nav-action', $.proxy(this._onClickNavButton, this));
            this.d.$nav.on('click', '.datepicker--nav-title', $.proxy(this._onClickNavTitle, this));
            this.d.$datepicker.on('click', '.datepicker--button', $.proxy(this._onClickNavButton, this));
        },

        /**
         * Build base HTML.
         *
         * @private
         * @returns {void}
         */
        _buildBaseHtml: function() {
            this._render();
            this._addButtonsIfNeed();
        },

        /**
         * Add buttons.
         *
         * @private
         * @returns {void}
         */
        _addButtonsIfNeed: function() {
            if (this.opts.todayButton) {
                this._addButton('today');
            }
            if (this.opts.clearButton) {
                this._addButton('clear');
            }
        },

        /**
         * Render view.
         *
         * @private
         * @returns {void}
         */
        _render: function() {
            var title = this._getTitle(this.d.currentDate);
            var html = Datepicker.template(template, $.extend({
                title: title
            }, this.opts));

            this.d.$nav.html(html);
            if (this.d.view == 'years') {
                $('.datepicker--nav-title', this.d.$nav).addClass('-disabled-');
            }
            this.setNavStatus();
        },

        /**
         * Get title.
         *
         * @private
         * @param   {Date} date - Date instance.
         * @returns {string} Title.
         */
        _getTitle: function(date) {
            return this.d.formatDate(this.opts.navTitles[this.d.view], date);
        },

        /**
         * Add button.
         *
         * @private
         * @param   {string} type - Button type.
         * @returns {void}
         */
        _addButton: function(type) {
            if (!this.$buttonsContainer.length) {
                this._addButtonsContainer();
            }

            var data = {
                action: type,
                label: this.d.loc[type]
            };
            var html = Datepicker.template(button, data);

            if ($('[data-action=' + type + ']', this.$buttonsContainer).length) return;
            this.$buttonsContainer.append(html);
        },

        /**
         * Add buttons container.
         *
         * @private
         * @returns {void}
         */
        _addButtonsContainer: function() {
            this.d.$datepicker.append(buttonsContainerTemplate);
            this.$buttonsContainer = $('.datepicker--buttons', this.d.$datepicker);
        },

        /**
         * Set nav status.
         *
         * @returns {void}
         */
        setNavStatus: function() {
            if (!(this.opts.minDate || this.opts.maxDate) || !this.opts.disableNavWhenOutOfRange) return;

            var date = this.d.parsedDate;
            var m = date.month;
            var y = date.year;
            var d = date.date;

            switch (this.d.view) {
                case 'days':
                    if (!this.d._isInRange(new Date(y, m - 1, d), 'month')) {
                        this._disableNav('prev');
                    }
                    if (!this.d._isInRange(new Date(y, m + 1, d), 'month')) {
                        this._disableNav('next');
                    }
                    break;
                case 'months':
                    if (!this.d._isInRange(new Date(y - 1, m, d), 'year')) {
                        this._disableNav('prev');
                    }
                    if (!this.d._isInRange(new Date(y + 1, m, d), 'year')) {
                        this._disableNav('next');
                    }
                    break;
                case 'years':
                    if (!this.d._isInRange(new Date(y - 10, m, d), 'year')) {
                        this._disableNav('prev');
                    }
                    if (!this.d._isInRange(new Date(y + 10, m, d), 'year')) {
                        this._disableNav('next');
                    }
                    break;
            }
        },

        /**
         * Disable nav.
         *
         * @private
         * @param   {string} nav - Nav type.
         * @returns {void}
         */
        _disableNav: function(nav) {
            $('[data-action="' + nav + '"]', this.d.$nav).addClass('-disabled-');
        },

        /**
         * Activate nav.
         *
         * @private
         * @param   {string} nav - Nav type.
         * @returns {void}
         */
        _activateNav: function(nav) {
            $('[data-action="' + nav + '"]', this.d.$nav).removeClass('-disabled-');
        },

        /**
         * Nav button click handler.
         *
         * @private
         * @param   {Event} e - Event object.
         * @returns {void}
         */
        _onClickNavButton: function(e) {
            var $el = $(e.target).closest('[data-action]');
            var action = $el.data('action');

            this.d[action]();
        },

        /**
         * Nav title click handler.
         *
         * @private
         * @param   {Event} e - Event object.
         * @returns {string|void} View name or void.
         */
        _onClickNavTitle: function(e) {
            if ($(e.target).hasClass('-disabled-')) return;

            if (this.d.view == 'days') {
                return this.d.view = 'months';
            }

            this.d.view = 'years';
        }
    };
})();

/* globals Datepicker */
Datepicker.language['en'] = {
    days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
    daysShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
    daysMin: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
    months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
    monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
    today: 'Today',
    clear: 'Clear',
    dateFormat: 'dd.mm.yyyy',
    firstDay: 1
};

/* globals Datepicker */
Datepicker.language['ru'] = {
    days: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
    daysShort: ['Вос', 'Пон', 'Вто', 'Сре', 'Чет', 'Пят', 'Суб'],
    daysMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
    months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
    monthsShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
    today: 'Сегодня',
    clear: 'Очистить',
    dateFormat: 'dd.mm.yyyy',
    firstDay: 1
};


;(function($, window, document) {
    var pluginName = 'teliaTextfield';
    var defaults = {
        focusClass: 'is-focused',
        dirtyClass: 'is-dirty',
        focusText: false,
        blurText: false,
        maxLines: false
    };
    var initSelector = '.form-textfield';

    /**
     * Textfield.
     *
     * @class
     * @param {HTMLElement} element - The HTML element the plugin is initialized on.
     * @param {Object} options - An options object.
     */
    function Textfield(element, options) {
        this.element = element;
        this.$element = $(this.element);
        this.options = $.extend({}, defaults, options, this.$element.data());
        this._defaults = defaults;
        this._name = pluginName;
        this.inputs = this.$element.find('.form-textfield__input:not(.bootstrap-select)');
        this.$input = this.inputs.length > 1 ? $(this.inputs[1]) : this.inputs;
        this.$label = this.$element.find('.form-textfield__label');
        this.id = this.$input.prop('id');

        this._init();
    }

    /**
     * Initializes the plugin.
     *
     * @function _init
     * @private
     * @returns {void}
     */
    Textfield.prototype._init = function() {
        var that = this;

        this._checkValue();
        this.$input.on('change.telia.textfield', this._checkValue.bind(this));

        $(window).on('load.telia.textfield.' + this.id, function() {
            that._checkValue();
            setTimeout(that._checkValue.bind(that), 500);
        });

        if (this.$input.is('textarea')) {
            this._resizeHeight();
            this.$input.on('input.telia.textfield', this._resizeHeight.bind(this));

            $(window).on('resize.telia.textfield.' + this.id, this._resizeHeight.bind(this));
        }

        if (this.options.focusText && this.options.blurText && this._isDirty()) {
            this.$label.text(this.options.focusText);
        }

        this.$input.on('focus.telia.textfield', this._focusHandler.bind(this));
        this.$input.on('blur.telia.textfield', this._blurHandler.bind(this));
    };

    /**
     * Add or remove dirty class.
     *
     * @function _checkValue
     * @private
     * @returns {void}
     */
    Textfield.prototype._checkValue = function() {
        if (this.$input.is('input, textarea, select')) {
            if (!this._isDirty()) {
                this.$element.removeClass(this.options.dirtyClass);
            } else {
                this.$element.addClass(this.options.dirtyClass);
            }
        }
    };

    /**
     * Check if input is dirty.
     *
     * @function _isDirty
     * @private
     * @returns {boolean} True if input has value.
     */
    Textfield.prototype._isDirty = function() {
        var value = this.$input.val();

        return value && value !== '' && value.length;
    };

    /**
     * Resize input height.
     *
     * @function _resizeHeight
     * @private
     * @returns {void}
     */
    Textfield.prototype._resizeHeight = function() {
        var padding = parseInt(this.$input.css('padding-bottom'), 10) + parseInt(this.$input.css('padding-top'), 10);
        var border = parseInt(this.$input.css('border-bottom-width'), 10) + parseInt(this.$input.css('border-top-width'), 10);
        var lineHeight = parseInt(this.$input.css('line-height'), 10);
        var maxHeight = padding + border + (lineHeight * this.options.maxLines);

        this.$input.css('min-height', 0);

        var normalHeight = (this.$input.prop('scrollHeight') + 1);
        var newHeight = this.options.maxLines ? Math.min(maxHeight, normalHeight) : normalHeight;

        this.$input.css('min-height', newHeight);
    };

    /**
     * Add class and change label when input is focused.
     *
     * @function _focusHandler
     * @private
     * @returns {void}
     */
    Textfield.prototype._focusHandler = function() {
        var that = this;

        this.$element.addClass(this.options.focusClass);

        if (this.options.focusText && this.options.blurText) {
            setTimeout(function() {
                that.$label.text(that.options.focusText);
            }, 50);
        }
    };

    /**
     * Remove class and change label when input is unfocused.
     *
     * @function _blurHandler
     * @private
     * @returns {void}
     */
    Textfield.prototype._blurHandler = function() {
        var that = this;

        setTimeout(function() {
            that.$element.removeClass(that.options.focusClass);
        }, 100);

        if (this.options.focusText && this.options.blurText && !this._isDirty()) {
            setTimeout(function() {
                that.$label.text(that.options.blurText);
            }, 200);
        }
    };

    /**
     * Destroy the plugin instance.
     *
     * @function destroy
     * @returns {void}
     */
    Textfield.prototype.destroy = function() {
        this.$input.off('change.telia.textfield');
        this.$input.off('input.telia.textfield');
        this.$input.off('focus.telia.textfield');
        this.$input.off('blur.telia.textfield');
        $(window).off('load.telia.textfield.' + this.id);
        $(window).off('resize.telia.textfield.' + this.id);
        this.$element.removeData('plugin_' + pluginName);
    };

    /**
     * Create jQuery plugin.
     *
     * @param {Object} options - Extended options.
     * @listens telia.textfield
     * @returns {Array<Textfield>} Array of Textfields.
     */
    $.fn[pluginName] = function(options) {
        var args = Array.prototype.slice.call(arguments, 1);

        return this.each(function() {
            var instance = $.data(this, 'plugin_' + pluginName);

            if (!instance) {
                if (options === 'destroy') {
                    return;
                }

                $.data(this, 'plugin_' + pluginName, new Textfield(this, options));
            } else if (typeof options === 'string') {
                instance[options].apply(instance, args);
            }
        });
    };

    /**
     * @param {Event} event
     * @event enhance.telia.textfield
     */
    $(document).on('enhance.telia.textfield', function(event) {
        $(event.target).find(initSelector)[pluginName]();
    });

    /**
     * @param {Event} event
     * @event enhance.form.textfield
     * @deprecated use enhance.telia.textfield event instead
     */
    $(document).on('enhance.form.textfield', function(event) {
        $(document).trigger('enhance.telia.textfield', event);
    });

    /**
     * @param {Event} event
     * @event destroy.telia.textfield
     */
    $(document).on('destroy.telia.textfield', function(event) {
        $(event.target).find(initSelector)[pluginName]('destroy');
    });
})(jQuery, window, document);

/**
 * Auto-init the plugin on DOM ready.
 * @fires enhance.telia.textfield
 */
$(function() {
    $(document).trigger('enhance.telia.textfield');
});

/* !
 * Bootstrap-select v1.10.0 (http://silviomoreto.github.io/bootstrap-select)
 *
 * Copyright 2013-2016 bootstrap-select
 * Licensed under MIT (https://github.com/silviomoreto/bootstrap-select/blob/master/LICENSE)
 */

(function(factory) {
    factory(jQuery);
}(function(jQuery) {
    (function($) {
        'use strict';

        // <editor-fold desc="Shims">
        if (!String.prototype.includes) {
            (function() {
                // needed to support `apply`/`call` with `undefined`/`null`
                'use strict';

                var toString = {}.toString;
                var defineProperty = (function() {
                    // IE 8 only supports `Object.defineProperty` on DOM elements
                    try {
                        var object = {};
                        var $defineProperty = Object.defineProperty;
                        var result = $defineProperty(object, object, object) && $defineProperty;
                    } catch (error) {
                        // continue regardless of error
                    }

                    return result;
                }());
                var indexOf = ''.indexOf;

                /**
                 * Includes.
                 *
                 * @param  {string} search - Search.
                 * @returns {Function} Function.
                 */
                function includes(search) {
                    if (this == null) {
                        throw new TypeError();
                    }
                    var string = String(this);

                    if (search && toString.call(search) == '[object RegExp]') {
                        throw new TypeError();
                    }
                    var stringLength = string.length;
                    var searchString = String(search);
                    var searchLength = searchString.length;
                    var position = arguments.length > 1 ? arguments[1] : undefined;
                    // `ToInteger`
                    var pos = position ? Number(position) : 0;
                    // better `isNaN`

                    if (pos != pos) {
                        pos = 0;
                    }
                    var start = Math.min(Math.max(pos, 0), stringLength);
                    // Avoid the `indexOf` call if no match is possible

                    if (searchLength + start > stringLength) {
                        return false;
                    }

                    return indexOf.call(string, searchString, pos) != -1;
                };
                if (defineProperty) {
                    defineProperty(String.prototype, 'includes', {
                        value: includes,
                        configurable: true,
                        writable: true
                    });
                } else {
                    String.prototype.includes = includes;
                }
            }());
        }

        if (!String.prototype.startsWith) {
            (function() {
                // needed to support `apply`/`call` with `undefined`/`null`
                'use strict';

                var defineProperty = (function() {
                    // IE 8 only supports `Object.defineProperty` on DOM elements
                    try {
                        var object = {};
                        var $defineProperty = Object.defineProperty;
                        var result = $defineProperty(object, object, object) && $defineProperty;
                    } catch (error) {
                        // continue regardless of error
                    }

                    return result;
                }());
                var toString = {}.toString;

                /**
                 * If string starts with given string.
                 *
                 * @param  {string} search - Searchable string.
                 * @returns {boolean} If string starts with given string.
                 */
                function startsWith(search) {
                    if (this == null) {
                        throw new TypeError();
                    }
                    var string = String(this);

                    if (search && toString.call(search) == '[object RegExp]') {
                        throw new TypeError();
                    }
                    var stringLength = string.length;
                    var searchString = String(search);
                    var searchLength = searchString.length;
                    var position = arguments.length > 1 ? arguments[1] : undefined;
                    // `ToInteger`
                    var pos = position ? Number(position) : 0;
                    // better `isNaN`

                    if (pos != pos) {
                        pos = 0;
                    }
                    var start = Math.min(Math.max(pos, 0), stringLength);
                    // Avoid the `indexOf` call if no match is possible

                    if (searchLength + start > stringLength) {
                        return false;
                    }
                    var index = -1;

                    while (++index < searchLength) {
                        if (string.charCodeAt(start + index) != searchString.charCodeAt(index)) {
                            return false;
                        }
                    }

                    return true;
                };
                if (defineProperty) {
                    defineProperty(String.prototype, 'startsWith', {
                        value: startsWith,
                        configurable: true,
                        writable: true
                    });
                } else {
                    String.prototype.startsWith = startsWith;
                }
            }());
        }

        if (!Object.keys) {
            // object, key, result
            Object.keys = function(o, k, r) {
                // initialize object and result
                r = [];
                // iterate over object keys
                for (k in o) {
                    // fill result array with non-prototypical keys
                    r.hasOwnProperty.call(o, k) && r.push(k);
                }

                // return result
                return r;
            };
        }

        $.fn.triggerNative = function(eventName) {
            var el = this[0];
            var event;

            if (el.dispatchEvent) {
                if (typeof Event === 'function') {
                    // For modern browsers
                    event = new Event(eventName, {
                        bubbles: true
                    });
                } else {
                    // For IE since it doesn't support Event constructor
                    event = document.createEvent('Event');
                    event.initEvent(eventName, true, false);
                }

                el.dispatchEvent(event);
            } else {
                if (el.fireEvent) {
                    event = document.createEventObject();
                    event.eventType = eventName;
                    el.fireEvent('on' + eventName, event);
                }

                this.trigger(eventName);
            }
        };
        // </editor-fold>

        // Case insensitive contains search
        $.expr[':'].icontains = function(obj, index, meta) {
            var $obj = $(obj);
            var haystack = ($obj.data('tokens') || $obj.text()).toUpperCase();

            return haystack.includes(meta[3].toUpperCase());
        };

        // Case insensitive begins search
        $.expr[':'].ibegins = function(obj, index, meta) {
            var $obj = $(obj);
            var haystack = ($obj.data('tokens') || $obj.text()).toUpperCase();

            return haystack.startsWith(meta[3].toUpperCase());
        };

        // Case and accent insensitive contains search
        $.expr[':'].aicontains = function(obj, index, meta) {
            var $obj = $(obj);
            var haystack = ($obj.data('tokens') || $obj.data('normalizedText') || $obj.text()).toUpperCase();

            return haystack.includes(meta[3].toUpperCase());
        };

        // Case and accent insensitive begins search
        $.expr[':'].aibegins = function(obj, index, meta) {
            var $obj = $(obj);
            var haystack = ($obj.data('tokens') || $obj.data('normalizedText') || $obj.text()).toUpperCase();

            return haystack.startsWith(meta[3].toUpperCase());
        };

        /**
         * Remove all diatrics from the given text.
         *
         * @access private
         * @param {string} text - Given text.
         * @returns {string} Normalized text.
         */
        function normalizeToBase(text) {
            var rExps = [
                {
                    re: /[\xC0-\xC6]/g,
                    ch: 'A'
                },
                {
                    re: /[\xE0-\xE6]/g,
                    ch: 'a'
                },
                {
                    re: /[\xC8-\xCB]/g,
                    ch: 'E'
                },
                {
                    re: /[\xE8-\xEB]/g,
                    ch: 'e'
                },
                {
                    re: /[\xCC-\xCF]/g,
                    ch: 'I'
                },
                {
                    re: /[\xEC-\xEF]/g,
                    ch: 'i'
                },
                {
                    re: /[\xD2-\xD6]/g,
                    ch: 'O'
                },
                {
                    re: /[\xF2-\xF6]/g,
                    ch: 'o'
                },
                {
                    re: /[\xD9-\xDC]/g,
                    ch: 'U'
                },
                {
                    re: /[\xF9-\xFC]/g,
                    ch: 'u'
                },
                {
                    re: /[\xC7-\xE7]/g,
                    ch: 'c'
                },
                {
                    re: /[\xD1]/g,
                    ch: 'N'
                },
                {
                    re: /[\xF1]/g,
                    ch: 'n'
                }
            ];

            $.each(rExps, function() {
                text = text.replace(this.re, this.ch);
            });

            return text;
        }

        /**
         * Escape html.
         *
         * @param  {HTMLElement} html - Given html.
         * @returns {HTMLElement} Escaped html.
         */
        function htmlEscape(html) {
            var escapeMap = {
                '&': '&amp;',
                '<': '&lt;',
                '>': '&gt;',
                '"': '&quot;',
                '\'': '&#x27;',
                '`': '&#x60;'
            };
            var source = '(?:' + Object.keys(escapeMap).join('|') + ')';
            var testRegexp = new RegExp(source);
            var replaceRegexp = new RegExp(source, 'g');
            var string = html == null ? '' : '' + html;

            return testRegexp.test(string) ? string.replace(replaceRegexp, function(match) {
                return escapeMap[match];
            }) : string;
        }

        /**
         * Selectpicker.
         *
         * @class
         * @param {HTMLElement} element - The HTML element the Selectpicker should be bound to.
         * @param {Object} options - An options map.
         * @param {Event} e - Event.
         */
        function Selectpicker(element, options, e) {
            if (e) {
                e.stopPropagation();
                e.preventDefault();
            }

            this.element = element;
            this.$element = $(element);
            this.$newElement = null;
            this.$button = null;
            this.$menu = null;
            this.$lis = null;
            this.options = options;

            // If we have no title yet, try to pull it from the html title attribute (jQuery doesnt' pick it up as it's not a
            // data-attribute)
            if (this.options.title === null) {
                this.options.title = this.$element.attr('title');
            }

            // Expose public methods
            this.val = Selectpicker.prototype.val;
            this.render = Selectpicker.prototype.render;
            this.refresh = Selectpicker.prototype.refresh;
            this.setStyle = Selectpicker.prototype.setStyle;
            this.selectAll = Selectpicker.prototype.selectAll;
            this.deselectAll = Selectpicker.prototype.deselectAll;
            this.destroy = Selectpicker.prototype.destroy;
            this.remove = Selectpicker.prototype.remove;
            this.show = Selectpicker.prototype.show;
            this.hide = Selectpicker.prototype.hide;

            this.init();
        };

        Selectpicker.VERSION = '1.10.0';

        // part of this is duplicated in i18n/defaults-en_US.js. Make sure to update both.
        Selectpicker.DEFAULTS = {
            noneSelectedText: 'Nothing selected',
            noneResultsText: 'No results matched {0}',

            /**
             * Get selected items text.
             *
             * @param   {number} numSelected - Number of selected items.
             * @returns {string} Selected items text.
             */
            countSelectedText: function(numSelected) {
                return (numSelected == 1) ? '{0} item selected' : '{0} items selected';
            },

            /**
             * Get maximum items selected text.
             *
             * @param   {number} numAll - Number of all selected items.
             * @param   {number} numGroup - Number of selected items in group.
             * @returns {Array<string>} Array with maximum items selected text strings.
             */
            maxOptionsText: function(numAll, numGroup) {
                return [
                    (numAll == 1) ? 'Limit reached ({n} item max)' : 'Limit reached ({n} items max)',
                    (numGroup == 1) ? 'Group limit reached ({n} item max)' : 'Group limit reached ({n} items max)'
                ];
            },
            selectAllText: 'Select All',
            deselectAllText: 'Deselect All',
            doneButton: false,
            doneButtonText: 'Close',
            multipleSeparator: ', ',
            styleBase: 'bootstrap-select__trigger',
            style: '',
            size: 10,
            title: null,
            selectedTextFormat: 'values',
            width: false,
            container: 'body',
            hideDisabled: false,
            showSubtext: false,
            showIcon: true,
            showContent: true,
            dropupAuto: false,
            header: false,
            liveSearch: false,
            liveSearchPlaceholder: null,
            liveSearchNormalize: false,
            liveSearchStyle: 'contains',
            actionsBox: false,
            iconBase: 'glyphicon',
            tickIcon: 'glyphicon-ok',
            showTick: false,
            template: {
                caret: ''
            },
            maxOptions: false,
            mobile: false,
            selectOnTab: false,
            dropdownAlignRight: false
        };

        Selectpicker.prototype = {

            constructor: Selectpicker,

            /**
             * Initialize Selectpicker plugin.
             *
             * @returns {void}
             */
            init: function() {
                var that = this;
                var id = this.$element.attr('id');

                this.$element.addClass('bs-select-hidden');

                // store originalIndex (key) and newIndex (value) in this.liObj for fast accessibility
                // allows us to do this.$lis.eq(that.liObj[index]) instead of this.$lis.filter('[data-original-index="' + index + '"]')
                this.liObj = {};
                this.multiple = this.$element.prop('multiple');
                this.autofocus = this.$element.prop('autofocus');
                this.$newElement = this.createView();
                this.$element
                    .after(this.$newElement)
                    .appendTo(this.$newElement);
                this.$button = this.$newElement.children('button');
                this.$menu = this.$newElement.children('.dropdown');
                this.$menuInner = this.$menu.children('.dropdown__menu');
                this.$searchbox = this.$menu.find('input');
                this.$root = this.$menu.parent().parent().parent();

                this.$element.removeClass('bs-select-hidden');

                if (this.options.dropdownAlignRight) this.$menu.addClass('dropdown-menu-right');

                if (typeof id !== 'undefined') {
                    this.$button.attr('data-id', id);
                    $('label[for="' + id + '"]').click(function(e) {
                        e.preventDefault();
                        that.$button.focus();
                    });
                }

                this.checkDisabled();
                this.clickListener();
                if (this.options.liveSearch) {
                    this.$root.addClass('form-select--live');
                    this.liveSearchListener();
                }
                this.render();
                this.setStyle();
                this.setWidth();
                if (this.options.container) this.selectPosition();
                this.$menu.data('this', this);
                this.$newElement.data('this', this);
                if (this.options.mobile) this.mobile();

                this.$newElement.on({
                    /**
                     * Hide event handler.
                     *
                     * @param   {Event} e - Event object.
                     * @returns {void}
                     */
                    'hide.bs.dropdown': function(e) {
                        // delay so that dropdown's own e.preventDefault() would apply
                        setTimeout(function() {
                            if (!e.isDefaultPrevented()) {
                                that.$element.trigger('hide.bs.select', e);
                            }
                        }, 10);
                    },

                    /**
                     * Hidden event handler.
                     *
                     * @param   {Event} e - Event object.
                     * @returns {void}
                     */
                    'hidden.bs.dropdown': function(e) {
                        that.$element.trigger('hidden.bs.select', e);
                    },

                    /**
                     * Show event handler.
                     *
                     * @param   {Event} e - Event object.
                     * @returns {void}
                     */
                    'show.bs.dropdown': function(e) {
                        that.$element.trigger('show.bs.select', e);
                    },

                    /**
                     * Shown event handler.
                     *
                     * @param   {Event} e - Event object.
                     * @returns {void}
                     */
                    'shown.bs.dropdown': function(e) {
                        that.$element.trigger('shown.bs.select', e);
                    }
                });

                if (that.$element[0].hasAttribute('required')) {
                    this.$element.on('invalid', function() {
                        that.$button
                            .addClass('bs-invalid')
                            .focus();

                        that.$element.on({
                            /**
                             * Focus event handler.
                             *
                             * @returns {void}
                             */
                            'focus.bs.select': function() {
                                that.$button.focus();
                                that.$element.off('focus.bs.select');
                            },

                            /**
                             * Shown event handler.
                             *
                             * @returns {void}
                             */
                            'shown.bs.select': function() {
                                that.$element
                                    // set the value to hide the validation message in Chrome when menu is opened
                                    .val(that.$element.val())
                                    .off('shown.bs.select');
                            },

                            /**
                             * Rendered event handler.
                             *
                             * @returns {void}
                             */
                            'rendered.bs.select': function() {
                                // if select is no longer invalid, remove the bs-invalid class
                                if (this.validity.valid) that.$button.removeClass('bs-invalid');
                                that.$element.off('rendered.bs.select');
                            }
                        });
                    });
                }

                setTimeout(function() {
                    that.$element.trigger('loaded.bs.select');
                });

                this.observer = this.createMutationObserver();
            },

            /**
             * Create a mutation observer that refreshes the plugin when options
             * are added/removed from the original select element.
             *
             * @returns {MutationObserver|boolean} False if browser does not support MutationObserver, else created MutationObserver instance.
             */
            createMutationObserver: function() {
                var that = this;
                var watch = this.element;

                if (typeof window.MutationObserver == 'function') {
                    var observer = new window.MutationObserver(function(mutations) {
                        mutations.forEach(function(mutation) {
                            if (mutation.type === 'childList') {
                                that.refresh();
                            }
                        });
                    });

                    observer.observe(watch, {
                        childList: true
                    });

                    return observer;
                } else {
                    return false;
                }
            },

            /**
             * Create select dropdown HTML.
             *
             * @returns {JQuery} Dropdown HTML as jQuery object.
             */
            createDropdown: function() {
                // Options
                // If we are multiple or showTick option is set, then add the show-tick class
                var showTick = (this.multiple || this.options.showTick) ? ' bootstrap-select--show-tick' : '';
                var inputGroup = this.$element.parent().hasClass('input-group') ? ' input-group-btn' : '';
                var autofocus = this.autofocus ? ' autofocus' : '';
                // Elements
                var header = this.options.header ? '<div class="popover-title"><button type="button" class="close" aria-hidden="true">&times;</button>' + this.options.header + '</div>' : '';
                var searchbox = '';

                if (this.options.liveSearch) {
                    searchbox = '<div class="bootstrap-select__searchbox">' +
                    '<input type="text" class="form-textfield__input" autocomplete="off"' +
                    (this.options.liveSearchPlaceholder === null ? '' : ' placeholder="' + htmlEscape(this.options.liveSearchPlaceholder) + '"') + '>' +
                    '</div>';
                }

                var actionsbox = '';

                if (this.multiple && this.options.actionsBox) {
                    actionsbox = '<div class="bs-actionsbox">' +
                    '<div class="btn-group btn-group-sm btn-block">' +
                    '<button type="button" class="actions-btn bs-select-all btn btn-default">' +
                    this.options.selectAllText +
                    '</button>' +
                    '<button type="button" class="actions-btn bs-deselect-all btn btn-default">' +
                    this.options.deselectAllText +
                    '</button>' +
                    '</div>' +
                    '</div>';
                }

                var donebutton = '';

                if (this.multiple && this.options.doneButton) {
                    donebutton = '<div class="bs-donebutton">' +
                    '<div class="btn-group btn-block">' +
                    '<button type="button" class="btn btn-sm btn-default">' +
                    this.options.doneButtonText +
                    '</button>' +
                    '</div>' +
                    '</div>';
                }
                var drop =
                    '<div class="bootstrap-select' + showTick + inputGroup + '">' +
                    '<button type="button" class="' + this.options.styleBase + ' dropdown-toggle" data-toggle="dropdown"' + autofocus + '>' +
                    '<span class="bootstrap-select__selected"></span>' +
                    '</button>' +
                    '<div class="dropdown">' +
                    header +
                    searchbox +
                    actionsbox +
                    '<ul class="dropdown__menu" role="menu">' +
                    '</ul>' +
                    donebutton +
                    '</div>' +
                    '</div>';

                return $(drop);
            },

            /**
             * Create select view.
             *
             * @returns {JQuery} View HTML as jQuery object.
             */
            createView: function() {
                var $drop = this.createDropdown();
                var li = this.createLi();

                $drop.find('ul')[0].innerHTML = li;

                return $drop;
            },

            /**
             * Replace list items.
             *
             * @returns {void}
             */
            reloadLi: function() {
                // Remove all children.
                this.destroyLi();
                // Re build
                var li = this.createLi();

                this.$menuInner[0].innerHTML = li;
            },

            /**
             * Remove all list items.
             *
             * @returns {void}
             */
            destroyLi: function() {
                this.$menu.find('li').remove();
            },

            /**
             * Create list items from options.
             *
             * @returns {string} List items as string.
             */
            createLi: function() {
                var that = this;
                var _li = [];
                var optID = 0;
                var titleOption = document.createElement('option');
                // increment liIndex whenever a new <li> element is created to ensure liObj is correct
                var liIndex = -1;

                // Helper functions
                /**
                 * Generate options.
                 *
                 * @param {HTMLElement} content - HTML content.
                 * @param {number} index - Index number.
                 * @param {string} classes - Classes names.
                 * @param {string} optgroup - Optgroup name.
                 * @returns {string} Options string.
                 */
                function generateLI(content, index, classes, optgroup) {
                    return '<li' +
                        ((typeof classes !== 'undefined' && classes !== '') ? ' class="' + classes + '"' : '') +
                        ((typeof index !== 'undefined' && index !== null) ? ' data-original-index="' + index + '"' : '') +
                        ((typeof optgroup !== 'undefined' && optgroup !== null) ? 'data-optgroup="' + optgroup + '"' : '') +
                        '>' + content + '</li>';
                };

                /**
                 * @param {string} text - Link text.
                 * @param {string} classes - Classes for link.
                 * @param {string} inline - Styles for link.
                 * @param {string} tokens - Tokens for link.
                 * @returns {string} String with link.
                 */
                function generateA(text, classes, inline, tokens) {
                    return '<a tabindex="0"' +
                        (typeof classes !== 'undefined' ? ' class="' + classes + '"' : '') +
                        (typeof inline !== 'undefined' ? ' style="' + inline + '"' : '') +
                        (that.options.liveSearchNormalize ? ' data-normalized-text="' + normalizeToBase(htmlEscape(text)) + '"' : '') +
                        (typeof tokens !== 'undefined' || tokens !== null ? ' data-tokens="' + tokens + '"' : '') +
                        '>' + text + telia.icon.render('#check', 'bootstrap-select__icon') +
                        '</a>';
                };

                if (this.options.title && !this.multiple) {
                    // this option doesn't create a new <li> element, but does add a new option, so liIndex is decreased
                    // since liObj is recalculated on every refresh, liIndex needs to be decreased even if the titleOption is already appended
                    liIndex--;

                    if (!this.$element.find('.bs-title-option').length) {
                        // Use native JS to prepend option (faster)
                        var element = this.$element[0];

                        titleOption.className = 'bs-title-option';
                        titleOption.appendChild(document.createTextNode(this.options.title));
                        titleOption.value = '';
                        element.insertBefore(titleOption, element.firstChild);
                        // Check if selected attribute is already set on an option. If not, select the titleOption option.
                        if ($(element.options[element.selectedIndex]).prop('selected') == false) titleOption.selected = true;
                    }
                }

                this.$element.find('option').each(function(index) {
                    var $this = $(this);

                    liIndex++;

                    if ($this.hasClass('bs-title-option')) return;

                    // Get the class and text for the option
                    var optionClass = this.className || '';
                    var inline = this.style.cssText;
                    var text = $this.data('content') ? $this.data('content') : $this.html();
                    var tokens = $this.data('tokens') ? $this.data('tokens') : null;
                    var subtext = typeof $this.data('subtext') !== 'undefined' ? '<small class="text-muted">' + $this.data('subtext') + '</small>' : '';
                    var icon = typeof $this.data('icon') !== 'undefined' ? '<span class="' + that.options.iconBase + ' ' + $this.data('icon') + '"></span> ' : '';
                    var isOptgroup = this.parentNode.tagName === 'OPTGROUP';
                    var isDisabled = this.disabled || (isOptgroup && this.parentNode.disabled);

                    if (icon !== '' && isDisabled) {
                        icon = '<span>' + icon + '</span>';
                    }

                    if (that.options.hideDisabled && isDisabled && !isOptgroup) {
                        liIndex--;

                        return;
                    }

                    if (!$this.data('content')) {
                        // Prepend any icons and append any subtext to the main text.
                        text = icon + '<span class="bootstrap-select__option">' + text + subtext + '</span>';
                    }

                    if (isOptgroup && $this.data('divider') !== true) {
                        var optGroupClass = ' ' + this.parentNode.className || '';

                        // Is it the first option of the optgroup?
                        if ($this.index() === 0) {
                            optID += 1;

                            // Get the opt group label
                            var label = this.parentNode.label;
                            var labelSubtext = typeof $this.parent().data('subtext') !== 'undefined' ? '<small class="text-muted">' + $this.parent().data('subtext') + '</small>' : '';
                            var labelIcon = $this.parent().data('icon') ? '<span class="' + that.options.iconBase + ' ' + $this.parent().data('icon') + '"></span> ' : '';

                            label = labelIcon + '<span class="bootstrap-select__option">' + label + labelSubtext + '</span>';

                            // Is it NOT the first option of the select && are there elements in the dropdown?
                            if (index !== 0 && _li.length > 0) {
                                liIndex++;
                                _li.push(generateLI('', null, 'divider', optID + 'div'));
                            }
                            liIndex++;
                            _li.push(generateLI(label, null, 'dropdown__header' + optGroupClass, optID));
                        }

                        if (that.options.hideDisabled && isDisabled) {
                            liIndex--;

                            return;
                        }

                        _li.push(generateLI(generateA(text, 'opt ' + optionClass + optGroupClass, inline, tokens), index, '', optID));
                    } else if ($this.data('divider') === true) {
                        _li.push(generateLI('', index, 'divider'));
                    } else if ($this.data('hidden') === true) {
                        _li.push(generateLI(generateA(text, optionClass, inline, tokens), index, 'hidden is-hidden'));
                    } else {
                        if (this.previousElementSibling && this.previousElementSibling.tagName === 'OPTGROUP') {
                            liIndex++;
                            _li.push(generateLI('', null, 'divider', optID + 'div'));
                        }
                        _li.push(generateLI(generateA(text, optionClass, inline, tokens), index));
                    }

                    that.liObj[index] = liIndex;
                });

                // If we are not multiple, we don't have a selected item, and we don't have a title, select the first element so something is set in the button
                if (!this.multiple && this.$element.find('option:selected').length === 0 && !this.options.title) {
                    this.$element.find('option').eq(0).prop('selected', true).attr('selected', 'selected');
                }

                return _li.join('');
            },

            /**
             * Find list items.
             *
             * @returns {JQuery} List items.
             */
            findLis: function() {
                if (this.$lis == null) this.$lis = this.$menu.find('li');

                return this.$lis;
            },

            /**
             * Render select.
             *
             * @param {boolean} updateLi - Defaults to true.
             * @returns {void}
             */
            render: function(updateLi) {
                var that = this;
                var notDisabled;

                // Update the LI to match the SELECT
                if (updateLi !== false) {
                    this.$element.find('option').each(function(index) {
                        var $lis = that.findLis().eq(that.liObj[index]);

                        that.setDisabled(index, this.disabled || this.parentNode.tagName === 'OPTGROUP' && this.parentNode.disabled, $lis);
                        that.setSelected(index, this.selected, $lis);
                    });
                }

                this.tabIndex();

                var selectedItems = this.$element.find('option').map(function() {
                    if (this.selected) {
                        if (that.options.hideDisabled && (this.disabled || this.parentNode.tagName === 'OPTGROUP' && this.parentNode.disabled)) return;

                        var $this = $(this);
                        var icon = $this.data('icon') && that.options.showIcon ? '<i class="' + that.options.iconBase + ' ' + $this.data('icon') + '"></i> ' : '';
                        var subtext;

                        if (that.options.showSubtext && $this.data('subtext') && !that.multiple) {
                            subtext = ' <small class="text-muted">' + $this.data('subtext') + '</small>';
                        } else {
                            subtext = '';
                        }
                        if (typeof $this.attr('title') !== 'undefined') {
                            return $this.attr('title');
                        } else if ($this.data('content') && that.options.showContent) {
                            return $this.data('content');
                        } else {
                            return icon + $this.html() + subtext;
                        }
                    }
                }).toArray();

                // Fixes issue in IE10 occurring when no default option is selected and at least one option is disabled
                // Convert all the values into a comma delimited string
                var title = !this.multiple ? selectedItems[0] : selectedItems.join(this.options.multipleSeparator);

                // If this is multi select, and the selectText type is count, the show 1 of 2 selected etc..
                if (this.multiple && this.options.selectedTextFormat.indexOf('count') > -1) {
                    var max = this.options.selectedTextFormat.split('>');

                    if ((max.length > 1 && selectedItems.length > max[1]) || (max.length == 1 && selectedItems.length >= 2)) {
                        notDisabled = this.options.hideDisabled ? ', [disabled]' : '';
                        var totalCount = this.$element.find('option').not('[data-divider="true"], [data-hidden="true"]' + notDisabled).length;
                        var tr8nText = (typeof this.options.countSelectedText === 'function') ? this.options.countSelectedText(selectedItems.length, totalCount) : this.options.countSelectedText;

                        title = tr8nText.replace('{0}', selectedItems.length.toString()).replace('{1}', totalCount.toString());
                    }
                }

                if (this.options.title == undefined) {
                    this.options.title = this.$element.attr('title');
                }

                if (this.options.selectedTextFormat == 'static') {
                    title = this.options.title;
                }

                // If we dont have a title, then use the default, or if nothing is set at all, use the not selected text
                if (!title) {
                    title = typeof this.options.title !== 'undefined' ? this.options.title : '';
                }

                // strip all html-tags and trim the result
                this.$button.attr('title', $.trim(title.replace(/<[^>]*>?/g, '')));
                this.$button.children('.bootstrap-select__selected').html(title);

                this.$element.trigger('rendered.bs.select');
            },

            /**
             * Set styles.
             *
             * @param {string} style - Button style classes.
             * @param {string} status - Status for button usage.
             * @returns {void}
             */
            setStyle: function(style, status) {
                if (this.$element.attr('class')) {
                    this.$newElement.addClass(this.$element.attr('class').replace(/selectpicker|mobile-device|bs-select-hidden|validate\[.*\]/gi, ''));
                }

                var buttonClass = style ? style : this.options.style;

                if (status == 'add') {
                    this.$button.addClass(buttonClass);
                } else if (status == 'remove') {
                    this.$button.removeClass(buttonClass);
                } else {
                    this.$button.removeClass(this.options.style);
                    this.$button.addClass(buttonClass);
                }
            },

            /**
             * Calculate list items height.
             *
             * @param   {boolean} refresh - If should refresh sizeInfo.
             * @returns {void}
             */
            liHeight: function(refresh) {
                if (!refresh && (this.options.size === false || this.sizeInfo)) return;

                var newElement = document.createElement('div');
                var menu = document.createElement('div');
                var menuInner = document.createElement('ul');
                var divider = document.createElement('li');
                var li = document.createElement('li');
                var a = document.createElement('a');
                var text = document.createElement('span');
                var header = this.options.header && this.$menu.find('.popover-title').length > 0 ? this.$menu.find('.popover-title')[0].cloneNode(true) : null;
                var search = this.options.liveSearch ? document.createElement('div') : null;
                var actions = this.options.actionsBox && this.multiple && this.$menu.find('.bs-actionsbox').length > 0 ? this.$menu.find('.bs-actionsbox')[0].cloneNode(true) : null;
                var doneButton = this.options.doneButton && this.multiple && this.$menu.find('.bs-donebutton').length > 0 ? this.$menu.find('.bs-donebutton')[0].cloneNode(true) : null;

                text.className = 'bootstrap-select__option';
                newElement.className = this.$menu[0].parentNode.className + ' open';
                menu.className = 'dropdown open';
                menuInner.className = 'dropdown__menu';
                divider.className = 'divider';

                text.appendChild(document.createTextNode('Inner text'));
                a.appendChild(text);
                li.appendChild(a);
                menuInner.appendChild(li);
                menuInner.appendChild(divider);
                if (header) menu.appendChild(header);
                if (search) {
                    // create a span instead of input as creating an input element is slower
                    var input = document.createElement('span');

                    search.className = 'bootstrap-select__searchbox';
                    input.className = 'form-textfield__input';
                    search.appendChild(input);
                    menu.appendChild(search);
                }
                if (actions) menu.appendChild(actions);
                menu.appendChild(menuInner);
                if (doneButton) menu.appendChild(doneButton);
                newElement.appendChild(menu);

                document.body.appendChild(newElement);
                var liHeight = a.offsetHeight;
                var headerHeight = header ? header.offsetHeight : 0;
                // input height 32px, offset gives 6px
                var searchHeight = search ? 32 : 0;
                var actionsHeight = actions ? actions.offsetHeight : 0;
                var doneButtonHeight = doneButton ? doneButton.offsetHeight : 0;
                var dividerHeight = $(divider).outerHeight(true);
                // fall back to jQuery if getComputedStyle is not supported
                var menuStyle = typeof getComputedStyle === 'function' ? getComputedStyle(menu) : false;
                var $menu = menuStyle ? null : $(menu);
                var menuPadding = parseInt(menuStyle ? menuStyle.paddingTop : $menu.css('paddingTop')) +
                        parseInt(menuStyle ? menuStyle.paddingBottom : $menu.css('paddingBottom')) +
                        parseInt(menuStyle ? menuStyle.borderTopWidth : $menu.css('borderTopWidth')) +
                        parseInt(menuStyle ? menuStyle.borderBottomWidth : $menu.css('borderBottomWidth'));
                var menuExtras =  menuPadding +
                        parseInt(menuStyle ? menuStyle.marginTop : $menu.css('marginTop')) +
                        parseInt(menuStyle ? menuStyle.marginBottom : $menu.css('marginBottom')) + 2;

                document.body.removeChild(newElement);

                this.sizeInfo = {
                    liHeight: liHeight,
                    headerHeight: headerHeight,
                    searchHeight: searchHeight,
                    actionsHeight: actionsHeight,
                    doneButtonHeight: doneButtonHeight,
                    dividerHeight: dividerHeight,
                    menuPadding: menuPadding,
                    menuExtras: menuExtras
                };
            },

            /**
             * Set dropdown size.
             *
             * @returns {void}
             */
            setSize: function() {
                this.findLis();
                this.liHeight();

                if (this.options.header) this.$menu.css('padding-top', 0);
                if (this.options.size === false) return;

                var that = this;
                var $menu = this.$menu;
                var $menuInner = this.$menuInner;
                var $window = $(window);
                var selectHeight = this.$newElement[0].offsetHeight;
                var liHeight = this.sizeInfo['liHeight'];
                var headerHeight = this.sizeInfo['headerHeight'];
                var searchHeight = this.sizeInfo['searchHeight'];
                var actionsHeight = this.sizeInfo['actionsHeight'];
                var doneButtonHeight = this.sizeInfo['doneButtonHeight'];
                var divHeight = this.sizeInfo['dividerHeight'];
                var menuPadding = this.sizeInfo['menuPadding'];
                var menuExtras = this.sizeInfo['menuExtras'];
                var notDisabled = this.options.hideDisabled ? '.disabled' : '';
                var menuHeight;
                var getHeight;
                var selectOffsetTop;
                var selectOffsetBot;

                /**
                 * Position vertically.
                 *
                 * @returns {void}
                 */
                function posVert() {
                    selectOffsetTop = that.$newElement.offset().top - $window.scrollTop();
                    selectOffsetBot = $window.height() - selectOffsetTop - selectHeight;
                };

                /**
                 * Get size.
                 *
                 * @returns {void}
                 */
                function getSize() {
                    var minHeight;
                    var lis = that.$menuInner[0].getElementsByTagName('li');
                    var lisVisible = Array.prototype.filter ? Array.prototype.filter.call(lis, hasClass('hidden', false)) : that.$lis.not('.hidden');
                    var optGroup = Array.prototype.filter ? Array.prototype.filter.call(lisVisible, hasClass('dropdown__header', true)) : lisVisible.filter('.dropdown__header');

                    /**
                     * @param  {string} className - Class name.
                     * @param  {boolean} include - Is included.
                     * @returns {boolean} If has class.
                     */
                    function hasClass(className, include) {
                        return function(element) {
                            if (include) {
                                return (element.classList ? element.classList.contains(className) : $(element).hasClass(className));
                            } else {
                                return !(element.classList ? element.classList.contains(className) : $(element).hasClass(className));
                            }
                        };
                    }

                    posVert();
                    menuHeight = selectOffsetBot - menuExtras;

                    if (that.options.container) {
                        if (!$menu.data('height')) $menu.data('height', $menu.height());
                        getHeight = $menu.data('height');
                    } else {
                        getHeight = $menu.height();
                    }

                    if (that.options.dropupAuto) {
                        that.$newElement.toggleClass('dropup', selectOffsetTop > selectOffsetBot && (menuHeight - menuExtras) < getHeight);
                    }
                    if (that.$newElement.hasClass('dropup')) {
                        menuHeight = selectOffsetTop - menuExtras;
                    }

                    if ((lisVisible.length + optGroup.length) > 3) {
                        minHeight = liHeight * 3 + menuExtras - 2;
                    } else {
                        minHeight = 0;
                    }

                    menuHeight -= 40;
                    $menu.css({
                        'max-height': menuHeight + 'px',
                        overflow: 'hidden',
                        'min-height': minHeight + headerHeight + searchHeight + actionsHeight + doneButtonHeight + 'px'
                    });
                    $menuInner.css({
                        'max-height': menuHeight - headerHeight - searchHeight - actionsHeight - doneButtonHeight - menuPadding + 'px',
                        'overflow-y': 'auto',
                        'min-height': Math.max(minHeight - menuPadding, 0) + 'px'
                    });
                }

                posVert();

                if (this.options.size === 'auto') {
                    getSize();
                    this.$searchbox.off('input.getSize propertychange.getSize').on('input.getSize propertychange.getSize', getSize);
                    $window.off('resize.getSize scroll.getSize').on('resize.getSize scroll.getSize', getSize);
                } else if (this.options.size && this.options.size != 'auto' && this.$lis.not(notDisabled).length > this.options.size) {
                    var optIndex = this.$lis.not('.divider').not(notDisabled).children().slice(0, this.options.size).last().parent().index();
                    var divLength = this.$lis.slice(0, optIndex + 1).filter('.divider').length;

                    menuHeight = liHeight * this.options.size + divLength * divHeight + menuPadding;

                    if (that.options.container) {
                        if (!$menu.data('height')) $menu.data('height', $menu.height());
                        getHeight = $menu.data('height');
                    } else {
                        getHeight = $menu.height();
                    }

                    if (that.options.dropupAuto) {
                        // noinspection JSUnusedAssignment
                        this.$newElement.toggleClass('dropup', selectOffsetTop > selectOffsetBot && (menuHeight - menuExtras) < getHeight);
                    }
                    $menu.css({
                        'max-height': menuHeight + headerHeight + searchHeight + actionsHeight + doneButtonHeight + 'px',
                        overflow: 'hidden',
                        'min-height': ''
                    });
                    $menuInner.css({
                        'max-height': menuHeight - menuPadding + 'px',
                        'overflow-y': 'auto',
                        'min-height': ''
                    });
                }
            },

            /**
             * Set dropdown width.
             *
             * @returns {void}
             */
            setWidth: function() {
                if (this.options.width === 'auto') {
                    this.$menu.css('min-width', '0');

                    // Get correct width if element is hidden
                    var $selectClone = this.$menu.parent().clone().appendTo('body');
                    var $selectClone2 = this.options.container ? this.$newElement.clone().appendTo('body') : $selectClone;
                    var ulWidth = $selectClone.children('.dropdown').outerWidth();
                    var btnWidth = $selectClone2.css('width', 'auto').children('button').outerWidth();

                    $selectClone.remove();
                    $selectClone2.remove();

                    // Set width to whatever's larger, button title or longest option
                    this.$newElement.css('width', Math.max(ulWidth, btnWidth) + 'px');
                } else if (this.options.width === 'fit') {
                    // Remove inline min-width so width can be changed from 'auto'
                    this.$menu.css('min-width', '');
                    this.$newElement.css('width', '').addClass('fit-width');
                } else if (this.options.width) {
                    // Remove inline min-width so width can be changed from 'auto'
                    this.$menu.css('min-width', '');
                    this.$newElement.css('width', this.options.width);
                } else {
                    // Remove inline min-width/width so width can be changed
                    this.$menu.css('min-width', '');
                    this.$newElement.css('width', '');
                }
                // Remove fit-width class if width is changed programmatically
                if (this.$newElement.hasClass('fit-width') && this.options.width !== 'fit') {
                    this.$newElement.removeClass('fit-width');
                }
            },

            /**
             * Position dropdown.
             *
             * @returns {void}
             */
            selectPosition: function() {
                this.$bsContainer = $('<div class="bootstrap-select-container" />');

                var that = this;
                var pos;

                /**
                 * Get element placement.
                 *
                 * @param  {JQuery} $element - Element.
                 * @returns {void}
                 */
                function getPlacement($element) {
                    that.$bsContainer.addClass($element.attr('class').replace(/form-textfield__input|fit-width/gi, '')).toggleClass('dropup', $element.hasClass('dropup'));
                    pos = $element.positionFromScrollParent();
                    that.$bsContainer.css({
                        top: pos.top,
                        left: pos.left,
                        width: $element[0].offsetWidth
                    });
                };

                this.$button.on('click', function() {
                    var $this = $(this);

                    if (that.isDisabled()) {
                        return;
                    }

                    that.options.container = window.getScrollParent(that.$newElement[0]);

                    getPlacement(that.$newElement);

                    that.$bsContainer
                        .appendTo(that.options.container)
                        .toggleClass('open', !$this.hasClass('open'))
                        .append(that.$menu);
                    that.$root.toggleClass('is-focused', !that.$root.hasClass('is-focused'));
                    that.$menu.toggleClass('open', !$this.hasClass('open'));
                    if (that.$root.hasClass('form-textfield--employee')) {
                        that.$menu.addClass('dropdown--employee');
                    }
                });

                $(window).on('resize scroll', function() {
                    getPlacement(that.$newElement);
                });

                this.$element.on('hide.bs.select', function() {
                    that.$menu.data('height', that.$menu.height());
                    that.$bsContainer.detach();
                    that.$root.removeClass('is-focused');
                });
            },

            /**
             * Set selected list items.
             *
             * @param   {number} index - List item index.
             * @param   {boolean} selected - If list item is selected.
             * @param   {JQuery} $lis - All list items.
             * @returns {void}
             */
            setSelected: function(index, selected, $lis) {
                if (!$lis) {
                    $lis = this.findLis().eq(this.liObj[index]);
                }

                $lis.toggleClass('selected', selected);
            },

            /**
             * Set disabled list items.
             *
             * @param   {number} index - List item index.
             * @param   {boolean} disabled - If list item is disabled.
             * @param   {JQuery} $lis - All list items.
             * @returns {void}
             */
            setDisabled: function(index, disabled, $lis) {
                if (!$lis) {
                    $lis = this.findLis().eq(this.liObj[index]);
                }

                if (disabled) {
                    $lis.addClass('disabled').children('a').attr('href', '#').attr('tabindex', -1);
                } else {
                    $lis.removeClass('disabled').children('a').removeAttr('href').attr('tabindex', 0);
                }
            },

            /**
             * Get if original select is disabled.
             *
             * @returns {boolean} True if is disabled.
             */
            isDisabled: function() {
                return this.$element[0].disabled;
            },

            /**
             * Check if original select is disabled and set custom dropdown to disabled accordingly.
             *
             * @returns {void}
             */
            checkDisabled: function() {
                var that = this;

                if (this.isDisabled()) {
                    this.$newElement.addClass('disabled');
                    this.$button.addClass('disabled').attr('tabindex', -1);
                } else {
                    if (this.$button.hasClass('disabled')) {
                        this.$newElement.removeClass('disabled');
                        this.$button.removeClass('disabled');
                    }

                    if (this.$button.attr('tabindex') == -1 && !this.$element.data('tabindex')) {
                        this.$button.removeAttr('tabindex');
                    }
                }

                this.$button.click(function() {
                    return !that.isDisabled();
                });
            },

            /**
             * Set original select tabIndex to -98.
             *
             * @returns {void}
             */
            tabIndex: function() {
                if (this.$element.data('tabindex') !== this.$element.attr('tabindex') &&
                    (this.$element.attr('tabindex') !== -98 && this.$element.attr('tabindex') !== '-98')) {
                    this.$element.data('tabindex', this.$element.attr('tabindex'));
                    this.$button.attr('tabindex', this.$element.data('tabindex'));
                }

                this.$element.attr('tabindex', -98);
            },

            /**
             * Dropdown button click listener.
             *
             * @returns {void}
             */
            clickListener: function() {
                var that = this;
                var $document = $(document);

                this.$newElement.on('touchstart.dropdown', '.dropdown', function(e) {
                    e.stopPropagation();
                });

                $document.data('spaceSelect', false);

                this.$button.on('keyup', function(e) {
                    if (/(32)/.test(e.keyCode.toString(10)) && $document.data('spaceSelect')) {
                        e.preventDefault();
                        $document.data('spaceSelect', false);
                    }
                });

                this.$button.on('click', function() {
                    that.setSize();
                });

                this.$element.on('shown.bs.select', function() {
                    if (!that.options.liveSearch && !that.multiple) {
                        that.$menuInner.find('.selected a').focus();
                    } else if (!that.multiple) {
                        var selectedIndex = that.liObj[that.$element[0].selectedIndex];

                        if (typeof selectedIndex !== 'number' || that.options.size === false) return;

                        // scroll to selected option
                        var offset = that.$lis.eq(selectedIndex)[0].offsetTop - that.$menuInner[0].offsetTop;

                        offset = offset - that.$menuInner[0].offsetHeight / 2 + that.sizeInfo.liHeight / 2;
                        that.$menuInner[0].scrollTop = offset;
                    }
                });

                this.$menuInner.on('click', 'li a', function(e) {
                    var $this = $(this);
                    var clickedIndex = $this.parent().data('originalIndex');
                    var prevValue = that.$element.val();
                    var prevIndex = that.$element.prop('selectedIndex');

                    // Don't close on multi choice menu
                    if (that.multiple) {
                        e.stopPropagation();
                    }

                    e.preventDefault();

                    // Don't run if we have been disabled
                    if (!that.isDisabled() && !$this.parent().hasClass('disabled')) {
                        var $options = that.$element.find('option');
                        var $option = $options.eq(clickedIndex);
                        var state = $option.prop('selected');
                        var $optgroup = $option.parent('optgroup');
                        var maxOptions = that.options.maxOptions;
                        var maxOptionsGrp = $optgroup.data('maxOptions') || false;

                        // Deselect all others if not multi select box
                        if (!that.multiple) {
                            $options.prop('selected', false);
                            $option.prop('selected', true);
                            that.$menuInner.find('.selected').removeClass('selected');
                            that.setSelected(clickedIndex, true);
                        // Toggle the one we have chosen if we are multi select.
                        } else {
                            $option.prop('selected', !state);
                            that.setSelected(clickedIndex, !state);
                            $this.blur();

                            if (maxOptions !== false || maxOptionsGrp !== false) {
                                var maxReached = maxOptions < $options.filter(':selected').length;
                                var maxReachedGrp = maxOptionsGrp < $optgroup.find('option:selected').length;

                                if ((maxOptions && maxReached) || (maxOptionsGrp && maxReachedGrp)) {
                                    if (maxOptions && maxOptions == 1) {
                                        $options.prop('selected', false);
                                        $option.prop('selected', true);
                                        that.$menuInner.find('.selected').removeClass('selected');
                                        that.setSelected(clickedIndex, true);
                                    } else if (maxOptionsGrp && maxOptionsGrp == 1) {
                                        $optgroup.find('option:selected').prop('selected', false);
                                        $option.prop('selected', true);
                                        var optgroupID = $this.parent().data('optgroup');

                                        that.$menuInner.find('[data-optgroup="' + optgroupID + '"]').removeClass('selected');
                                        that.setSelected(clickedIndex, true);
                                    } else {
                                        var maxOptionsArr = that.options.maxOptionsText;

                                        if (typeof that.options.maxOptionsText === 'function') {
                                            maxOptionsArr = that.options.maxOptionsText(maxOptions, maxOptionsGrp);
                                        }
                                        var maxTxt = maxOptionsArr[0].replace('{n}', maxOptions);
                                        var maxTxtGrp = maxOptionsArr[1].replace('{n}', maxOptionsGrp);
                                        var $notify = $('<div class="notify"></div>');
                                        // If {var} is set in array, replace it
                                        /** @deprecated */

                                        if (maxOptionsArr[2]) {
                                            maxTxt = maxTxt.replace('{var}', maxOptionsArr[2][maxOptions > 1 ? 0 : 1]);
                                            maxTxtGrp = maxTxtGrp.replace('{var}', maxOptionsArr[2][maxOptionsGrp > 1 ? 0 : 1]);
                                        }

                                        $option.prop('selected', false);

                                        that.$menu.append($notify);

                                        if (maxOptions && maxReached) {
                                            $notify.append($('<div>' + maxTxt + '</div>'));
                                            that.$element.trigger('maxReached.bs.select');
                                        }

                                        if (maxOptionsGrp && maxReachedGrp) {
                                            $notify.append($('<div>' + maxTxtGrp + '</div>'));
                                            that.$element.trigger('maxReachedGrp.bs.select');
                                        }

                                        setTimeout(function() {
                                            that.setSelected(clickedIndex, false);
                                        }, 10);

                                        $notify.delay(750).fadeOut(300, function() {
                                            $(this).remove();
                                        });
                                    }
                                }
                            }
                        }

                        if (!that.multiple) {
                            that.$button.focus();
                        } else if (that.options.liveSearch) {
                            that.$searchbox.focus();
                        }

                        // Trigger select 'change'
                        if ((prevValue != that.$element.val() && that.multiple) || (prevIndex != that.$element.prop('selectedIndex') && !that.multiple)) {
                            // $option.prop('selected') is current option state (selected/unselected). state is previous option state.
                            that.$element
                                .trigger('changed.bs.select', [clickedIndex, $option.prop('selected'), state])
                                .triggerNative('change');
                        }
                    }
                });

                this.$menu.on('click', 'li.disabled a, .popover-title, .popover-title :not(.close)', function(e) {
                    if (e.currentTarget == this) {
                        e.preventDefault();
                        e.stopPropagation();
                        if (that.options.liveSearch && !$(e.target).hasClass('close')) {
                            that.$searchbox.focus();
                        } else {
                            that.$button.focus();
                        }
                    }
                });

                this.$menuInner.on('click', '.divider, .dropdown__header', function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                    if (that.options.liveSearch) {
                        that.$searchbox.focus();
                    } else {
                        that.$button.focus();
                    }
                });

                this.$menu.on('click', '.popover-title .close', function() {
                    that.$button.click();
                });

                this.$searchbox.on('click', function(e) {
                    e.stopPropagation();
                });

                this.$menu.on('click', '.actions-btn', function(e) {
                    if (that.options.liveSearch) {
                        that.$searchbox.focus();
                    } else {
                        that.$button.focus();
                    }

                    e.preventDefault();
                    e.stopPropagation();

                    if ($(this).hasClass('bs-select-all')) {
                        that.selectAll();
                    } else {
                        that.deselectAll();
                    }
                });

                this.$element.change(function() {
                    that.render(false);
                });
            },

            /**
             * Live search listener.
             *
             * @returns {void}
             */
            liveSearchListener: function() {
                var that = this;
                var $noResults = $('<li class="bootstrap-select__no-results"></li>');

                this.$button.on('click.dropdown.data-api touchstart.dropdown.data-api', function() {
                    that.$menuInner.find('.is-active').removeClass('is-active');
                    if (that.$searchbox.val()) {
                        that.$searchbox.val('');
                        that.$lis.not('.is-hidden').removeClass('hidden');
                        if ($noResults.parent().length) $noResults.remove();
                    }
                    if (!that.multiple) that.$menuInner.find('.selected').addClass('is-active');
                    setTimeout(function() {
                        that.$searchbox.focus();
                    }, 10);
                });

                this.$searchbox.on('click.dropdown.data-api focus.dropdown.data-api touchend.dropdown.data-api', function(e) {
                    e.stopPropagation();
                });

                this.$searchbox.on('input propertychange', function() {
                    if (that.$searchbox.val()) {
                        var $searchBase = that.$lis.not('.is-hidden').removeClass('hidden').children('a');

                        if (that.options.liveSearchNormalize) {
                            $searchBase = $searchBase.not(':a' + that._searchStyle() + '("' + normalizeToBase(that.$searchbox.val()) + '")');
                        } else {
                            $searchBase = $searchBase.not(':' + that._searchStyle() + '("' + that.$searchbox.val() + '")');
                        }
                        $searchBase.parent().addClass('hidden');

                        that.$lis.filter('.dropdown__header').each(function() {
                            var $this = $(this);
                            var optgroup = $this.data('optgroup');

                            if (that.$lis.filter('[data-optgroup=' + optgroup + ']').not($this).not('.hidden').length === 0) {
                                $this.addClass('hidden');
                                that.$lis.filter('[data-optgroup=' + optgroup + 'div]').addClass('hidden');
                            }
                        });

                        var $lisVisible = that.$lis.not('.hidden');

                        // hide divider if first or last visible, or if followed by another divider
                        $lisVisible.each(function(index) {
                            var $this = $(this);

                            if ($this.hasClass('divider') && (
                                $this.index() === $lisVisible.first().index() ||
                                $this.index() === $lisVisible.last().index() ||
                                $lisVisible.eq(index + 1).hasClass('divider'))) {
                                $this.addClass('hidden');
                            }
                        });

                        if (!that.$lis.not('.hidden, .bootstrap-select__no-results').length) {
                            if ($noResults.parent().length) {
                                $noResults.remove();
                            }
                            $noResults.html(that.options.noneResultsText.replace('{0}', '"' + htmlEscape(that.$searchbox.val()) + '"')).show();
                            that.$menuInner.append($noResults);
                        } else if ($noResults.parent().length) {
                            $noResults.remove();
                        }
                    } else {
                        that.$lis.not('.is-hidden').removeClass('hidden');
                        if ($noResults.parent().length) {
                            $noResults.remove();
                        }
                    }

                    that.$lis.filter('.is-active').removeClass('is-active');
                    if (that.$searchbox.val()) that.$lis.not('.hidden, .divider, .dropdown__header').eq(0).addClass('is-active').children('a').focus();
                    $(this).focus();
                });
            },

            /**
             * Get live search style.
             *
             * @returns {string} Live search style.
             */
            _searchStyle: function() {
                var styles = {
                    begins: 'ibegins',
                    startsWith: 'ibegins'
                };

                return styles[this.options.liveSearchStyle] || 'icontains';
            },

            /**
             * Get or set select value.
             *
             * @param   {value} value - New value.
             * @returns {JQuery|string} String if getting value, else select element as jQuery object.
             */
            val: function(value) {
                if (typeof value !== 'undefined') {
                    this.$element.val(value);
                    this.render();

                    return this.$element;
                } else {
                    return this.$element.val();
                }
            },

            /**
             * Change all options.
             *
             * @param   {boolean} status - If should select or deselect all options.
             * @returns {void}
             */
            changeAll: function(status) {
                if (typeof status === 'undefined') status = true;

                this.findLis();

                var $options = this.$element.find('option');
                var $lisVisible = this.$lis.not('.divider, .dropdown__header, .disabled, .hidden').toggleClass('selected', status);
                var lisVisLen = $lisVisible.length;
                var selectedOptions = [];

                for (var i = 0; i < lisVisLen; i++) {
                    var origIndex = $lisVisible[i].getAttribute('data-original-index');

                    selectedOptions[selectedOptions.length] = $options.eq(origIndex)[0];
                }

                $(selectedOptions).prop('selected', status);

                this.render(false);

                this.$element
                    .trigger('changed.bs.select')
                    .triggerNative('change');
            },

            /**
             * Select all options.
             *
             * @returns {void}
             */
            selectAll: function() {
                return this.changeAll(true);
            },

            /**
             * Deselect all options.
             *
             * @returns {void}
             */
            deselectAll: function() {
                return this.changeAll(false);
            },

            /**
             * Toggle dropdown state.
             *
             * @param   {Event} e - Event object.
             * @returns {void}
             */
            toggle: function(e) {
                e = e || window.event;

                if (e) e.stopPropagation();

                this.$button.trigger('click');
            },

            /**
             * Handle keyboard events.
             *
             * @param   {Event} e - Event object.
             * @returns {void}
             */
            keydown: function(e) {
                var $this = $(this);
                var $parent = $this.is('input') ? $this.parent().parent() : $this.parent();
                var $items;
                var that = $parent.data('this');
                var index;
                var next;
                var first;
                var last;
                var prev;
                var nextPrev;
                var prevIndex;
                var isActive;
                var selector = ':not(.disabled, .hidden, .dropdown__header, .divider)';
                var keyCodeMap = {
                    32: ' ',
                    48: '0',
                    49: '1',
                    50: '2',
                    51: '3',
                    52: '4',
                    53: '5',
                    54: '6',
                    55: '7',
                    56: '8',
                    57: '9',
                    59: ';',
                    65: 'a',
                    66: 'b',
                    67: 'c',
                    68: 'd',
                    69: 'e',
                    70: 'f',
                    71: 'g',
                    72: 'h',
                    73: 'i',
                    74: 'j',
                    75: 'k',
                    76: 'l',
                    77: 'm',
                    78: 'n',
                    79: 'o',
                    80: 'p',
                    81: 'q',
                    82: 'r',
                    83: 's',
                    84: 't',
                    85: 'u',
                    86: 'v',
                    87: 'w',
                    88: 'x',
                    89: 'y',
                    90: 'z',
                    96: '0',
                    97: '1',
                    98: '2',
                    99: '3',
                    100: '4',
                    101: '5',
                    102: '6',
                    103: '7',
                    104: '8',
                    105: '9'
                };

                if (that.options.liveSearch) $parent = $this.parent().parent();

                if (that.options.container) $parent = that.$menu;

                $items = $('[role=menu] li', $parent);

                isActive = that.$newElement.hasClass('open');

                if (!isActive && (e.keyCode >= 48 && e.keyCode <= 57 || e.keyCode >= 96 && e.keyCode <= 105 || e.keyCode >= 65 && e.keyCode <= 90)) {
                    if (!that.options.container) {
                        that.setSize();
                        that.$menu.parent().addClass('open');
                        isActive = true;
                    } else {
                        that.$button.trigger('click');
                    }
                    that.$searchbox.focus();
                }

                if (that.options.liveSearch) {
                    if (/(^9$|27)/.test(e.keyCode.toString(10)) && isActive && that.$menu.find('.is-active').length === 0) {
                        e.preventDefault();
                        that.$menu.parent().removeClass('open');
                        if (that.options.container) that.$newElement.removeClass('open');
                        that.$button.focus();
                    }
                    // $items contains li elements when liveSearch is enabled
                    $items = $('[role=menu] li' + selector, $parent);
                    if (!$this.val() && !(/(38|40)/).test(e.keyCode.toString(10))) {
                        if ($items.filter('.is-active').length === 0) {
                            $items = that.$menuInner.find('li');
                            if (that.options.liveSearchNormalize) {
                                $items = $items.filter(':a' + that._searchStyle() + '(' + normalizeToBase(keyCodeMap[e.keyCode]) + ')');
                            } else {
                                $items = $items.filter(':' + that._searchStyle() + '(' + keyCodeMap[e.keyCode] + ')');
                            }
                        }
                    }
                }

                if (!$items.length) return;

                if (/(38|40)/.test(e.keyCode.toString(10))) {
                    index = $items.index($items.find('a').filter(':focus').parent());
                    first = $items.filter(selector).first().index();
                    last = $items.filter(selector).last().index();
                    next = $items.eq(index).nextAll(selector).eq(0).index();
                    prev = $items.eq(index).prevAll(selector).eq(0).index();
                    nextPrev = $items.eq(next).prevAll(selector).eq(0).index();

                    if (that.options.liveSearch) {
                        $items.each(function(i) {
                            if (!$(this).hasClass('disabled')) {
                                $(this).data('index', i);
                            }
                        });
                        index = $items.index($items.filter('.is-active'));
                        first = $items.first().data('index');
                        last = $items.last().data('index');
                        next = $items.eq(index).nextAll().eq(0).data('index');
                        prev = $items.eq(index).prevAll().eq(0).data('index');
                        nextPrev = $items.eq(next).prevAll().eq(0).data('index');
                    }

                    prevIndex = $this.data('prevIndex');

                    if (e.keyCode == 38) {
                        if (that.options.liveSearch) index--;
                        if (index != nextPrev && index > prev) index = prev;
                        if (index < first) index = first;
                        if (index == prevIndex) index = last;
                    } else if (e.keyCode == 40) {
                        if (that.options.liveSearch) index++;
                        if (index == -1) index = 0;
                        if (index != nextPrev && index < next) index = next;
                        if (index > last) index = last;
                        if (index == prevIndex) index = first;
                    }

                    $this.data('prevIndex', index);

                    if (!that.options.liveSearch) {
                        $items.eq(index).children('a').focus();
                    } else {
                        e.preventDefault();
                        if (!$this.hasClass('dropdown-toggle')) {
                            $items.removeClass('is-active').eq(index).addClass('is-active').children('a').focus();
                            $this.focus();
                        }
                    }
                } else if (!$this.is('input')) {
                    var keyIndex = [];
                    var count;
                    var prevKey;

                    $items.each(function() {
                        if (!$(this).hasClass('disabled')) {
                            if ($.trim($(this).children('a').text().toLowerCase()).substring(0, 1) == keyCodeMap[e.keyCode]) {
                                keyIndex.push($(this).index());
                            }
                        }
                    });

                    count = $(document).data('keycount');
                    count++;
                    $(document).data('keycount', count);

                    prevKey = $.trim($(':focus').text().toLowerCase()).substring(0, 1);

                    if (prevKey != keyCodeMap[e.keyCode]) {
                        count = 1;
                        $(document).data('keycount', count);
                    } else if (count >= keyIndex.length) {
                        $(document).data('keycount', 0);
                        if (count > keyIndex.length) count = 1;
                    }

                    $items.eq(keyIndex[count - 1]).children('a').focus();
                }

                // Select focused option if "Enter", "Spacebar" or "Tab" (when selectOnTab is true) are pressed inside the menu.
                if ((/(13|32)/.test(e.keyCode.toString(10)) || (/(^9$)/.test(e.keyCode.toString(10)) && that.options.selectOnTab)) && isActive) {
                    if (!(/(32)/).test(e.keyCode.toString(10))) e.preventDefault();
                    if (!that.options.liveSearch) {
                        var elem = $(':focus');

                        elem.click();
                        // Bring back focus for multiselects
                        elem.focus();
                        // Prevent screen from scrolling if the user hit the spacebar
                        e.preventDefault();
                        // Fixes spacebar selection of dropdown items in FF & IE
                        $(document).data('spaceSelect', true);
                    } else if (!(/(32)/).test(e.keyCode.toString(10))) {
                        that.$menuInner.find('.is-active a').click();
                        $this.focus();
                    }
                    $(document).data('keycount', 0);
                }

                if ((/(^9$|27)/.test(e.keyCode.toString(10)) && isActive && (that.multiple || that.options.liveSearch)) || (/(27)/.test(e.keyCode.toString(10)) && !isActive)) {
                    that.$menu.parent().removeClass('open');
                    that.$menu.removeClass('open');
                    if (that.options.container) that.$newElement.removeClass('open');
                    that.$button.focus();
                }
            },

            /**
             * Set dropdown to mobile device mode.
             *
             * @returns {void}
             */
            mobile: function() {
                this.$element.addClass('mobile-device');
            },

            /**
             * Refresh dropdown HTML.
             *
             * @returns {void}
             */
            refresh: function() {
                this.$lis = null;
                this.liObj = {};
                this.reloadLi();
                this.render();
                this.checkDisabled();
                this.liHeight(true);
                this.setStyle();
                this.setWidth();
                if (this.$lis) this.$searchbox.trigger('propertychange');

                this.$element.trigger('refreshed.bs.select');
            },

            /**
             * Hide dropdown.
             *
             * @returns {void}
             */
            hide: function() {
                this.$newElement.hide();
            },

            /**
             * Show dropdown.
             *
             * @returns {void}
             */
            show: function() {
                this.$newElement.show();
            },

            /**
             * Remove select from DOM.
             *
             * @returns {void}
             */
            remove: function() {
                this.$newElement.remove();
                this.$element.remove();
            },

            /**
             * Destroy plugin instance.
             *
             * @returns {void}
             */
            destroy: function() {
                this.$newElement.before(this.$element).remove();

                if (this.$bsContainer) {
                    this.$bsContainer.remove();
                } else {
                    this.$menu.remove();
                }

                if (this.observer) {
                    this.observer.disconnect();
                }

                this.$element
                    .off('.bs.select')
                    .removeData('selectpicker')
                    .removeClass('bs-select-hidden selectpicker');
            }
        };

        /**
         * SELECTPICKER PLUGIN DEFINITION.
         *
         * @class
         * @param {Object} option - Extended options.
         * @param {Event} event - Event.
         */
        function Plugin(option, event) {
            // get the args of the outer function..
            var args = arguments;
            // The arguments of the function are explicitly re-defined from the argument list, because the shift causes them
            // to get lost/corrupted in android 2.3 and IE9 #715 #775
            var _option = option;
            var _event = event;

            [].shift.apply(args);

            var value;
            var chain = this.each(function() {
                var $this = $(this);

                if ($this.is('select')) {
                    var data = $this.data('selectpicker');
                    var options = typeof _option == 'object' && _option;

                    if (!data) {
                        var config = $.extend({}, Selectpicker.DEFAULTS, $.fn.selectpicker.defaults || {}, $this.data(), options);

                        config.template = $.extend({}, Selectpicker.DEFAULTS.template, ($.fn.selectpicker.defaults ? $.fn.selectpicker.defaults.template : {}), $this.data().template, options.template);
                        $this.data('selectpicker', (data = new Selectpicker(this, config, _event)));
                    } else if (options) {
                        for (var i in options) {
                            if (options.hasOwnProperty(i)) {
                                data.options[i] = options[i];
                            }
                        }
                    }

                    if (typeof _option == 'string') {
                        if (data[_option] instanceof Function) {
                            value = data[_option].apply(data, args);
                        } else {
                            value = data.options[_option];
                        }
                    }
                }
            });

            if (typeof value !== 'undefined') {
                // noinspection JSUnusedAssignment
                return value;
            } else {
                return chain;
            }
        }

        var old = $.fn.selectpicker;

        $.fn.selectpicker = Plugin;
        $.fn.selectpicker.Constructor = Selectpicker;

        // SELECTPICKER NO CONFLICT
        // ========================
        $.fn.selectpicker.noConflict = function() {
            $.fn.selectpicker = old;

            return this;
        };

        $(document)
            .data('keycount', 0)
            .on('keydown.bs.select', '.bootstrap-select [data-toggle=dropdown], .bootstrap-select [role="menu"], .bootstrap-select__searchbox input', Selectpicker.prototype.keydown)
            .on('focusin.modal', '.bootstrap-select [data-toggle=dropdown], .bootstrap-select [role="menu"], .bootstrap-select__searchbox input', function(e) {
                e.stopPropagation();
            });

        // SELECTPICKER DATA-API
        // =====================
        $(window).on('load.bs.select.data-api', function() {
            $('.form-select select').each(function() {
                var $selectpicker = $(this);

                Plugin.call($selectpicker, $selectpicker.data());
            });
        });

        /**
         * @param {Event} event
         * @event enhance.form.select
         */
        $(document).on('enhance.form.select', function(event) {
            $(event.target).find('.form-select select').each(function() {
                var $this = $(this);

                if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) && !$this.data('live-search')) {
                    $this.selectpicker('mobile');
                } else {
                    $this.selectpicker();
                }
            });
        });

        $(document).trigger('enhance.form.select');
    })(jQuery);
}));

;(function($, window, document) {
    var pluginName = 'teliaClearField';
    var defaults = {
        visibleClass: 'is-visible'
    };
    var initSelector = '[data-clear]';

    /**
     * ClearField.
     *
     * @class
     * @param {HTMLElement} element - The HTML element the plugin is initialized on.
     * @param {Object} options - An options object.
     */
    function ClearField(element, options) {
        this.element = element;
        this.$element = $(this.element);
        this.options = $.extend({}, defaults, options, this.$element.data());
        this._defaults = defaults;
        this._name = pluginName;
        this.$button = $('<a href="javascript:void(0)" class="clear-btn">' + telia.icon.render('#close-round', 'clear-btn__icon') + '</a>');
        this.hasButton = this.$element.parent().find('.clear-btn').length > 0;

        this._init();
    }

    /**
     * Initializes the plugin.
     *
     * @function _init
     * @private
     * @returns {void}
     */
    ClearField.prototype._init = function() {
        // if button doesn't exist
        if (!this.hasButton) {
            this.$button.insertAfter(this.$element);
            this.hasButton = true;

            this._check();

            // Button events
            this.$button.on('click.telia.clearField', this._empty.bind(this));
        }

        // input events
        this.$element.on('keyup.telia.clearField blur.telia.clearField change.telia.clearField', this._check.bind(this));
    };

    /**
     * Check if field is not empty, show button if true.
     *
     * @function _check
     * @private
     * @returns {void}
     */
    ClearField.prototype._check = function() {
        if (this.$element.val() || this.$element.val() != '') {
            this.$button.addClass(this.options.visibleClass);
        } else {
            this.$button.removeClass(this.options.visibleClass);
        }
    };

    /**
     * Empty the input and focus it.
     *
     * @function _empty
     * @private
     * @returns {void}
     */
    ClearField.prototype._empty = function() {
        this.$element.val('');
        this._check();
        this.$element.focus();
    };

    /**
     * Destroy the plugin instance.
     *
     * @function destroy
     * @returns {void}
     */
    ClearField.prototype.destroy = function() {
        this.$element.off('keyup.telia.clearField blur.telia.clearField change.telia.clearField');
        this.$button.remove();
        this.$element.removeData('plugin_' + pluginName);
    };

    /**
     * Create jQuery plugin.
     *
     * @param {Object} options - Extended options.
     * @listens telia.clearField
     * @returns {Array<ClearField>} Array of ClearFields.
     */

    $.fn[pluginName] = function(options) {
        var args = Array.prototype.slice.call(arguments, 1);
        var returnValue = [];

        this.each(function() {
            var instance = $.data(this, 'plugin_' + pluginName);

            if (!instance) {
                if (options === 'destroy') {
                    return;
                }

                var newInstance = new ClearField(this, options);

                returnValue.push(newInstance);
                $.data(this, 'plugin_' + pluginName, newInstance);
            } else if (typeof options === 'string') {
                var methodReturnValue = instance[options].apply(instance, args);

                if (methodReturnValue) {
                    returnValue = methodReturnValue;
                } else {
                    returnValue.push(instance);
                }
            }
        });

        return returnValue;
    };

    /**
     * @param {Event} event
     * @event enhance.telia.clearField
     */
    $(document).on('enhance.telia.clearField', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]();
    });

    /**
     * @param {Event} event
     * @event enhance.form
     * @fires enhance.telia.cleatField
     * @deprecated use enhance.telia.clearField event instead
     */
    $(document).on('enhance.form', function(event) {
        $(document).trigger('enhance.telia.clearField', event);
    });

    /**
     * @param {Event} even
     * @event destroy.telia.clearField
     */
    $(document).on('destroy.telia.clearField', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]('destroy');
    });
})(jQuery, window, document);

/**
 * Auto-init the plugin on DOM ready.
 * @fires enhance.telia.clearField
 */
$(function() {
    $(document).trigger('enhance.telia.clearField');
});

;(function($, window, document) {
    var pluginName = 'teliaSuggestion';
    var attribute = 'data-suggestion-list';
    var initSelector = '[' + attribute + ']';
    var defaults = {
        activeClass: 'active',
        visibleClass: 'visible'
    };

    /**
     * Suggestion.
     *
     * @class
     * @param {HTMLElement} element - The HTML element the Suggestion should be bound to.
     * @param {Object} options - An option map.
     */
    function Suggestion(element, options) {
        this.element = element;
        this.options = $.extend({}, defaults, options);

        this.$element = $(this.element);
        this.$list = $(this.$element.attr(attribute));

        this._init();
    }

    /**
     * Initialize the plugin.
     *
     * @function init
     * @private
     * @returns {void}
     */
    Suggestion.prototype._init = function() {
        var that = this;

        that.$list.find('a').on('click.telia.suggestion', that._setValue.bind(that));

        $(document).on('keydown.telia.suggestion', that._bindKeyboardEvents.bind(that));

        that.$element.on('click.telia.suggestion', function(event) {
            if (that.$element.hasClass(that.options.activeClass)) {
                event.stopPropagation();
            }
        });

        that.$list.on('click.telia.suggestion', function(event) { event.stopPropagation(); });

        that.$element.on('show.telia.suggestion', this.show.bind(this));
        that.$element.on('hide.telia.suggestion', this.hide.bind(this));
    };

    /**
     * Show suggestion list.
     *
     * @function show
     * @returns {void}
     */
    Suggestion.prototype.show = function() {
        this.$element.addClass(this.options.activeClass);
        this.$list.addClass(this.options.visibleClass);

        $(document).one('click.telia.suggestion', this.hide.bind(this));

        this.$element.trigger('afterShow.telia.suggestion');
    };

    /**
     * Hide suggestion list.
     *
     * @function hide
     * @returns {void}
     */
    Suggestion.prototype.hide = function() {
        this.$element.removeClass(this.options.activeClass);
        this.$list.removeClass(this.options.visibleClass);

        this.$element.trigger('afterHide.telia.suggestion');
    };

    /**
     * Set suggestion value.
     *
     * @function setValue
     * @param {Event} event - Event.
     * @private
     * @returns {void}
     */
    Suggestion.prototype._setValue = function(event) {
        this.$element.val($(event.target).text().trim());
        this.$element.trigger('change');
        this.hide();
    };

    /**
     * Define keyboard events.
     *
     * @function bindKeyboardEvents
     * @param {Event} event - Event.
     * @private
     * @returns {void}
     */
    Suggestion.prototype._bindKeyboardEvents = function(event) {
        if (event.which == 27 && $(event.target).hasClass(this.options.activeClass)) {
            this.hide();
        }
    };

    /**
     * Destroy plugin.
     *
     * @function destroy
     * @returns {void}
     */
    Suggestion.prototype.destroy = function() {
        $(document).off('keydown.telia.suggestion');
        this.$element.off('click.telia.suggestion');
        this.$list.find('a').off('click.telia.suggestion');

        this.$element.removeData('plugin_' + pluginName);
    };

    /**
     * Create jQuery plugin.
     *
     * @param {Object} options - Extended options.
     * @listens telia.suggestion
     * @returns {Array<Suggestion>} Array of Suggestions.
     */
    $.fn[pluginName] = function(options) {
        var args = Array.prototype.slice.call(arguments, 1);

        return this.each(function() {
            var instance = $.data(this, 'plugin_' + pluginName);

            if (!instance) {
                if (options === 'destroy') {
                    return;
                }

                $.data(this, 'plugin_' + pluginName, new Suggestion(this, options));
            } else if (typeof options === 'string') {
                instance[options].apply(instance, args);
            }
        });
    };

    /**
     * @param {Event} event
     * @event enhance.telia.suggestion
     */
    $(document).on('enhance.telia.suggestion', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]();
    });

    /**
     * @param {Event} even
     * @event destroy.telia.suggestion
     */
    $(document).on('destroy.telia.suggestion', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]('destroy');
    });
})(jQuery, window, document);

/**
 * Auto-init the plugin on DOM ready.
 * @fires enhance.telia.suggestion
 */
$(function() {
    $(document).trigger('enhance.telia.suggestion');
});

;(function($, window, document) {
    var pluginName = 'teliaNumberSelect';
    var defaults = {
        min: 1,
        max: 999,
        step: 1
    };
    var initSelector = '.number';

    /**
     * NumberSelect.
     *
     * @class
     * @param {HTMLElement} element - The HTML element the NumberSelect should be bound to.
     * @param {Object} options - An option map.
     */
    function NumberSelect(element, options) {
        this.element = element;
        this.$element = $(this.element);
        this.options = $.extend({}, defaults, options, this.$element.data());
        this._defaults = defaults;
        this._name = pluginName;
        this.$decreaseButton = this.$element.find('.number__decrease');
        this.$increaseButton = this.$element.find('.number__increase');
        this.$input = this.$element.find('.number__input');
        this.disabled = this.$element.hasClass('is-disabled');

        this.init();
    }

    /**
     * Initialize the plugin.
     *
     * @function init
     * @returns {void}
     */
    NumberSelect.prototype.init = function() {
        this.$decreaseButton.on('click.telia.numberSelect', this.decrease.bind(this));
        this.$increaseButton.on('click.telia.numberSelect', this.increase.bind(this));
        this.$input.on('change.telia.numberSelect', this.change.bind(this));
        this.$input.on('keydown.telia.numberSelect', this.keyboard.bind(this));
        this.$input.on('input.telia.numberSelect', this.change.bind(this));

        if (!this.disabled) {
            this.change();
        }
    };

    NumberSelect.prototype.keyboard = function(event) {
        if (event.which == 38) {
            event.preventDefault();
            this.increase();
        } else if (event.which == 40) {
            event.preventDefault();
            this.decrease();
        }

        // Allow: backspace, delete, tab, escape, enter
        if ($.inArray(event.keyCode, [46, 8, 9, 27, 13]) !== -1 || (event.keyCode === 65 && (event.ctrlKey === true || event.metaKey === true)) || (event.keyCode >= 35 && event.keyCode <= 40)) {
            return;
        }
        // Ensure that it is a number and stop the keypress'
        if ((event.shiftKey || (event.keyCode < 48 || event.keyCode > 57)) && (event.keyCode < 96 || event.keyCode > 105)) {
            event.preventDefault();
        }
    };

    /**
     * Decrease input value by step.
     *
     * @function decrease
     * @param  {Event} event - Event.
     * @returns {void}
     */
    NumberSelect.prototype.decrease = function(event) {
        if (event !== undefined) {
            event.preventDefault();
        }
        this.setValue(this.getValue() - this.options.step).trigger('change.telia.numberSelect');
    };

    /**
     * Decrease input value by step.
     *
     * @function increase
     * @param  {Event} event - Event.
     * @returns {void}
     */
    NumberSelect.prototype.increase = function(event) {
        if (event !== undefined) {
            event.preventDefault();
        }
        this.setValue(this.getValue() + this.options.step).trigger('change.telia.numberSelect');
    };

    /**
     * Update increase/decrease buttons when input value changes.
     *
     * @function change
     * @returns {void}
     */
    NumberSelect.prototype.change = function() {
        // if the value is not numeric and cannot be converted to numbers,
        // set the value to the minimum value
        if (!$.isNumeric(this.$input.val())) {
            this.setValue(this.$input.val().replace(/\D/g, ''));
        }

        // if the value is smaller than minimum value,
        // reset the value to the minimum value
        if (this.getValue() < this.options.min) {
            this.setValue(this.options.min);
        }

        // if the value is greater than maximum value,
        // reset the value to the maximum value
        if (this.getValue() > this.options.max) {
            this.setValue(this.options.max);
        }

        // if the value is equal to minimum value, disable decrease button
        // otherwise, keep decrease button enabled
        if (this.getValue() == this.options.min) {
            this.$decreaseButton.prop('disabled', true);
        } else {
            this.$decreaseButton.prop('disabled', false);
        }

        // if the value is equal to maximum value, disable increase button
        // otherwise, keep increase button enabled
        if (this.getValue() == this.options.max) {
            this.$increaseButton.prop('disabled', true);
        } else {
            this.$increaseButton.prop('disabled', false);
        }
    };

    /**
     * Get the value of the input.
     *
     * @function getValue
     * @returns {number} Value.
     */
    NumberSelect.prototype.getValue = function() {
        return parseInt(this.$input.val());
    };

    /**
     * Set the value of the input.
     *
     * @param   {number} value - New value.
     * @returns {JQuery} Input jQuery object.
     */
    NumberSelect.prototype.setValue = function(value) {
        return this.$input.val(value);
    };

    /**
     * Disable the component.
     *
     * @function destroy
     * @returns {void}
     */
    NumberSelect.prototype.disable = function() {
        this.$element.addClass('is-disabled');
        this.$input.prop('disabled', true);
        this.$decreaseButton.prop('disabled', true);
        this.$increaseButton.prop('disabled', true);
        this.disabled = true;
    };

    /**
     * Enable the component.
     *
     * @function enable
     * @returns {void}
     */
    NumberSelect.prototype.enable = function() {
        this.$element.removeClass('is-disabled');
        this.$input.prop('disabled', false);
        this.$decreaseButton.prop('disabled', false);
        this.$increaseButton.prop('disabled', false);
        this.disabled = false;
    };

    /**
     * Destroy the plugin instance.
     *
     * @function destroy
     * @returns {void}
     */
    NumberSelect.prototype.destroy = function() {
        this.$decreaseButton.off('click.telia.numberSelect');
        this.$increaseButton.off('click.telia.numberSelect');
        this.$input.off('change.telia.numberSelect');
        this.$element.removeData('plugin_' + pluginName);
    };

    /**
     * Create jQuery plugin.
     *
     * @param {Object} options - Extended options.
     * @listens telia.numberSelect
     * @returns {Array<NumberSelect>|Number} Array of NumberSelects or Number if using getValue method.
     */
    $.fn[pluginName] = function(options) {
        var args = Array.prototype.slice.call(arguments, 1);
        var returnValue = [];

        this.each(function() {
            var instance = $.data(this, 'plugin_' + pluginName);

            if (!instance) {
                if (options === 'destroy') {
                    return;
                }

                var newInstance = new NumberSelect(this, options);

                returnValue.push(newInstance);
                $.data(this, 'plugin_' + pluginName, newInstance);
            } else if (typeof options === 'string') {
                var methodReturnValue = instance[options].apply(instance, args);

                if (methodReturnValue) {
                    returnValue = methodReturnValue;
                } else {
                    returnValue.push(instance);
                }
            }
        });

        return returnValue;
    };

    /**
     * @param {Event} event
     * @event enhance.telia.numberSelect
     */
    $(document).on('enhance.telia.numberSelect', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]();
    });

    /**
     * @param {Event} event
     * @event destroy.telia.numberSelect
     */
    $(document).on('destroy.telia.numberSelect', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]('destroy');
    });
})(jQuery, window, document);

/**
 * Auto-init the plugin on DOM ready.
 * @fires enhance.telia.numberSelect
 */
$(function() {
    $(document).trigger('enhance.telia.numberSelect');
});

;(function($, window, document) {
    var pluginName = 'teliaPassfield';
    var defaults = {
        visibleClass: 'is-visible'
    };
    var initSelector = '.js-passfield';

    /**
     * Passfield.
     *
     * @class
     * @param {Element} element - The HTML element the plugin is initialized on.
     * @param {Object} options - An options object.
     */
    function Passfield(element, options) {
        this.element = element;
        this.$element = $(this.element);
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.type = 'password';
        this.$input = this.$element.find('.passfield__input');
        this.$button = this.$element.find('.passfield__button');

        this._init();
    }

    /**
     * Initializes the plugin.
     *
     * @function _init
     * @private
     * @returns {void}
     */
    Passfield.prototype._init = function() {
        this.$button.on('click.telia.passfield', this.toggle.bind(this));
    };

    /**
     * Toggle password visibility.
     *
     * @function toggle
     * @returns {void}
     */
    Passfield.prototype.toggle = function() {
        if (this.type == 'password') {
            this.show();
        } else {
            this.hide();
        }
    };

    /**
     * Show password in input.
     *
     * @function show
     * @returns {void}
     */
    Passfield.prototype.show = function() {
        this.$input.prop('type', 'text');
        this.type = 'text';
        this.$element.addClass(this.options.visibleClass);
    };

    /**
     * Hide password in input.
     *
     * @function hide
     * @returns {void}
     */
    Passfield.prototype.hide = function() {
        this.$input.prop('type', 'password');
        this.type = 'password';
        this.$element.removeClass(this.options.visibleClass);
    };

    /**
     * Destroy the plugin instance..
     *
     * @function destroy
     * @returns {void}
     */
    Passfield.prototype.destroy = function() {
        this.$button.off('click.telia.passfield', this.toggle);
        this.$element.removeData('plugin_' + pluginName);
    };

    /**
     * Create jQuery plugin.
     *
     * @param {Object} options - Extended options.
     * @listens telia.header
     * @returns {Array<Passfield>} Array of Passfields.
     */
    $.fn[pluginName] = function(options) {
        var args = Array.prototype.slice.call(arguments, 1);

        return this.each(function() {
            var instance = $.data(this, 'plugin_' + pluginName);

            if (!instance) {
                if (options === 'destroy') {
                    return;
                }

                $.data(this, 'plugin_' + pluginName, new Passfield(this, options));
            } else if (typeof options === 'string') {
                instance[options].apply(instance, args);
            }
        });
    };

    /**
     * @param {Event} event
     * @event enhance.telia.passfield
     */
    $(document).on('enhance.telia.passfield', function(event) {
        $(event.target).find(initSelector)[pluginName]();
    });

    /**
     * @param {Event} even
     * @event destroy.telia.passfield
     */
    $(document).on('destroy.telia.passfield', function(event) {
        $(event.target).find(initSelector)[pluginName]('destroy');
    });
})(jQuery, window, document);

/**
 * Auto-init the plugin on DOM ready.
 * @fires enhance.telia.passfield
 */
$(function() {
    $(document).trigger('enhance.telia.passfield');
});

;(function($, window, document) {
    var pluginName = 'teliaSlider';
    var defaults = {};
    var initSelector = '.js-slider';

    /**
     * Slider.
     *
     * @class
     * @param {HTMLElement} element - The HTML element the Slider should be bound to.
     * @param {Object} options - An option map.
     */
    function Slider(element, options) {
        this.element = element;
        this.$element = $(this.element);
        this.options = $.extend({}, defaults, options, this.$element.data());
        this._defaults = defaults;
        this._name = pluginName;
        this.$slider = this.$element.find('.slider__inner');
        this.slider = this.$slider[0];
        this.hasRange = false;
        this.$handle = false;
        this.amountOfDots = 0;
        this.activeHandleIndex = 0;
        this.sliderOptions = {};

        this.init();
    }

    /**
     * Initialize the plugin.
     *
     * @function init
     * @returns {void}
     */
    Slider.prototype.init = function() {
        this.options.sliderUnit = this.options.sliderUnit ? this.options.sliderUnit : '';

        this._createNoUiSlider();
        this._bindEvents();
        this._addDots();
        this._change();

        // Add keyboard support
        this.$handle.attr('tabindex', 0);
    };

    /**
     * Create noUiSlider with meta from data attributes.
     *
     * @function _createNoUislider
     * @private
     * @returns {void}
     */
    Slider.prototype._createNoUiSlider = function() {
        var that = this;

        this.amountOfDots = (this.options.sliderMax - this.options.sliderMin) / this.options.sliderStep;

        var start = [this.options.sliderStartValue];
        var connect = [false];

        if (typeof this.options.sliderEndValue === 'number') {
            this.hasRange = true;
            start.push(this.options.sliderEndValue);
        }

        if (start.length < 2) {
            connect.push(false);
        } else {
            connect.push(true);
            connect.push(false);
        }

        this.sliderOptions = {
            start: start,
            connect: connect,
            range: {
                min: this.options.sliderMin,
                max: this.options.sliderMax
            },
            step: this.options.sliderStep,
            cssPrefix: 'slider__'
        };

        if (this.hasRange) {
            this.sliderOptions.tooltips = [true, true];
            this.sliderOptions.format = {
                /**
                 * Encode label with unit.
                 *
                 * @param   {value} value - Slider value.
                 * @returns {string} Formatted label.
                 */
                to: function(value) {
                    return value.toFixed() + ' ' + that.options.sliderUnit;
                },

                /**
                 * Decode label with unit.
                 *
                 * @param   {value} value - Slider value.
                 * @returns {string} Formatted label.
                 */
                from: function(value) {
                    return value;
                }
            };
        } else {
            this.sliderOptions.pips = {
                mode: 'count',
                values: this.amountOfDots + 1,
                stepped: true
            };
        }

        window.noUiSlider.create(this.slider, this.sliderOptions);
    };

    /**
     * Bind events.
     *
     * @function _bindEvents
     * @private
     * @returns {void}
     */
    Slider.prototype._bindEvents = function() {
        this.$handle = this.$element.find('.slider__handle');

        this.slider.noUiSlider.on('update.telia.slider', this._change.bind(this));
        this.slider.noUiSlider.on('set.telia.slider', this._set.bind(this));
        this.$handle.on('keydown.telia.slider', this._onKeydown.bind(this));
        this.$handle.on('focus.telia.slider', this._onFocus.bind(this));
        this.$handle.on('click.telia.slider', this._onClick.bind(this));
    };

    /**
     * Calculate dot positions and append to slider.
     *
     * @function _addDots
     * @private
     * @returns {void}
     */
    Slider.prototype._addDots = function() {
        if ((this.options.sliderMax / this.options.sliderStep) >= 15) {
            return;
        }

        var divideDots = 100 / this.amountOfDots;

        for (var x = 0; x <= this.amountOfDots; x++) {
            this.$element.find('.slider__base').append('<span class="slider__dots" style="left:' + x * divideDots + '%"></span>');
        }
    };

    /**
     * Update the slider values when reloading page or sliding the handles.
     *
     * @private
     * @function _change
     * @returns {void}
     */
    Slider.prototype._change = function() {
        var e = $.Event('change.telia.slider');

        this.$element.trigger(e);

        if (this.sliderOptions.start.length > 1) {
            var value = this.getValue();
            var minValue = parseFloat(value[0]);
            var maxValue = parseFloat(value[1]);
            var x = 0;

            for (var i = this.sliderOptions.range.min; i <= this.sliderOptions.range.max; i += this.options.sliderStep) {
                this.$element.find('.slider__dots').eq(x).toggleClass('is-active', i >= minValue && i <= maxValue);

                x++;
            }
        }
    };

    /**
     * Trigger set event after handle has been released.
     *
     * @private
     * @function _set
     * @returns {void}
     */
    Slider.prototype._set = function() {
        var e = $.Event('set.telia.slider');

        this.$element.trigger(e);
    };

    /**
     * Activate focus method on slider to activate slider handle.
     *
     * @function _onClick
     * @param {Event} event - Event.
     * @private
     * @returns {void}
     */
    Slider.prototype._onClick = function(event) {
        event.target.focus();
    };

    /**
     * Find the correct handle according to the clicked handle.
     *
     * @function _onFocus
     * @param {Event} event - Event.
     * @private
     * @returns {void}
     */
    Slider.prototype._onFocus = function(event) {
        this.activeHandleIndex = this.$handle.index(event.target);
    };

    /**
     * When handle is chosen, press arrows left or right to change handle values.
     *
     * @function onKeydown
     * @param {Event} event - Event.
     * @private
     * @returns {void}
     */
    Slider.prototype._onKeydown = function(event) {
        var value = this.getValue();

        if (Array.isArray(value)) {
            var newValue = value.slice();

            switch (event.which) {
                case 37: newValue[this.activeHandleIndex] = Number(value[this.activeHandleIndex]) - this.options.sliderStep;
                    break;
                case 39: newValue[this.activeHandleIndex] = Number(value[this.activeHandleIndex]) + this.options.sliderStep;
                    break;
            }
            this.slider.noUiSlider.set(newValue);
        } else {
            switch (event.which) {
                case 37: this.slider.noUiSlider.set(Number(value) - this.options.sliderStep);
                    break;
                case 39: this.slider.noUiSlider.set(Number(value) + this.options.sliderStep);
                    break;
            }
        }
    };

    /**
     * Get current slider handle position values, with 1 handle only int, with
     * 2 you get an array.
     *
     * @function getValue
     * @returns {void}
     */
    Slider.prototype.getValue = function() {
        var result = this.slider.noUiSlider.get();

        if (Array.isArray(result)) {
            var values = [];

            result.forEach(function(key) {
                var onlyNumber = key.split(' ');

                values.push(onlyNumber[0]);
            });

            return values;
        } else {
            return Number(result);
        }
    };

    /**
     * Disable slider.
     *
     * @function disable
     * @returns {void}
     */
    Slider.prototype.disable = function() {
        this.$slider.attr('disabled', true);
    };

    /**
     * Enable slider.
     *
     * @function enable
     * @returns {void}
     */
    Slider.prototype.enable = function() {
        this.$slider.attr('disabled', false);
    };

    /**
     * Destroy plugin instance.
     *
     * @function destroy
     * @returns {void}
     */
    Slider.prototype.destroy = function() {
        this.slider.noUiSlider.off();
        this.$handle.off('click.telia.slider');
        this.$handle.off('focus.telia.slider');
        this.$handle.off('keydown.telia.slider');

        this.$element.removeData('plugin_' + pluginName);
    };

    /**
     * Create jQuery plugin.
     *
     * @param {Object} options - Extended options.
     * @listens telia.Slider
     * @returns {Array<Slider>} Array of Sliders.
     */
    $.fn[pluginName] = function(options) {
        var args = Array.prototype.slice.call(arguments, 1);
        var returnValue = [];

        this.each(function() {
            var instance = $.data(this, 'plugin_' + pluginName);

            if (!instance) {
                if (options === 'destroy') {
                    return;
                }

                var newInstance = new Slider(this, options);

                returnValue.push(newInstance);
                $.data(this, 'plugin_' + pluginName, newInstance);
            } else if (typeof options === 'string') {
                var methodReturnValue = instance[options].apply(instance, args);

                if (methodReturnValue) {
                    returnValue = methodReturnValue;
                } else {
                    returnValue.push(instance);
                }
            }
        });

        return returnValue;
    };

    /**
     * @param {Event} event
     * @event enhance.telia.slider
     */
    $(document).on('enhance.telia.slider', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]();
    });

    /**
     * @param {Event} event
     * @event destroy.telia.slider
     */
    $(document).on('destroy.telia.slider', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]('destroy');
    });
})(jQuery, window, document);

/**
 * Auto-init the plugin on DOM ready.
 * @fires enhance.telia.slider
 */
$(function() {
    $(document).trigger('enhance.telia.slider');
});

;(function($, window, document) {
    var pluginName = 'teliaToggle';
    var defaults = {};
    var initSelector = '.js-toggle';

    /**
     * Toggle.
     *
     * @class
     * @param {HTMLElement} element - The HTML element the Toggle should be bound to.
     * @param {Object} options - An option map.
     */
    function Toggle(element, options) {
        this.element = element;
        this.$element = $(this.element);
        this.options = $.extend({}, defaults, options, this.$element.data());
        this._defaults = defaults;
        this._name = pluginName;
        this.$input = this.$element.find('.toggle__input');
        this.$label = this.$element.find('.toggle__text');

        this.init();
    }

    /**
     * Initialize the plugin.
     *
     * @function init
     * @returns {void}
     */
    Toggle.prototype.init = function() {
        this.$input.on('change.telia.toggle', this._changeText.bind(this));
    };

    /**
     * Change text on toggle.
     *
     * @private
     * @function _changeText
     * @returns {void}
     */
    Toggle.prototype._changeText = function() {
        if (!this.options.onText || !this.options.offText) {
            return;
        }

        var that = this;
        var newText = this.$input.prop('checked') ? this.options.onText : this.options.offText;

        setTimeout(function() {
            that.$label.html(newText);
        }, 100);
    };

    /**
     * Destroy the plugin instance.
     *
     * @function destroy
     * @returns {void}
     */
    Toggle.prototype.destroy = function() {
        this.$input.off('change.telia.toggle');
        this.$element.removeData('plugin_' + pluginName);
    };

    /**
     * Create jQuery plugin.
     *
     * @param {Object} options - Extended options.
     * @listens telia.toggle
     * @returns {Array<toggle>} Array of Toggles.
     */
    $.fn[pluginName] = function(options) {
        var args = Array.prototype.slice.call(arguments, 1);

        return this.each(function() {
            var instance = $.data(this, 'plugin_' + pluginName);

            if (!instance) {
                if (options === 'destroy') {
                    return;
                }

                $.data(this, 'plugin_' + pluginName, new Toggle(this, options));
            } else if (typeof options === 'string') {
                instance[options].apply(instance, args);
            }
        });
    };

    /**
     * @param {Event} event
     * @event enhance.telia.toggle
     */
    $(document).on('enhance.telia.toggle', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]();
    });

    /**
     * @param {Event} event
     * @event destroy.telia.toggle
     */
    $(document).on('destroy.telia.toggle', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]('destroy');
    });
})(jQuery, window, document);

/**
 * Auto-init the plugin on DOM ready.
 * @fires enhance.telia.toggle
 */
$(function() {
    $(document).trigger('enhance.telia.toggle');
});

;(function($) {
    'use strict';

    var pluginName = 'validetta';
    var initSelector = 'form[data-validate]';

    /**
     *  Declare variables
     */
    // Plugin Class
    var Validetta = {};
    // Current fields/fields
    var FIELDS = {};
    // RegExp for input validate rules
    var RRULE = new RegExp(/^(minChecked|maxChecked|minSelected|maxSelected|minLength|maxLength|equalTo|different|regExp|remote|callback)\[(\w{1,15})\]/i);
    // RegExp for mail control method
    // @from ( http://www.whatwg.org/specs/web-apps/current-work/multipage/states-of-the-type-attribute.html#e-mail-state-%28type=email%29 )
    var RMAIL = new RegExp(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/);
    // RegExp for input number control method
    var RNUMBER = new RegExp(/^[\-\+]?(\d+|\d+\.?\d+)$/);

    /**
     *  Form validate error messages
     */
    var messages = {
        required: 'This field is required. Please be sure to check.',
        email: 'Your E-mail address appears to be invalid. Please be sure to check.',
        number: 'You can enter only numbers in this field.',
        maxLength: 'Maximum {count} characters allowed!',
        minLength: 'Minimum {count} characters allowed!',
        maxChecked: 'Maximum {count} options allowed. Please be sure to check.',
        minChecked: 'Please select minimum {count} options.',
        maxSelected: 'Maximum {count} selection allowed. Please be sure to check.',
        minSelected: 'Minimum {count} selection allowed. Please be sure to check.',
        notEqual: 'Fields do not match. Please be sure to check.',
        different: 'Fields cannot be the same as each other',
        creditCard: 'Invalid credit card number. Please be sure to check.'
    };

    /**
     *  Plugin defaults
     */
    var defaults = {
        // If you dont want to display error messages set this options false
        showErrorMessages: true,
        // Class of the element that would receive error message
        errorTemplateClass: 'form-error',
        // Class that would be added on every failing validation field
        errorClass: 'is-invalid',
        // Same for valid validation
        validClass: 'is-valid',
        validIconClass: 'form-textfield__icon--valid',
        // To enable real-time form control, set this option true.
        realTime: false,

        /**
         * Function that is called when form is valid.
         *
         * @param   {Event} event - Event object.
         * @returns {void}
         */
        onValid: function(event) {
            // This function to be called when the user submits the form and there is no error.
            $(this.form).trigger('valid.telia.validation', event);
        },

        /**
         * Function that is called when form is invalid.
         *
         * @param   {Event} event - Event object.
         * @returns {void}
         */
        onError: function(event) {
            // This function to be called when the user submits the form and there are some errors
            $(this.form).trigger('error.telia.validation', event);
        },
        validators: {
            regExp: {
                date: {
                    pattern: /^(?:(?:31(\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/,
                    errorMessage: 'You have not entered a correctly formatted date.'
                },
                number: {
                    pattern: /^[\-\+]?(\d+|\d+[,.]?\d+)$/,
                    errorMessage: 'You can enter only numbers in this field.'
                }
            }
        }
    };

    /**
     * Clears the left and right spaces of given parameter.
     * This is the function for string parameter.
     * If parameter is an array, function will return the parameter without trimed.
     *
     * @param {string} value - Any value for trimming.
     * @returns {mixed} Trimmed value.
     */
    function trim(value) {
        return typeof value === 'string' ? value.replace(/^\s+|\s+$/g, '') : value;
    };

    /**
     * Validator
     * {count} which used below is the specified maximum or minimum value
     * e.g if method is minLength and  rule is 2 ( minLength[2] )
     * Output error windows text will be : 'Please select minimum 2 options.'
     *
     * @namespace
     * @param {object} tmp = this.tmp Tmp object for store current field and its value
     * @param {string} val : field value
     */
    var Validator = {
        /**
         * Test if required field is filled/checked.
         *
         * @param {Object} tmp - Object with field element, value and parent.
         * @param {Validetta} self - Validetta instance.
         * @returns {boolean} True if field is filled/checked.
         */
        required: function(tmp, self) {
            switch (tmp.el.type) {
                case 'checkbox' : return tmp.el.checked || messages.required;
                case 'radio' : return this.radio.call(self, tmp.el) || messages.required;
                case 'select-multiple' : return tmp.val.length > 0 || messages.required;
                default : return tmp.val !== '' || messages.required;
            }
        },

        /**
         * Test if the field value is valid email address.
         *
         * @param {Object} tmp - Object with field element, value and parent.
         * @returns {boolean|string} True if is valid, else error message.
         */
        email: function(tmp) {
            return RMAIL.test(tmp.val) || messages.email;
        },

        /**
         * Test if field value is valid number.
         *
         * @param {Object} tmp - Object with field element, value and parent.
         * @returns {boolean|string} True if is valid, else error message.
         */
        number: function(tmp) {
            return RNUMBER.test(tmp.val) || messages.number;
        },

        /**
         * Test field value minimum length.
         *
         * @param {Object} tmp - Object with field element, value and parent.
         * @returns {boolean|string} True if is valid, else error message.
         */
        minLength: function(tmp) {
            var _length = tmp.val.length;

            return _length === 0 || _length >= tmp.arg || messages.minLength.replace('{count}', tmp.arg);
        },

        /**
         * Test field value maximum length.
         *
         * @param {Object} tmp - Object with field element, value and parent.
         * @returns {boolean|string} True if is valid, else error message.
         */
        maxLength: function(tmp) {
            return tmp.val.length <= tmp.arg || messages.maxLength.replace('{count}', tmp.arg);
        },

        /**
         * Test if field value is equal to another field value.
         *
         * @param {Object} tmp - Object with field element, value and parent.
         * @param {Validetta} self - Validetta instance.
         * @returns {boolean|string} True if is valid, else error message.
         */
        equalTo: function(tmp, self) {
            return self.form.querySelector('input[name="' + tmp.arg + '"]').value === tmp.val || messages.notEqual;
        },

        /**
         * Test if field value is different from another field value.
         *
         * @param {Object} tmp - Object with field element, value and parent.
         * @param {Validetta} self - Validetta instance.
         * @returns {boolean|string} True if is valid, else error message.
         */
        different: function(tmp, self) {
            return self.form.querySelector('input[name="' + tmp.arg + '"]').value !== tmp.val || messages.different;
        },

        /**
         * Test if field value is valid credit card number.
         * From http://af-design.com/blog/2010/08/18/validating-credit-card-numbers.
         *
         * @param {Object} tmp - Object with field element, value and parent.
         * @returns {boolean|string} True if is valid, else error message.
         */
        creditCard: function(tmp) {
            // allow empty because empty check does by required metheod
            if (tmp.val === '') return true;
            var reg;
            var cardNumber;
            var pos;
            var digit;
            var i;
            var subTotal;
            var sum = 0;
            var strlen;

            reg = new RegExp(/[^0-9]+/g);
            cardNumber = tmp.val.replace(reg, '');
            strlen = cardNumber.length;
            if (strlen < 16) return messages.creditCard;
            for (i = 0; i < strlen; i++) {
                pos = strlen - i;
                digit = parseInt(cardNumber.substring(pos - 1, pos), 10);
                if (i % 2 === 1) {
                    subTotal = digit * 2;
                    if (subTotal > 9) {
                        subTotal = 1 + (subTotal - 10);
                    }
                } else {
                    subTotal = digit;
                }
                sum += subTotal;
            }
            if (sum > 0 && sum % 10 === 0) return true;

            return messages.creditCard;
        },

        /**
         * Test if maximum checked checkbox count is less than the specified number.
         *
         * @param {Object} tmp - Object with field element, value and parent.
         * @param {Validetta} self - Validetta instance.
         * @returns {boolean|string} True if is valid, else error message.
         */
        maxChecked: function(tmp, self) {
            var cont = $(self.form.querySelectorAll('input[type=checkbox][name="' + tmp.el.name + '"]'));
            // we dont want to open an error window for all checkboxes which have same "name"

            if (cont.index(tmp.el) !== 0) return;
            var count =  cont.filter(':checked').length;

            if (count === 0) return;

            return count <= tmp.arg || messages.maxChecked.replace('{count}', tmp.arg);
        },

        /**
         * Test if minimum checked checkbox count is greater than the specified number.
         *
         * @param {Object} tmp - Object with field element, value and parent.
         * @param {Validetta} self - Validetta instance.
         * @returns {boolean|string} True if is valid, else error message.
         */
        minChecked: function(tmp, self) {
            var cont = $(self.form.querySelectorAll('input[type=checkbox][name="' + tmp.el.name + '"]'));
            // same as above

            if (cont.index(tmp.el) !== 0) return;
            var count =  cont.filter(':checked').length;

            return count >= tmp.arg || messages.minChecked.replace('{count}', tmp.arg);
        },

        /**
         * Test if maximum selected select options count is less than the specified number.
         *
         * @param {Object} tmp - Object with field element, value and parent.
         * @returns {boolean|string} True if is valid, else error message.
         */
        maxSelected: function(tmp) {
            if (tmp.val === null) return;

            return tmp.val.length <= tmp.arg || messages.maxSelected.replace('{count}', tmp.arg);
        },

        /**
         * Test if minimum selected select options count is greater than the specified number.
         *
         * @param {Object} tmp - Object with field element, value and parent.
         * @returns {boolean|string} True if is valid, else error message.
         */
        minSelected: function(tmp) {
            return (tmp.val !== null && tmp.val.length >= tmp.arg) || messages.minSelected.replace('{count}', tmp.arg);
        },

        /**
         * Test if something is checked in a radio group.
         *
         * @param {Object} el - Object with field element data.
         * @returns {boolean|string} True if is valid, else error message.
         */
        radio: function(el) {
            var count = this.form.querySelectorAll('input[type=radio][name="' + el.name + '"]:checked').length;

            return count === 1;
        },

        /**
         * Test if custom regExp is valid for the field.
         *
         * @param {Object} tmp - Object with field element, value and parent.
         * @param {Validetta} self - Validetta instance.
         * @returns {boolean|string} True if is valid, else error message.
         */
        regExp: function(tmp, self) {
            var _arg = self.options.validators.regExp[tmp.arg];
            var _reg = new RegExp(_arg.pattern);

            return _reg.test(tmp.val) || _arg.errorMessage;
        },

        /**
         * Remote validator.
         *
         * @param {Object} tmp - Object with field element, value and parent.
         * @returns {void}
         */
        remote: function(tmp) {
            tmp.remote = tmp.arg;

            return;
        },

        /**
         * Custom callback validator.
         *
         * @param {Object} tmp - Object with field element, value and parent.
         * @param {Validetta} self - Validetta instance.
         * @returns {boolean|string} True if is valid, else error message.
         */
        callback: function(tmp, self) {
            var _cb = self.options.validators.callback[tmp.arg];

            return _cb.callback(tmp.el, tmp.val) || _cb.errorMessage;
        }
    };

    /**
     * Plugin Class.
     *
     * @class
     * @param {Object} form - <form> element which being controlled.
     * @param {Object} options - User-specified settings.
     * @returns {method} Events.
     */
    Validetta = function(form, options) {
        /**
         *  Public  Properties
         *  @property {mixed} handler It is used to stop or resume submit event handler
         *  @property {object} options Property is stored in plugin options
         *  @property {object} xhr Stores xhr requests
         *  @property {object} form Property is stored in <form> element
         */
        this.handler = false;
        this.options = $.extend(true, {}, defaults, options);
        this.form = form;
        this.xhr = {};
        this._init();
    };

    Validetta.prototype = {

        constructor: Validetta,

        /**
         * Initialize the plugin.
         *
         * @function _init
         * @private
         * @returns {void}
         */
        _init: function() {
            if ($(this.form).data('validate') == 'inline') {
                this.options.realTime = true;
            }

            this.events();
        },

        /**
         * This is the method of handling events.
         *
         * @returns {mixed} Function.
         */
        events: function() {
            // stored this
            var self = this;
            // Handle submit event

            $(this.form).submit(function(e) {
                // fields to be controlled transferred to global variable
                FIELDS = this.querySelectorAll('[data-validetta]');

                return self.init(e);
            });
            // real-time option control
            if (this.options.realTime === true) {
                // handle change event for form elements (without checkbox)
                $(this.form).find('[data-validetta]').not('[type=checkbox]').on('change', function(e) {
                    // field to be controlled transferred to global variable
                    FIELDS = $(this);

                    return self.init(e);
                });
                // handle click event for checkboxes
                $(this.form).find('[data-validetta][type=checkbox]').on('click', function(e) {
                    // fields to be controlled transferred to global variable
                    FIELDS = self.form.querySelectorAll('[data-validetta][type=checkbox][name="' + this.name + '"]');

                    return self.init(e);
                });
            }
            // handle <form> reset button to clear error messages
            $(this.form).on('reset', function() {
                $(self.form.querySelectorAll('.' + self.options.errorClass + ', .' + self.options.validClass)).removeClass(self.options.errorClass + ' ' + self.options.validClass);

                return self.reset();
            });
        },

        /**
         * In this method, fields are validated.
         *
         * @param {Object} e - Event object.
         * @returns {mixed} Mixed return.
         */
        init: function(e) {
            // Reset error windows from all elements
            this.reset(FIELDS);
            // Start control each elements
            this.checkFields(e);
            // if event type is not submit, break
            if (e.type !== 'submit') return;
            // This is for when running remote request, return false and wait request response
            else if (this.handler === 'pending') return false;
            // if event type is submit and handler is true, break submit and call onError() function
            else if (this.handler === true) {
                this.options.onError.call(this, e);

                return false;
                // if form is valid call onValid() function
            } else return this.options.onValid.call(this, e);
        },

        /**
         * Checks Fields.
         *
         * @param  {Object} e - Event object.
         * @returns {void}
         */
        checkFields: function(e) {
            // stored this
            var self = this;
            var invalidFields = [];
            // Make invalidFields accessible

            this.getInvalidFields = function() {
                return invalidFields;
            };
            for (var i = 0, _lengthFields = FIELDS.length; i < _lengthFields; i++) {
                // if field is disabled, do not check
                if (FIELDS[i].disabled) continue;
                // current field
                var el = FIELDS[i];
                // current field's errors
                var errors = '';
                // current field's value
                var val = trim($(el).val());
                // current field's control methods
                var methods = el.getAttribute('data-validetta').split(',');
                // Validation state
                var state;

                // Create tmp
                this.tmp = {};
                // store el and val variables in tmp
                this.tmp = {
                    el: el,
                    val: val,
                    parent: this.parents(el)
                };
                // Start to check fields
                // Validator : Fields Control Object
                for (var j = 0, _lengthMethods = methods.length; j < _lengthMethods; j++) {
                    // Check Rule
                    var rule = methods[j].match(RRULE);
                    var method;
                    // Does it have rule?

                    if (rule !== null) {
                        // Does it have any argument ?
                        if (typeof rule[2] !== 'undefined') this.tmp.arg = rule[2];
                        // Set method name
                        method = rule[1];
                    } else {
                        method = methods[j];
                    }
                    // prevent empty validation if method is not required
                    if (val === '' && method !== 'required' && method !== 'equalTo') continue;
                    // Is there a methot in Validator ?
                    if (Validator.hasOwnProperty(method)) {
                        // Validator returns error message if method invalid
                        state = Validator[method](self.tmp, self);
                        if (typeof state !== 'undefined' && state !== true) {
                            var _dataMsg = el.getAttribute('data-vd-message-' + method);

                            if (_dataMsg !== null) state = _dataMsg;
                            errors += state + '<br/>';
                        }
                    }
                }
                // Check the errors
                if (errors !== '') {
                    invalidFields.push({
                        field: el,
                        errors: errors
                    });
                    $(el).parent().find('.' + this.options.validIconClass).remove();
                    // if parent element has valid class, remove and add error class
                    this.addErrorClass(this.tmp.parent);
                    // open error window
                    this.window.open.call(this, el, errors);
                // Check remote validation
                } else if (typeof this.tmp.remote !== 'undefined') {
                    this.checkRemote(el, e);
                } else {
                    // Nice, there are no error
                    // Append icon if element is textfield & valid, but is not a select.
                    if ($(el).hasClass('form-textfield__input') && !$(el).is('select')) {
                        $(el).after(telia.icon.render('#check', 'form-textfield__icon ' + this.options.validIconClass));
                    }
                    if (typeof state !== 'undefined') this.addValidClass(this.tmp.parent);
                    else $(this.tmp.parent).removeClass(this.options.errorClass + ' ' + this.options.validClass);
                    // Reset state variable
                    state = undefined;
                }
            }
        },

        /**
         * Checks remote validations.
         *
         * @param  {Object} el - Current field.
         * @param  {Object} e - Event object.
         * @throws {error} If previous remote request for same value has rejected.
         * @returns {void}
         */
        checkRemote: function(el, e) {
            var ajaxOptions = {};
            var data = {};
            var fieldName = el.name || el.id;

            if (typeof this.remoteCache === 'undefined') this.remoteCache = {};

            // Set data
            data[fieldName] = this.tmp.val;
            // exends ajax options
            ajaxOptions = $.extend(true, {}, {
                data: data
            }, this.options.validators.remote[this.tmp.remote] || {});

            // use $.param() function for generate specific cache key
            var cacheKey = $.param(ajaxOptions);

            // Check cache
            var cache = this.remoteCache[cacheKey];

            if (typeof cache !== 'undefined') {
                switch (cache.state) {
                    // pending means remote request not finished yet
                    case 'pending' :
                        // update handler and cache event type
                        this.handler = 'pending';
                        cache.event = e.type;
                        break;
                    // rejected means remote request could not be performed
                    case 'rejected' :
                        // we have to break submit because of throw error
                        e.preventDefault();
                        throw new Error(cache.result.message);
                    // resolved means remote request has done
                    case 'resolved' :
                        // Check to cache, if result is invalid, open an error window
                        if (cache.result.valid === false) {
                            this.addErrorClass(this.tmp.parent);
                            this.window.open.call(this, el, cache.result.message);
                        } else {
                            this.addValidClass(this.tmp.parent);
                        }
                }
            } else {
                // Abort if previous ajax request still running
                var _xhr = this.xhr[fieldName];

                if (typeof _xhr !== 'undefined' && _xhr.state() === 'pending') _xhr.abort();
                // Start caching
                cache = this.remoteCache[cacheKey] = {
                    state: 'pending',
                    event: e.type
                };
                // make a remote request
                this.remoteRequest(ajaxOptions, cache, el, fieldName);
            }
        },

        /**
         * Calls ajax request for remote validations.
         *
         * @param  {Object} e - Event object.
         * @param  {Object} ajaxOptions - Ajax options.
         * @param  {Object} cache - Cache object.
         * @param  {Object} el - Processing element.
         * @param  {string} fieldName - Field name for make specific caching.
         * @returns {void}
         */
        remoteRequest: function(e, ajaxOptions, cache, el, fieldName) {
            var self = this;

            $(this.tmp.parent).addClass('validetta-pending');

            // cache xhr
            this.xhr[fieldName] = $.ajax(ajaxOptions)
                .done(function(result) {
                    if (typeof result !== 'object') result = JSON.parse(result);
                    cache.state = 'resolved';
                    cache.result = result;
                    if (cache.event === 'submit') {
                        self.handler = false;
                        $(self.form).trigger('submit');
                    } else if (result.valid === false) {
                        self.addErrorClass(self.tmp.parent);
                        self.window.open.call(self, el, result.message);
                    } else {
                        self.addValidClass(self.tmp.parent);
                    }
                })
                .fail(function(jqXHR, textStatus) {
                // Dont throw error if request is aborted
                    if (textStatus !== 'abort') {
                        var _msg = 'Ajax request failed for field (' + fieldName + ') : ' + jqXHR.status + ' ' + jqXHR.statusText;

                        cache.state = 'rejected';
                        cache.result = {
                            valid: false,
                            message: _msg
                        };
                        throw new Error(_msg);
                    }
                })
                .always(function() { $(self.tmp.parent).removeClass('validetta-pending'); });

            this.handler = 'pending';
        },

        /**
         * This the section which opening or closing error windows process is done
         *
         * @namespace
         */
        window: {
            /**
             * Error window opens.
             *
             * @param {Object} el - Element which has an error (it can be native element or jQuery object).
             * @param {string} error - Error messages.
             * @returns {void}
             */
            open: function(el, error) {
                // We want display errors ?
                if (!this.options.showErrorMessages) {
                    // because of form not valid, set handler true for break submit
                    this.handler = true;

                    return;
                }
                var elParent = this.parents(el);
                // If the parent element undefined, that means el is an object. So we need to transform to the element

                if (typeof elParent === 'undefined') elParent = el[0].parentNode;
                // if there is an error window which previously opened for el, return
                if (elParent.querySelectorAll('.' + this.options.errorTemplateClass).length) return;

                var errorElement = document.createElement('span');

                errorElement.className = this.options.errorTemplateClass;
                errorElement.innerHTML = '<span class="form-error__icon">' + telia.icon.render('#alert') + '</span><span class="form-error__content">' + error + '</span>';

                elParent.appendChild(errorElement);

                // we have an error so we need to break submit
                // set to handler true
                this.handler = true;
            },

            /**
             * Error window closes.
             *
             * @param {HTMLElement} el - The error message window which will be disappear.
             * @returns {void}
             */
            close: function(el) {
                el.parentNode.removeChild(el);
            }
        },


        /**
         * Removes all error messages windows.
         *
         * @param {Object} el - Form elements which have an error message window.
         * @returns {void}
         */
        reset: function(el) {
            var _errorMessages = {};
            // if el is undefined ( This is the process of resetting all <form> )
            // or el is an object that has element more than one
            // and these elements are not checkbox

            if (typeof el === 'undefined' || (el.length > 1 && el[0].type !== 'checkbox')) {
                _errorMessages = this.form.querySelectorAll('.' + this.options.errorTemplateClass);
            } else {
                _errorMessages = this.parents(el[0]).querySelectorAll('.' + this.options.errorTemplateClass);
            }
            for (var i = 0, _lengthErrorMessages = _errorMessages.length; i < _lengthErrorMessages; i++) {
                this.window.close.call(this, _errorMessages[i]);
            }
            // set to handler false
            // otherwise at the next validation attempt, submit will not continue even the validation is successful
            this.handler = false;
        },

        /**
         * Adds error class and removes valid class if exist.
         *
         * @param {Object} el - Element.
         * @returns {void}
         */
        addErrorClass: function(el) {
            $(el).removeClass(this.options.validClass).addClass(this.options.errorClass);
        },

        /**
         * Adds valid class and removes error class if exist
         * if error class not exist, do not add valid class.
         *
         * @param {Object} el - Element.
         * @returns {void}
         */
        addValidClass: function(el) {
            $(el).removeClass(this.options.errorClass).addClass(this.options.validClass);
        },

        /**
         * Finds parent element.
         *
         * @param {Object} el - Element.
         * @returns {Object} Parent element.
         */
        parents: function(el) {
            var defaultUp = 0;

            if ($(el).hasClass('form-textfield__input')) {
                defaultUp = 1;

                if ($(el).prop('tagName') == 'SELECT') {
                    defaultUp = 2;
                }
            }
            var upLength = parseInt(el.getAttribute('data-vd-parent-up'), 10) || defaultUp;

            for (var i = 0; i <= upLength; i++) {
                el = el.parentNode;
            }

            return el;
        }
    };

    /**
     * Plugin Validetta.
     *
     * @param {Object} options - User-specified settings.
     * @param {Object} _messages - Extended arguments.
     * @returns {Object} This.
     */
    $.fn[pluginName] = function(options, _messages) {
        if ($.validettaLanguage) {
            messages = $.extend(true, {}, messages, $.validettaLanguage.messages);
        }
        if (typeof _messages !== 'undefined') {
            messages = $.extend(true, {}, messages, _messages);
        }

        return this.each(function() {
            if (!$.data(this, 'plugin_' + pluginName)) {
                $.data(this, 'plugin_' + pluginName, new Validetta(this, options));
            }
        });
    };

    /**
     * @param {Event} event
     * @event enhance.telia.validation
     */
    $(document).on('enhance.telia.validation', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]();
    });

    /**
     * @param {Event} event
     * @event enhance.form.validate
     * @deprecated use enhance.telia.validation directly
     */
    $(document).on('enhance.form.validate', function(event) {
        $(document).trigger('enhance.telia.validation', event);
    });
})(jQuery);

(function() {
    'use strict';

    $.fn.weekDatepicker = function() {
        var $trigger = $(this);
        var pickerClass = 'weekdatepicker';
        var activeClass = 'active';

        /**
         * Set position of weekdatepicker.
         *
         * @private
         * @param {JQuery} $el - WeekDatepicker element.
         * @class
         */
        function _setPosition($el) {
            var triggerDims = _getDimensions($trigger);
            var elDims = _getDimensions($el);
            var left;
            var top;
            var bodyRect;
            var triggerRect;
            var pointerRight = 3;

            left = triggerDims.width - elDims.width;
            top = triggerDims.top + triggerDims.height + 10;

            bodyRect = $('body')[0].getBoundingClientRect();
            triggerRect = $trigger[0].getBoundingClientRect();

            if ((triggerRect.right + 15) < bodyRect.right) {
                left += 15;
                pointerRight += 15;
            }

            $($el[0].firstElementChild).css({
                right: pointerRight
            });

            $el.css({
                left: left,
                top: top
            });
        };

        /**
         * Get dimensions of elements.
         *
         * @param {JQuery} $el - Weekdatepicker element.
         * @private
         * @returns {Object} Dimensions object.
         */
        function _getDimensions($el) {
            var offset = $el.position();

            return {
                width: $el.outerWidth(),
                height: $el.outerHeight(),
                left: offset.left,
                top: offset.top
            };
        };

        /**
         * Show weekdatepicker.
         *
         * @param  {JQuery} $el - Weekdatepicker element.
         * @returns {void}
         */
        function show($el) {
            _setPosition($el);
            $el.addClass(activeClass);
        };

        /**
         * Hide weekdatepicker.
         *
         * @param  {JQuery} $el - Weekdatepicker element.
         * @returns {void}
         */
        function hide($el) {
            $el.removeClass(activeClass).css({
                left: '-10000px'
            });
        };

        return this.each(function() {
            var $this = $(this);
            var $el = $this.parent().siblings('.' + pickerClass);

            $this.on('focus', function() {
                show($el);
            });
            $this.on('blur', function() {
                hide($el);
            });
        });
    };
}());

;(function($, window, document) {
    var pluginName = 'teliaNavigation';
    var defaults = {
        breakPoint: 1023
    };
    var initSelector = '.nav';

    /**
     * Navigation.
     *
     * @class
     * @param {HTMLElement} element - The HTML element the Navigation should be bound to.
     * @param {Object} options - An option map.
     */
    function Navigation(element, options) {
        this.element = element;
        this.$element = $(this.element);
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this.windowWidth = $(window).width();

        this._init();
    }

    /**
     * Initialize the plugin.
     *
     * @function init
     * @private
     * @returns {void}
     */
    Navigation.prototype._init = function() {
        var that = this;

        this._bindClickEvents();
        this._bindKeyboardEvents();
        this.updateHeight();

        $(window).on('resize.telia.navigation', function() {
            clearTimeout(that.navigationResizeTimer);
            that.navigationResizeTimer = setTimeout(that.resize.bind(that), 250);
        });

        this.$element.on('update.telia.navigation', function(event, height) {
            that.height = height;
        });
    };

    /**
     * Adjust navigation according to width.
     *
     * @function resize
     * @returns {void}
     */
    Navigation.prototype.resize = function() {
        var windowWidthOld = this.windowWidth;
        var that = this;

        this.windowWidth = $(window).width();

        if (this.windowWidth >= this.options.breakPoint && windowWidthOld < this.options.breakPoint) {
            this.$element.find('.nav__sub-container').width('auto');
            this._hideSubmenu();
        } else if (this.windowWidth >= this.options.breakPoint && windowWidthOld >= this.options.breakPoint) {
            var parent = this.$element.find('.nav__item--open');

            if (parent.length > 0) {
                var subnav = parent.find('> .nav__sub-container');
                var prevChild = subnav;

                this._setSubnavMaxHeight(subnav);
                this._setSubnavWidth(subnav);
                this._setSubnavHorizontalMovement(parent, subnav);
                subnav.find('.nav__sub-container--visible').each(function() {
                    var child = $(this);
                    var parent = prevChild.find('.nav__item--open-fake');
                    var level = child.parents('.nav__sub-container').length;
                    var realSubnav = parent.find('> .nav__sub-container');

                    that._setSubnavWidth(child);

                    var newTop = that._getSubnavVerticalMovement(parent, realSubnav, level);

                    child.css({
                        top: newTop
                    });
                    prevChild = child;
                });
            }
        }

        this.updateHeight();
    };

    /**
     * Updates height property and related elements.
     *
     * @function updateHeight
     * @returns {void}
     */
    Navigation.prototype.updateHeight = function() {
        var that = this;

        setTimeout(function() {
            that.height = that.$element.height();
        }, 300);
    };

    /**
     * Bind click events.
     *
     * @function bindClickEvents
     * @private
     * @returns {void}
     */
    Navigation.prototype._bindClickEvents = function() {
        this.$element.find('.nav__link').on('click.telia.navigation', this._toggleSubmenu.bind(this));
    };

    /**
     * Close sub navigation.
     *
     * @function closeSubnav
     * @param {HTMLElement} parent - Parent element of sub navigation.
     * @private
     * @returns {void}
     */
    Navigation.prototype._closeSubnav = function(parent) {
        parent.parent().parent().find('.nav__item').removeClass('nav__item--open nav__item--open-fake');

        var subContainer = parent.closest('.nav__sub-container');

        if (!subContainer.length) {
            subContainer = parent.closest('.nav');
        }

        subContainer.find('.nav__sub-container--visible').remove();
    };

    /**
     * Get submenu top position.
     *
     * @function getSubnavVerticalMovemen
     * @param {HTMLElement} parent - Parent element of subnav.
     * @param {HTMLElement} subnav - Sub navigation.
     * @param {string} level - Level on nav.
     * @private
     * @returns {number} Top position.
     */
    Navigation.prototype._getSubnavVerticalMovement = function(parent, subnav, level) {
        var parentTop = parent.position().top;
        var newTop = parentTop;
        var outerTop = parent.parent().parent().parent().position().top;

        if (level == 1) {
            outerTop = 0;
        }
        var subnavHeight = subnav.outerHeight();

        if (parentTop + outerTop + subnavHeight > this._getSubnavMaxHeight()) {
            var diff = subnavHeight - (this._getSubnavMaxHeight() - (outerTop + parentTop));

            newTop = parentTop - diff;
        }
        if (parent.parent().parent().scrollTop() > parentTop) {
            newTop = 0;
        }

        return newTop;
    };

    /**
     * Set subnavigation horizontal position.
     *
     * @function setSubnavHorizontalMovement
     * @param {HTMLElement} parent - The parent element of subnav.
     * @param {HTMLElement} subnav - Sub navigation.
     * @private
     * @returns {void}
     */
    Navigation.prototype._setSubnavHorizontalMovement = function(parent, subnav) {
        var maxLevels = this._getLevelCount(parent);
        var maxWidth = maxLevels * this._getSubnavWidth();
        var parentWidth = this.windowWidth - parent.position().left;

        if (subnav.hasClass('nav__sub-container--right')) {
            return;
        }

        if (maxWidth > parentWidth) {
            var negLeft = maxWidth - parentWidth;

            subnav.css({
                left: -negLeft
            });
        }
    };

    /**
     * Get depth of submenu.
     *
     * @function getLevelCount
     * @param {HTMLElement} parent - Parent element.
     * @returns {number} How many levels submenu has.
     * @private
     */
    Navigation.prototype._getLevelCount = function(parent) {
        var max = 1;

        parent.find('.nav__sub-container').each(function() {
            var test = $(this).parents('.nav__sub-container').length + 1;

            if (test > max) {
                max = test;
            }
        });

        return max;
    };

    /**
     * Get submenu width.
     *
     * @function getSubnavWidth
     * @returns {number} Submenu width.
     * @private
     */
    Navigation.prototype._getSubnavWidth = function() {
        var max = 1200;

        if (this.windowWidth < 1200) {
            max = this.windowWidth;
        }

        var result = max / 3;

        if (result > 220) {
            result = 220;
        }

        return result;
    };

    /**
     * Set submenu width.
     *
     * @function setSubnavWidth
     * @param {HTMLElement} subnav - Subnav element.
     * @private
     * @returns {void}
     */
    Navigation.prototype._setSubnavWidth = function(subnav) {
        subnav.width(this._getSubnavWidth());
    };

    /**
     * Get max height of submenu.
     *
     * @function getSubnavMaxHeight
     * @returns {number} Max height of subnav.
     * @private
     */
    Navigation.prototype._getSubnavMaxHeight = function() {
        var maxHeight = $(window).height() - this.height;

        return maxHeight;
    };

    /**
     * Set max height of submenu.
     *
     * @function setSubnavMaxHeight
     * @param {HTMLElement} submenu - Subnav element.
     * @private
     * @returns {void}
     */
    Navigation.prototype._setSubnavMaxHeight = function(submenu) {
        submenu.attr('style', false);
        var maxHeight = this._getSubnavMaxHeight();

        if (submenu.outerHeight() > maxHeight) {
            submenu.outerHeight(maxHeight);
        }
    };

    /**
     * Toggle submenu.
     *
     * @function toggleSubmenu
     * @param {Event} event - Event.
     * @private
     * @returns {void}
     */
    Navigation.prototype._toggleSubmenu = function(event) {
        var self = $(event.currentTarget);
        var parent = self.parent();

        var subnav = parent.find('> .nav__sub-container');
        var level = self.parents('.nav__sub-container').length;

        var isDesktop = this.windowWidth > this.options.breakPoint;

        if (subnav.length > 0) {
            event.preventDefault();

            if (parent.hasClass('nav__item--open')) {
                if (isDesktop && level == 0) {
                    this.$element.trigger('hide.telia.navigation');
                    this.$element.removeClass('nav--open');
                    this._hideSubmenu();
                } else {
                    parent.removeClass('nav__item--open');
                    parent.find('.nav__item--open').removeClass('nav__item--open');
                }
            } else if (parent.hasClass('nav__item--open-fake')) {
                this._closeSubnav(parent);
            } else if (isDesktop) {
                this.$element.trigger('show.telia.navigation');
                this._closeSubnav(parent);
                this._setSubnavMaxHeight(subnav);
                this._setSubnavWidth(subnav);
                if (level == 0) {
                    this.$element.addClass('nav--open');
                    parent.addClass('nav__item--open');
                    this._setSubnavHorizontalMovement(parent, subnav);
                } else if (level > 0) {
                    parent.addClass('nav__item--open-fake');
                    var newTop = this._getSubnavVerticalMovement(parent, subnav, level);
                    var subnavClone = subnav.clone(true).css({
                        top: newTop
                    }).addClass('nav__sub-container--visible');

                    subnavClone.data('subnavTrigger', self);
                    self.data('subnavClone', subnavClone);
                    parent.closest('.nav__sub-container').append(subnavClone);
                }
            } else {
                parent.addClass('nav__item--open');
            }
        }
    };

    /**
     * Hide submenu.
     *
     * @function hideSubmenu
     * @private
     * @returns {void}
     */
    Navigation.prototype._hideSubmenu = function() {
        var parent = this.$element.find('.nav__item');

        parent.removeClass('nav__item--open nav__item--open-fake');
        parent.find('.nav__item--open', this.$element).removeClass('nav__item--open');
        this.$element.removeClass('nav--open');
        this.$element.find('.nav__sub-container .nav__sub-container--visible').remove();
    };

    /**
     * Define keyboard navigation.
     *
     * @function bindKeyboardEvents
     * @private
     * @returns {void}
     */
    Navigation.prototype._bindKeyboardEvents = function() {
        var that = this;

        $(document).on('keydown.telia.navigation', '.nav', function(e) {
            if (!(/(9|37|38|39|40|27|32)/).test(e.which)) return;

            var $target = $(e.target);

            // space triggers click
            if (e.which == 32) {
                e.preventDefault();
                e.stopPropagation();

                return $(e.target).trigger('click');
            }

            // esc triggers close menu
            if (e.which == 27) {
                e.preventDefault();
                e.stopPropagation();
                if ($target.hasClass('nav__link')) {
                    if ($target.parent().hasClass('nav__item--open-fake')) {
                        $target.trigger('click');
                    } else if ($target.closest('.nav__sub-container').length > 0 && $target.closest('.nav__sub-container').data('subnavTrigger')) {
                        $target.closest('.nav__sub-container').data('subnavTrigger').focus().click();
                    } else if ($target.closest('.nav__sub-container').length > 0) {
                        $target.closest('.nav__sub-container').prev().focus().click();
                    }
                }
            }

            if (e.which == 9) {
                if ($target.hasClass('nav__link')) {
                    if ($target.parent().hasClass('nav__item--open-fake')) {
                        // tab to cloned submenu
                        if ($target.data('subnavClone')) {
                            e.preventDefault();
                            e.stopPropagation();
                            $target.data('subnavClone').find('a').first().focus();
                        }
                    }
                }
            }

            if (e.which == 37 || e.which == 38 || e.which == 39 || e.which == 40) {
                if ($target.hasClass('nav__link')) {
                    var $items;
                    // horizontal menu

                    if ($target.closest('.nav__sub-container').length == 0) {
                        $items = $target.closest('.nav').find('> .nav__item > .nav__link');
                        that._moveTarget(e, $items);
                    } else {
                        $items = $target.closest('.nav__sub-container').find('> .nav__sub-inner > .nav__sub > .nav__item > .nav__link');
                        that._moveTarget(e, $items);
                    }
                }
            }
        });
    };

    /**
     * Move target with keyboard.
     *
     * @param  {Event} e - Event.
     * @param  {Array[]} items - Array of items to move.
     * @returns {void}
     */
    Navigation.prototype._moveTarget = function(e, items) {
        if (!items.length) return;

        e.preventDefault();
        e.stopPropagation();

        var index = items.index(e.target);

        // up
        if (e.which == 38 || e.which == 37 && index > 0) index--;
        // down
        if (e.which == 40 || e.which == 39 && index < items.length - 1) index++;

        if (index >= 0) {
            items.eq(index).trigger('focus');
        }
    };

    /**
     * Destroy plugin.
     *
     * @function destroy
     * @returns {void}
     */
    Navigation.prototype.destroy = function() {
        $(window).off('resize.telia.navigation');
        $(window).off('scroll.telia.navigation');
        $(document).off('keydown.telia.navigation');
        this.$element.find('.nav__link').off('click.telia.navigation');

        this.$element.removeData('plugin_' + pluginName);
    };

    /**
     * Create jQuery plugin.
     *
     * @param {Object} options - Extended options.
     * @listens telia.navigation
     * @returns {Array<Navigation>} Array of Navigations.
     */
    $.fn[pluginName] = function(options) {
        var args = Array.prototype.slice.call(arguments, 1);

        return this.each(function() {
            var instance = $.data(this, 'plugin_' + pluginName);

            if (!instance) {
                if (options === 'destroy') {
                    return;
                }

                $.data(this, 'plugin_' + pluginName, new Navigation(this, options));
            } else if (typeof options === 'string') {
                instance[options].apply(instance, args);
            }
        });
    };

    /**
     * @param {Event} event
     * @event enhance.telia.navigation
     */
    $(document).on('enhance.telia.navigation', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]();
    });

    /**
     * @param {Event} even
     * @event destroy.telia.navigation
     */
    $(document).on('destroy.telia.navigation', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]('destroy');
    });
})(jQuery, window, document);

/**
 * Auto-init the plugin on DOM ready.
 * @fires enhance.telia.navigation
 */
$(function() {
    $(document).trigger('enhance.telia.navigation');
});

;(function($, window, document) {
    var pluginName = 'teliaHeader';
    var defaults = {
        headerBreak: 1023
    };
    var initSelector = '.header';

    /**
     * Header.
     *
     * @class
     * @param {HTMLElement} element - The HTML element the Header should be bound to.
     * @param {Object} options - An option map.
     */
    function Header(element, options) {
        this.element = element;
        this.$element = $(this.element);
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this.$headerDropdown = this.$element.find('.header__dropdown');
        this.$headerBackdrop = $('<div class="header__backdrop" />');
        this.$nav = this.$element.find('.header__nav');
        this.headerWidth = $(window).width();
        this._init();
    }

    /**
     * Initialize the plugin.
     *
     * @function init
     * @private
     * @returns {void}
     */
    Header.prototype._init = function() {
        var that = this;

        this._bindClickEvents();
        this.initSuggestionList();
        this._copyPebbleClasses();
        this.hasScrolled();
        this._bindKeyboardEvents();
        this._initIframeResizer();
        this.updateHeight();

        $(window).on('resize.telia.header', function() {
            clearTimeout(that.headerResizeTimer);
            that.headerResizeTimer = setTimeout(that.resize.bind(that), 250);
        });

        $(window).on('scroll.telia.header', function() {
            clearTimeout(that.headerScrollTimeout);
            that.headerScrollTimeout = setTimeout(function() {
                that.hasScrolled();
            }, 100);
        });

        /**
         * @event show.telia.navigation
         */
        this.$nav.on('show.telia.navigation', function() {
            that.$element.addClass('header--open-nav');
            that._headerDisableScroll('header__backdrop--nav');
        });

        /**
         * @event hide.telia.navigation
         */
        this.$nav.on('hide.telia.navigation', function() {
            that._enableScroll();
        });
    };

    /**
     * Copy Logo classes from desktop to mobile view.
     *
     * @function copyPebbleClasses
     * @private
     * @returns {void}
     */
    Header.prototype._copyPebbleClasses = function() {
        var topLogoPebbleClass = this.$element.find('.header__top .logo__pebble').attr('class');

        this.$element.find('.header__bottom .logo__pebble').attr('class', topLogoPebbleClass);
    };

    /**
     * Adjust header according to width.
     *
     * @function resize
     * @returns {void}
     */
    Header.prototype.resize = function() {
        var headerWidthOld = this.headerWidth;

        this.headerWidth = $(window).width();

        if (this.headerWidth < this.options.headerBreak && headerWidthOld >= this.options.headerBreak) {
            this.hideDropdown();
        } else if (this.headerWidth >= this.options.headerBreak && headerWidthOld < this.options.headerBreak) {
            this.hideDropdown();
            this._enableScroll();
        }

        this.updateHeight();
    };

    /**
     * Updates height property and related elements.
     *
     * @function updateHeight
     * @returns {void}
     */
    Header.prototype.updateHeight = function() {
        var that = this;

        setTimeout(function() {
            that.height = that.$element.height();
            that.pushDropdown();
            that.pushContent();

            that.$nav.trigger('update.telia.navigation', [that.height]);
        }, 300);
    };

    /**
     * Updates the header if page is scrolled.
     *
     * @function hasScrolled
     * @returns {void}
     */
    Header.prototype.hasScrolled = function() {
        var topPosition = $(window).scrollTop();

        if (topPosition > this.height) {
            if (!this.$element.hasClass('header--scroll')) {
                this.$element.trigger('cssClassChanged');
                this.$element.addClass('header--scroll');
                this.$nav.addClass('nav--compact');
            }
            this.$element.find('.header__top a').attr('tabindex', '-1');
        } else if (!$('body').hasClass('scroll-disabled')) {
            this.$element.trigger('data.cssClassChanged');
            this.$element.removeClass('header--scroll');
            this.$nav.removeClass('nav--compact');
            this.$element.find('.header__top a').removeAttr('tabindex');
        }

        this.updateHeight();
    };

    /**
     * Bind click events.
     *
     * @function bindClickEvents
     * @private
     * @returns {void}
     */
    Header.prototype._bindClickEvents = function() {
        this.$element.find('[data-toggle="header"]').on('click.telia.header', this._toggleDropdown.bind(this));

        this.$element.find('.header__actions-item [data-toggle="dropdown"]').on('click.telia.header', this._enablePage.bind(this));

        this.$element.find('.search__results').on('click.telia.header', function(event) {
            event.stopPropagation();
        });

        this.$headerBackdrop.on('click.telia.header', this._enablePage.bind(this));
    };

    /**
     * Toggle header dropdown.
     *
     * @function toggleDropdown
     * @param {Event} event - Event.
     * @private
     * @returns {void}
     */
    Header.prototype._toggleDropdown = function(event) {
        event.preventDefault();
        this.updateHeight();
        var self = this.$element.find(event.currentTarget);
        var target = $(self.attr('data-target'));
        var cloneTarget = $(self.attr('data-clone-target')) || $(self.attr('data-target'));

        if (self.attr('data-origin')) {
            var origin = $(self.attr('data-origin'));
            var newID = 'header-nav-clone';

            if (target.find('#' + newID).length == 0) {
                var originClone = origin.clone(true).attr('id', newID);

                cloneTarget.append(originClone);
            }
        }
        if (target.hasClass('header__dropdown__section--visible')) {
            this.hideDropdown(target);
        } else {
            this.showDropdown(target);

            if (this.$headerBackdrop.hasClass('header__backdrop--nav')) {
                this.$nav.teliaNavigation('_hideSubmenu');
                this.$element.removeClass('header--open-nav');
                this.$headerBackdrop.removeClass('header__backdrop--nav');
            }
        }
    };

    /**
     * Close all menus and enables page scroll.
     *
     * @function enablePage
     * @param {Event} event - Event.
     * @private
     * @returns {void}
     */
    Header.prototype._enablePage = function(event) {
        event.preventDefault();
        if (this.$headerBackdrop.hasClass('header__backdrop--nav')) {
            this.$nav.teliaNavigation('_hideSubmenu');
            this._enableScroll();
        } else {
            this.hideDropdown();
        }
    };

    /**
     * Init iframe resizer.
     *
     * @function initIframeResizer
     * @private
     * @returns {void}
     */
    Header.prototype._initIframeResizer = function() {
        this.$element.find('.header__iframe').iFrameResize({
            checkOrigin: false
        });
    };

    /**
     * Hide specified header drop down or all if target is not specified.
     *
     * @function hideDropdown
     * @param {HTMLElement} target - Specified drop down trarget.
     * @param {boolean} keepScrollDisabled - Keep scroll disabled.
     * @returns {void}
     */
    Header.prototype.hideDropdown = function(target, keepScrollDisabled) {
        var parent;
        var _target;

        if (!keepScrollDisabled) {
            this._enableScroll();
        }

        if (target) {
            var id = target.attr('id');
            var anchor = this.$element.find('[data-toggle="header"][data-target="#' + id + '"]');

            parent = anchor.parent();
            _target = target;
            anchor.focus();
        } else {
            parent = this.$element.find('.header__actions-item, .header__employee-settings');
            _target = this.$element.find('.header__dropdown__section');
        }

        parent.removeClass('is-current');
        _target.removeClass('header__dropdown__section--visible');
        this.$headerDropdown.removeClass('header__dropdown--visible');

        toggleMenuAnimatedIcon(this.$element.find('.menu__icon-animate'), this.$element.find('#header-menu').hasClass('header__dropdown__section--visible'));
    };

    /**
     * Show header dropdown.
     *
     * @function showDropdown
     * @param {HTMLelement} target - Target element.
     * @returns {void}
     */
    Header.prototype.showDropdown = function(target) {
        this._headerDisableScroll();

        var id = target.attr('id');
        var parent = $('[data-toggle="header"][data-target="#' + id + '"]').parent();

        parent.siblings().removeClass('is-current');
        parent.addClass('is-current');

        target.siblings().removeClass('header__dropdown__section--visible');
        target.addClass('header__dropdown__section--visible');
        target.attr('tabindex', '-1');

        this.$headerDropdown.addClass('header__dropdown--visible');

        toggleMenuAnimatedIcon(this.$element.find('.menu__icon-animate'), this.$element.find('#header-menu').hasClass('header__dropdown__section--visible'));
    };

    /**
     * Disable scroll.
     *
     * @function headerDisableScroll
     * @param {string} backdropClass - Class for backdrop.
     * @private
     * @returns {void}
     */
    Header.prototype._headerDisableScroll = function(backdropClass) {
        window.disableScroll();
        // show backdrop only if no backdrop exists
        this._showBackDrop(backdropClass);
    };

    /**
     * Enable page scroll.
     *
     * @function enableScroll
     * @private
     * @returns {void}
     */
    Header.prototype._enableScroll = function() {
        if (this.$headerBackdrop.length > 0) {
            window.enableScroll();
        }
        // hide backdrop
        this._hideBackDrop();
    };

    /**
     * Add header backdrop to DOM and set visible.
     *
     * @function showBackDrop
     * @param {string} extraClass - Extra class will be added to the `.header__backdrop` element.
     * @private
     * @returns {void}
     */
    Header.prototype._showBackDrop = function(extraClass) {
        var that = this;

        this.$element.append(this.$headerBackdrop);
        setTimeout(function() {
            that.$headerBackdrop.addClass('header__backdrop--visible ' + extraClass);
        }, 10);
    };

    /**
     * Hide header backdrop and detach from DOM.
     *
     * @function hideBackDrop
     * @private
     * @returns {void}
     */
    Header.prototype._hideBackDrop = function() {
        var that = this;

        this.$headerBackdrop.removeClass('header__backdrop--visible');
        setTimeout(function() {
            that.$headerBackdrop.detach();
            //remove all extra classes
            that.$headerBackdrop.removeClass().addClass('header__backdrop');
            that.$element.removeClass('header--open-nav');
        }, 200);
    };

    /**
     * Init suggestion list.
     *
     * @function initSuggestionList
     * @returns {void}
     */
    Header.prototype.initSuggestionList = function() {
        var that = this;
        var attribute = 'data-suggestion-list';
        var suggestions = that.$element.find('[' + attribute + ']');

        suggestions.on('afterShow.telia.suggestion', that._headerDisableScroll.bind(that));

        suggestions.on('afterHide.telia.suggestion', function() {
            if ($(this).parents('.header__dropdown').length == 0) {
                that._enableScroll();
            }
        });

        suggestions.on('focus.telia.header', function() {
            if ($(this).parents('.header__dropdown').length == 0 && !$(this).hasClass('active')) {
                that.hideDropdown();
                that.$nav.teliaNavigation('_hideSubmenu');
            }
        });
    };

    /**
     * Detects if there are notice in the header and adjust content position
     * on notice close.
     *
     * @function noticeListener
     * @private
     * @returns {void}
     */
    Header.prototype._noticeListener = function() {
        var that = this;
        var $headerNotice = this.$element.find('.header__notice');

        $.each($headerNotice, function(index, value) {
            if (!this.pushHeader) {
                $(value).on('teliaNotice.close', function() {
                    that.updateHeight();
                });
                this.pushHeader = true;
            }
        });
    };

    /**
     * Set content top position according to menu height.
     *
     * @function pushContent
     * @returns {void}
     */
    Header.prototype.pushContent = function() {
        var that = this;

        this._noticeListener();
        if (this.$element.hasClass('header--scroll')) return;
        setTimeout(function() {
            $('body').css('padding-top', that.height + 'px');
        }, 250);
    };

    /**
     * Set menu dropdown top position according to menu height.
     *
     * @function pushDropdown
     * @returns {void}
     */
    Header.prototype.pushDropdown = function() {
        var headerDropdownTop = this.height;
        var headerBottomHeight = this.$element.find('.header__bottom').height();

        if (this.headerWidth >= this.options.headerBreak && !this.$element.hasClass('header--scroll')) {
            headerDropdownTop = this.height - headerBottomHeight;
        }

        this.$headerDropdown.css('top', headerDropdownTop + 'px');
    };

    /**
     * Define keyboard navigation.
     *
     * @function bindKeyboardEvents
     * @private
     * @returns {void}
     */
    Header.prototype._bindKeyboardEvents = function() {
        $(document).on('keydown.telia.header', '.header', function(e) {
            if (!(/(9|37|38|39|40|27|32)/).test(e.which) || (/input|textarea/i).test(e.target.tagName)) return;

            var $target = $(e.target);

            // space triggers click
            if (e.which == 32) {
                e.preventDefault();
                e.stopPropagation();

                return $(e.target).trigger('click');
            }

            // esc triggers close menu
            if (e.which == 27) {
                e.preventDefault();
                e.stopPropagation();
                if ($target.hasClass('menu__link')) {
                    if ($target.parent().hasClass('is-current')) {
                        $target.click();
                    }
                }
            }

            if (e.which == 9) {
                if ($target.hasClass('menu__link') && $target.attr('data-toggle') == 'header' && $target.parent().hasClass('is-current')) {
                    e.preventDefault();
                    e.stopPropagation();
                    $($target.attr('data-target')).find('a, input, button, select, textarea').filter(':visible:not([disabled])').first().focus();
                }
            }
        });
    };

    /**
     * Move target with keyboard.
     *
     * @function _moveTarget
     * @param  {Event} event - Event.
     * @param  {Array[]} items - Array of target items.
     * @private
     * @returns {void}
     */
    Header.prototype._moveTarget = function(event, items) {
        if (!items.length) return;

        event.preventDefault();
        event.stopPropagation();

        var index = items.index(event.target);

        // up
        if (event.which == 38 || event.which == 37 && index > 0) index--;
        // down
        if (event.which == 40 || event.which == 39 && index < items.length - 1) index++;

        if (index >= 0) {
            items.eq(index).trigger('focus');
        }
    };

    /**
     * Destroy plugin.
     *
     * @function destroy
     * @returns {void}
     */
    Header.prototype.destroy = function() {
        $(window).off('resize.telia.header');
        $(window).off('scroll.telia.header');
        $(document).off('keydown.telia.header');
        this.$element.find('.header').off('keydown.telia.header');
        this.$element.find('[data-toggle="header"]').off('click.telia.header');
        this.$element.find('.header__actions-item [data-toggle="dropdown"]').off('click.telia.header');
        this.$element.find('.search__results').off('click.telia.header');
        this.$element.find('.header__notice').off('teliaNotice.close');
        this.$headerBackdrop.off('click.telia.header');

        this.$element.removeData('plugin_' + pluginName);
    };

    /**
     * Toggles menu icon animation.
     *
     * @function toggleMenuAnimatedIcon
     * @param {HTMLelement} target - Icon element.
     * @param {boolean} condition - If true, active class will be added, else active class will be removed.
     * @returns {void}
     */
    function toggleMenuAnimatedIcon(target, condition) {
        if (target.length > 0) {
            if (condition) {
                target.addClass('menu__icon-animate--active');
            } else {
                target.removeClass('menu__icon-animate--active');
            }
        }
    };

    /**
     * Create jQuery plugin.
     *
     * @param {Object} options - Extended options.
     * @listens telia.header
     * @returns {Array<Header>} Array of Headers.
     */
    $.fn[pluginName] = function(options) {
        var args = Array.prototype.slice.call(arguments, 1);

        return this.each(function() {
            var instance = $.data(this, 'plugin_' + pluginName);

            if (!instance) {
                if (options === 'destroy') {
                    return;
                }

                $.data(this, 'plugin_' + pluginName, new Header(this, options));
            } else if (typeof options === 'string') {
                instance[options].apply(instance, args);
            }
        });
    };

    /**
     * @param {Event} event
     * @event enhance.telia.header
     */
    $(document).on('enhance.telia.header', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]();
    });

    /**
     * @param {Event} even
     * @event destroy.telia.header
     */
    $(document).on('destroy.telia.header', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]('destroy');
    });
})(jQuery, window, document);

/**
 * Auto-init the plugin on DOM ready.
 * @fires enhance.telia.header
 */
$(function() {
    $(document).trigger('enhance.telia.header');
});

;(function($, window, document) {
    var pluginName = 'teliaLivechat';
    var defaults = {};
    var initSelector = '.js-livechat';

    /**
     * Livechat.
     *
     * @class
     * @param {Element} element - The HTML element the plugin is initialized on.
     * @param {Object} options - An options object.
     */
    function Livechat(element, options) {
        this.element = element;
        this.$element = $(this.element);
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.resizeTimeout = 0;

        this._init();
    }

    /**
     * Initializes the plugin.
     *
     * @function _init
     * @private
     * @returns {void}
     */
    Livechat.prototype._init = function() {
        window.LC_API = typeof window.LC_API != 'undefined' ? window.LC_API : {};

        this.$element.on('click.telia.livechat', this._clickHandler.bind(this));

        window.LC_API.on_after_load = this._afterLoad.bind(this); // eslint-disable-line camelcase
        window.LC_API.on_chat_window_minimized = this._afterLoad.bind(this); // eslint-disable-line camelcase

        if (typeof window.LC_API.open_chat_window == 'undefined') {
            window.LC_API.open_chat_window = this._openChatWindow.bind(this); // eslint-disable-line camelcase
        }

        $(window).on('resize.telia.livechat', this._resizeHandler.bind(this));

        if (telia.openLivechatOnLoad) {
            this._showCorrectChatWindow();
        }

        if (telia.initLivechatOnLoad) {
            this._loadLivechatAPI();
        }
    };

    /**
     * Prevent default click action and load API.
     *
     * @function _clickHandler
     * @private
     * @param   {Evenet} event - An event object.
     * @returns {void}
     */
    Livechat.prototype._clickHandler = function(event) {
        event.preventDefault();

        this._loadLivechatAPI();
        this._showCorrectChatWindow();
    };

    /**
     * Load Livechat API.
     *
     * @function _loadLivechatAPI
     * @private
     * @returns {void}
     */
    Livechat.prototype._loadLivechatAPI = function() {
        if (telia.initLivechatOnLoad) {
            this._hideFakeChatButton();
        }

        var lc = document.createElement('script');

        lc.type = 'text/javascript';
        lc.async = true;
        lc.src = (document.location.protocol == 'https:'  ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';

        var s = document.getElementsByTagName('script')[0];

        s.parentNode.insertBefore(lc, s);
    };

    /**
     * Hide fake chat window and show the original.
     *
     * @function _showCorrectChatWindow
     * @private
     * @returns {void}
     */
    Livechat.prototype._showCorrectChatWindow = function() {
        this._hideFakeChatButton();
        window.LC_API.open_chat_window();
    };

    /**
     * Hide fake chat window.
     *
     * @function _hideFakeChatButton
     * @private
     * @returns {void}
     */
    Livechat.prototype._hideFakeChatButton = function() {
        this.$element.addClass('hidden');
    };

    /**
     * Modify livechat styles after load.
     *
     * @function _afterLoad
     * @private
     * @returns {void}
     */
    Livechat.prototype._afterLoad = function() {
        setTimeout(this._fixedButtonsMod.bind(this), 100);
    };

    /**
     * Load Livechat API and open the chat.
     *
     * @function _openChatWindow
     * @private
     * @returns {void}
     */
    Livechat.prototype._openChatWindow = function() {
        this._loadLivechatAPI();

        window.LC_API.on_before_load = this._showCorrectChatWindow.bind(this); // eslint-disable-line camelcase
    };

    /**
     * Initialize fixed buttons mod.
     *
     * @returns {void}
     */
    Livechat.prototype._fixedButtonsMod = function() {
        var $container = $('#livechat-compact-container');
        var $iframe = $('#livechat-compact-view');
        var $wrapper = $('.fixed-buttons-wrapper');
        var $fixedButtonsWrapper = $('.fixed-button');
        var $fixedButtons = $('.fixed-button__btn');

        var windowWidth = $(window).width();
        var bottom = 10;
        var width = windowWidth - $container.outerWidth() - 30;
        var buttonsWidth = 0;

        if (!$iframe.length) {
            return;
        }

        if (window.LC_API.new_mobile_is_detected()) {
            $container.css({
                'z-index': 21
            });
            // ios fix to force ios update css rules with !important
            $container[0].style.setProperty('width', '68px', 'important');
            $container[0].style.setProperty('height', '82px', 'important');
            $container[0].style.setProperty('bottom', '10px', 'important');

            $fixedButtonsWrapper.css({
                bottom: 10
            });
        } else {
            $container.css({
                height: 36,
                'z-index': 21
            });
            $iframe.css({
                top: 0
            });

            $.each($fixedButtons, function(key, item) {
                buttonsWidth += $(item).outerWidth();
            });

            if (windowWidth < (buttonsWidth + $container.outerWidth() + 35)) {
                bottom = 46;
                width = '97%';
            }

            $fixedButtonsWrapper.css({
                bottom: bottom,
                width: width
            });
        }

        $wrapper.css({
            height: $fixedButtonsWrapper.outerHeight() + bottom
        });
    };

    /**
     * Initialize fixed buttons mod after resize.
     *
     * @returns {void}
     */
    Livechat.prototype._resizeHandler = function() {
        clearTimeout(this.resizeTimeout);
        this.resizeTimeout = setTimeout(this._fixedButtonsMod.bind(this), 250);
    };

    /**
     * Destroy the plugin instance.
     *
     * @function destroy
     * @returns {void}
     */
    Livechat.prototype.destroy = function() {
        this.$element.off('click.telia.livechat');
        $(window).off('resize.telia.livechat');
        this.$element.removeData('plugin_' + pluginName);
    };

    /**
     * Create jQuery plugin.
     *
     * @param {Object} options - Extended options.
     * @listens telia.header
     * @returns {Array<Livechat>} Array of Livechats.
     */
    $.fn[pluginName] = function(options) {
        var args = Array.prototype.slice.call(arguments, 1);

        return this.each(function() {
            var instance = $.data(this, 'plugin_' + pluginName);

            if (!instance) {
                if (options === 'destroy') {
                    return;
                }

                $.data(this, 'plugin_' + pluginName, new Livechat(this, options));
            } else if (typeof options === 'string') {
                instance[options].apply(instance, args);
            }
        });
    };

    /**
     * @param {Event} event
     * @event enhance.telia.livechat
     */
    $(document).on('enhance.telia.livechat', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]();
    });

    /**
     * @param {Event} event
     * @event destroy.telia.livechat
     */
    $(document).on('destroy.telia.livechat', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]('destroy');
    });
})(jQuery, window, document);

/**
 * Auto-init the plugin on DOM ready.
 * @fires enhance.telia.livechat
 */
$(function() {
    $(document).trigger('enhance.telia.livechat');
});

/**
 * Show overlay.
 *
 * @function showOverlay
 * @param {string} container - Container selector.
 * @param {string} classes - List of overlay classes.
 * @param {boolean} loader - Enable loader.
 * @param {string} loaderText - Loader text.
 * @returns {JQuery|HTMLElement} Overlay element.
 */
function showOverlay(container, classes, loader, loaderText) {
    if (!container) {
        container = 'body';
    }

    var overlay = $('<div class="overlay ' + classes + '"></div>');

    if (loader) {
        var spinner = $('<div class="spinner overlay__spinner"><div class="spinner__loader"></div></div>');

        if (loaderText) {
            spinner.append('<div class="spinner__text">' + loaderText + '</div>');
        }

        overlay.append(spinner);
    }

    $(container).append(overlay);

    return overlay;
}

/**
 * Hide overlay.
 *
 * @function hideOverlay
 * @param {jQuery|HTMLElement} overlay - JQuery element.
 * @returns {void}
 */
function hideOverlay(overlay) {
    if (overlay) {
        overlay.remove();
    } else {
        $('body > .overlay').remove();
    }
}

window.showOverlay = showOverlay;
window.hideOverlay = hideOverlay;

/**
 * Start loader.
 *
 * @param  {string} container - Container selector.
 * @param  {string} classes - List of classes for overlay.
 * @param  {string} loaderText - Loader text.
 * @returns {JQuery|HTMLElement} Overlay element.
 */
function startLoad(container, classes, loaderText) {
    if (!classes && !container) {
        classes = 'overlay--fixed';
    }

    return window.showOverlay(container, classes, true, loaderText);
}

/**
 * End loader.
 *
 * @param  {JQuery} overlay - Overlay element.
 * @returns {void}
 */
function endLoad(overlay) {
    window.hideOverlay(overlay);
}

window.startLoad = startLoad;
window.endLoad = endLoad;

+function($) {
    'use strict';

    /**
     * CollapseLayer.
     *
     * @class
     * @param {HTMLElement} element - The HTML element the CollapseLayer should be bound to.
     * @param {Object} options - An option map.
     */
    function CollapseLayer(element, options) {
        this.$element       = $(element);
        this.options        = $.extend({}, CollapseLayer.DEFAULTS, options);
        this.id             = element.id;
        this.$selector       = $('[data-toggle="collapse-layer"][href="#' + element.id + '"], [data-toggle="collapse-layer"][data-target="#' + element.id + '"]');
        this.$trigger = this.$selector.length ? this.$selector : $(this.options.trigger);
        this.$contentWrapper = $('<div class="grid__col--xs-12 collapse-dropdown" />');
        this.transitioning = null;
        this.$collapseLayer = null;
        this.$isOpen = false;
        this.$arrow = $('<div class="collapse__arrow"/>');
        this.$close = $('<a href="#" data-hide="collapse-layer" class="collapse__close">' + telia.icon.render('#close-round') + '</a>');
        this.$parent = this.getParent();
        this.$collapseLayer = this.getCollapseLayer();
        this.$scrollable = this.$contentWrapper;
        this.$windowWidth = $(window).width();
        this.$collapseLayerResizeTimer = null;

        if (this.options.toggle) this.toggle();
    };

    if (!$.fn.collapse) throw new Error('CollapseLayer requires collapse.js');

    CollapseLayer.VERSION = '0.0.2';

    CollapseLayer.DEFAULTS = $.extend({}, $.fn.collapse.Constructor.DEFAULTS, {
        closeButton: true
    });

    // NOTE: COLLAPSE_LAYER EXTENDS collapse.js
    // =========================================

    CollapseLayer.prototype = $.extend({}, $.fn.collapse.Constructor.prototype);

    // For easier access to parent class methods
    CollapseLayer.prototype.super = $.extend({}, $.fn.collapse.Constructor.prototype);

    CollapseLayer.constructor = CollapseLayer;

    /**
     * Toggle collapse-layer.
     *
     * @function toggle
     * @returns {void}
     */
    CollapseLayer.prototype.toggle = function() {
        this[(this.$element.hasClass('is-visible') || this.$element.hasClass('collapse--visible')) ? 'hide' : 'show']();
    };

    /**
     * Show collapse-layer.
     *
     * @function show
     * @returns {void}
     */
    CollapseLayer.prototype.show = function() {
        if (this.transitioning || (this.$element.hasClass('is-visible') && this.$element.hasClass('collapse--visible'))) {
            this.super.show.call(this);

            return;
        }

        // Close open collapse-layers in the group
        this.$parent.find('.collapse').filter(function() {
            return typeof $(this).data('bs.collapseLayer') == 'object';
        }).collapseLayer('hide', true);

        // find EOL
        this.getContentWrapper();
        // Relocate content
        this.$contentWrapper.append(this.$element.detach());

        // Open
        this.$contentWrapper.removeClass('hidden').addClass('collapse-dropdown--visible');
        this.$collapseLayer.addClass('collapse-layer--current');
        this.$isOpen = true;
        this.super.show.call(this);
    };

    /**
     * Handle window resize.
     *
     * @function resizeHandler
     * @returns {void}
     */
    CollapseLayer.prototype.resizeHandler = function() {
        var currentWidth = $(window).width();

        if (this.$windowWidth != currentWidth) {
            if (this.$collapseLayerResizeTimer) {
                clearTimeout(this.$collapseLayerResizeTimer);
            }
            this.super.resizeHandler.call(this);
            var that = this;

            this.$collapseLayerResizeTimer = setTimeout(function() {
                if (that.$isOpen) {
                    that.$parent.append(that.$contentWrapper.detach());
                    that.getContentWrapper();
                }
            }, 500);
            this.$windowWidth = currentWidth;
        }
    };

    /**
     * Hide collapse-layer.
     *
     * @function hide
     * @param {boolean} keepContentWrapperVisible - Is another collapse-layer being opened.
     * @returns {void}
     */
    CollapseLayer.prototype.hide = function(keepContentWrapperVisible) {
        /**
         * Add class hidden if collapse-layer is closed.
         *
         * @returns {void}
         */
        function completed() {
            this.$contentWrapper.addClass('hidden');
        }

        if (this.$isOpen) {
            this.super.hide.call(this);
            this.$contentWrapper.removeClass('collapse-dropdown--visible');
            this.$collapseLayer.removeClass('collapse-layer--current');
            this.$isOpen = false;

            if (!$.support.transition) return completed.call(this);

            // if another collapse-layer will be opened, do not call complete function
            if (!keepContentWrapperVisible) {
                this.$element.one('hidden.bs.collapse', $.proxy(completed, this));
            }
        }
    };

    /**
     * Get the closest element that has class `collapse-layer`.
     *
     * @function getCollapseLayer
     * @returns {JQuery} Element that has class `collapse-layer`.
     */
    CollapseLayer.prototype.getCollapseLayer = function() {
        return this.$element.closest('.collapse-layer');
    };

    /**
     * Get wrapper element.
     *
     * @function getContentWrapper
     * @returns {void}
     */
    CollapseLayer.prototype.getContentWrapper = function() {
        var _tmp = this.getLastInRow();
        var next = _tmp.next();
        var parcnt = this.$parent.find('.collapse-dropdown');

        if (parcnt.length > 0) {
            this.$contentWrapper = parcnt;
            this.$scrollable = this.$contentWrapper;
        }

        if (!next.hasClass('collapse-dropdown')) {
            _tmp.after(this.$contentWrapper);
        }
    };

    /**
     * Get last element in the row containing the current collapse-layer.
     *
     * @function getLastInRow
     * @returns {JQuery} The last element in the current row.
     */
    CollapseLayer.prototype.getLastInRow = function() {
        var _tmp;
        var comp;

        _tmp = comp = this.$collapseLayer;
        var isReverse = window.getComputedStyle(this.$parent[0], null).getPropertyValue('flex-direction') === 'row-reverse';

        switch (isReverse) {
            case true:
                while (comp.position().left > nextLeft(_tmp, true)) {
                    _tmp = _tmp.next();
                }
                break;
            default:
                while (comp.position().left < nextLeft(_tmp)) {
                    _tmp = _tmp.next();
                }
        }

        /**
         * Check if last element has bigger/smaller position left.
         *
         * @param {JQuery} t - JQuery element from who next element is taken.
         * @param {boolean} isReverse - Which way flex grid is.
         * @returns {number} Position from left.
         */
        function nextLeft(t, isReverse) {
            var nxt;

            try {
                nxt = t.next();
                if (nxt.is(':visible')) {
                    return nxt.position().left;
                } else if (!isReverse) {
                    return nextLeft(nxt, isReverse);
                }
            } catch (e) {
                return null;
            }
        }

        return _tmp;
    };

    /**
     * Get parent element.
     *
     * @function getParent
     * @returns {JQuery} Parent element.
     */
    CollapseLayer.prototype.getParent = function() {
        if (!this.options.parent || this.options.parent === '.grid') {
            var closestGrid = this.$element.closest('.grid');
            var _id = '';

            if (closestGrid.attr('id') && closestGrid.attr('id').length > 0) {
                _id = closestGrid.attr('id');
            } else {
                _id = 'collapseLayerGroup-' + new Date().getTime();
                closestGrid.attr('id', _id);
            }

            this.options.parent = '#' + _id;
        }

        return $(this.options.parent)
            .find('[data-toggle="collapse-layer"][data-parent="' + this.options.parent + '"]')
            .each($.proxy(function(i, element) {
                var $element = $(element);

                this.addAriaAndCollapsedClass(getTargetFromTrigger($element), $element);
            }, this))
            .end();
    };

    /**
     * Get target from trigger.
     *
     * @param  {JQuery} $trigger - Trigger element.
     * @returns {JQuery} Target element.
     */
    function getTargetFromTrigger($trigger) {
        var href;
        // strip for ie7
        var target = $trigger.attr('data-target') || (href = $trigger.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '');

        return $(target);
    }

    /**
     * Create plugin.
     *
     * @param {Object} option - Extended options.
     * @param {Array[]} args - Extended arguments.
     * @returns {Array<CollapseLayer>} Array of CollapseLayers.
     */
    function Plugin(option, args) {
        return this.each(function() {
            var $this = $(this);
            var data = $this.data('bs.collapseLayer');
            var options = $.extend({}, CollapseLayer.DEFAULTS, $this.data(), typeof option == 'object' && option);

            if (!data && options.toggle && (/show|hide/).test(option)) options.toggle = false;
            if (!data) $this.data('bs.collapseLayer', (data = new CollapseLayer(this, options)));
            if (typeof option == 'string') data[option](args);
        });
    }

    var old = $.fn.collapseLayer;

    $.fn.collapseLayer = Plugin;
    $.fn.collapseLayer.Conditional = CollapseLayer;

    // COLLAPSE-LAYER NO CONFLICT
    // ==========================

    $.fn.collapseLayer.noConflict = function() {
        $.fn.collapseLayer = old;

        return this;
    };

    // COLLAPSE DATA-API
    // =================

    /**
     * @param {Event} e
     * @event click.bs.collapseLayer.data-api
     */
    $(document).on('click.bs.collapseLayer.data-api', '[data-toggle="collapse-layer"]', function(e) {
        var $this   = $(this);

        if (!$this.attr('data-target')) e.preventDefault();
        var $target = getTargetFromTrigger($this);
        var data = $target.data('bs.collapseLayer');
        var extraOptions = {
            trigger: this
        };

        if (!$this.attr('data-target')) {
            // strip for ie7
            extraOptions.target = $this.attr('href').replace(/.*(?=#[^\s]+$)/, '');
            e.preventDefault();
        }

        var option = data ? 'toggle' : $.extend({}, $this.data(), extraOptions);

        Plugin.call($target, option);
    });

    /**
     * @param {Event} e
     * @event click.bs.collapseLayer.data-api
     */
    $(document).on('click.bs.collapseLayer.data-api', '[data-show="collapse-layer"]', function(e) {
        var $this   = $(this);

        if (!$this.attr('data-target')) e.preventDefault();
        var $target = getTargetFromTrigger($this);
        var extraOptions = {
            trigger: this
        };

        if (!$this.attr('data-target')) {
            // strip for ie7
            extraOptions.target = $this.attr('href').replace(/.*(?=#[^\s]+$)/, '');
            e.preventDefault();
        }

        var option = 'show';

        Plugin.call($target, option);
    });

    /**
     * @param {Event} e
     * @event click.bs.collapseLayer.data-api
     */
    $(document).on('click.bs.collapseLayer.data-api', '[data-hide="collapse-layer"]', function(e) {
        var $this   = $(this);

        if (!$this.attr('data-target')) e.preventDefault();
        var $target = getTargetFromTrigger($this);
        var extraOptions = {
            trigger: this
        };

        if (!$this.attr('data-target')) {
            // strip for ie7
            extraOptions.target = $this.attr('href').replace(/.*(?=#[^\s]+$)/, '');
            e.preventDefault();
        }

        var option = 'hide';

        Plugin.call($target, option);
    });
}(jQuery);

/**
 * Scroll to element.
 *
 * @function autoScroll
 * @param {HTMLElement} element - Element to scroll to.
 * @param {number} speed - Scrolling speed.
 * @param {number} offset - Offset from element.
 * @returns {void}
 */
function autoScroll(element, speed, offset) {
    if (!element || !element.length) {
        return;
    }

    var $parent = $('html, body');
    var navHeight = 50;
    var newLocation = element.offset().top;
    var currentLocation = window.pageYOffset;
    var toTravel = (currentLocation < newLocation) ? (currentLocation - newLocation) * -1 : ((currentLocation - newLocation) + offset) * -1;

    if (element.closest('.modal-container').length > 0) {
        $parent = element.closest('.modal-container');
    }

    $parent.stop().animate({
        scrollTop: (currentLocation + toTravel) - offset - navHeight
    }, speed, 'swing');
};
window.autoScroll = autoScroll;

/**
 * Bind click event to [data-autoscroll] elements
 * @fires autoScroll
 */
$(function() {
    $(document).on('click', '[data-autoscroll]', function(event) {
        event.preventDefault();
        var self = $(this);
        var target = self.attr('href');
        var speed = self.data('speed') || 500;
        var offset = self.data('offset') || 50;

        if (!target) {
            target = self.attr('data-autoscroll');
        }

        autoScroll($(target), speed, offset);
    });
});

;(function($, window, undefined) {
    /** Default settings */
    var defaults = {
        active: null,
        event: 'click',
        disabled: [],
        collapsible: 'accordion',
        startCollapsed: 'accordion',
        rotate: false,
        setHash: false,
        animation: 'default',
        animationQueue: false,
        duration: 350,
        scrollToAccordion: true,
        scrollToAccordionOffset: 0,
        scrollToAccordionSpeed: 500,
        accordionTabElement: '<div></div>',

        /**
         * Callback called after activation.
         *
         * @returns {void}
         */
        activate: function() {},

        /**
         * Callback called after deactivation.
         *
         * @returns {void}
         */
        deactivate: function() {},

        /**
         * Callback called after load.
         *
         * @returns {void}
         */
        load: function() {},

        /**
         * Callback called after activateState.
         *
         * @returns {void}
         */
        activateState: function() {},
        classes: {
            stateDefault: '',
            stateActive: 'tabs__item--current',
            stateDisabled: '',
            stateExcluded: '',
            container: 'tabs',
            ul: 'tabs__nav',
            tab: 'tabs__item',
            tabEmployee: 'tabs__item--employee',
            anchor: 'tabs__link',
            panel: 'tabs__content-block',
            panelActive: 'tabs__content-block--current',
            accordionTitle: 'tabs__accordion-title',
            accordionActive: 'tabs__accordion-title--current',
            accordionEmployee: 'tabs__accordion-title--employee',
            accordionLink: 'tabs__accordion-link'
        }
    };

    /**
     * Responsive Tabs.
     *
     * @class
     * @param {Object} element - The HTML element the validator should be bound to.
     * @param {Object} options - An option map.
     */
    function ResponsiveTabs(element, options) {
        // Selected DOM element
        this.element = element;
        // Selected jQuery element
        this.$element = $(element);
        // Create tabs array
        this.tabs = [];
        // Define the plugin state (tabs/accordion)
        this.state = '';
        // Define rotate interval
        this.rotateInterval = 0;
        this.$queue = $({});

        // Extend the defaults with the passed options
        this.options = $.extend({}, defaults, options);

        this.init();
    }

    /**
     * This function initializes the tab plugin.
     *
     * @function init
     * @returns {void}
     */
    ResponsiveTabs.prototype.init = function() {
        var _this = this;

        // Load all the elements
        this.tabs = this._loadElements();
        this._loadClasses();
        this._loadEvents();

        // Window resize bind to check state
        $(window).on('resize', function(e) {
            _this._setState(e);
        });

        // Hashchange event
        $(window).on('hashchange', function(e) {
            var tabRef = _this._getTabRefBySelector(window.location.hash);
            var oTab = _this._getTab(tabRef);

            // Check if a tab is found that matches the hash
            if (tabRef >= 0 && !oTab._ignoreHashChange && !oTab.disabled) {
                // If so, open the tab and auto close the current one
                _this._openTab(e, _this._getTab(tabRef), true);
            }
        });

        // Start rotate event if rotate option is defined
        if (this.options.rotate !== false) {
            this.startRotation();
        }

        // --------------------
        // Define plugin events
        //

        // Activate: this event is called when a tab is selected
        this.$element.bind('tabs-activate', function(e, oTab) {
            _this.options.activate.call(this, e, oTab);
        });
        // Deactivate: this event is called when a tab is closed
        this.$element.bind('tabs-deactivate', function(e, oTab) {
            _this.options.deactivate.call(this, e, oTab);
        });
        // Activate State: this event is called when the plugin switches states
        this.$element.bind('tabs-activate-state', function(e, state) {
            _this.options.activateState.call(this, e, state);
        });

        // Load: this event is called when the plugin has been loaded
        this.$element.bind('tabs-load', function(e) {
            var startTab;

            // Set state
            _this._setState(e);

            // Check if the panel should be collaped on load
            if (_this.options.startCollapsed !== true && !(_this.options.startCollapsed === 'accordion' && _this.state === 'accordion')) {
                startTab = _this._getStartTab();

                // Open the initial tab
                _this._openTab(e, startTab);

                // Call the callback function
                _this.options.load.call(this, e, startTab);
            }
            if (typeof hashTrigger == 'function') {
                window.hashTrigger();
            }
        });
        // Trigger loaded event
        this.$element.trigger('tabs-load');
    };

    //
    // PRIVATE FUNCTIONS
    //

    /**
     * This function loads the tab elements and stores them in an array.
     *
     * @function _loadElements
     * @returns {Array} Array of tab elements.
     * @private
     */
    ResponsiveTabs.prototype._loadElements = function() {
        var _this = this;
        var $ul = this.$element.children('ul');
        var tabs = [];
        var id = 0;

        // Add the classes to the basic html elements
        this.$element.addClass(_this.options.classes.container);
        $ul.addClass(_this.options.classes.ul);

        // Get tab buttons and store their data in an array
        $('li', $ul).each(function() {
            var $tab = $(this);
            var isExcluded = $tab.hasClass(_this.options.classes.stateExcluded);
            var $anchor;
            var $panel;
            var $accordionTab;
            var $accordionAnchor;
            var panelSelector;

            // Check if the tab should be excluded
            if (!isExcluded) {
                $anchor = $('a', $tab);
                panelSelector = $anchor.attr('href');
                $panel = $(panelSelector);
                $accordionTab = $(_this.options.accordionTabElement).insertBefore($panel);
                $accordionAnchor = $('<a></a>');
                $accordionAnchor.attr('href', panelSelector).html($anchor.html());
                if ($(_this.$element[0]).hasClass('tabs--background-light')) {
                    var iconPlus = '#plus';
                    var iconMinus = '#minus';

                    $(telia.icon.render(iconPlus, 'tabs__icon tabs__icon--plus') + telia.icon.render(iconMinus, 'tabs__icon tabs__icon--minus')).prependTo($accordionAnchor);
                } else {
                    var icon = '#arrow-down';

                    $(telia.icon.render(icon, 'tabs__icon')).prependTo($accordionAnchor);
                }                $accordionAnchor.appendTo($accordionTab);

                var oTab = {
                    _ignoreHashChange: false,
                    id: id,
                    disabled: ($.inArray(id, _this.options.disabled) !== -1),
                    tab: $(this),
                    anchor: $('a', $tab),
                    panel: $panel,
                    selector: panelSelector,
                    accordionTab: $accordionTab,
                    accordionAnchor: $accordionAnchor,
                    active: false
                };

                // 1up the ID
                id++;
                // Add to tab array
                tabs.push(oTab);
            }
        });

        return tabs;
    };

    /**
     * This function adds classes to the tab elements based on the options.
     *
     * @function _loadClasses
     * @private
     * @returns {void}
     */
    ResponsiveTabs.prototype._loadClasses = function() {
        for (var i = 0; i < this.tabs.length; i++) {
            this.tabs[i].tab.addClass(this.options.classes.stateDefault).addClass(this.options.classes.tab);
            this.tabs[i].anchor.addClass(this.options.classes.anchor);
            this.tabs[i].panel.addClass(this.options.classes.stateDefault).addClass(this.options.classes.panel);
            this.tabs[i].accordionTab.addClass(this.options.classes.accordionTitle);
            if (this.tabs[i].tab.hasClass(this.options.classes.tabEmployee)) {
                this.tabs[i].accordionTab.addClass(this.options.classes.accordionEmployee);
            }
            this.tabs[i].accordionAnchor.addClass(this.options.classes.accordionLink);
            if (this.tabs[i].disabled) {
                this.tabs[i].tab.removeClass(this.options.classes.stateDefault).addClass(this.options.classes.stateDisabled);
                this.tabs[i].accordionTab.removeClass(this.options.classes.stateDefault).addClass(this.options.classes.stateDisabled);
            }
        }
    };

    /**
     * This function adds events to the tab elements.
     *
     * @function _loadEvents
     * @private
     * @returns {void}
     */
    ResponsiveTabs.prototype._loadEvents = function() {
        var _this = this;

        /**
         * Define activate event on a tab element.
         *
         * @param {Event} e - Event.
         * @returns {void}
         */
        function fActivate(e) {
            // Fetch current tab
            var current = _this._getCurrentTab();
            var activatedTab = e.data.tab;

            e.preventDefault();

            // Make sure this tab isn't disabled
            if (!activatedTab.disabled) {
                // Check if hash has to be set in the URL location
                if (_this.options.setHash) {
                    // Set the hash using the history api if available to tackle Chromes repaint bug on hash change
                    if (history.pushState) {
                        history.pushState(null, null, activatedTab.selector);
                    } else {
                        // Otherwise fallback to the hash update for sites that don't support the history api
                        window.location.hash = activatedTab.selector;
                    }
                }

                e.data.tab._ignoreHashChange = true;

                // Check if the activated tab isnt the current one or if its collapsible. If not, do nothing
                if (current !== activatedTab || _this._isCollapisble()) {
                    // The activated tab is either another tab of the current one. If it's the current tab it is collapsible
                    // Either way, the current tab can be closed
                    _this._closeTab(e, current);

                    // Check if the activated tab isnt the current one or if it isnt collapsible
                    if (current !== activatedTab || !_this._isCollapisble()) {
                        _this._openTab(e, activatedTab, false, true);
                    }
                }
            }
        };

        // Loop tabs
        for (var i = 0; i < this.tabs.length; i++) {
            // Add activate function to the tab and accordion selection element
            this.tabs[i].anchor.on(_this.options.event, {
                tab: _this.tabs[i]
            }, fActivate);
            this.tabs[i].accordionAnchor.on(_this.options.event, {
                tab: _this.tabs[i]
            }, fActivate);
        }
    };

    /**
     * This function gets the tab that should be opened at start.
     *
     * @function _getStartTab
     * @returns {Object} Tab object.
     * @private
     */
    ResponsiveTabs.prototype._getStartTab = function() {
        var tabRef = this._getTabRefBySelector('');
        var $currentTab = this.$element.find('.tabs__item--current');
        var startTab;

        // Check if the page has a hash set that is linked to a tab
        if (tabRef >= 0 && !this._getTab(tabRef).disabled) {
            // If so, set the current tab to the linked tab
            startTab = this._getTab(tabRef);
        } else if (this.options.active > 0 && !this._getTab(this.options.active).disabled) {
            startTab = this._getTab(this.options.active);
        } else if ($currentTab.length > 0) {
            var tabRefByClass = $currentTab.find('.tabs__link').attr('href');
            var tabRefSelected = this._getTabRefBySelector(tabRefByClass);

            startTab = this._getTab(tabRefSelected);
        } else {
            // If not, just get the first one
            startTab = this._getTab(0);
        }

        return startTab;
    };

    /**
     * This function sets the current state of the plugin.
     *
     * @function _setState
     * @param {Event} e - The event that triggers the state change.
     * @private
     * @returns {void}
     */
    ResponsiveTabs.prototype._setState = function(e) {
        var $ul = $('.' + this.options.classes.ul, this.$element);
        var oldState = this.state;
        var startCollapsedIsState = (typeof this.options.startCollapsed === 'string');
        var startTab;

        // The state is based on the visibility of the tabs list
        if ($ul.is(':visible')) {
            // Tab list is visible, so the state is 'tabs'
            this.state = 'tabs';
        } else {
            // Tab list is invisible, so the state is 'accordion'
            this.state = 'accordion';
        }

        // If the new state is different from the old state
        if (this.state !== oldState) {
            // If so, the state activate trigger must be called
            this.$element.trigger('tabs-activate-state', {
                oldState: oldState,
                newState: this.state
            });

            // Check if the state switch should open a tab
            if (oldState && startCollapsedIsState && this.options.startCollapsed !== this.state && this._getCurrentTab() === undefined) {
                // Get initial tab
                startTab = this._getStartTab(e);
                // Open the initial tab
                this._openTab(e, startTab);
            } else if (this._getCurrentTab() === undefined) {
                // Remove active classes only from parent tabs
                this.$element.find('.' + this.options.classes.panel).not('.' + this.options.classes.panel + ' .' + this.options.classes.panel).removeClass(this.options.classes.panelActive);
            }

            this.$element.find('.' + this.options.classes.panel).not('.' + this.options.classes.panel + ' .' + this.options.classes.panel).removeAttr('style');
        }
    };

    /**
     * This function opens a tab.
     *
     * @function _openTab
     * @param {Event} e - The event that triggers the tab opening.
     * @param {Object} oTab - The tab object that should be opened.
     * @param {boolean} closeCurrent - Defines if the current tab should be closed.
     * @param {boolean} stopRotation - Defines if the tab rotation loop should be stopped.
     * @private
     * @returns {void}
     */
    ResponsiveTabs.prototype._openTab = function(e, oTab, closeCurrent, stopRotation) {
        if (oTab === undefined) return;

        var _this = this;

        // Check if the current tab has to be closed
        if (closeCurrent) {
            this._closeTab(e, this._getCurrentTab());
        }

        // Check if the rotation has to be stopped when activated
        if (stopRotation && this.rotateInterval > 0) {
            this.stopRotation();
        }

        // Set this tab to active
        oTab.active = true;
        // Set active classes to the tab button and accordion tab button
        oTab.tab.removeClass(_this.options.classes.stateDefault).addClass(_this.options.classes.stateActive);
        oTab.accordionTab.removeClass(_this.options.classes.stateDefault).addClass(_this.options.classes.accordionActive);

        // Run panel transiton
        _this._doTransition(oTab.panel, _this.options.animation, 'open', function() {
            // When finished, set active class to the panel
            if (_this.state == 'accordion') {
                oTab.panel.addClass(_this.options.classes.panelActive).hide().stop().slideDown(_this.options.duration, function() {
                    oTab.panel.removeClass(_this.options.classes.stateDefault).removeAttr('style');
                });
            } else {
                oTab.panel.removeClass(_this.options.classes.stateDefault).addClass(_this.options.classes.panelActive);
            }

            // And if enabled and state is accordion, scroll to the accordion tab
            if (_this.getState() === 'accordion' && _this.options.scrollToAccordion && (!_this._isInView(oTab.accordionTab) || _this.options.animation !== 'default')) {
                window.autoScroll(oTab.accordionTab, _this.options.scrollToAccordionSpeed, _this.options.scrollToAccordionOffset);
            }
        });

        this.$element.trigger('tabs-activate', oTab);
        $(document).trigger('refresh.telia', oTab.panel[0]);
    };

    /**
     * This function closes a tab.
     *
     * @function _closeTab
     * @param {Event} e - The event that is triggered when a tab is closed.
     * @param {Object} oTab - The tab object that should be closed.
     * @private
     * @returns {void}
     */
    ResponsiveTabs.prototype._closeTab = function(e, oTab) {
        var _this = this;
        var doQueueOnState = typeof _this.options.animationQueue === 'string';
        var doQueue;

        if (oTab !== undefined) {
            if (doQueueOnState && _this.getState() === _this.options.animationQueue) {
                doQueue = true;
            } else if (doQueueOnState) {
                doQueue = false;
            } else {
                doQueue = _this.options.animationQueue;
            }

            // Deactivate tab
            oTab.active = false;
            // Set default class to the tab button
            oTab.tab.removeClass(_this.options.classes.stateActive).addClass(_this.options.classes.stateDefault);

            // Run panel transition
            _this._doTransition(oTab.panel, _this.options.animation, 'close', function() {
                // Set default class to the accordion tab button and tab panel
                oTab.accordionTab.removeClass(_this.options.classes.accordionActive).addClass(_this.options.classes.stateDefault);

                if (_this.state == 'accordion') {
                    oTab.panel.stop().slideUp(_this.options.duration, function() {
                        oTab.panel.removeClass(_this.options.classes.panelActive).addClass(_this.options.classes.stateDefault).removeAttr('style');
                    });
                } else {
                    oTab.panel.removeClass(_this.options.classes.panelActive).addClass(_this.options.classes.stateDefault);
                }
            }, !doQueue);

            this.$element.trigger('tabs-deactivate', oTab);
            $(document).trigger('reset.telia', oTab.panel[0]);
        }
    };

    /**
     * This function runs an effect on a panel.
     *
     * @function _doTransition
     * @param {Element} panel - The HTML element of the tab panel.
     * @param {string} method - The transition method reference.
     * @param {string} state - The state (open/closed) that the panel should transition to.
     * @param {Function} callback - The callback function that is called after the transition.
     * @param {boolean} dequeue - Defines if the event queue should be dequeued after the transition.
     * @private
     * @returns {void}
     */
    ResponsiveTabs.prototype._doTransition = function(panel, method, state, callback, dequeue) {
        // Add the transition to a custom queue
        this.$queue.queue('responsive-tabs', function() {
            // Run the transition on the panel
            callback.call(panel, method, state);
        });

        // When the panel is openend, dequeue everything so the animation starts
        if (state === 'open' || dequeue) {
            this.$queue.dequeue('responsive-tabs');
        }
    };

    /**
     * This function returns the collapsibility of the tab in this state.
     *
     * @function _isCollapisble
     * @returns {boolean} The collapsibility of the tab.
     * @private
     */
    ResponsiveTabs.prototype._isCollapisble = function() {
        return (typeof this.options.collapsible === 'boolean' && this.options.collapsible) || (typeof this.options.collapsible === 'string' && this.options.collapsible === this.getState());
    };

    /**
     * This function returns a tab by numeric reference.
     *
     * @function _getTab
     * @param {Integer} numRef - Numeric tab reference.
     * @returns {Object} Tab object.
     * @private
     */
    ResponsiveTabs.prototype._getTab = function(numRef) {
        return this.tabs[numRef];
    };

    /**
     * This function returns the numeric tab reference based on a hash selector.
     *
     * @function _getTabRefBySelector
     * @param {string} selector - Hash selector.
     * @returns {Integer} Numeric tab reference.
     * @private
     */
    ResponsiveTabs.prototype._getTabRefBySelector = function(selector) {
        // Loop all tabs
        for (var i = 0; i < this.tabs.length; i++) {
            // Check if the hash selector is equal to the tab selector
            if (this.tabs[i].selector === selector) {
                return i;
            }
        }

        // If none is found return a negative index
        return -1;
    };

    /**
     * This function returns the current tab element.
     *
     * @function _getCurrentTab
     * @returns {Object} Current tab element.
     * @private
     */
    ResponsiveTabs.prototype._getCurrentTab = function() {
        return this._getTab(this._getCurrentTabRef());
    };

    /**
     * This function returns the next tab's numeric reference.
     *
     * @function _getNextTabRef
     * @param {Integer} currentTabRef - Current numeric tab reference.
     * @returns {Integer} Numeric tab reference.
     * @private
     */
    ResponsiveTabs.prototype._getNextTabRef = function(currentTabRef) {
        var tabRef = (currentTabRef || this._getCurrentTabRef());
        var nextTabRef = (tabRef === this.tabs.length - 1) ? 0 : tabRef + 1;

        return (this._getTab(nextTabRef).disabled) ? this._getNextTabRef(nextTabRef) : nextTabRef;
    };

    /**
     * This function returns the previous tab's numeric reference.
     *
     * @function _getPreviousTabRef
     * @returns {Integer} Numeric tab reference.
     * @private
     */
    ResponsiveTabs.prototype._getPreviousTabRef = function() {
        return (this._getCurrentTabRef() === 0) ? this.tabs.length - 1 : this._getCurrentTabRef() - 1;
    };

    /**
     * This function returns the current tab's numeric reference.
     *
     * @function _getCurrentTabRef
     * @returns {Integer} Numeric tab reference.
     * @private
     */
    ResponsiveTabs.prototype._getCurrentTabRef = function() {
        // Loop all tabs
        for (var i = 0; i < this.tabs.length; i++) {
            // If this tab is active, return it
            if (this.tabs[i].active) {
                return i;
            }
        }

        // No tabs have been found, return negative index
        return -1;
    };

    //
    // HELPER FUNCTIONS
    //

    /**
     * Check if is in view.
     *
     * @function _isInView
     * @param  {Object} $element - Tabs element.
     * @returns {boolean} If element is in view.
     * @private
     */
    ResponsiveTabs.prototype._isInView = function($element) {
        var docViewTop = $(window).scrollTop();
        var docViewBottom = docViewTop + $(window).height();
        var elemTop = $element.offset().top;
        var elemBottom = elemTop + $element.height();

        return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
    };

    //
    // PUBLIC FUNCTIONS
    //

    /**
     * This function activates a tab.
     *
     * @function activate
     * @param {Integer} tabRef - Numeric tab reference.
     * @param {boolean} stopRotation - Defines if the tab rotation should stop after activation.
     * @public
     * @returns {void}
     */
    ResponsiveTabs.prototype.activate = function(tabRef, stopRotation) {
        var e = jQuery.Event('tabs-activate');
        var oTab = this._getTab(tabRef);

        if (!oTab.disabled) {
            this._openTab(e, oTab, true, stopRotation || true);
        }
    };

    /**
     * This function deactivates a tab.
     *
     * @function deactivate
     * @param {Integer} tabRef - Numeric tab reference.
     * @public
     * @returns {void}
     */
    ResponsiveTabs.prototype.deactivate = function(tabRef) {
        var e = jQuery.Event('tabs-dectivate');
        var oTab = this._getTab(tabRef);

        if (!oTab.disabled) {
            this._closeTab(e, oTab);
        }
    };

    /**
     * This function enables a tab.
     *
     * @function enable
     * @param {Integer} tabRef - Numeric tab reference.
     * @public
     * @returns {void}
     */
    ResponsiveTabs.prototype.enable = function(tabRef) {
        var oTab = this._getTab(tabRef);

        if (oTab) {
            oTab.disabled = false;
            oTab.tab.addClass(this.options.classes.stateDefault).removeClass(this.options.classes.stateDisabled);
            oTab.accordionTab.addClass(this.options.classes.stateDefault).removeClass(this.options.classes.stateDisabled);
        }
    };

    /**
     * This function disable a tab.
     *
     * @function disable
     * @param {Integer} tabRef - Numeric tab reference.
     * @public
     * @returns {void}
     */
    ResponsiveTabs.prototype.disable = function(tabRef) {
        var oTab = this._getTab(tabRef);

        if (oTab) {
            oTab.disabled = true;
            oTab.tab.removeClass(this.options.classes.stateDefault).addClass(this.options.classes.stateDisabled);
            oTab.accordionTab.removeClass(this.options.classes.stateDefault).addClass(this.options.classes.stateDisabled);
        }
    };

    /**
     * This function gets the current state of the plugin.
     *
     * @function getState
     * @returns {string} State of the plugin.
     * @public
     */
    ResponsiveTabs.prototype.getState = function() {
        return this.state;
    };

    /**
     * This function starts the rotation of the tabs.
     *
     * @function startRotation
     * @param {Integer} speed - The speed of the rotation.
     * @public
     * @returns {void}
     */
    ResponsiveTabs.prototype.startRotation = function(speed) {
        var _this = this;
        // Make sure not all tabs are disabled

        if (this.tabs.length > this.options.disabled.length) {
            this.rotateInterval = setInterval(function() {
                var e = jQuery.Event('rotate');

                _this._openTab(e, _this._getTab(_this._getNextTabRef()), true);
            }, speed || (($.isNumeric(_this.options.rotate)) ? _this.options.rotate : 4000));
        } else {
            throw new Error('Rotation is not possible if all tabs are disabled');
        }
    };

    /**
     * This function stops the rotation of the tabs.
     *
     * @function stopRotation
     * @public
     * @returns {void}
     */
    ResponsiveTabs.prototype.stopRotation = function() {
        window.clearInterval(this.rotateInterval);
        this.rotateInterval = 0;
    };

    /**
     * This function can be used to get/set options.
     *
     * @function option
     * @param {number} key - Key of option.
     * @param {number} value - Value of option.
     * @returns {any} Option value.
     * @public
     */
    ResponsiveTabs.prototype.option = function(key, value) {
        if (value) {
            this.options[key] = value;
        }

        return this.options[key];
    };

    /**
     * Create jQuery plugin.
     *
     * @param {Object} options - Extended options.
     * @returns {Array<ResponsiveTabs>} Array of ResponsiveTabs.
     */
    $.fn.responsiveTabs = function(options) {
        var args = arguments;
        var instance;

        if (options === undefined || typeof options === 'object') {
            return this.each(function() {
                if (!$.data(this, 'responsivetabs')) {
                    $.data(this, 'responsivetabs', new ResponsiveTabs(this, options));
                }
            });
        } else if (typeof options === 'string' && options[0] !== '_' && options !== 'init') {
            instance = $.data(this[0], 'responsivetabs');

            // Allow instances to be destroyed via the 'destroy' method
            if (options === 'destroy') {
                // TODO: destroy instance classes, etc
                $.data(this, 'responsivetabs', null);
            }

            if (instance instanceof ResponsiveTabs && typeof instance[options] === 'function') {
                return instance[options].apply(instance, Array.prototype.slice.call(args, 1));
            } else {
                return this;
            }
        }
    };
}(jQuery, window));


;(function($, window, document) {
    var pluginName = 'teliaTabs';
    var defaults = {};
    var initSelector = '.tabs';

    /**
     * Tabs.
     *
     * @class
     * @param {HTMLElement} element - The HTML element the Tabs should be bound to.
     * @param {Object} options - An option map.
     */
    function Tabs(element, options) {
        this.element = element;
        this.$element = $(this.element);
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.$nav = this.$element.find('.tabs__nav');
        this.$content = this.$element.find('.tabs__content');

        this.init();
    }

    /**
    * Initialize the plugin.
    *
    * @function init
    * @returns {void}
    */
    Tabs.prototype.init = function() {
        var that = this;

        if (this.$element.hasClass('tabs--vertical')) {
            if (this.$nav.length > 0 && this.$content.length > 0) {
                this.$nav.find('.sidemenu__link').on('click', function(event) {
                    event.preventDefault();
                    var link = $(this);
                    var tabContent = $(link.attr('href'));

                    if (tabContent.length > 0) {
                        $(document).trigger('reset.telia', that.$content[0]);
                        that.$content.find('.tabs__content-block').removeClass('tabs__content-block--current');

                        tabContent.addClass('tabs__content-block--current');
                        $(document).trigger('refresh.telia', tabContent[0]);

                        that.$nav.find('.sidemenu__item').removeClass('sidemenu__item--current');

                        link.parent().addClass('sidemenu__item--current');
                    }
                });
            }
        } else {
            this.$element.responsiveTabs();
        }
    };

    /**
    * Destroy the plugin instance.
    *
    * @function destroy
    * @returns {void}
    */
    Tabs.prototype.destroy = function() {
        this.$element.removeData('plugin_' + pluginName);
    };

    /**
     * Create jQuery plugin.
     *
     * @param {Object} options - Extended options.
     * @listens telia.tabs
     * @returns {Array<Tabs>} Array of Tabs.
     */
    $.fn[pluginName] = function(options) {
        var args = Array.prototype.slice.call(arguments, 1);

        return this.each(function() {
            var instance = $.data(this, 'plugin_' + pluginName);

            if (!instance) {
                if (options === 'destroy') {
                    return;
                }

                $.data(this, 'plugin_' + pluginName, new Tabs(this, options));
            } else if (typeof options === 'string') {
                instance[options].apply(instance, args);
            }
        });
    };

    /**
     * @param {Event} event
     * @event enhance.telia.tabs
     */
    $(document).on('enhance.telia.tabs', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]();
    });

    /**
     * @param {Event} event
     * @event destroy.telia.tabs
     */
    $(document).on('destroy.telia.tabs', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]('destroy');
    });
})(jQuery, window, document);

/**
 * Auto-init the plugin on DOM ready.
 * @fires enhance.telia.tabs
 */
$(function() {
    $(document).trigger('enhance.telia.tabs');
});

/**
 * Old init for compatibility.
 *
 * @fires enhance.telia.tabs
 * @deprecated Use enhance.telia.tabs event directly.
 * @returns {void}
 */
function initTabs() {
    $(document).trigger('enhance.telia.tabs');
}
window.initTabs = initTabs;

;(function($, window, document) {
    var pluginName = 'teliaAffix';
    var defaults = {};
    var initSelector = '.affix';

    /**
     * Affix.
     *
     * @class
     * @param {HTMLElement} element - The HTML element the Affix should be bound to.
     * @param {Object} options - An option map.
     */
    function Affix(element, options) {
        this.element = element;
        this.$element = $(this.element);
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;

        this._init();
    }

    /**
     * Initialize the plugin.
     *
     * @function _init
     * @private
     * @returns {void}
     */
    Affix.prototype._init = function() {
        window.Stickyfill.add(this.element);

        this.position();
        $(window).on('resize scroll', this.position.bind(this));
        $('.header').on('cssClassChanged', this._positionTimeout.bind(this));
    };

    /**
     * Timeout the position method.
     *
     * @function _positionTimeout
     * @private
     * @returns {void}
     */
    Affix.prototype._positionTimeout = function() {
        // wait until header height has transitioned
        setTimeout(this.position.bind(this), 300);
    };

    /**
     * Position the element under the header.
     *
     * @function position
     * @returns {void}
     */
    Affix.prototype.position = function() {
        var top = $('.header').height();

        this.$element.css('top', top);
    };

    /**
     * Destroy plugin.
     *
     * @function destroy
     * @returns {void}
     */
    Affix.prototype.destroy = function() {
        window.Stickyfill.remove(this.element);
        $(this.element).removeData('plugin_' + pluginName);
        $(window).off('resize scroll', this.position);
        $('.header').off('cssClassChanged', this._positionTimeout);
    };

    /**
     * Create jQuery plugin.
     *
     * @param {Object} options - Extended options.
     * @listens telia.affix
     * @returns {Array<Affix>} Array of Affixes.
     */
    $.fn[pluginName] = function(options) {
        var args = Array.prototype.slice.call(arguments, 1);

        return this.each(function() {
            var instance = $.data(this, 'plugin_' + pluginName);

            if (!instance) {
                if (options === 'destroy') {
                    return;
                }

                $.data(this, 'plugin_' + pluginName, new Affix(this, options));
            } else if (typeof options === 'string') {
                instance[options].apply(instance, args);
            }
        });
    };

    /**
     * @param {Event} event
     * @event enhance.telia.affix
     */
    $(document).on('enhance.telia.affix', function(event) {
        $(event.target).find(initSelector)[pluginName]();
    });

    /**
     * @param {Event} even
     * @event destroy.telia.affix
     */
    $(document).on('destroy.telia.affix', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]('destroy');
    });
})(jQuery, window, document);

/**
 * Auto-init the plugin on DOM ready.
 * @fires enhance.telia.affix
 */
$(function() {
    $(document).trigger('enhance.telia.affix');
});

;(function($, window, document) {
    var pluginName = 'teliaProgress';
    var defaults = {};
    var initSelector = '.progress';

    /**
     * Progress.
     *
     * @class
     * @param {HTMLElement} element - The HTML element the Progress should be bound to.
     * @param {Object} options - An option map.
     */
    function Progress(element, options) {
        this.element = element;
        this.$element = $(this.element);
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.progressBreaks = {
            sm: 600,
            md: 1024
        };
        this.currentBreak = this.$element.data('break');
        this.progressWindowWidth = $(window).width();
        this.steps = this.$element.find('.progress__item');
        this.progressContentContainer = this.$element.find('.progress__container');
        this.progressResizeTimer;

        this.init();
    }

    /**
     * Initialize the plugin.
     *
     * @function init
     * @returns {void}
     */
    Progress.prototype.init = function() {
        if (this.currentBreak in this.progressBreaks) {
            var currentProgressBreak = this.progressBreaks[this.currentBreak];

            if (this.progressWindowWidth < currentProgressBreak) {
                this.convertToNarrow();
            }
        } else {
            this.convertToNarrow();
        }

        $(window).on('resize.telia.progress', this.resize.bind(this));
    };

    /**
     * Resize.
     *
     * @function resize
     * @returns {void}
     */
    Progress.prototype.resize = function() {
        var that = this;

        clearTimeout(this.progressResizeTimer);
        this.progressResizeTimer = setTimeout(function() {
            var progressWindowWidthNew = $(window).width();

            if (that.currentBreak in that.progressBreaks) {
                var currentProgressBreak = that.progressBreaks[that.currentBreak];

                if (that.progressWindowWidth < currentProgressBreak && progressWindowWidthNew >= currentProgressBreak) {
                    that.convertToWide();
                } else if (that.progressWindowWidth >= currentProgressBreak && progressWindowWidthNew < currentProgressBreak) {
                    that.convertToNarrow();
                }
            }

            that.progressWindowWidth = progressWindowWidthNew;
        }, 250);
    };

    /**
     * Convert progress steps to column view.
     *
     * @function convertToNarrow
     * @returns {void}
     */
    Progress.prototype.convertToNarrow = function() {
        if (this.$element.data('break-content') !== undefined && !this.$element.data('break-content')) {
            return;
        }

        this.steps.each(function() {
            var step = $(this);
            var contentID = step.attr('data-content');
            var content = $(contentID);

            step.append(content);
        });

        this.$element.addClass('progress--enhanced');
    };

    /**
     * Convert progress steps to row view.
     *
     * @function convertToWide
     * @param {HTMLElement} $parent - Parent element.
     * @returns {void}
     */
    Progress.prototype.convertToWide = function($parent) {
        var container = ($parent && $parent.length) ? $parent : this.progressContentContainer;

        this.steps.each(function() {
            var step = $(this);
            var contentID = step.attr('data-content');
            var content = $(contentID);

            container.append(content);
        });

        this.$element.removeClass('progress--enhanced');
    };

    /**
     * Initialize slick plugin.
     *
     * @function destroy
     * @returns {void}
     */
    Progress.prototype.destroy = function() {
        $(window).off('resize.telia.progress');

        this.$element.removeData('plugin_' + pluginName);
    };

    /**
     * Create jQuery plugin.
     *
     * @param {Object} options - Extended options.
     * @listens telia.progress
     * @returns {Array<Progress>} Array of Progresses.
     */
    $.fn[pluginName] = function(options) {
        var args = Array.prototype.slice.call(arguments, 1);

        return this.each(function() {
            var instance = $.data(this, 'plugin_' + pluginName);

            if (!instance) {
                if (options === 'destroy') {
                    return;
                }

                $.data(this, 'plugin_' + pluginName, new Progress(this, options));
            } else if (typeof options === 'string') {
                instance[options].apply(instance, args);
            }
        });
    };

    /**
     * @param {Event} event
     * @event enhance.telia.progress
     */
    $(document).on('enhance.telia.progress', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]();
    });

    /**
     * @param {Event} event
     * @event destroy.telia.progress
     */
    $(document).on('destroy.telia.progress', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]('destroy');
    });
})(jQuery, window, document);

/**
 * Auto-init the plugin on DOM ready.
 * @fires enhance.telia.progress
 */
$(function() {
    $(document).trigger('enhance.telia.progress');
});

/**
 * Old init for compatibility.
 *
 * @fires enhance.telia.progress
 * @deprecated Use enhance.telia.progress event directly.
 * @returns {void}
 */
function initProgress() {
    $(document).trigger('enhance.telia.progress');
}
window.initProgress = initProgress;

/* globals ga */

/**
 * Initialize MELU.
 *
 * @param  {string} __theme - Type of MELU.
 * @returns {void}
 */
function initMELU(__theme) {
    if (typeof __theme === 'undefined') {
        __theme = 'era';
    }

    if (__theme === 'era') {
        $('[data-era]').removeClass('hidden');
        $('[data-ari]').addClass('hidden');
    } else {
        $('[data-era]').addClass('hidden');
        $('[data-ari]').removeClass('hidden');
    }

    // stats object to keep user selected values in one place
    var stats = {
        __deviceCount: 0,
        __internetCapacity: 0,
        __internetPrice: 0,
        __callsPrice: 0,
        __monthlyPrice: 0
    };
    // elements used in MELU
    var $elements = {
        choosePackage: $('[name="melu-choose-package"]'),
        choosePackageStep: $('#progress-content-0'),
        headerSteps: $('#js-calc-header-steps'),
        contentSteps: $('#js-calc-content-steps'),
        progressHeader: $('.js-progressHeader'),
        inputDeviceCount: $('[name="melu-device-count"]'),
        inputInternetCapacity: $('[name="melu-internet-capacity"]'),
        inputCallsGroup: $('.js-callsGroup'),
        deviceCount: $('[data-id="deviceCount"]'),
        internetPrice: $('[data-id="internetPrice"]'),
        callsPrice: $('[data-id="callsPrice"]'),
        progressItem: $('.js-progressItem'),
        progressContent: $('.js-progressContent'),
        sidemenuItem: $('.js-sidemenuItem'),
        tabsContentBlock: $('.js-tabContentBlock'),
        firstTab: $('[data-id="tab-1"]'),
        firstTabContent: $('[data-id="tab-content-1"]'),
        monthlyPrice: $('[data-id="monthlyPrice"]'),
        finalDeviceCount: $('.finalDeviceCount'),
        finalInternetCapacity: $('[data-id="finalInternetCapacity"]'),
        finalInternetPrice: $('[data-id="finalInternetPrice"]'),
        finalCallsPrice: $('[data-id="finalCallsPrice"]'),
        finalMonthlyPrice: $('[data-id="finalMonthlyPrice"]'),
        tableRow: $('.js-tableRow'),
        jsFeatures: $('.js-features'),
        jsFeatureGroup: $('.js-featureGroup'),
        deviceIndicator: $('[data-device-indicator]'),
        deviceIndicatorWrapper: $('.device-indicator-wrapper'),
        optionGroup: $('[data-option-group]'),
        multipleTexts: $('[data-multiple-texts]'),
        footerButton: $('[data-id="js-footerButton"]'),
        mobileOptionCollapseTrigger: $('.form-option-card__item--collapse-trigger'),
        backToStart: $('#js-backToStart')
    };
    // steps array of objects to keep steps states and objects
    var steps = [
        {
            progressNumber: 1,
            progressObject: $('[data-content="#progress-content-1"]'),
            progressContentObject: $('#progress-content-1'),
            isActive: false,
            isDone: false
        },
        {
            progressNumber: 2,
            progressObject: $('[data-content="#progress-content-2"]'),
            progressContentObject: $('#progress-content-2'),
            isActive: false,
            isDone: false
        },
        {
            progressNumber: 3,
            progressObject: $('[data-content="#progress-content-3"]'),
            progressContentObject: $('#progress-content-3'),
            isActive: false,
            isDone: false
        },
        {
            progressNumber: 4,
            progressObject: $('[data-content="#progress-content-4"]'),
            progressContentObject: $('#progress-content-4'),
            isActive: false,
            isDone: false
        }
    ];
    // tabs array of objects to track tabs activity
    var tabs = [
        {
            tabNumber: 1,
            price: 0,
            isValueSelected: false,
            package: 0,
            tabObject: $('#tab-1'),
            tabContentObject: $('[data-id="tab-content-1"]'),
            finalTableRowObject: $('[data-id="finalDevice-1"]')
        },
        {
            tabNumber: 2,
            price: 0,
            isValueSelected: false,
            package: 0,
            tabObject: $('#tab-2'),
            tabContentObject: $('[data-id="tab-content-2"]'),
            finalTableRowObject: $('[data-id="finalDevice-2"]')
        },
        {
            tabNumber: 3,
            price: 0,
            isValueSelected: false,
            package: 0,
            tabObject: $('#tab-3'),
            tabContentObject: $('[data-id="tab-content-3"]'),
            finalTableRowObject: $('[data-id="finalDevice-3"]')
        },
        {
            tabNumber: 4,
            price: 0,
            isValueSelected: false,
            package: 0,
            tabObject: $('#tab-4'),
            tabContentObject: $('[data-id="tab-content-4"]'),
            finalTableRowObject: $('[data-id="finalDevice-4"]')
        },
        {
            tabNumber: 5,
            price: 0,
            isValueSelected: false,
            package: 0,
            tabObject: $('#tab-5'),
            tabContentObject: $('[data-id="tab-content-5"]'),
            finalTableRowObject: $('[data-id="finalDevice-5"]')
        }
    ];
    // object of features that are selected with internet selection
    var features = [
        {
            id: 1,
            name: 'roaming',
            isSelected: false,
            group: 1
        },
        {
            id: 2,
            name: 'internet',
            isSelected: false,
            group: 1
        },
        {
            id: 3,
            name: 'mytv',
            isSelected: false,
            group: 2
        },
        {
            id: 4,
            name: 'spotify',
            isSelected: false,
            group: 2
        }
    ];

    var __globalPackage = 0;

    /**
     * Bind all click events.
     *
     * @returns {void}
     */
    function bindClickEvents() {
        // progress clicks
        $elements.progressHeader.on('click', function(e) {
            e.preventDefault();
            var $this = $(this);
            var $activeProgress = $this.parent();

            Object.keys(steps).forEach(function(key) {
                if ($activeProgress[0] === steps[key].progressObject[0]) {
                    // prevent changing tab if isDone is false
                    if (steps[key].isDone) {
                        changeProgress(steps[key], true);
                    }
                }
            });
        });

        // step 0 click
        $elements.choosePackage.on('click', function() {
            var $this = $(this);

            // reset js-callsGroups visibility
            $elements.inputCallsGroup.each(function() {
                $(this).parent().removeClass('hidden');
            });

            switch ($this.val()) {
                case '1':
                    __globalPackage = 1;
                    changeProgress(steps[0], true);
                    break;
                case '2':
                    __globalPackage = 2;
                    changeProgress(steps[0], true);
                    changeDeviceCountInternetFrom($this.data('internetFrom'));
                    break;
                case '3':
                    __globalPackage = 3;
                    $($elements.inputDeviceCount[0]).trigger('click');
                    $($elements.inputInternetCapacity[$elements.inputInternetCapacity.length - 1]).trigger('click');
                    $elements.tabsContentBlock
                        .filter(function(i) {
                            return i < stats.__deviceCount;
                        })
                        .each(function() {
                            var $this = $(this);

                            $this.find('.js-callsGroup').each(function() {
                                var $this = $(this);

                                // if internet package (eq 4)
                                if ($this.data('package') == 4) {
                                    $this.parent().addClass('hidden');
                                }
                            });
                        });
                    break;
            }
        });

        $elements.backToStart.on('click', function() {
            changeProgress();
        });

        // step 1 click
        $elements.inputDeviceCount.on('click', function() {
            var $this = $(this);

            setDeviceCount(parseInt($this.val()));
            showInternetOptions($this);
            resetCallsPriceAndTabs();
            calcMonthlyPrice();
            setFinalDetails();
            resetDeviceIndicators();
            changeCallsPriceForOneDevice();

            changeProgress(steps[1], true);

            if (__theme == 'ari' && steps[3].isDone) {
                setFooterButtonDisabledState(false);
            }

            window.autoScroll(steps[1].progressContentObject, 500, 0);
        });

        $elements.mobileOptionCollapseTrigger.on('click', function(e) {
            e.preventDefault();
        });

        // step 2 click
        $elements.inputInternetCapacity.on('click', function(e) {
            var $this = $(this);

            if (!$(e.target).hasClass('form-option-card__item--collapse-trigger')) {
                setInternetPrice(replaceComma($this.val()), replaceComma($this.data('price')));
                setFeatures($this.data('features'));

                calcMonthlyPrice();
                setFinalDetails();

                if (__globalPackage === 2) {
                    // get only as many tabs as is device count
                    $elements.tabsContentBlock
                        .filter(function(i) {
                            return i < stats.__deviceCount;
                        })
                        .each(function() {
                            var $this = $(this);

                            $this.find('.js-callsGroup').each(function() {
                                var $this = $(this);

                                // if internet package (eq 4)
                                if ($this.data('package') == 4) {
                                    $this.trigger('click');
                                }
                            });
                        });

                    setProgressClickable(2);
                    changeProgress(steps[3], true);
                } else {
                    changeProgress(steps[2], true);
                }
            }

            if (__theme == 'ari' && steps[3].isDone) {
                setFooterButtonDisabledState(false);
            }

            if (MEWindowWidth < 1024) {
                window.autoScroll(steps[2].progressContentObject, 500, 0);
            }
        });

        // step 3 click
        $elements.inputCallsGroup.on('click', function() {
            var $this = $(this);
            var groupNumber = $this.data('group') - 1;
            var packageNumber = $this.data('package');
            var isProgressChanged;

            tabs[groupNumber].isValueSelected = true;

            if (__theme == 'ari' && stats.__deviceCount > 1) {
                for (var i = 1; i <=  stats.__deviceCount; i++) {
                    setPackage(tabs[i - 1], 4);
                    if (tabs[0] != tabs[i - 1]) {
                        tabs[i - 1].price = 1.49;
                    }
                }

                $('[data-id="finalDevicePrice-1"]').html(setTwoDecimalPlaces($this.data('price')));
                setFooterButtonDisabledState(false);
                changeProgress(steps[3], true);
            } else {
                isProgressChanged = openNextTab(tabs[groupNumber]);
            }

            setTabAsDone(tabs[groupNumber], replaceComma($this.data('price')));
            setPackage(tabs[groupNumber], packageNumber);
            setDeviceIndicatorPlacement(tabs[groupNumber], packageNumber);

            setCallsPrice();

            calcMonthlyPrice();
            setFinalDetails();
            toggleCallsFeatures();

            if (MEWindowWidth < 1024) {
                if (!isProgressChanged) {
                    window.autoScroll(steps[2].progressContentObject, 500, 0);
                }
            }
        });

        // button from 3rd step click
        $elements.footerButton.on('click', function() {
            changeProgress(steps[3], true);
            if (MEWindowWidth < 1024) {
                window.autoScroll(steps[3].progressContentObject, 500, 0);
            }
        });

        initTabsClicks();
    }

    /**
     * Change progress items state.
     *
     * @param  {Object} activeProgressObj - Currently active progress step.
     * @param  {boolean} forward - If step is going forward.
     * @returns {void}
     */
    function changeProgress(activeProgressObj, forward) {
        if (!activeProgressObj) {
            $elements.progressItem.each(function() {
                var $this = $(this);

                $this.removeClass('is-current');
                $this.removeClass('is-done');
                $this.removeClass('is-active');
            });

            setActiveProgressObject();

            return;
        }

        setActiveProgressObject(activeProgressObj.progressObject);

        $elements.progressItem.each(function() {
            var $this = $(this);
            var str = $this.data('content').slice(1);
            var number = str.substring(str.length - 1);

            $this.removeClass('is-current');
            $this.removeClass('is-done');
            if ($this[0] === activeProgressObj.progressObject[0]) {
                $this.addClass('is-current');
                $this.find('.progress__header').attr('href', '#0');
            } else if (number < activeProgressObj.progressNumber) {
                $this.addClass('is-done');
            }
        });

        $(document).trigger('refresh.telia', tabs[0].tabContentObject[0]);
        resetTabToFirst();

        backtofuture(forward, activeProgressObj.progressNumber);
    };

    /**
     * Change progress content state.
     *
     * @param  {Object} obj - Open current progress step.
     * @returns {void}
     */
    function openProgress(obj) {
        if (!obj) {
            $elements.progressContent.each(function() {
                var $this = $(this);

                $this.removeClass('is-visible');
                $this.removeClass('is-done');

                if ($this[0] === $elements.choosePackageStep[0]) {
                    $this.addClass('is-visible');
                }
            });

            return;
        }

        setOpenTabs(obj);

        $elements.progressContent.each(function() {
            var $this = $(this);
            var str = $this.attr('id');
            var number = str.substring(str.length - 1);

            $this.removeClass('is-visible');
            $this.removeClass('is-done');
            if ($this[0] === obj.progressContentObject[0]) {
                $this.addClass('is-visible');
            } else if (number < obj.progressNumber) {
                $this.addClass('is-done');
            }
        });
    };

    /**
     * Removes click functionality from progress items.
     *
     * @param  {number} stepNumber - Step number.
     * @returns {void}
     */
    function preventProgressClick(stepNumber) {
        steps[stepNumber].isDone = false;
        steps[stepNumber].progressObject.find('.progress__header').removeAttr('href');

        if (stepNumber == 3) {
            setFooterButtonDisabledState(true);
        }
    };

    /**
     * Set progress item clickable.
     *
     * @param {number} stepNumber - Step Number.
     * @returns {void}
     */
    function setProgressClickable(stepNumber) {
        steps[stepNumber].isDone = true;
        steps[stepNumber].progressObject.find('.progress__header').attr('href', '#0');

        if (stepNumber == 3) {
            setFooterButtonDisabledState(false);
        }
    };

    /**
     * Change starting internet capacity.
     *
     * @param  {string} from - Starting capacity.
     * @returns {void}
     */
    function changeDeviceCountInternetFrom(from) {
        $elements.inputDeviceCount.each(function() {
            var $this = $(this);

            if ($this.data('internetFrom') < from) {
                $this.attr('data-internet-from-changed', from);
            }
        });
    };

    /**
     * Toggle footer button disabled state.
     *
     * @param {boolean} bool - If button must be disabled.
     * @returns {void}
     */
    function setFooterButtonDisabledState(bool) {
        if (bool) {
            $elements.footerButton.addClass('btn--disabled').prop('disabled', true);
        } else {
            $elements.footerButton.removeClass('btn--disabled').prop('disabled', false);
        }
    };

    /**
     * Set steps objects values.
     *
     * @param {JQuery} $activeProgress - Active progress step.
     * @returns {void}
     */
    function setActiveProgressObject($activeProgress) {
        if (!$activeProgress) {
            Object.keys(steps).forEach(function(key) {
                steps[key].isActive = false;
                steps[key].isDone = false;
            });

            openProgress();

            return;
        }

        Object.keys(steps).forEach(function(key) {
            if ($activeProgress[0] === steps[key].progressObject[0]) {
                steps[key].isActive = true;
                steps[key].isDone = true;
                openProgress(steps[key]);
            } else {
                steps[key].isActive = false;
            }
        });
    };

    /**
     * Initialize tabs click and trigger equalheight.
     *
     * @returns {void}
     */
    function initTabsClicks() {
        var nav = $('.tabs__nav');
        var content = $('.tabs__content');

        if (nav.length > 0 && content.length > 0) {
            nav.find('.sidemenu__link-link').on('click', function(event) {
                event.preventDefault();
                var link = $(this);
                var tabContent = $('[data-id="' + link.data('xhref') + '"]');

                if (tabContent.length > 0) {
                    $('.tabs__content-block').removeClass('tabs__content-block--current');
                    tabContent.addClass('tabs__content-block--current');
                    nav.find('.sidemenu__item').removeClass('sidemenu__item--current');
                    link.parent().addClass('sidemenu__item--current');
                    $(document).trigger('refresh.telia', tabContent[0]);

                    if (MEWindowWidth < 1024) {
                        window.autoScroll(steps[2].progressContentObject, 500, 0);
                    }
                }
            });
        }
    };

    /**
     * Set pervious tab as done if value is selected.
     *
     * @param {Object} tabGroup - Tabs group.
     * @param {number} price - Price from tab option-group.
     * @returns {void}
     */
    function setTabAsDone(tabGroup, price) {
        tabGroup.price = price;
        tabGroup.isValueSelected = true;

        $elements.sidemenuItem.each(function() {
            var $this = $(this);
            var str = $this.data('id');
            var number = str.substring(str.length - 1);

            if (number == tabGroup.tabNumber) {
                $this.addClass('sidemenu__item--done');
                $('[data-id="devicePrice-' + number + '"]').html(setTwoDecimalPlaces(tabGroup.price));
                if ($(window).width() < 1024) {
                    $('[data-id="devicePrice-' + number + '"]').parent().css('display', 'inline-block');
                } else {
                    $('[data-id="devicePrice-' + number + '"]').parent().css('display', 'block');
                }
                // set final step table values for specific row
                $('[data-id="finalDevicePrice-' + number + '"]').html(setTwoDecimalPlaces(tabGroup.price));
                if (tabGroup.price == 0) {
                    $('[data-id="finalDeviceInfo-' + number + '"]').removeClass('hidden');
                } else {
                    $('[data-id="finalDeviceInfo-' + number + '"]').addClass('hidden');
                }
            }
        });
    };

    /**
     * Opens next tab.
     *
     * @param  {Object} tabGroup - Tabs group.
     * @returns {void}
     */
    function openNextTab(tabGroup) {
        var nextTabGroupNumber = tabGroup.tabNumber + 1;
        var countSelectedTabs = 0;

        Object.keys(tabs).forEach(function(key) {
            // change tab if there is next tab
            if (tabs[key].tabNumber == nextTabGroupNumber && tabs[key].tabNumber <= stats.__deviceCount) {
                $elements.sidemenuItem.removeClass('sidemenu__item--current');
                $elements.tabsContentBlock.removeClass('tabs__content-block--current');
                tabs[key].tabObject.addClass('sidemenu__item--current');
                tabs[key].tabContentObject.addClass('tabs__content-block--current');
                $(document).trigger('refresh.telia', tabs[key].tabContentObject[0]);
            }

            if (tabs[key].isValueSelected) {
                countSelectedTabs++;
            }
        });

        // change progress to last step automatically if last step is not done. Prevents auto step change if user comes back from last step
        if (countSelectedTabs == stats.__deviceCount && !steps[3].isDone) {
            setFooterButtonDisabledState(false);
            changeProgress(steps[3], true);
            if (MEWindowWidth < 1024) {
                window.autoScroll(steps[3].progressContentObject, 500, 0);
            } else {
                window.autoScroll(steps[3].progressContentObject, 500, 220);
            }

            return true;
        }

        if (countSelectedTabs == stats.__deviceCount) {
            setFooterButtonDisabledState(false);
            changeProgress(steps[3], true);
            if (MEWindowWidth < 1024) {
                window.autoScroll(steps[3].progressContentObject, 500, 0);
            } else {
                window.autoScroll(steps[3].progressContentObject, 500, 220);
            }

            return true;
        }

        return false;
    };

    /**
     * Open first tab when coming back to calls view.
     *
     * @returns {void}
     */
    function resetTabToFirst() {
        $elements.sidemenuItem.removeClass('sidemenu__item--current');
        $elements.tabsContentBlock.removeClass('tabs__content-block--current');

        $elements.firstTab.addClass('sidemenu__item--current');
        $elements.firstTabContent.addClass('tabs__content-block--current');
    };

    /**
     * Resets all tabs and their content.
     *
     * @returns {void}
     */
    function resetAllTabs() {
        $elements.inputCallsGroup.each(function() {
            $(this).prop('checked', false);
        });

        resetAllDeviceIndicators();
    };

    /**
     * Reset calls prices and tab values.
     *
     * @param {boolean} resetAll - If all should be resetted.
     * @returns {void}
     */
    function resetCallsPriceAndTabs(resetAll) {
        var countSelectedTabs = 0;

        stats.__callsPrice = 0;
        Object.keys(tabs).forEach(function(key) {
            if (resetAll) {
                // reset all tabs because there are different option cards
                tabs[key].price = 0;
                tabs[key].isValueSelected = false;
                tabs[key].tabObject.removeClass('sidemenu__item--done');
                $('[data-id="devicePrice-' + tabs[key].tabNumber + '"]').html('');
                $('[data-id="devicePrice-' + tabs[key].tabNumber + '"]').parent().css('display', 'none');
            } else if (tabs[key].tabNumber > stats.__deviceCount) {
                // reset all tabs that are not selected
                tabs[key].price = 0;
                tabs[key].isValueSelected = false;
                tabs[key].tabObject.removeClass('sidemenu__item--done');
                $('[data-id="devicePrice-' + tabs[key].tabNumber + '"]').html('');
                $('[data-id="devicePrice-' + tabs[key].tabNumber + '"]').parent().css('display', 'none');
                tabs[key].tabContentObject.find($elements.inputCallsGroup).each(function() {
                    $(this).prop('checked', false);
                });
            }

            if (tabs[key].isValueSelected) {
                countSelectedTabs++;
            }

            stats.__callsPrice += tabs[key].price;
        });

        $('[data-device-feature]').addClass('hidden');

        if (resetAll) {
            resetAllTabs();
        }

        $elements.callsPrice.html(setTwoDecimalPlaces(stats.__callsPrice));

        // if selected tabs count is smaller than device count, do not let user to last step, because they have to choose all calls view values
        if (countSelectedTabs < stats.__deviceCount) {
            // reset last tab done status to false, to prevent click
            preventProgressClick(3);
        } else {
            setProgressClickable(3);
        }
    };

    /**
     * Sets package to final view table popover.
     *
     * @param {Object} tabGroup - Tabs group.
     * @param {number} package - Selected package number.
     * @returns {void}
     */
    function setPackage(tabGroup, package) {
        tabGroup.package = package;
        var packageId = '#package-' + package;

        tabGroup.finalTableRowObject.find('[data-toggle="tooltip"]').data('content', packageId);
    };

    /**
     * Move device indicators around inside tabs.
     *
     * @param {Object} tabGroup - Tabs group.
     * @param {number} package - Selected package number.
     * @returns {void}
     */
    function setDeviceIndicatorPlacement(tabGroup, package) {
        $elements.tabsContentBlock.each(function() {
            var $tabElement = $(this);

            $tabElement.find($elements.inputCallsGroup).each(function() {
                var $this = $(this);
                // if is the same package as just selected

                if ($this.data('package') == package) {
                    $tabElement.find($elements.deviceIndicator).each(function() {
                        var $self = $(this);

                        if ($self.data('deviceIndicator') == tabGroup.tabNumber) {
                            $self.removeClass('hidden');
                            $this.parent().find($elements.deviceIndicatorWrapper).append($self);
                        }
                    });
                }
            });
        });
    };

    /**
     * Reset device indicators if user changes device count.
     *
     * @returns {void}
     */
    function resetDeviceIndicators() {
        $elements.deviceIndicator.each(function() {
            var $this = $(this);

            if ($this.data('deviceIndicator') > stats.__deviceCount) {
                $this.addClass('hidden');
            }
        });
    };

    /**
     * Reset device indicators.
     *
     * @returns {void}
     */
    function resetAllDeviceIndicators() {
        $elements.deviceIndicator.each(function() {
            $(this).addClass('hidden');
        });
    };

    /**
     * Set two decimal places for prices and replace dot with comma.
     *
     * @param {number} num - Any number.
     * @returns {number} Number fixed to 2 decimal places.
     */
    function setTwoDecimalPlaces(num) {
        return parseFloat(num).toFixed(2).replace('.', ',');
    };

    /**
     * Replaces all commas with dots if commas are found.
     *
     * @param {string} str - Any string.
     * @returns {number} Parsed number.
     */
    function replaceComma(str) {
        if (str.toString().indexOf(',') > -1) {
            str = str.replace(/,/g, '.');
        }

        return parseFloat(str);
    };

    /**
     * Calculate monthly price eq calls price + internet price.
     *
     * @returns {void}
     */
    function calcMonthlyPrice() {
        stats.__monthlyPrice = stats.__callsPrice + stats.__internetPrice;

        $elements.monthlyPrice.html(setTwoDecimalPlaces(stats.__monthlyPrice));
        $elements.monthlyPrice.parent().parent().css('display', 'block');
    };

    /**
     * Populate last step with data.
     *
     * @returns {void}
     */
    function setFinalDetails() {
        $elements.finalDeviceCount.html(stats.__deviceCount);
        $elements.finalInternetCapacity.html(stats.__internetCapacity);
        $elements.finalInternetPrice.html(setTwoDecimalPlaces(stats.__internetPrice));
        $elements.finalCallsPrice.html(setTwoDecimalPlaces(stats.__callsPrice));
        $elements.finalMonthlyPrice.html(setTwoDecimalPlaces(stats.__monthlyPrice));

        var devices = stats.__deviceCount;

        var j = Number(devices);

        var $selectedData = $('input[name="melu-internet-capacity"]:checked');
        var dataID = (j == 1) ? $selectedData.attr('data-id1') : $selectedData.attr('data-id');
        var callsID = 0;
        var selectedAdditionalValue = $selectedData.data('additional-value');
        var selectedAdditionalUnit = $selectedData.data('additional-unit');

        $('[data-bind-additional-value]').text(selectedAdditionalValue);
        $('[data-bind-additional-unit]').text(selectedAdditionalUnit);

        if (selectedAdditionalValue == 0) {
            $('[data-id="full"]').removeClass('hidden');
            $('[data-id="half"]').addClass('hidden');
        } else {
            $('[data-id="full"]').addClass('hidden');
            $('[data-id="half"]').removeClass('hidden');
        }

        for (var i = 1; i <= j; i++) {
            var currentCallsID = $('input[name="melu-calls-group-' + i + '"]:checked').attr('data-id');

            if (callsID == 0) {
                callsID = '&calls=' + currentCallsID;
            } else {
                callsID += '&calls=' + currentCallsID;
            }
        }

        var href = '';
        var $link = $('#orderButton');
        var currentHref = $link.attr('href');
        var hrefParts = currentHref.split('&_ga');

        if (window.location.href.indexOf('/ari/') > -1) {
            href += '&private=false';
        } else {
            href += '&private=true';
        }
        if (dataID) {
            href += '&data=' + dataID;
        }

        if (typeof callsID !== 'undefined' && callsID != '') {
            href +=   callsID;
        }

        var newHref = 'https://www.emt.ee/et/eteenindus/multisim/-/multi?loggedin=true&toNewContract=true' + href;

        if (typeof (hrefParts[1]) !== 'undefined') {
            newHref += '&_ga' + hrefParts[1];
        }

        $link.attr('href', newHref);
    };

    /**
     * Set same amout of tab open as devices count is, do also same for table in final step.
     *
     * @param {Object} obj - Tabs object.
     * @returns {void}
     */
    function setOpenTabs(obj) {
        if (obj.progressNumber == 3) {
            $('.sidemenu__item:lt(' + stats.__deviceCount + ')').each(function() {
                $(this).addClass('sidemenu__item--visible');
            });
        } else {
            $elements.sidemenuItem.removeClass('sidemenu__item--visible');
        }

        // set visible table rows in final step
        if (obj.progressNumber == 4) {
            if (stats.__deviceCount > 1) {
                $('.table__row:lt(' + stats.__deviceCount + ')').each(function() {
                    $(this).addClass('table__row--visible');
                });
            }
        } else {
            $elements.tableRow.removeClass('table__row--visible');
        }
    };

    /**
     * Set device count to global variable.
     *
     * @param {number} count - Device count.
     * @returns {void}
     */
    function setDeviceCount(count) {
        stats.__deviceCount = count;

        setDeviceCountToDom();
        setTextsBasedOnDeviceCount();
    };

    /**
     * Sets device count to DOM.
     *
     * @returns {void}
     */
    function setDeviceCountToDom() {
        $elements.deviceCount.html(stats.__deviceCount);
        $elements.deviceCount.parent().parent().css('display', 'block');
    };

    /**
     * Sets correct texts based on device count.
     *
     * @returns {void}
     */
    function setTextsBasedOnDeviceCount() {
        if (stats.__deviceCount == 1) {
            $elements.multipleTexts.each(function() {
                var $this = $(this);

                if ($this.data('multipleTexts')) {
                    $this.addClass('hidden');
                } else {
                    $this.removeClass('hidden');
                }
            });
        } else {
            $elements.multipleTexts.each(function() {
                var $this = $(this);

                if (!$this.data('multipleTexts')) {
                    $this.addClass('hidden');
                } else {
                    $this.removeClass('hidden');
                }
            });
        }
    };

    /**
     * Set internet capacity and price to global variable.
     *
     * @param {number} number - Internet capacity.
     * @param {number} price - Internet price.
     * @returns {void}
     */
    function setInternetPrice(number, price) {
        stats.__internetCapacity = number;
        stats.__internetPrice = price;

        setInternetPriceToDOM();
    };

    /**
     * Sets internet price to DOM.
     *
     * @returns {void}
     */
    function setInternetPriceToDOM() {
        $elements.internetPrice.html(setTwoDecimalPlaces(stats.__internetPrice));
        $elements.internetPrice.parent().parent().css('display', 'block');
    };

    /**
     * Set calls price to global variable.
     *
     * @returns {void}
     */
    function setCallsPrice() {
        stats.__callsPrice = 0;
        Object.keys(tabs).forEach(function(key) {
            stats.__callsPrice += tabs[key].price;
        });

        setCallsPriceToDOM();
    };

    /**
     * Sets calls price to DOM.
     *
     * @returns {void}
     */
    function setCallsPriceToDOM() {
        $elements.callsPrice.html(setTwoDecimalPlaces(stats.__callsPrice));
        $elements.callsPrice.parent().parent().css('display', 'block');
    };

    /**
     * Change calls package price if device count is 1.
     *
     * @returns {void}
     */
    function changeCallsPriceForOneDevice() {
        if (__theme === 'ari') return;

        if (stats.__deviceCount == 1) {
            $elements.inputCallsGroup.each(function() {
                var $this = $(this);

                if ($this.data('group') === 1 && $this.data('package') === 4) {
                    $this.data('price', 0);
                    $('#js-callsOneDevicePrice').text('0');
                }
            });

            if (tabs[0].package === 4) {
                tabs[0].price = 0;
            }

            $('[data-id="devicePrice-1"]').html(setTwoDecimalPlaces(0));
        } else {
            $elements.inputCallsGroup.each(function() {
                var $this = $(this);

                if ($this.data('group') === 1 && $this.data('package') === 4) {
                    $this.data('price', 1.49);
                    $('#js-callsOneDevicePrice').text('1,49');
                }
            });

            if (tabs[0].package === 4) {
                tabs[0].price = 1.49;
            }

            $('[data-id="devicePrice-1"]').html(setTwoDecimalPlaces(1.49));
        }

        setCallsPrice();
    };

    /**
     * Sets features that are selected with internet selection.
     *
     * @param {Object} featuresObject - Available features.
     * @returns {void}
     */
    function setFeatures(featuresObject) {
        resetFeatures();

        var featuresArray = featuresObject.split(',');

        for (var i = 0; i < featuresArray.length; i++) {
            if (featuresArray[i] != '') {
                var selectedObject = features[featuresArray[i] - 1];

                selectedObject.isSelected = true;

                $elements.jsFeatures.each(function() {
                    var $this = $(this);

                    if ($this.data('feature') == selectedObject.id) {
                        $this.removeClass('hidden');
                    }
                });

                $elements.jsFeatureGroup.each(function() {
                    var $this = $(this);

                    if ($this.data('feature-group') == selectedObject.group) {
                        $this.removeClass('hidden');
                    }
                });
            }
        }

        // if 1 device and 0.5 internet - specialcase
        if (stats.__deviceCount == 1 && stats.__internetCapacity == 0.5) {
            resetCallsPriceAndTabs(true);

            $elements.optionGroup.eq(0).find($elements.inputCallsGroup).each(function() {
                var $this = $(this);

                if ($this.data('package') === 4) {
                    $this.parent().addClass('hidden');
                }
            });
        } else {
            $elements.optionGroup.eq(0).find($elements.inputCallsGroup).each(function() {
                $(this).parent().removeClass('hidden');
            });
        }
    };

    /**
     * Reset features object and DOM.
     *
     * @returns {void}
     */
    function resetFeatures() {
        $elements.jsFeatures.addClass('hidden');
        $elements.jsFeatureGroup.addClass('hidden');

        Object.keys(features).forEach(function(key) {
            features[key].isSelected = false;
        });
    };

    /**
     * Show internet options and recommendations based on device count.
     *
     * @param {JQuery} $deviceInput - Selected device element.
     * @returns {void}
     */
    function showInternetOptions($deviceInput) {
        var internetFrom = $deviceInput.attr('data-internet-from-changed') ? parseFloat($deviceInput.attr('data-internet-from-changed')) : parseFloat($deviceInput.attr('data-internet-from'));
        var recommend = parseFloat($deviceInput.data('recommend'));

        $elements.inputInternetCapacity.each(function() {
            var $this = $(this);
            var thisCapacity = parseFloat($this.val());

            // remove recommend badge from all option card
            $this.parent().removeClass('form-option-card--recommended hidden');
            // add recommend badge to right option card
            if (thisCapacity === recommend) {
                $this.parent().addClass('form-option-card--recommended');
            }

            if (thisCapacity < internetFrom) {
                $this.parent().addClass('hidden');
            }
        });

        if (internetFrom >= stats.__internetCapacity) {
            resetInternetOptions();
            preventProgressClick(2);
            preventProgressClick(3);
        }
    };

    /**
     * Reset internet value.
     *
     * @returns {void}
     */
    function resetInternetOptions() {
        setInternetPrice(0, 0);
    };

    /**
     * Convert tabs to narrow.
     *
     * @returns {void}
     */
    function convertTabsNarrow() {
        $elements.sidemenuItem.each(function() {
            var $this = $(this);
            var tabID = $this.find('.sidemenu__link-link').data('xhref');

            $this.append($('#' + tabID));
        });
    };

    /**
     * Convert tabs to wide.
     *
     * @returns {void}
     */
    function convertTabsWide() {
        $('.mobile-life .progress').teliaProgress('convertToWide', $('.mobile-life .progress__container'));

        $elements.tabsContentBlock.each(function() {
            var $this = $(this);
            var tabID = $this.data('id');

            $('.tabs__content').append($('#' + tabID));
            $(document).trigger('refresh.telia', $this[0]);
        });
    };

    /**
     * Check window width and convert tabs.
     *
     * @param  {number} windowWidth - Width of window.
     * @returns {void}
     */
    function checkTabsState(windowWidth) {
        if (windowWidth < 1024) {
            convertTabsNarrow();
        } else {
            convertTabsWide();
        }
    };

    /**
     * Toggle features for calls.
     *
     * @returns {void}
     */
    function toggleCallsFeatures() {
        var countCheckedCallsGroup = 0;

        Object.keys(tabs).forEach(function(key) {
            if (tabs[key].isValueSelected) {
                tabs[key].tabContentObject.find('.js-callsGroup').each(function() {
                    var $this = $(this);

                    if ($this.is(':checked') && $this.data('price') > 1.49) {
                        countCheckedCallsGroup++;
                    }

                    if (countCheckedCallsGroup > 0) {
                        $('[data-device-feature]').removeClass('hidden');
                    } else {
                        $('[data-device-feature]').addClass('hidden');
                    }
                });
            }
        });
    };

    var MEResizeTimer;
    var MEWindowWidth = $(window).width();

    $(window).on('resize', function() {
        clearTimeout(MEResizeTimer);
        MEResizeTimer = setTimeout(function() {
            var MEWindowWidthNew = $(window).width();

            checkTabsState(MEWindowWidthNew);

            MEWindowWidth = MEWindowWidthNew;
        }, 250);
    });

    bindClickEvents();
    checkTabsState(MEWindowWidth);

    /**
     * Change url hash and window location.
     *
     * @param  {boolean} setNewHash - If new hash has to added to url.
     * @param  {number} nextProgress - Next progres step number.
     * @returns {void}
     */
    function backtofuture(setNewHash, nextProgress) {
        var historyAPI = typeof history.pushState !== 'undefined';
        var backtofuturehash = '#step/' + nextProgress + '/';

        if (nextProgress > 0 && setNewHash) {
            if (historyAPI) history.pushState({
                step: nextProgress - 1
            }, '', backtofuturehash);
            else location.hash = backtofuturehash;
        }
    }

    /**
     * Change progress to next step after hash change.
     *
     * @param  {number} nextStep - Next step number.
     * @returns {void}
     */
    function hashChange(nextStep) {
        if (nextStep >= 0) {
            changeProgress(steps[nextStep], false);
            window.autoScroll($('.mobile-life'), 100, 50);
            ga('send', 'event', 'Era&Ari - Internet - Nutiseadmes', 'Mobiilne elu', 'Back button pressed');
        }
    }

    window.addEventListener('popstate', function(event) {
        if (event.state) {
            hashChange(event.state.step);
        } else {
            hashChange(0);
        }
    });
}
window.initMELU = initMELU;

$(function() {
    if (window.location.href.indexOf('/ari/') > -1 || $('.mobile-life').data('mobile-life') === 'ari') {
        initMELU('ari');
    } else {
        initMELU();
    }
});

;(function($, window, document) {
    var pluginName = 'teliaPriceToggle';
    var defaults = {};
    var initSelector = '.js-price-toggle';

    /**
     * PriceToggle.
     *
     * @class
     * @param {HTMLElement} element - The HTML element the PriceToggle should be bound to.
     * @param {Object} options - An option map.
     */
    function PriceToggle(element, options) {
        this.element = element;
        this.$element = $(this.element);
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.controls = this.$element.find('.js-price-toggle-control');
        this.targets = [];

        this.init();
    }

    /**
     * Initialize the plugin.
     *
     * @function init
     * @returns {void}
     */
    PriceToggle.prototype.init = function() {
        var that = this;

        /* save all possible targets to an array */
        this.controls.each(function() {
            var target = $(this).data('target');

            if (target) {
                if ($(target).length > 0) {
                    that.targets.push(target);
                }
            }
        });

        this.controls.on('change', this.change.bind(this));
    };

    /**
     * Show/hide targets on change.
     *
     * @function change
     * @param   {Event} event - An event object.
     * @returns {void}
     */
    PriceToggle.prototype.change = function(event) {
        var newTarget = $(event.target).data('target');

        /* hide old targets, if any */
        for (var i = 0; i < this.targets.length; ++i) {
            // do something with `substr[i]`
            $(this.targets[i]).removeClass('is-visible');
        }

        $(newTarget).addClass('is-visible');
    };

    /**
     * Destroy the plugin instance.
     *
     * @function destroy
     * @returns {void}
     */
    PriceToggle.prototype.destroy = function() {
        this.controls.off(this.change());
        this.$element.removeData('plugin_' + pluginName);
    };

    /**
     * Create jQuery plugin.
     *
     * @param {Object} options - Extended options.
     * @listens telia.priceToggle
     * @returns {Array<PriceToggle>} Array of PriceToggles.
     */
    $.fn[pluginName] = function(options) {
        var args = Array.prototype.slice.call(arguments, 1);

        return this.each(function() {
            var instance = $.data(this, 'plugin_' + pluginName);

            if (!instance) {
                if (options === 'destroy') {
                    return;
                }

                $.data(this, 'plugin_' + pluginName, new PriceToggle(this, options));
            } else if (typeof options === 'string') {
                instance[options].apply(instance, args);
            }
        });
    };

    /**
     * @param {Event} event
     * @event enhance.telia.priceToggle
     */
    $(document).on('enhance.telia.priceToggle', function(event) {
        $(event.target).find(initSelector)[pluginName]();
    });

    /**
     * @param {Event} event
     * @event destroy.telia.priceToggle
     */
    $(document).on('destroy.telia.priceToggle', function(event) {
        $(event.target).find(initSelector)[pluginName]('destroy');
    });
})(jQuery, window, document);

/**
 * Auto-init the plugin on DOM ready
 * @fires enhance.telia.priceToggle
 */
$(function() {
    $(document).trigger('enhance.telia.priceToggle');
});

;(function($, window, document) {
    var pluginName = 'teliaNotice';
    var defaults = {};
    var initSelector = '.notice';

    /**
     * Notice.
     *
     * @class
     * @param {HTMLElement} element - The HTML element the Notice should be bound to.
     * @param {Object} options - An option map.
     */
    function Notice(element, options) {
        this.element = element;
        this.$element = $(this.element);
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.dismissButton = this.$element.find('.notice__dismiss__btn');

        this.init();
    }

    /**
     * Initialize the plugin.
     *
     * @function init
     * @returns {void}
     */
    Notice.prototype.init = function() {
        this.dismissButton.on('click.telia.notice', this.remove.bind(this));
    };

    /**
     * Remove notice from DOM.
     *
     * @function remove
     * @returns {void}
     */
    Notice.prototype.remove = function() {
        this.$element.detach().trigger('teliaNotice.close').remove();
    };

    /**
     * Destroy the plugin instance.
     *
     * @function destroy
     * @returns {void}
     */
    Notice.prototype.destroy = function() {
        this.dismissButton.off('click.telia.notice');

        this.$element.removeData('plugin_' + pluginName);
    };

    /**
     * Create jQuery plugin.
     *
     * @param {Object} options - Extended options.
     * @listens telia.notice
     * @returns {Array<Notice>} Array of Notices.
     */
    $.fn[pluginName] = function(options) {
        var args = Array.prototype.slice.call(arguments, 1);

        return this.each(function() {
            var instance = $.data(this, 'plugin_' + pluginName);

            if (!instance) {
                if (options === 'destroy') {
                    return;
                }

                $.data(this, 'plugin_' + pluginName, new Notice(this, options));
            } else if (typeof options === 'string') {
                instance[options].apply(instance, args);
            }
        });
    };

    /**
     * @param {Event} event
     * @event enhance.telia.notice
     */
    $(document).on('enhance.telia.notice', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]();
    });

    /**
     * @param {Event} event
     * @event destroy.telia.notice
     */
    $(document).on('destroy.telia.notice', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]('destroy');
    });
})(jQuery, window, document);

/**
 * Auto-init the plugin on DOM ready.
 * @fires enhance.telia.notice
 */
$(function() {
    $(document).trigger('enhance.telia.notice');
});

/**
 * Old init for compatibility.
 *
 * @fires enhance.telia.notice
 * @deprecated Use enhance.telia.notice event directly.
 * @returns {void}
 */
function initNotice() {
    $(document).trigger('enhance.telia.notice');
}
window.initNotice = initNotice;

;(function($, window, document) {
    var pluginName = 'teliaReadmore';
    var defaults = {
        moreText: 'read more',
        chars: 100
    };
    var initSelector = '.js-readmore';

    /**
     * Readmore.
     *
     * @class
     * @param {HTMLElement} element - The HTML element the Readmore should be bound to.
     * @param {Object} options - An option map.
     */
    function Readmore(element, options) {
        this.element = element;
        this.$element = $(this.element);
        this.options = $.extend({}, defaults, options, this.$element.data());
        this._defaults = defaults;
        this._name = pluginName;
        this.$moreButton = $('<button class="btn btn--link readmore__button">' + this.options.moreText + '</button>');

        this._init();
    }

    /**
     * Initialize the plugin.
     *
     * @function init
     * @private
     * @returns {void}
     */
    Readmore.prototype._init = function() {
        this.originalText = this.$element.text();

        this.$moreButton.on('click.telia.readmore', this.show.bind(this));

        this.hide();
    };

    /**
     * Show the original text.
     *
     * @function show
     * @param   {Event} event - Event.
     * @returns {void}
     */
    Readmore.prototype.show = function(event) {
        if (event) {
            event.preventDefault();
        }

        this.$element.text(this.originalText);
    };

    /**
     * Show the trimmed text.
     *
     * @function hide
     * @returns {void}
     */
    Readmore.prototype.hide = function() {
        if (this.originalText.length > this.options.chars) {
            this.$element.text(this.originalText.substring(0, this.options.chars) + '... ');
            this.$element.append(this.$moreButton);
        }
    };

    /**
     * Destroy the plugin instance.
     *
     * @function destroy
     * @returns {void}
     */
    Readmore.prototype.destroy = function() {
        this.show();
        this.$element.removeData('plugin_' + pluginName);
    };

    /**
     * Create jQuery plugin.
     *
     * @param   {Object} options - Extended options.
     * @listens telia.readmore
     * @returns {Array<Readmore>} Array of Readmores.
     */
    $.fn[pluginName] = function(options) {
        var args = Array.prototype.slice.call(arguments, 1);

        return this.each(function() {
            var instance = $.data(this, 'plugin_' + pluginName);

            if (!instance) {
                if (options === 'destroy') {
                    return;
                }

                $.data(this, 'plugin_' + pluginName, new Readmore(this, options));
            } else if (typeof options === 'string') {
                instance[options].apply(instance, args);
            }
        });
    };

    /**
     * @param {Event} event
     * @event enhance.telia.readmore
     */
    $(document).on('enhance.telia.readmore', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]();
    });

    /**
     * @param {Event} event
     * @event destroy.telia.readmore
     */
    $(document).on('destroy.telia.readmore', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]('destroy');
    });
})(jQuery, window, document);

/**
 * Auto-init the plugin on DOM ready.
 * @fires enhance.telia.readmore
 */
$(function() {
    $(document).trigger('enhance.telia.readmore');
});

/* globals Sortable */

;(function($, window, document) {
    var pluginName = 'teliaSortableList';
    var defaults = {
        min: 0,
        max: 0
    };
    var initSelector = '.js-sortable-list';

    /**
     * SortableList.
     *
     * @class
     * @param {HTMLElement} element - The HTML element the SortableList should be bound to.
     * @param {Object} options - An option map.
     */
    function SortableList(element, options) {
        this.id = new Date().getTime();
        this.element = element;
        this.$element = $(this.element);
        this.options = $.extend({}, defaults, options, this.$element.data());
        this._defaults = defaults;
        this._name = pluginName;
        this.$selectedList = this.$element.find('.sortable-list__list--selected');
        this.$allList = this.$element.find('.sortable-list__list--all');
        this.$items = this.$element.find('.sortable-list__item');
        this.$count = this.$element.find('.js-sortable-list-count');
        this.$button = this.$element.find('.sortable-list__button');

        this.init();
    }

    /**
     * Initialize the plugin.
     *
     * @function init
     * @returns {void}
     */
    SortableList.prototype.init = function() {
        this.selectedSortable = new Sortable(this.$selectedList[0], {
            forceFallback: true,
            animation: 200,
            ghostClass: 'is-ghost',
            chosenClass: 'is-chosen',
            dragClass: 'is-dragged',
            fallbackClass: 'is-fallback',
            handle: '.sortable-list__handle',
            group: {
                name: this.id,
                pull: false,
                put: false
            },
            onSort: this._onChange.bind(this)
        });

        this.allSortable = new Sortable(this.$allList[0], {
            forceFallback: true,
            animation: 200,
            ghostClass: 'is-ghost',
            chosenClass: 'is-chosen',
            dragClass: 'is-dragged',
            fallbackClass: 'is-fallback',
            handle: '.sortable-list__handle',
            group: {
                name: this.id,
                pull: false,
                put: false
            }
        });
        this.$button.on('click.telia.sortableList', this._toggle.bind(this));
    };

    /**
     * Get selected items count.
     *
     * @function _getSelectedItemsCount
     * @private
     * @returns {number} Selected items count.
     */
    SortableList.prototype._getSelectedItemsCount = function() {
        return this.$selectedList.find('.sortable-list__item:visible:not(.is-ghost)').toArray().length;
    };

    /**
     * Get selected items ID array.
     *
     * @function _getSelectedItems
     * @private
     * @returns {Array<string>} Selected items IDs.
     */
    SortableList.prototype._getSelectedItems = function() {
        var items = [];

        this.$selectedList.find('.sortable-list__item:visible:not(.is-ghost)').each(function() {
            items.push($(this).attr('data-id'));
        });

        return items;
    };

    /**
     * Set selected element count in counter.
     *
     * @function _select
     * @private
     * @returns {void}
     */
    SortableList.prototype._select = function() {
        var items = this._getSelectedItemsCount();

        this.$count.text(items > this.options.max ? this.options.max : items);
    };

    /**
     * Toggle the location of an item.
     *
     * @function _toggle
     * @private
     * @param   {Event} event - Event object.
     * @returns {void}
     */
    SortableList.prototype._toggle = function(event) {
        var currentItem = $(event.currentTarget).closest('.sortable-list__item');

        event.preventDefault();

        if (currentItem.closest(this.$selectedList).length > 0 && this._getSelectedItemsCount() > this.options.min) {
            currentItem.appendTo(this.$allList);
            this._onChange();
        } else if (this._getSelectedItemsCount() < this.options.max) {
            currentItem.appendTo(this.$selectedList);
            this._onChange();
        }

        this._select();
    };

    /**
     * Trigger an event when something changes in the instance.
     *
     * @function _onChange
     * @private
     * @fires change.telia.sortableList
     * @returns {void}
     */
    SortableList.prototype._onChange = function() {
        this.$element.trigger('change.telia.sortableList', [this._getSelectedItems()]);
    };

    /**
     * Destroy the plugin instance.
     *
     * @function destroy
     * @returns {void}
     */
    SortableList.prototype.destroy = function() {
        this.selectedSortable.destroy();
        this.allSortable.destroy();
        this.$button.off('click.telia.sortableList');
        this.$element.removeData('plugin_' + pluginName);
    };

    /**
     * Create jQuery plugin.
     *
     * @param {Object} options - Extended options.
     * @listens telia.sortableList
     * @returns {Array<SortableList>} Array of SortableLists.
     */
    $.fn[pluginName] = function(options) {
        var args = Array.prototype.slice.call(arguments, 1);

        return this.each(function() {
            var instance = $.data(this, 'plugin_' + pluginName);

            if (!instance) {
                if (options === 'destroy') {
                    return;
                }

                $.data(this, 'plugin_' + pluginName, new SortableList(this, options));
            } else if (typeof options === 'string') {
                instance[options].apply(instance, args);
            }
        });
    };

    /**
     * @param {Event} event
     * @event enhance.telia.sortableList
     */
    $(document).on('enhance.telia.sortableList', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]();
    });

    /**
     * @param {Event} event
     * @event destroy.telia.sortableList
     */
    $(document).on('destroy.telia.sortableList', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]('destroy');
    });
})(jQuery, window, document);

/**
 * Auto-init the plugin on DOM ready.
 * @fires enhance.telia.sortableList
 */
$(function() {
    $(document).trigger('enhance.telia.sortableList');
});

/* ! Tablesaw - v3.0.0-beta.4 - 2016-10-12
* https://github.com/filamentgroup/tablesaw
* Copyright (c) 2016 Filament Group; Licensed MIT */

;(function($) {
    var Tablesaw;

    /*
    * tablesaw: A set of plugins for responsive tables
    * Stack and Column Toggle tables
    * Copyright (c) 2013 Filament Group, Inc.
    * MIT License
    */

    if (typeof Tablesaw === 'undefined') {
        Tablesaw = {
            i18n: {
                modes: ['Stack', 'Swipe', 'Toggle'],
                columns: 'Col<span class=\"a11y-sm\">umn</span>s',
                columnBtnText: 'Columns',
                columnsDialogError: 'No eligible columns.',
                sort: 'Sort'
            },
            // cut the mustard
            mustard: ('querySelector' in document) &&
            ('head' in document) &&
            (!window.blackberry || window.WebKitPoint) &&
            !window.operamini
        };
    }
    if (!Tablesaw.config) {
        Tablesaw.config = {};
    }
    if (Tablesaw.mustard) {
        $(document.documentElement).addClass('tablesaw-enhanced');
    }

    (function() {
        var pluginName = 'tablesaw';
        var classes = {
            toolbar: 'tablesaw-bar'
        };
        var events = {
            create: 'tablesawcreate',
            destroy: 'tablesawdestroy',
            refresh: 'tablesawrefresh'
        };
        var defaultMode = 'stack';
        var initSelector = 'table[data-tablesaw-mode],table[data-tablesaw-sortable]';

        /**
         * Table.
         *
         * @class
         * @param {HTMLElement} element - The HTML element the Table should be bound to.
         */
        function Table(element) {
            if (!element) {
                throw new Error('Tablesaw requires an element.');
            }

            this.table = element;
            this.$table = $(element);

            this.mode = this.$table.attr('data-tablesaw-mode') || defaultMode;

            this.init();
        };

        /**
         * Init tablesaw.
         *
         * @function init
         * @returns {void}
         */
        Table.prototype.init = function() {
            // assign an id if there is none
            if (!this.$table.attr('id')) {
                this.$table.attr('id', pluginName + '-' + Math.round(Math.random() * 10000));
            }

            this.createToolbar();

            var colstart = this._initCells();

            this.$table.trigger(events.create, [this, colstart]);
        };

        /**
         * Initialize cells.
         *
         * @private
         * @function _initCells
         * @returns {number} - Number of columns.
         */
        Table.prototype._initCells = function() {
            var colstart;
            var thrs = this.table.querySelectorAll('thead tr');
            var self = this;

            $(thrs).each(function() {
                var coltally = 0;

                var children = $(this).children();
                var columnlookup = [];

                children.each(function() {
                    var span = parseInt(this.getAttribute('colspan'), 10);

                    columnlookup[coltally] = this;
                    colstart = coltally + 1;

                    if (span) {
                        for (var k = 0; k < span - 1; k++) {
                            coltally++;
                            columnlookup[coltally] = this;
                        }
                    }
                    this.cells = [];
                    coltally++;
                });
                // Note that this assumes that children() returns its results in document order. jQuery doesn't
                // promise that in the docs, but it's a pretty safe assumption.
                self.$table.find('> * > tr').not(thrs[0]).each(function() {
                    var cellcoltally = 0;

                    $(this).children().each(function() {
                        var span = parseInt(this.getAttribute('colspan'), 10);

                        if (columnlookup[cellcoltally] && columnlookup[cellcoltally].cells) {
                            columnlookup[cellcoltally].cells.push(this);
                        }
                        if (span) {
                            cellcoltally += span;
                        } else {
                            cellcoltally++;
                        }
                    });
                });
            });

            return colstart;
        };

        /**
         * Refresh table.
         *
         * @function refresh
         * @returns {void}
         */
        Table.prototype.refresh = function() {
            this._initCells();

            this.$table.trigger(events.refresh);
        };

        /**
         * Create toolbar.
         *
         * @function createToolbar
         * @returns {void}
         */
        Table.prototype.createToolbar = function() {
            // Insert the toolbar
            // TODO move this into a separate component
            var $toolbar = this.$table.prev().filter('.' + classes.toolbar);

            if (!$toolbar.length) {
                $toolbar = $('<div>')
                    .addClass(classes.toolbar)
                    .insertBefore(this.$table);
            }
            this.$toolbar = $toolbar;

            if (this.mode) {
                this.$toolbar.addClass('tablesaw-mode-' + this.mode);
            }
        };

        /**
         * Destroy plugin instance.
         *
         * @function destroy
         * @returns {void}
         */
        Table.prototype.destroy = function() {
            // Don’t remove the toolbar. Some of the table features are not yet destroy-friendly.
            this.$table.prev().filter('.' + classes.toolbar).each(function() {
                this.className = this.className.replace(/\btablesaw-mode\-\w*\b/gi, '');
            });

            var tableId = this.$table.attr('id');

            $(document).off('.' + tableId);
            $(window).off('.' + tableId);

            // other plugins
            this.$table.trigger(events.destroy, [this]);

            this.$table.removeData(pluginName);
        };

        /**
         * Create jQuery plugin.
         *
         * @listens enhance.tablesaw
         * @returns {Array<Tablesaw>} Array of Tablesaw.
         */
        $.fn[pluginName] = function() {
            return this.each(function() {
                var $t = $(this);

                if ($t.data(pluginName)) {
                    return;
                }

                var table = new Table(this);

                $t.data(pluginName, table);
            });
        };

        /**
         * @param {Event} event
         * @event enhance.tablesaw
         */
        $(document).on('enhance.tablesaw', function(e) {
            // Cut the mustard
            if (Tablesaw.mustard) {
                $(e.target).find(initSelector)[pluginName]();
            }
        });
    }());

    ;(function() {
        var classes = {
            stackTable: 'tablesaw-stack',
            cellLabels: 'tablesaw-cell-label',
            cellContentLabels: 'tablesaw-cell-content'
        };

        var data = {
            obj: 'tablesaw-stack'
        };

        var attrs = {
            labelless: 'data-tablesaw-no-labels',
            hideempty: 'data-tablesaw-hide-empty'
        };

        /**
         * Stack.
         *
         * @class
         * @param {HTMLElement} element - The HTML element the Stack should be bound to.
         */
        function Stack(element) {
            this.$table = $(element);

            this.labelless = this.$table.is('[' + attrs.labelless + ']');
            this.hideempty = this.$table.is('[' + attrs.hideempty + ']');

            if (!this.labelless) {
                // allHeaders references headers, plus all THs in the thead, which may include several rows, or not
                this.allHeaders = this.$table.find('> thead th');
            }

            this.$table.data(data.obj, this);
        };

        /**
         * Initialize Stack.
         *
         * @function init
         * @param  {number} colstart - Number of columns.
         * @returns {void}
         */
        Stack.prototype.init = function(colstart) {
            this.$table.addClass(classes.stackTable);

            if (this.labelless) {
                return;
            }

            // get headers in reverse order so that top-level headers are appended last
            var reverseHeaders = $($(this.allHeaders).get().reverse());
            var hideempty = this.hideempty;
            var headerStart = colstart;


            // create the hide/show toggles
            reverseHeaders.each(function() {
                var $t = $(this);
                var $cells = $(this.cells).filter(function() {
                    return !$(this).parent().is('[' + attrs.labelless + ']') && !$(this).is('[' + attrs.labelless + ']') && (!hideempty || !$(this).is(':empty'));
                });
                var hierarchyClass = $cells.not(this).filter('thead th').length;
                // TODO reduce coupling with sortable
                var $sortableButton = $t.find('.tablesaw-sortable-btn');
                var html = $sortableButton.length ? $sortableButton.html() : $t.html();

                if (html !== '') {
                    if (hierarchyClass) {
                        var iteration = parseInt($(this).attr('colspan'), 10);
                        var filter = ':nth-child(n)';

                        if ($(this).is(':last-child')) {
                            headerStart = colstart;
                        }

                        if (iteration) {
                            filter = 'td:nth-child(' + (headerStart - iteration + 1) + ')';
                            headerStart -= iteration;
                            hierarchyClass = ' tablesaw-cell-label-top';
                        } else {
                            headerStart -= 1;
                            hierarchyClass = '';
                        }

                        $cells.filter(filter).prepend('<b class=\'' + classes.cellLabels + hierarchyClass + '\'>' + html + '</b>');
                    } else {
                        $cells.wrapInner('<span class=\'' + classes.cellContentLabels + '\'></span>');
                        $cells.prepend('<b class=\'' + classes.cellLabels + '\'>' + html + '</b>');
                    }
                }
            });
        };

        /**
         * Destroys plugin instance.
         *
         * @function destroy
         * @returns {void}
         */
        Stack.prototype.destroy = function() {
            this.$table.removeClass(classes.stackTable);
            this.$table.find('.' + classes.cellLabels).remove();
            this.$table.find('.' + classes.cellContentLabels).each(function() {
                $(this).replaceWith(this.childNodes);
            });
        };

        /**
         * @param {Event} event
         * @event tablesawcreate
         */
        $(document).on('tablesawcreate', function(e, tablesaw, colstart) {
            if (tablesaw.mode === 'stack') {
                var table = new Stack(tablesaw.table);

                table.init(colstart);
            }
        });

        /**
         * @param {Event} event
         * @event tablesawdestroy
         */
        $(document).on('tablesawdestroy', function(e, tablesaw) {
            if (tablesaw.mode === 'stack') {
                $(tablesaw.table).data(data.obj).destroy();
            }
        });
    }());
})(jQuery);


/* ! Tablesaw - v2.0.2 - 2015-10-27
* https://github.com/filamentgroup/tablesaw
* Copyright (c) 2015 Filament Group; Licensed MIT */
(function($) {
    /**
     * Auto-init the plugin on DOM ready.
     * @fires enhance.tablesaw
     */
    $(function() {
        $(document).trigger('enhance.tablesaw');
    });
})(jQuery);

/*
 * tablesaw: A set of plugins for responsive tables
 * Sortable column headers
 * Copyright (c) 2013 Filament Group, Inc.
 * MIT License
 */

/* globals Tablesaw */

;(function($) {
    /**
     * Get sorting value.
     *
     * @param  {HTMLElement} cell - Cell element.
     * @returns {Array[]} Array of cell element values.
     */
    function getSortValue(cell) {
        return $.map(cell.childNodes, function(el) {
            var $el = $(el);

            if ($el.is('input, select')) {
                return $el.val();
            } else if ($el.hasClass('tablesaw-cell-label')) {
                return;
            }

            return $.trim($el.text());
        }).join('');
    }

    var pluginName = 'tablesaw-sortable';
    var initSelector = 'table[data-' + pluginName + ']';
    var sortableSwitchSelector = '[data-' + pluginName + '-switch]';
    var attrs = {
        defaultCol: 'data-tablesaw-sortable-default-col',
        numericCol: 'data-tablesaw-sortable-numeric'
    };
    var classes = {
        head: pluginName + '__head',
        ascend: pluginName + '-ascending',
        descend: pluginName + '-descending',
        switcher: pluginName + '-switch',
        tableToolbar: 'tablesaw-toolbar',
        sortButton: pluginName + '-btn'
    };
    var methods = {
        /**
         * Create sortable table instance.
         *
         * @private
         * @function _create
         * @param {Object} o - Options object.
         * @returns {Array} Array of jQuery plugin instances.
         */
        _create: function(o) {
            return $(this).each(function() {
                var init = $(this).data('init' + pluginName);

                if (init) {
                    return false;
                }
                $(this)
                    .data('init' + pluginName, true)
                    .trigger('beforecreate.' + pluginName)[pluginName]('_init', o)
                    .trigger('create.' + pluginName);
            });
        },

        /**
         * Initialize the plugin.
         *
         * @private
         * @function _init
         * @returns {void}
         */
        _init: function() {
            var el = $(this);
            var heads;
            var $switcher;

            /**
             * Add class to table.
             *
             * @private
             * @returns {void}
             */
            function addClassToTable() {
                el.addClass(pluginName);
            }

            /**
             * Add class to thead.
             *
             * @param {Array[]} h - <thead> element.
             * @returns {void}
             */
            function addClassToHeads(h) {
                $.each(h, function(i, v) {
                    $(v).addClass(classes.head);
                });
            }

            /**
             * Sortable button on click callback.
             *
             * @callback sortableButtonOnClick
             * @param {Event} event - Event object.
             */

            /**
             * Make thead elements clickable.
             *
             * @param  {Array[]} h -  <th> element.
             * @param  {sortableButtonOnClick} fn - Trigger function.
             * @returns {void}
             */
            function makeHeadsActionable(h, fn) {
                $.each(h, function(i, v) {
                    if (v.innerText) {
                        v.innerText = v.innerText.trim();

                        // if node still has value, it is text node and should be wrapped in span
                        if (v.innerText) {
                            $(v).wrapInner('<span class="tablesaw-sortable-btn__text" />');
                        }
                    }

                    var b = $('<button class=\'' + classes.sortButton + '\'/>');

                    b.on('click', {
                        col: v
                    }, fn);
                    $(v).append($('<span class="table-sort-indicator"></span>'));
                    $(v).wrapInner(b);
                });
            }

            /**
             * Clear other columns.
             *
             * @param  {Array[]} sibs - Sibling elements.
             * @returns {void}
             */
            function clearOthers(sibs) {
                $.each(sibs, function(i, v) {
                    var col = $(v);

                    col.removeAttr(attrs.defaultCol);
                    col.removeClass(classes.ascend);
                    col.removeClass(classes.descend);
                });
            }

            /**
             * Set th actions.
             *
             * @param  {Event} e - Event.
             * @returns {void}
             */
            function headsOnAction(e) {
                if ($(e.target).is('a[href]')) {
                    return;
                }

                e.stopPropagation();
                var head = $(this).parent();
                var v = e.data.col;
                var newSortValue = heads.index(head);

                clearOthers(head.siblings());
                if (head.hasClass(classes.descend)) {
                    el[pluginName]('sortBy', v, true);
                    newSortValue += '_asc';
                } else {
                    el[pluginName]('sortBy', v);
                    newSortValue += '_desc';
                }
                if ($switcher) {
                    $switcher.find('select').val(newSortValue).trigger('refresh');
                }

                e.preventDefault();
            }

            /**
             * Handle default sorting.
             *
             * @param {Array[]} heads - <th> elements.
             * @returns {void}
             */
            function handleDefault(heads) {
                var _el = el;

                $.each(heads, function(idx, el) {
                    var $el = $(el);

                    if ($el.is('[' + attrs.defaultCol + ']')) {
                        if (!$el.hasClass(classes.descend)) {
                            $el.addClass(classes.ascend);
                            _el[pluginName]('sortBy', $el, true);
                        } else {
                            _el[pluginName]('sortBy', $el);
                        }
                    }
                });
            }

            /**
             * Add switcher to DOM.
             *
             * @param {Array[]} heads - Array of th elements.
             * @returns {HTMLElement} Switcher element.
             */
            function addSwitcher(heads) {
                $switcher = $('<div>').addClass(classes.switcher).addClass(classes.tableToolbar).html(function() {
                    var html = ['<label>' + Tablesaw.i18n.sort + ':'];

                    html.push('<span class="btn btn-small">&#160;<select>');
                    heads.each(function(j) {
                        var $t = $(this);
                        var isDefaultCol = $t.is('[' + attrs.defaultCol + ']');
                        var isDescending = $t.hasClass(classes.descend);

                        var hasNumericAttribute = $t.is('[data-sortable-numeric]');
                        var numericCount = 0;
                        // Check only the first four rows to see if the column is numbers.
                        var numericCountMax = 5;

                        $(this.cells).slice(0, numericCountMax).each(function() {
                            if (!isNaN(parseInt(getSortValue(this), 10))) {
                                numericCount++;
                            }
                        });
                        var isNumeric = numericCount === numericCountMax;

                        if (!hasNumericAttribute) {
                            $t.attr('data-sortable-numeric', isNumeric ? '' : 'false');
                        }

                        html.push('<option' + (isDefaultCol && !isDescending ? ' selected' : '') + ' value="' + j + '_asc">' + $t.text() + ' ' + (isNumeric ? '&#x2191;' : '(A-Z)') + '</option>');
                        html.push('<option' + (isDefaultCol && isDescending ? ' selected' : '') + ' value="' + j + '_desc">' + $t.text() + ' ' + (isNumeric ? '&#x2193;' : '(Z-A)') + '</option>');
                    });
                    html.push('</select></span></label>');

                    return html.join('');
                });

                var $toolbar = el.prev().filter('.tablesaw-bar');
                var $firstChild = $toolbar.children().eq(0);

                if ($firstChild.length) {
                    $switcher.insertBefore($firstChild);
                } else {
                    $switcher.appendTo($toolbar);
                }
                $switcher.find('.btn').tablesawbtn();
                $switcher.find('select').on('change', function() {
                    var val = $(this).val().split('_');
                    var head = heads.eq(val[0]);

                    clearOthers(head.siblings());
                    el[pluginName]('sortBy', head.get(0), val[1] === 'asc');
                });
            }

            addClassToTable();
            heads = el.find('thead th[data-' + pluginName + '-col]');
            addClassToHeads(heads);
            makeHeadsActionable(heads, headsOnAction);
            handleDefault(heads);

            if (el.is(sortableSwitchSelector)) {
                addSwitcher(heads, el.find('tbody tr:nth-child(-n+3)'));
            }
        },

        /**
         * Get column number.
         *
         * @function getColumnNumber
         * @param {[type]} col - Column whose number to get.
         * @returns {number} Column number.
         */
        getColumnNumber: function(col) {
            return $(col).prevAll().length;
        },

        /**
         * Get rows in the table body.
         *
         * @function getTableRows
         * @returns {JQuery} Rows in the table body.
         */
        getTableRows: function() {
            return $(this).find('tbody tr');
        },

        /**
         * Sort rows in the table.
         *
         * @function sortRows
         * @param {Array} rows - Rows in the table.
         * @param {number} colNum - Number of the column by which we're sorting the table.
         * @param {boolean} ascending - If sorting should be ascending or descending.
         * @param {JQuery} col - Column by which we're sorting the table.
         * @returns {Array} Sorted rows.
         */
        sortRows: function(rows, colNum, ascending, col) {
            var cells;
            var fn;
            var sorted;

            /**
             * Get cells from rows.
             *
             * @param {Array[]} rows - Array of rows.
             * @returns {Array[]} Array of cells.
             */
            function getCells(rows) {
                var cells = [];

                $.each(rows, function(i, r) {
                    var element = $(r).children().get(colNum);

                    cells.push({
                        element: element,
                        cell: getSortValue(element),
                        rowNum: i
                    });
                });

                return cells;
            }

            /**
             * Sorting function.
             *
             * @param {boolean} ascending -Direction to sort.
             * @param {boolean} forceNumeric - Force numeric sorting.
             * @returns {number} Order position.
             */
            function getSortFxn(ascending, forceNumeric) {
                var fn;
                var regex = /[^\-\+\d\.]/g;

                if (ascending) {
                    fn = function(a, b) {
                        if (forceNumeric) {
                            return parseFloat(a.cell.replace(regex, '')) - parseFloat(b.cell.replace(regex, ''));
                        } else {
                            return a.cell.toLowerCase() > b.cell.toLowerCase() ? 1 : -1;
                        }
                    };
                } else {
                    fn = function(a, b) {
                        if (forceNumeric) {
                            return parseFloat(b.cell.replace(regex, '')) - parseFloat(a.cell.replace(regex, ''));
                        } else {
                            return a.cell.toLowerCase() < b.cell.toLowerCase() ? 1 : -1;
                        }
                    };
                }

                return fn;
            }

            /**
             * Apply rows.
             *
             * @param {Array[]} sorted - Sorted elements.
             * @param {Array[]} rows - Old rows.
             * @returns {Array[]} New rows.
             */
            function applyToRows(sorted, rows) {
                var newRows = [];
                var i;
                var l;
                var cur;

                for (i = 0, l = sorted.length; i < l; i++) {
                    cur = sorted[i].rowNum;
                    newRows.push(rows[cur]);
                }

                return newRows;
            };

            cells = getCells(rows);
            var customFn = $(col).data('tablesaw-sort');

            fn = (customFn && typeof customFn === 'function' ? customFn(ascending) : false) ||
                getSortFxn(ascending, $(col).is('[data-sortable-numeric]') && !$(col).is('[data-sortable-numeric="false"]'));
            sorted = cells.sort(fn);
            rows = applyToRows(sorted, rows);

            return rows;
        },

        /**
         * Replace rows in the table with sorted rows.
         *
         * @function replaceTableRows
         * @param {Array} rows - New, sorted rows.
         * @returns {void}
         */
        replaceTableRows: function(rows) {
            var el = $(this);
            var body = el.find('tbody');

            body.html(rows);
        },

        /**
         * Make column the default column.
         *
         * @function makeColDefault
         * @param {HTMLElement} col - Column that should be made default.
         * @param {boolean} a - If is ascending.
         * @returns {void}
         */
        makeColDefault: function(col, a) {
            var c = $(col);

            c.attr(attrs.defaultCol, 'true');
            if (a) {
                c.removeClass(classes.descend);
                c.addClass(classes.ascend);
            } else {
                c.removeClass(classes.ascend);
                c.addClass(classes.descend);
            }
        },

        /**
         * Sort rows by column.
         *
         * @function sortBy
         * @param {HTMLElement} col - Column by which should sort.
         * @param {boolean} ascending - If should sort ascending.
         * @returns {void}
         */
        sortBy: function(col, ascending) {
            var el = $(this);
            var colNum;
            var rows;

            colNum = el[pluginName]('getColumnNumber', col);
            rows = el[pluginName]('getTableRows');
            rows = el[pluginName]('sortRows', rows, colNum, ascending, col);
            el[pluginName]('replaceTableRows', rows);
            el[pluginName]('makeColDefault', col, ascending);
        }
    };

    /**
     * Create jQuery plugin.
     *
     * @param {Object} arrg - Extended options.
     * @listens tablesawcreate
     * @returns {Array<TablesawSortable>} Array of TablesawSortable.
     */
    $.fn[pluginName] = function(arrg) {
        var args = Array.prototype.slice.call(arguments, 1);
        var returnVal;

        // if it's a method
        if (arrg && typeof (arrg) === 'string') {
            returnVal = $.fn[pluginName].prototype[arrg].apply(this[0], args);

            return (typeof returnVal !== 'undefined') ? returnVal : $(this);
        }
        // check init
        if (!$(this).data(pluginName + 'data')) {
            $(this).data(pluginName + 'active', true);
            $.fn[pluginName].prototype._create.call(this, arrg);
        }

        return $(this);
    };
    // add methods
    $.extend($.fn[pluginName].prototype, methods);

    /**
     * @param {Event} event
     * @param {Object} Tablesaw
     * @event tablesawcreate
     */
    $(document).on('tablesawcreate', function(e, Tablesaw) {
        if (Tablesaw.$table.is(initSelector)) {
            Tablesaw.$table[pluginName]();
        }
    });
}(jQuery));


;(function($, window, document) {
    var pluginName = 'teliaTableHighlight';
    var defaults = {};
    var initSelector = '.js-table-highlight';

    /**
     * TableHighlight.
     *
     * @class
     * @param {HTMLElement} element - The HTML element the TableHighlight should be bound to.
     * @param {Object} options - An option map.
     */
    function TableHighlight(element, options) {
        this.element = element;
        this.$element = $(this.element);
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.break = parseInt(this.$element.data('columns-break'), 10) || 0;
        this.$table = this.$element.find('.table-highlight__table');
        this.$highlightOld = $('<div class="table-highlight__background table-highlight__background--left"></div>');
        this.$highlightNew = $('<div class="table-highlight__background table-highlight__background--right"></div>');

        this.init();
    }

    /**
     * Initialize the plugin.
     *
     * @function init
     * @returns {void}
     */
    TableHighlight.prototype.init = function() {
        this.$element.prepend(this.$highlightOld);
        this.$element.prepend(this.$highlightNew);
        this.highlight();
        this.$element.addClass('table-highlight--enhanced');

        $(window).on('resize.telia.tableHighlight', this.highlight.bind(this));
    };

    /**
     * Highlight cells.
     *
     * @function highlight
     * @returns {void}
     */
    TableHighlight.prototype.highlight = function() {
        var oldWidth = 0;
        var newWidth = 0;
        var limit = this.break;

        this.$table.find('tbody tr:first-child td').each(function() {
            var elem = $(this);
            var elIndex = elem.index();

            if (elIndex < limit) {
                oldWidth += elem.outerWidth();
            } else {
                newWidth += elem.outerWidth();
            }
        });

        this.$highlightOld.css({
            width: oldWidth
        });

        this.$highlightNew.css({
            left: oldWidth,
            width: newWidth
        });
    };

    /**
     * Destroy the plugin instance.
     *
     * @function destroy
     * @returns {void}
     */
    TableHighlight.prototype.destroy = function() {
        this.$highlightOld.remove();
        this.$highlightNew.remove();
        this.$element.removeClass('table-highlight--enhanced');

        $(window).off('resize.telia.tableHighlight', this.highlight);

        this.$element.removeData('plugin_' + pluginName);
    };

    /**
     * Create jQuery plugin.
     *
     * @param {Object} options - Extended options.
     * @listens telia.tableHighlight
     * @returns {Array<TableHighlight>} Array of TableHighlights.
     */
    $.fn[pluginName] = function(options) {
        var args = Array.prototype.slice.call(arguments, 1);

        return this.each(function() {
            var instance = $.data(this, 'plugin_' + pluginName);

            if (!instance) {
                if (options === 'destroy') {
                    return;
                }

                $.data(this, 'plugin_' + pluginName, new TableHighlight(this, options));
            } else if (typeof options === 'string') {
                instance[options].apply(instance, args);
            }
        });
    };

    /**
     * @param {Event} event
     * @event enhance.telia.tableHighlight
     */
    $(document).on('enhance.telia.tableHighlight', function(event) {
        $(event.target).find(initSelector)[pluginName]();
    });

    /**
     * @param {Event} even
     * @event destroy.telia.tableHighlight
     */
    $(document).on('destroy.telia.tableHighlight', function(event) {
        $(event.target).find(initSelector)[pluginName]('destroy');
    });
})(jQuery, window, document);

/**
 * Auto-init the plugin on DOM ready.
 * @fires enhance.telia.tableHighlight
 */
$(function() {
    $(document).trigger('enhance.telia.tableHighlight');
});

/* ========================================================================
* bootstrap-tour - v0.10.1
* http://bootstraptour.com
* ========================================================================
* Copyright 2012-2013 Ulrich Sossou
*
* ========================================================================
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* ========================================================================
*/

(function($, window) {
    var Tour;
    var document;

    document = window.document;
    Tour = (function() {
        /**
         * Tour.
         *
         * @param {Object} options - An options map.
         * @class
         */
        function Tour(options) {
            var storage;

            try {
                storage = window.localStorage;
            } catch (_error) {
                storage = false;
            }
            this._options = $.extend({
                name: 'tour',
                steps: [],
                container: 'body',
                autoscroll: true,
                keyboard: true,
                storage: storage,
                debug: false,
                backdrop: false,
                backdropPadding: 0,
                redirect: true,
                orphan: false,
                duration: false,
                delay: false,
                basePath: '',
                template: '<div class="tooltip tooltip--blue" role="tooltip"> <div class="tooltip__arrow-triangle"></div> <h4 class="tooltip__title"></h4> <div class="tooltip__content"></div> <div class="tooltip__navigation">  <button class="btn btn--variant-dark" data-role="next" data-end-text="Sulgen">Järgmine ' + telia.icon.render('#arrow-right', 'btn__icon-right') + '</button> <button class="btn btn-sm btn-default" data-role="pause-resume" data-pause-text="Pause" data-resume-text="Resume">Paus</button> <a href="#" class="tooltip__close" data-role="end">' + telia.icon.render('#close-round') + '</a> </div> </div>',

                /**
                 * Callback called after setState.
                 *
                 * @returns {void}
                 */
                afterSetState: function() {},

                /**
                 * Callback called after getState.
                 *
                 * @returns {void}
                 */
                afterGetState: function() {},

                /**
                 * Callback called after removeState.
                 *
                 * @returns {void}
                 */
                afterRemoveState: function() {},

                /**
                 * Callback called onStart.
                 *
                 * @returns {void}
                 */
                onStart: function() {},

                /**
                 * Callback called onEnd.
                 *
                 * @returns {void}
                 */
                onEnd: function() {},

                /**
                 * Callback called onShow.
                 *
                 * @returns {void}
                 */
                onShow: function() {},

                /**
                 * Callback called onShown.
                 *
                 * @returns {void}
                 */
                onShown: function() {},

                /**
                 * Callback called onHide.
                 *
                 * @returns {void}
                 */
                onHide: function() {},

                /**
                 * Callback called onHidden.
                 *
                 * @returns {void}
                 */
                onHidden: function() {},

                /**
                 * Callback called onNext.
                 *
                 * @returns {void}
                 */
                onNext: function() {},

                /**
                 * Callback called onPrev.
                 *
                 * @returns {void}
                 */
                onPrev: function() {},

                /**
                 * Callback called onPause.
                 *
                 * @returns {void}
                 */
                onPause: function() {},

                /**
                 * Callback called onResume.
                 *
                 * @returns {void}
                 */
                onResume: function() {}
            }, options);
            this._force = false;
            this._inited = false;
            this.backdrop = {
                overlay: null,
                $element: null,
                $background: null,
                backgroundShown: false,
                overlayElementShown: false
            };
            this;
        }

        /**
         * Add steps.
         *
         * @function addSteps
         * @param  {Array[]} steps - Array of steps.
         * @returns {Object} - Tour object.
         */
        Tour.prototype.addSteps = function(steps) {
            var step;
            var _i;
            var _len;

            for (_i = 0, _len = steps.length; _i < _len; _i++) {
                step = steps[_i];
                this.addStep(step);
            }

            return this;
        };

        /**
         * Add one step.
         *
         * @function addStep
         * @param  {Object} step - One step object.
         * @returns {Object} - Tour object.
         */
        Tour.prototype.addStep = function(step) {
            this._options.steps.push(step);

            return this;
        };

        /**
         * Get step.
         *
         * @function getStep
         * @param  {number} i - Step index.
         * @returns {Object} - Step.
         */
        Tour.prototype.getStep = function(i) {
            if (this._options.steps[i] != null) {
                return $.extend({
                    id: 'step-' + i,
                    path: '',
                    placement: 'right',
                    title: '',
                    content: '<p></p>',
                    next: i === this._options.steps.length - 1 ? -1 : i + 1,
                    prev: i - 1,
                    animation: true,
                    container: this._options.container,
                    autoscroll: this._options.autoscroll,
                    backdrop: this._options.backdrop,
                    backdropPadding: this._options.backdropPadding,
                    redirect: this._options.redirect,
                    orphan: this._options.orphan,
                    duration: this._options.duration,
                    delay: this._options.delay,
                    template: this._options.template,
                    onShow: this._options.onShow,
                    onShown: this._options.onShown,
                    onHide: this._options.onHide,
                    onHidden: this._options.onHidden,
                    onNext: this._options.onNext,
                    onPrev: this._options.onPrev,
                    onPause: this._options.onPause,
                    onResume: this._options.onResume
                }, this._options.steps[i]);
            }
        };

        /**
         * Initialize Tour.
         *
         * @function init
         * @param  {boolean} force - Force tour.
         * @returns {Object} - Tour object.
         */
        Tour.prototype.init = function(force) {
            this._force = force;
            if (this.ended()) {
                this._debug('Tour ended, init prevented.');

                return this;
            }
            this.setCurrentStep();
            this._initMouseNavigation();
            this._initKeyboardNavigation();
            this._onResize((function(_this) {
                return function() {
                    return _this.showStep(_this._current);
                };
            })(this));
            if (this._current !== null) {
                this.showStep(this._current);
            }
            this._inited = true;

            return this;
        };

        /**
         * Start tour.
         *
         * @function start
         * @param  {boolean} force - Force start.
         * @returns {Object} - Tour object.
         */
        Tour.prototype.start = function(force) {
            var promise;

            if (force == null) {
                force = false;
            }
            if (!this._inited) {
                this.init(force);
            }
            if (this._current === null) {
                promise = this._makePromise(this._options.onStart != null ? this._options.onStart(this) : void 0);
                this._callOnPromiseDone(promise, this.showStep, 0);
            }

            return this;
        };

        /**
         * Call next step.
         *
         * @function next
         * @returns {Object} - Tour object.
         */
        Tour.prototype.next = function() {
            var promise;

            promise = this.hideStep(this._current);

            return this._callOnPromiseDone(promise, this._showNextStep);
        };

        /**
         * Call previous step.
         *
         * @function prev
         * @returns {Object} - Tour object.
         */
        Tour.prototype.prev = function() {
            var promise;

            promise = this.hideStep(this._current);

            return this._callOnPromiseDone(promise, this._showPrevStep);
        };

        /**
         * Go to specific step.
         *
         * @function goTo
         * @param  {number} i - Step index.
         * @returns {Object} - Tour object.
         */
        Tour.prototype.goTo = function(i) {
            var promise;

            promise = this.hideStep(this._current);

            return this._callOnPromiseDone(promise, this.showStep, i);
        };

        /**
         * End tour.
         *
         * @function end
         * @returns {Object} - Tour object.
         */
        Tour.prototype.end = function() {
            var endHelper;
            var promise;

            endHelper = (function(_this) {
                return function() {
                    $(document).off('click.tour-' + _this._options.name);
                    $(document).off('keyup.tour-' + _this._options.name);
                    $(window).off('resize.tour-' + _this._options.name);
                    _this._setState('end', 'yes');
                    _this._inited = false;
                    _this._force = false;
                    _this._clearTimer();
                    if (_this._options.onEnd != null) {
                        return _this._options.onEnd(_this);
                    }
                };
            })(this);
            promise = this.hideStep(this._current);

            return this._callOnPromiseDone(promise, endHelper);
        };

        /**
         * Check if tour has ended.
         *
         * @function ended
         * @returns {boolean} - Has tour ended.
         */
        Tour.prototype.ended = function() {
            return !this._force && !!this._getState('end');
        };

        /**
         * Restart tour.
         *
         * @function restart
         * @returns {Object} - Tour object.
         */
        Tour.prototype.restart = function() {
            this._removeState('current_step');
            this._removeState('end');

            return this.start();
        };

        /**
         * Pause tour.
         *
         * @function pause
         * @returns {Object} - Tour object.
         */
        Tour.prototype.pause = function() {
            var step;

            step = this.getStep(this._current);
            if (!(step && step.duration)) {
                return this;
            }
            this._paused = true;
            this._duration -= new Date().getTime() - this._start;
            window.clearTimeout(this._timer);
            this._debug('Paused/Stopped step ' + (this._current + 1) + ' timer (' + this._duration + ' remaining).');
            if (step.onPause != null) {
                return step.onPause(this, this._duration);
            }
        };

        /**
         * Resume to tour.
         *
         * @function resume
         * @returns {Object} - Tour object.
         */
        Tour.prototype.resume = function() {
            var step;

            step = this.getStep(this._current);
            if (!(step && step.duration)) {
                return this;
            }
            this._paused = false;
            this._start = new Date().getTime();
            this._duration = this._duration || step.duration;
            this._timer = window.setTimeout((function(_this) {
                return function() {
                    if (_this._isLast()) {
                        return _this.next();
                    } else {
                        return _this.end();
                    }
                };
            })(this), this._duration);
            this._debug('Started step ' + (this._current + 1) + ' timer with duration ' + this._duration);
            if ((step.onResume != null) && this._duration !== step.duration) {
                return step.onResume(this, this._duration);
            }
        };

        /**
         * Hide specific step.
         *
         * @function hideStep
         * @param  {number} i - Index of step to hide.
         * @returns {Promise} - Hide step.
         */
        Tour.prototype.hideStep = function(i) {
            var hideStepHelper;
            var promise;
            var step;

            step = this.getStep(i);
            if (!step) {
                return;
            }
            this._clearTimer();
            promise = this._makePromise(step.onHide != null ? step.onHide(this, i) : void 0);
            hideStepHelper = (function(_this) {
                return function() {
                    var $element;

                    $element = $(step.element);
                    if (!($element.data('bs.tooltip') || $element.data('tooltip'))) {
                        $element = $('body');
                    }
                    $element.teliaTooltip('destroy').removeClass('tour-' + _this._options.name + '-element tour-' + _this._options.name + '-' + i + '-element');
                    if (step.reflex) {
                        $element.removeClass('tour-step-element-reflex').off('' + (_this._reflexEvent(step.reflex)) + '.tour-' + _this._options.name);
                    }
                    if (step.backdrop) {
                        _this._hideBackdrop();
                    }
                    if (step.onHidden != null) {
                        return step.onHidden(_this);
                    }
                };
            })(this);
            this._callOnPromiseDone(promise, hideStepHelper);

            return promise;
        };

        /**
         * Show previously hidden step.
         *
         * @function showStep
         * @param  {number} i - Index of step to show.
         * @returns {Promise} - Show step.
         */
        Tour.prototype.showStep = function(i) {
            var promise;
            var showStepHelper;
            var skipToPrevious;
            var step;

            if (this.ended()) {
                this._debug('Tour ended, showStep prevented.');

                return this;
            }
            step = this.getStep(i);
            if (!step) {
                return;
            }
            skipToPrevious = i < this._current;
            promise = this._makePromise(step.onShow != null ? step.onShow(this, i) : void 0);
            showStepHelper = (function(_this) {
                return function() {
                    var currentPath;
                    var path;
                    var showPopoverAndOverlay;

                    _this.setCurrentStep(i);
                    path = (function() {
                        switch ({}.toString.call(step.path)) {
                            case '[object Function]':
                                return step.path();
                            case '[object String]':
                                return this._options.basePath + step.path;
                            default:
                                return step.path;
                        }
                    }).call(_this);
                    currentPath = [document.location.pathname, document.location.hash].join('');
                    if (_this._isRedirect(path, currentPath)) {
                        _this._redirect(step, path);

                        return;
                    }
                    if (_this._isOrphan(step)) {
                        if (!step.orphan) {
                            _this._debug('Skip the orphan step ' + (_this._current + 1) + '.\nOrphan option is false and the element does not exist or is hidden.');
                            if (skipToPrevious) {
                                _this._showPrevStep();
                            } else {
                                _this._showNextStep();
                            }

                            return;
                        }
                        _this._debug('Show the orphan step ' + (_this._current + 1) + '. Orphans option is true.');
                    }
                    if (step.backdrop) {
                        _this._showBackdrop(!_this._isOrphan(step) ? step.element : void 0);
                    }
                    showPopoverAndOverlay = function() {
                        if (_this.getCurrentStep() !== i) {
                            return;
                        }
                        if ((step.element != null) && step.backdrop) {
                            _this._showOverlayElement(step);
                        }
                        _this._showPopover(step, i);
                        if (step.onShown != null) {
                            step.onShown(_this);
                        }

                        return _this._debug('Step ' + (_this._current + 1) + ' of ' + _this._options.steps.length);
                    };
                    if (step.autoscroll) {
                        _this._scrollIntoView(step.element, showPopoverAndOverlay);
                    } else {
                        showPopoverAndOverlay();
                    }
                    if (step.duration) {
                        return _this.resume();
                    }
                };
            })(this);
            if (step.delay) {
                this._debug('Wait ' + step.delay + ' milliseconds to show the step ' + (this._current + 1));
                window.setTimeout((function(_this) {
                    return function() {
                        return _this._callOnPromiseDone(promise, showStepHelper);
                    };
                })(this), step.delay);
            } else {
                this._callOnPromiseDone(promise, showStepHelper);
            }

            return promise;
        };

        /**
         * Get current active step.
         *
         * @function getCurrentStep
         * @returns {number} - Current step.
         */
        Tour.prototype.getCurrentStep = function() {
            return this._current;
        };

        /**
         * Set current active step.
         *
         * @function setCurrentStep
         * @param  {number} value - Step number.
         * @returns {Object} - Tour object.
         */
        Tour.prototype.setCurrentStep = function(value) {
            if (value != null) {
                this._current = value;
                this._setState('current_step', value);
            } else {
                this._current = this._getState('current_step');
                this._current = this._current === null ? null : parseInt(this._current, 10);
            }

            return this;
        };

        /**
         * Set state.
         *
         * @private
         * @function _setState
         * @param  {number} key - Key.
         * @param  {any} value - Value.
         * @returns {Object} - State.
         */
        Tour.prototype._setState = function(key, value) {
            var e;
            var keyName;

            if (this._options.storage) {
                keyName = '' + this._options.name + '_' + key;
                try {
                    this._options.storage.setItem(keyName, value);
                } catch (_error) {
                    e = _error;
                    if (e.code === DOMException.QUOTA_EXCEEDED_ERR) {
                        this._debug('LocalStorage quota exceeded. State storage failed.');
                    }
                }

                return this._options.afterSetState(keyName, value);
            } else {
                if (this._state == null) {
                    this._state = {};
                }

                return this._state[key] = value;
            }
        };

        /**
         * Remove state.
         *
         * @private
         * @function _removeState
         * @param  {number} key - Key.
         * @returns {void}
         */
        Tour.prototype._removeState = function(key) {
            var keyName;

            if (this._options.storage) {
                keyName = '' + this._options.name + '_' + key;
                this._options.storage.removeItem(keyName);

                return this._options.afterRemoveState(keyName);
            } else if (this._state != null) {
                return delete this._state[key];
            }
        };

        /**
         * Get state.
         *
         * @private
         * @function _getState
         * @param  {number} key - Key.
         * @returns {Object} - State.
         */
        Tour.prototype._getState = function(key) {
            var keyName;
            var value;

            if (this._options.storage) {
                keyName = '' + this._options.name + '_' + key;
                value = this._options.storage.getItem(keyName);
            } else if (this._state != null) {
                value = this._state[key];
            }
            if (value === void 0 || value === 'null') {
                value = null;
            }
            this._options.afterGetState(key, value);

            return value;
        };

        /**
         * Show next step.
         *
         * @private
         * @function _showNextStep
         * @returns {Object} - Tour object.
         */
        Tour.prototype._showNextStep = function() {
            var promise;
            var showNextStepHelper;
            var step;

            step = this.getStep(this._current);
            showNextStepHelper = (function(_this) {
                return function() {
                    return _this.showStep(step.next);
                };
            })(this);
            promise = this._makePromise(step.onNext != null ? step.onNext(this) : void 0);

            return this._callOnPromiseDone(promise, showNextStepHelper);
        };

        /**
         * Show previous step.
         *
         * @private
         * @function _showPrevStep
         * @returns {Object} - Tour object.
         */
        Tour.prototype._showPrevStep = function() {
            var promise;
            var showPrevStepHelper;
            var step;

            step = this.getStep(this._current);
            showPrevStepHelper = (function(_this) {
                return function() {
                    return _this.showStep(step.prev);
                };
            })(this);
            promise = this._makePromise(step.onPrev != null ? step.onPrev(this) : void 0);

            return this._callOnPromiseDone(promise, showPrevStepHelper);
        };

        /**
         * Debug.
         *
         * @private
         * @function _debug
         * @param  {string} text - Text to show.
         * @returns {string} - Text.
         */
        Tour.prototype._debug = function(text) {
            if (this._options.debug) {
                return window.console.log('Bootstrap Tour \'' + this._options.name + '\' | ' + text);
            }
        };

        /**
         * Is redirected.
         *
         * @private
         * @function _isRedirect
         * @param  {any} path - Path.
         * @param  {string} currentPath - Current location.
         * @returns {boolean} - Is redirect.
         */
        Tour.prototype._isRedirect = function(path, currentPath) {
            return (path != null) && path !== '' && (({}.toString.call(path) === '[object RegExp]' && !path.test(currentPath)) || ({}.toString.call(path) === '[object String]' && path.replace(/\?.*$/, '').replace(/\/?$/, '') !== currentPath.replace(/\/?$/, '')));
        };

        /**
         * Redirect to new path.
         *
         * @private
         * @function _redirect
         * @param  {Object} step - Step.
         * @param  {string} path - Path to redirect.
         * @returns {string} - New path.
         */
        Tour.prototype._redirect = function(step, path) {
            if ($.isFunction(step.redirect)) {
                return step.redirect.call(this, path);
            } else if (step.redirect === true) {
                this._debug('Redirect to ' + path);

                return document.location.href = path;
            }
        };

        /**
         * Is orphan.
         *
         * @private
         * @function _isOrphan
         * @param  {HTMLElement} step - Step element.
         * @returns {boolean} - Is orphan.
         */
        Tour.prototype._isOrphan = function(step) {
            return (step.element == null) || !$(step.element).length || $(step.element).is(':hidden') && ($(step.element)[0].namespaceURI !== 'http://www.w3.org/2000/svg');
        };

        /**
         * Is last step.
         *
         * @private
         * @function _isLast
         * @returns {boolean} - Is last.
         */
        Tour.prototype._isLast = function() {
            return this._current < this._options.steps.length - 1;
        };

        /**
         * Show telia tooltip.
         *
         * @private
         * @function _showPopover
         * @param  {Object} step - Step object.
         * @param  {number} i - Index of step.
         * @returns {void|Object} - If is orphan return tour object.
         */
        Tour.prototype._showPopover = function(step, i) {
            var $element;
            var $tip;
            var isOrphan;
            var options;

            $('.tour-' + this._options.name).remove();
            options = $.extend({}, this._options);
            isOrphan = this._isOrphan(step);
            step.template = this._template(step, i);
            if (isOrphan) {
                step.element = 'body';
                step.placement = 'top';
            }
            $element = $(step.element);
            $element.addClass('tour-' + this._options.name + '-element tour-' + this._options.name + '-' + i + '-element');
            if (step.options) {
                $.extend(options, step.options);
            }
            if (step.reflex && !isOrphan) {
                $element.addClass('tour-step-element-reflex');
                $element.off('' + (this._reflexEvent(step.reflex)) + '.tour-' + this._options.name);
                $element.on('' + (this._reflexEvent(step.reflex)) + '.tour-' + this._options.name, (function(_this) {
                    return function() {
                        if (_this._isLast()) {
                            return _this.next();
                        } else {
                            return _this.end();
                        }
                    };
                })(this));
            }
            $element.teliaTooltip({
                placement: step.placement,
                trigger: 'manual',
                title: step.title,
                content: step.content,
                html: true,
                animation: step.animation,
                container: step.container,
                template: step.template,
                selector: step.element
            }).teliaTooltip('show');
            $tip = $element.data('plugin_teliaTooltip') ? $element.data('plugin_teliaTooltip')._tip() : $element.data('tooltip').tip();
            $tip.attr('id', step.id);
            this._reposition($tip, step);
            if (isOrphan) {
                return this._center($tip);
            }
        };

        /**
         * Create template.
         *
         * @private
         * @function _template
         * @param  {HTMLElement} step - Step element.
         * @param  {number} i - Index.
         * @returns {jQuery} - Template.
         */
        Tour.prototype._template = function(step, i) {
            var $navigation;
            var $next;
            var $prev;
            var $resume;
            var $template;

            $template = $.isFunction(step.template) ? $(step.template(i, step)) : $(step.template);
            $navigation = $template.find('.tooltip__navigation');
            $prev = $navigation.find('[data-role="prev"]');
            $next = $navigation.find('[data-role="next"]');
            $resume = $navigation.find('[data-role="pause-resume"]');
            if (this._isOrphan(step)) {
                $template.addClass('orphan');
            }
            $template.addClass('tour-' + this._options.name + ' tour-' + this._options.name + '-' + i);
            if (step.prev < 0) {
                $prev.prop('disabled', true);
            }
            if (step.next < 0) {
                // $next.prop('disabled', true);
                $next.text($next.data('end-text'));
            }
            if (!step.duration) {
                $resume.remove();
            }

            return $template.clone().wrap('<div>').parent().html();
        };

        /**
         * Reflex event.
         *
         * @private
         * @function _reflexEvent
         * @param  {Object} reflex - Reflex object.
         * @returns {string} - Relfex.
         */
        Tour.prototype._reflexEvent = function(reflex) {
            if ({}.toString.call(reflex) === '[object Boolean]') {
                return 'click';
            } else {
                return reflex;
            }
        };

        /**
         * Reposition tooltip.
         *
         * @private
         * @function _reposition
         * @param  {jQuery} $tip - Tip element.
         * @param  {Object} step - Step.
         * @returns {Object} - Tour object.
         */
        Tour.prototype._reposition = function($tip, step) {
            var offsetBottom;
            var offsetHeight;
            var offsetRight;
            var offsetWidth;
            var originalLeft;
            var originalTop;
            var tipOffset;

            offsetWidth = $tip[0].offsetWidth;
            offsetHeight = $tip[0].offsetHeight;
            tipOffset = $tip.offset();
            originalLeft = tipOffset.left;
            originalTop = tipOffset.top;
            offsetBottom = $(document).outerHeight() - tipOffset.top - $tip.outerHeight();
            if (offsetBottom < 0) {
                tipOffset.top += offsetBottom;
            }
            offsetRight = $('html').outerWidth() - tipOffset.left - $tip.outerWidth();
            if (offsetRight < 0) {
                tipOffset.left += offsetRight;
            }
            if (tipOffset.top < 0) {
                tipOffset.top = 0;
            }
            if (tipOffset.left < 0) {
                tipOffset.left = 0;
            }
            $tip.offset(tipOffset);
            if (step.placement === 'bottom' || step.placement === 'top') {
                if (originalLeft !== tipOffset.left) {
                    return this._replaceArrow($tip, (tipOffset.left - originalLeft) * 2, offsetWidth, 'left');
                }
            } else if (originalTop !== tipOffset.top) {
                return this._replaceArrow($tip, (tipOffset.top - originalTop) * 2, offsetHeight, 'top');
            }
        };

        /**
         * Put tip to center.
         *
         * @private
         * @function _center
         * @param  {jQuery} $tip - Tip element.
         * @returns {Object} - Tour object.
         */
        Tour.prototype._center = function($tip) {
            return $tip.css('top', $(window).outerHeight() / 2 - $tip.outerHeight() / 2);
        };

        /**
         * Replace tooltip indicator arrow.
         *
         * @private
         * @function _replaceArrow
         * @param  {jQuery} $tip - Tip element.
         * @param  {boolean} delta - Delta.
         * @param  {number} dimension - Dimension.
         * @param  {string} position - Position.
         * @returns {Object} - Tour object.
         */
        Tour.prototype._replaceArrow = function($tip, delta, dimension, position) {
            return $tip.find('.arrow').css(position, delta ? 50 * (1 - delta / dimension) + '%' : '');
        };

        /**
         * Scroll to element.
         *
         * @private
         * @function _scrollIntoView
         * @param  {HTMLElement}   element - Element to scroll.
         * @param  {Function} callback - Callback.
         * @returns {Function} - Scrolling function.
         */
        Tour.prototype._scrollIntoView = function(element, callback) {
            var $element;
            var $window;
            var counter;
            var offsetTop;
            var scrollTop;
            var windowHeight;

            $element = $(element);
            if (!$element.length) {
                return callback();
            }
            $window = $(window);
            offsetTop = $element.offset().top;
            windowHeight = $window.height();
            scrollTop = Math.max(0, offsetTop - (windowHeight / 2));
            this._debug('Scroll into view. ScrollTop: ' + scrollTop + '. Element offset: ' + offsetTop + '. Window height: ' + windowHeight + '.');
            counter = 0;

            return $('body, html').stop(true, true).animate({
                scrollTop: Math.ceil(scrollTop)
            }, (function(_this) {
                return function() {
                    if (++counter === 2) {
                        callback();

                        return _this._debug('Scroll into view.\nAnimation end element offset: ' + ($element.offset().top) + '.\nWindow height: ' + ($window.height()) + '.');
                    }
                };
            })(this));
        };

        /**
         * Resize.
         *
         * @private
         * @function _onResize
         * @param  {Function} callback - Callback.
         * @param  {number}   timeout - Debounce timeout.
         * @returns {Function} - Window resize.
         */
        Tour.prototype._onResize = function(callback, timeout) {
            return $(window).on('resize.tour-' + this._options.name, function() {
                clearTimeout(timeout);

                return timeout = setTimeout(callback, 100);
            });
        };

        /**
         * Initialize mouse click.
         *
         * @private
         * @function _initMouseNavigation
         * @returns {Function} - Click events.
         */
        Tour.prototype._initMouseNavigation = function() {
            var _this;

            _this = this;

            return $(document).off('click.tour-' + this._options.name, '.tooltip.tour-' + this._options.name + ' *[data-role=\'prev\']').off('click.tour-' + this._options.name, '.tooltip.tour-' + this._options.name + ' *[data-role=\'next\']').off('click.tour-' + this._options.name, '.tooltip.tour-' + this._options.name + ' *[data-role=\'end\']').off('click.tour-' + this._options.name, '.tooltip.tour-' + this._options.name + ' *[data-role=\'pause-resume\']').on('click.tour-' + this._options.name, '.tooltip.tour-' + this._options.name + ' *[data-role=\'next\']', (function(_this) {
                return function(e) {
                    e.preventDefault();
                    if (_this._isLast()) {
                        return _this.next();
                    } else {
                        return _this.end();
                    }
                };
            })(this)).on('click.tour-' + this._options.name, '.tooltip.tour-' + this._options.name + ' *[data-role=\'prev\']', (function(_this) {
                return function(e) {
                    e.preventDefault();

                    return _this.prev();
                };
            })(this)).on('click.tour-' + this._options.name, '.tooltip.tour-' + this._options.name + ' *[data-role=\'end\']', (function(_this) {
                return function(e) {
                    e.preventDefault();

                    return _this.end();
                };
            })(this)).on('click.tour-' + this._options.name, '.tooltip.tour-' + this._options.name + ' *[data-role=\'pause-resume\']', function(e) {
                var $this;

                e.preventDefault();
                $this = $(this);
                $this.text(_this._paused ? $this.data('pause-text') : $this.data('resume-text'));
                if (_this._paused) {
                    return _this.resume();
                } else {
                    return _this.pause();
                }
            });
        };

        /**
         * Initialize keyboard navigation.
         *
         * @private
         * @function _initKeyboardNavigation
         * @returns {Function} - Keyboard keyup.
         */
        Tour.prototype._initKeyboardNavigation = function() {
            if (!this._options.keyboard) {
                return;
            }

            return $(document).on('keyup.tour-' + this._options.name, (function(_this) {
                return function(e) {
                    if (!e.which) {
                        return;
                    }
                    switch (e.which) {
                        case 39:
                            e.preventDefault();
                            if (_this._isLast()) {
                                return _this.next();
                            } else {
                                return _this.end();
                            }
                        case 37:
                            e.preventDefault();
                            if (_this._current > 0) {
                                return _this.prev();
                            }
                            break;
                        case 27:
                            e.preventDefault();

                            return _this.end();
                    }
                };
            })(this));
        };

        /**
         * Make promise.
         *
         * @private
         * @function _makePromise
         * @param  {Object} result - Result object.
         * @returns {null|Object} - Is promise.
         */
        Tour.prototype._makePromise = function(result) {
            if (result && $.isFunction(result.then)) {
                return result;
            } else {
                return null;
            }
        };

        /**
         * Call on promise done.
         *
         * @private
         * @function _callOnPromiseDone
         * @param  {Promise}   promise - Promise.
         * @param  {Function} cb      - Callback.
         * @param  {any}   arg     - Additional arguments.
         * @returns {Function} - Callback.
         */
        Tour.prototype._callOnPromiseDone = function(promise, cb, arg) {
            if (promise) {
                return promise.then((function(_this) {
                    return function() {
                        return cb.call(_this, arg);
                    };
                })(this));
            } else {
                return cb.call(this, arg);
            }
        };

        /**
         * Show backdrop.
         *
         * @private
         * @function _showBackdrop
         * @returns {jQuery} - JQuery element.
         */
        Tour.prototype._showBackdrop = function() {
            if (this.backdrop.backgroundShown) {
                return;
            }
            this.backdrop = $('<div>', {
                class: 'tour-backdrop'
            });
            this.backdrop.backgroundShown = true;

            return $('body').append(this.backdrop);
        };

        /**
         * Hide backdrop.
         *
         * @private
         * @function _hideBackdrop
         * @returns {Function} - Hide background.
         */
        Tour.prototype._hideBackdrop = function() {
            this._hideOverlayElement();

            return this._hideBackground();
        };

        /**
         * Hide background.
         *
         * @private
         * @function _hideBackground
         * @returns {boolean} - Is hidden.
         */
        Tour.prototype._hideBackground = function() {
            if (this.backdrop) {
                this.backdrop.remove();
                this.backdrop.overlay = null;

                return this.backdrop.backgroundShown = false;
            }
        };

        /**
         * Show overlay element.
         *
         * @private
         * @function _showOverlayElement
         * @param  {HTMLElement} step - Step.
         * @returns {jQuery} - Overlay element.
         */
        Tour.prototype._showOverlayElement = function(step) {
            var $element;
            var elementData;

            $element = $(step.element);
            if (!$element || $element.length === 0 || this.backdrop.overlayElementShown) {
                return;
            }
            this.backdrop.overlayElementShown = true;
            this.backdrop.$element = $element.addClass('tour-step-backdrop');
            this.backdrop.$background = $('<div>', {
                class: 'tour-step-background'
            });
            elementData = {
                width: $element.innerWidth(),
                height: $element.innerHeight(),
                offset: $element.offset()
            };
            this.backdrop.$background.appendTo('body');
            if (step.backdropPadding) {
                elementData = this._applyBackdropPadding(step.backdropPadding, elementData);
            }

            return this.backdrop.$background.width(elementData.width).height(elementData.height).offset(elementData.offset);
        };

        /**
         * Hide overlay element.
         *
         * @private
         * @function _hideOverlayElement
         * @returns {boolean} - Is hidden.
         */
        Tour.prototype._hideOverlayElement = function() {
            if (!this.backdrop.overlayElementShown) {
                return;
            }
            this.backdrop.$element.removeClass('tour-step-backdrop');
            this.backdrop.$background.remove();
            this.backdrop.$element = null;
            this.backdrop.$background = null;

            return this.backdrop.overlayElementShown = false;
        };

        /**
         * Add padding to backdrop.
         *
         * @private
         * @function _applyBackdropPadding
         * @param  {Object} padding - Padding object.
         * @param  {Object} data    - Meta object.
         * @returns {Objecct} - Data object.
         */
        Tour.prototype._applyBackdropPadding = function(padding, data) {
            if (typeof padding === 'object') {
                if (padding.top == null) {
                    padding.top = 0;
                }
                if (padding.right == null) {
                    padding.right = 0;
                }
                if (padding.bottom == null) {
                    padding.bottom = 0;
                }
                if (padding.left == null) {
                    padding.left = 0;
                }
                data.offset.top -= padding.top;
                data.offset.left -= padding.left;
                data.width += padding.left + padding.right;
                data.height += padding.top + padding.bottom;
            } else {
                data.offset.top -= padding;
                data.offset.left -= padding;
                data.width += (padding * 2);
                data.height += (padding * 2);
            }

            return data;
        };

        /**
         * Clear timer.
         *
         * @private
         * @function _clearTimer
         * @returns {Object} - Tour object.
         */
        Tour.prototype._clearTimer = function() {
            window.clearTimeout(this._timer);
            this._timer = null;

            return this._duration = null;
        };

        return Tour;
    })();

    return window.Tour = Tour;
})(jQuery, window);

;(function($, window, document) {
    var pluginName = 'vasCard';
    var defaults = {};
    var initSelector = '.vas-card';

    /**
     * VasCard.
     *
     * @class
     * @param {Element} element - The HTML element the plugin is initialized on.
     * @param {Object} options - An options object.
     */
    function VasCard(element, options) {
        this.element = element;
        this.$element = $(this.element);
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.$overlay = this.$element.find('.vas-card__overlay');
        this.$openButton = this.$element.find('.js-open');
        this.$closeButton = $('<button class="vas-card__close js-vas-card-close">' + telia.icon.render('#close-round') + '</button>');

        this.init();
    }

    /**
     * Initializes the plugin.
     *
     * @function init
     * @returns {void}
     */
    VasCard.prototype.init = function() {
        this.$overlay.prepend(this.$closeButton);
        this.$openButton.on('click', this.open.bind(this));
        this.$closeButton.on('click', this.close.bind(this));
    };

    /**
     * Open the VAS card.
     *
     * @function open
     * @param {Event} event - Event.
     * @returns {void}
     */
    VasCard.prototype.open = function(event) {
        event.preventDefault();
        this.$element.addClass('is-open');
    };

    /**
     * Close the VAS card.
     *
     * @param  {Event} event - Event.
     * @returns {void}
     */
    VasCard.prototype.close = function(event) {
        event.preventDefault();
        this.$element.removeClass('is-open');
    };

    /**
     * Destroy the plugin instance.
     *
     * @function destroy
     * @returns {void}
     */
    VasCard.prototype.destroy = function() {
        this.$element.removeData('plugin_' + pluginName);
    };

    /**
     * Create jQuery plugin.
     *
     * @param {Object} options - Extended options.
     * @listens telia.header
     * @returns {Array<VasCard>} Array of VasCards.
     */
    $.fn[pluginName] = function(options) {
        var args = Array.prototype.slice.call(arguments, 1);

        return this.each(function() {
            var instance = $.data(this, 'plugin_' + pluginName);

            if (!instance) {
                if (options === 'destroy') {
                    return;
                }

                $.data(this, 'plugin_' + pluginName, new VasCard(this, options));
            } else if (typeof options === 'string') {
                instance[options].apply(instance, args);
            }
        });
    };

    /**
     * @param {Event} event
     * @event enhance.telia.vasCard
     */
    $(document).on('enhance.telia.vasCard', function(event) {
        $(event.target).find(initSelector)[pluginName]();
    });

    /**
     * @param {Event} even
     * @event destroy.telia.vasCard
     */
    $(document).on('destroy.telia.vasCard', function(event) {
        $(event.target).find(initSelector)[pluginName]('destroy');
    });
})(jQuery, window, document);

/**
 * Auto-init the plugin on DOM ready.
 * @fires enhance.telia.vasCard
 */
$(function() {
    $(document).trigger('enhance.telia.vasCard');
});

;(function($, window, document) {
    var pluginName = 'teliaVideo';
    var defaults = {
        playingClass: 'is-playing'
    };
    var initSelector = '.video';

    /**
     * Video.
     *
     * @class
     * @param {HTMLElement} element - The HTML element the CarouselHero should be bound to.
     * @param {Object} options - An option map.
     */
    function Video(element, options) {
        this.element = element;
        this.$element = $(this.element);
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.$button = this.$element.find('.video__btn');
        this.$iframe = this.$element.find('.video__iframe');

        this.init();
    }

    /**
     * Initialize the plugin.
     *
     * @function init
     * @returns {void}
     */
    Video.prototype.init = function() {
        this.$button.on('click.telia.video', this.play.bind(this));
        $(document).on('reset.telia.video', this._reset.bind(this));
    };

    /**
     * Start playing the video.
     *
     * @function play
     * @returns {void}
     */
    Video.prototype.play = function() {
        this.$element.addClass(this.options.playingClass);
    };

    /**
     * Reset plugin instance to unstarted state.
     *
     * @function stop
     * @returns {void}
     */
    Video.prototype.stop = function() {
        if (this.$element.hasClass(this.options.playingClass)) {
            this.$element.removeClass(this.options.playingClass);
            this.$iframe.attr('src', '');
            this.$iframe.removeClass('lazyloaded').addClass('lazyload');
        }
    };

    /**
     * Check if target contains element, stop if true.
     *
     * @function reset
     * @private
     * @param   {Event} event - Event.
     * @param   {HTMLElement} target - Target element.
     * @returns {void}
     */
    Video.prototype._reset = function(event, target) {
        if (target && $.contains(target, this.element)) {
            this.stop();
        }
    };

    /**
     * Destroy the plugin instance.
     *
     * @function destroy
     * @returns {void}
     */
    Video.prototype.destroy = function() {
        this.$element.off('reset.telia.video');
        this.$button.off('click.telia.video');
        this.$element.removeData('plugin_' + pluginName);
    };

    /**
     * Create jQuery plugin.
     *
     * @param {Object} options - Extended options.
     * @listens telia.video
     * @returns {Array<Video>} Array of Videos.
     */
    $.fn[pluginName] = function(options) {
        var args = Array.prototype.slice.call(arguments, 1);

        return this.each(function() {
            var instance = $.data(this, 'plugin_' + pluginName);

            if (!instance) {
                if (options === 'destroy') {
                    return;
                }

                $.data(this, 'plugin_' + pluginName, new Video(this, options));
            } else if (typeof options === 'string') {
                instance[options].apply(instance, args);
            }
        });
    };

    /**
     * @param {Event} event
     * @event enhance.telia.video
     */
    $(document).on('enhance.telia.video', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]();
    });

    /**
     * @param {Event} event
     * @event destroy.telia.video
     */
    $(document).on('destroy.telia.video', function(event) {
        $(event.target).find(initSelector).addBack(initSelector)[pluginName]('destroy');
    });
})(jQuery, window, document);

/**
 * Auto-init the plugin on DOM ready.
 * @fires enhance.telia.video
 */
$(function() {
    $(document).trigger('enhance.telia.video');
});

/**
 * Old init for compatibility.
 *
 * @fires enhance.telia.video
 * @deprecated Use enhance.telia.video event directly.
 * @returns {void}
 */
function initVideo() {
    $(document).trigger('enhance.telia.video');
}
window.initVideo = initVideo;

/**
 * Trigger resize on image that has loaded.
 */
$(function() {
    var lazyloadResizeTimer;

    $('.lazyload').on('load', function() {
        clearTimeout(lazyloadResizeTimer);
        lazyloadResizeTimer = setTimeout(function() {
            $(window).trigger('resize');
        }, 250);
    });
});

/**
 * track-focus v 1.0.0 | Author: Jeremy Fields [jeremy.fields@vget.com], 2015
 * License: MIT
 * inspired by: http://irama.org/pkg/keyboard-focus-0.3/jquery.keyboard-focus.js
 */
(function(body) {
    var usingMouse;

    /**
     * Check focus event type.
     *
     * @param  {Event} event - Event.
     * @returns {void}
     */
    function preFocus(event) {
        usingMouse = (event.type === 'mousedown');
    };

    /**
     * Add focus class.
     *
     * @param {Event} event - Event.
     * @returns {void}
     */
    function addFocus(event) {
        if (usingMouse) event.target.classList.add('is-focus-hidden');
    };

    /**
     * Remove focus class.
     *
     * @param  {Event} event - Event.
     * @returns {void}
     */
    function removeFocus(event) {
        event.target.classList.remove('is-focus-hidden');
    };

    /**
     * Bind events.
     *
     * @returns {void}
     */
    function bindEvents() {
        body.addEventListener('keydown', preFocus);
        body.addEventListener('mousedown', preFocus);
        body.addEventListener('focusin', addFocus);
        body.addEventListener('focusout', removeFocus);
    };

    bindEvents();
})(document.body);

/**
 * Disable window scrolling.
 *
 * @param  {boolean} hideScrollbar - If scrollbar should be hidden.
 * @returns {void}
 */
function disableScroll(hideScrollbar) {
    if (!$('body').hasClass('scroll-disabled')) {
        var curScroll = $(window).scrollTop();

        $('body').addClass('scroll-disabled');
        if (hideScrollbar) {
            $('body').addClass('scroll-disabled--hide-scrollbar');
        }
        $('body').css('top', -curScroll);
    }
}

/**
 * Enable window scrolling.
 *
 * @returns {void}
 */
function enableScroll() {
    var bodyScroll = parseInt($('body').css('top'), 10);

    $('body').removeClass('scroll-disabled scroll-disabled--hide-scrollbar');
    if (bodyScroll) {
        $(window).scrollTop(-bodyScroll);
        $('body').css('top', 0);
    }
}

window.disableScroll = disableScroll;
window.enableScroll = enableScroll;

/**
 * Check if element is visible.
 *
 * @param  {number} threshold - Threshold to add to element position.
 * @returns {boolean} - Is on screen or not.
 */
$.fn.isOnScreen = function(threshold) {
    var win = $(window);

    var viewport = {
        top: win.scrollTop(),
        left: win.scrollLeft()
    };

    viewport.right = viewport.left + win.width();
    viewport.bottom = viewport.top + win.height();

    var bounds = this.offset();

    bounds.right = bounds.left + this.outerWidth();
    bounds.bottom = bounds.top + this.outerHeight();

    return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top + threshold || viewport.top > bounds.bottom));
};

/**
 * Get closest parent that has scroll.
 *
 * @param   {HTMLElement} element - The element whose parent we are trying to find.
 * @param   {boolean} includeHidden - Whether to include parents with hidden overflow.
 * @returns {HTMLElement} Closest parent that is scrollable.
 */
function getScrollParent(element, includeHidden) {
    var style = getComputedStyle(element);
    var excludeStaticParent = style.position === 'absolute';
    var overflowRegex = includeHidden ? /(auto|scroll|hidden)/ : /(auto|scroll)/;

    if (style.position === 'fixed') return document.body;

    for (var parent = element; (parent = parent.parentElement);) {
        style = getComputedStyle(parent);
        if (excludeStaticParent && style.position === 'static') {
            continue;
        }
        if (overflowRegex.test(style.overflow + style.overflowY + style.overflowX)) return parent;
    }

    return document.body;
}

window.getScrollParent = getScrollParent;

/**
 * Get position from parent.
 *
 * @returns {Object} - Position.
 */
$.fn.positionFromScrollParent = function() {
    var pos = {
        top: 0,
        left: 0
    };

    var scrollParent = window.getScrollParent(this[0]);

    $(this).parentsUntil(scrollParent).each(function() {
        if ($(this).css('position') === 'static') return true;
        var parentPos = $(this).position();
        var styles = getComputedStyle(this);

        pos.top += parentPos.top;
        pos.left += parentPos.left;
        pos.top += parseInt(styles.marginTop, 10);
        pos.left += parseInt(styles.marginLeft, 10);
    });

    if (scrollParent !== document.body) {
        pos.top += $(scrollParent).scrollTop();
        pos.left += $(scrollParent).scrollLeft();
    }

    return pos;
};

/**
 * Trigger element if hash is in url.
 *
 * @returns {void}
 */
function hashTrigger() {
    var hashes;
    var hash = window.location.hash;

    if (!hash) {
        return;
    }

    hashes = hash.split('/');

    $.each(hashes, function(index, value) {
        if (value.indexOf('?') !== 0 && value.length != 1 && value.length != 0) {
            if (value.charAt(0) != '#') {
                value = '#' + value;
            }
            var first = $('[data-target="' + value + '"][data-toggle],[href="' + value + '"][data-toggle], .sidemenu__link[href="' + value + '"], .tabs__link[href="' + value + '"], .tabs__accordion-link[href="' + value + '"]').first();

            first.trigger('click');
        }
    });
}
window.hashTrigger = hashTrigger;

$(function() {
    hashTrigger();
});

/**
 * ChildNode.remove() polyfill.
 * From: https://github.com/jserz/js_piece/blob/master/DOM/ChildNode/remove()/remove().md
*/
(function(arr) {
    arr.forEach(function(item) {
        item.remove = item.remove || function() {
            this.parentNode.removeChild(this);
        };
    });
})([Element.prototype, CharacterData.prototype, DocumentType.prototype]);

//# sourceMappingURL=telia.js.map
