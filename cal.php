<?php
if(empty($_GET['unique_hash'])) {
	header("Location: http://www.telia.ee/minutehnik-not-found");
	die();
}

error_reporting(E_ALL);

$db = new SQLite3('../minutehnik-db.orumets.ee/minutehnik.sqlite');
$results = $db->query('SELECT * FROM customer where unique_hash="'. $_GET['unique_hash'] .'"');

$numRows = 0;

while ($row = $results->fetchArray()) {
		#var_dump($row);
		#print_r($row);
		#echo '<br />';
		#echo $row['tech_first_name'];
		$tech_first_name = ucwords(strtolower($row['tech_first_name']));
		$customer_first_name = ucwords(strtolower($row['customer_first_name']));
		$visitdate = $row['visitdate'];
		$address = $row['address'];
		$appointment = $row['appointment'];
		list($time_from, $time_to) = explode(' - ', $appointment);
		#echo '<br />';
		++$numRows;
}

if($numRows < 1) {
	header("Location: http://www.telia.ee/minutehnik-not-found");
	die();
}

$calendar="BEGIN:VCALENDAR
VERSION:2.0
CALSCALE:GREGORIAN
METHOD:REQUEST
BEGIN:VEVENT
DTSTART:".date('Ymd\THis\Z', strtotime($visitdate." ".$time_from))."
DTEND:".date('Ymd\THis\Z', strtotime($visitdate." ".$time_to))."
DTSTAMP:".date('Ymd\THis\Z', time())."
ORGANIZER;CN=From Name:Minu Tehnik <raido.orumets@telia.ee>
UID:".$_GET['unique_hash']."
ATTENDEE;PARTSTAT=NEEDS-ACTION;RSVP=TRUE;CN=Tehnik ".$tech_first_name.":MAILTO:raido.orumets@telia.ee
DESCRIPTION:Oled broneerinud Telia tehniku visiidiaja, tehnik saabub ".$visitdate." ajavahemikus ".$appointment.". Töid teostab tehnik: ".$tech_first_name.".\\n\\nVaata ja muuda viisidi andmeid siit:\\nhttps://minutehnik.telia.ee/?unique_hash=".$_GET['unique_hash']."\\n\\nKuidas visiidiks valmistuda?\\n\\nSAABUMISE AEG - Tehnik saabub märgitud ajavahemiku jooksul. Anname endast parima, et kokkulepitud töö saaks teostatud hiljemalt lubatud ajavahemiku lõpuks.\\n\\nKOHALOLEK - Visiidi toimumise hetkel on vajalik kliendi enda või täisealise volitatud isiku kohal viibimine. Volitatud isikuid saab lihtsasti lisada ja hallata Telia iseteeninduses: https://iseteenindus.telia.ee.\\n\\nPUURIMINE - Ühenduse loomiseks vajaliku kaabelduse paigaldamiseks võib olla vajalik avade puurimine vaheseintesse. Vajadusel tuleks selle jaoks eelnevalt luba hankida.\\n\\nSEADMETE ASUKOHT - Mõtle läbi, kus soovid tellitud teenuseid (televiisor, telefon, arvuti) kasutada. Sobiva asukoha planeerimisel on oluline jälgida ka elektritoite olemasolu.\\n\\nÕRNAD ESEMED - Tehniku töö lihtsustamiseks ja visiidi ladusamaks kulgemiseks soovitame planeeritud tööpiirkonnast ajutiselt eemaldada kergesti purunevad esemed (vaasid, serviis vms).
LOCATION: ".$address."
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:Telia tehniku visiit
TRANSP:OPAQUE
PRIORITY:1
BEGIN:VALARM
TRIGGER:-PT30M
REPEAT:2
DURATION:PT15M
ACTION:DISPLAY
DESCRIPTION:Telia tehnik saabub ".$visitdate." ajavahemikus ".$appointment.".
END:VALARM
END:VEVENT
END:VCALENDAR";

//quoted_printable_encode()
//URL:http://minutehnik.telia.ee/?unique_hash=".$_GET['unique_hash']."

header('Content-type: text/calendar; charset=utf-8');
header('Content-Disposition: inline; filename=calendar.ics');
echo $calendar;
exit;

/*
// Setting the header part, this is important
$headers = "From: Minu Tehnik <info@telia.ee>\n";
$headers .= "MIME-Version: 1.0\n";
$headers .= "Content-Type: text/calendar; method=REQUEST;\n";
$headers .= '        charset="UTF-8"';
$headers .= "\n";
$headers .= "Content-Transfer-Encoding: 7bit";

// mail content , attaching the ics detail in the mail as content
$subject = "Telia tehniku visiit";
$subject = html_entity_decode($subject, ENT_QUOTES, 'UTF-8');

// mail send
if(mail("To email", $subject, $message, $headers)) {

    echo "sent";
}else {
    echo "error";
} */
?>
