<h1>MinuTehnik Admin</h1>
<br />
<?php

error_reporting(E_ALL);

$db = new SQLite3('../minutehnik-db.orumets.ee/minutehnik.sqlite');
$rowcount = 0;

echo 'Unikaalseid külastusi:<br />';
$u_results = $db->query('SELECT unique_hash, count(*) FROM user_log group by unique_hash');
while ($row = $u_results->fetchArray()) {
		echo $rowcount .'. '. $row['unique_hash'] .' - ' .$row['count(*)'];
		echo '<br />';
		++$rowcount;
}

echo '<br /><br />';

$results = $db->query('SELECT * FROM user_log order by unique_hash, log_date, log_time');

while ($row = $results->fetchArray()) {
		#var_dump($row);
		#print_r($row);
		echo $row['action'] .' -- '. $row['log_date'] .' '. $row['log_time'] .' : '. $row['unique_hash'] .' -> ' .$row['new_time'];
		echo '<br />';
}
